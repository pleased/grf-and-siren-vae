import sys
import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt
import argparse

from mine.models.mine import Mine

def between_true_and_inferred_latents(path, samples, f, device):
    f.write("-- Between z* ~ p*(z) and z ~ q(z|x): --\n")
    true_z = samples['true_z'].float().to(device)
    inferred_z = samples['inferred_z'].float().to(device)
    num_latent = true_z.shape[1]

    # As a whole
    print('-- Between z* ~ p*(z) and z ~ q(z|x): --')
    statistics_network = nn.Sequential(nn.Linear(num_latent + num_latent, 100),
                                       nn.ReLU(), nn.Linear(100, 100),
                                       nn.ReLU(), nn.Linear(100, 1))
    mine = Mine(T = statistics_network,
                loss = 'mine', 
                method = 'concat').to(device)
    mi = mine.optimize(true_z, inferred_z, iters=20, batch_size=100)
    f.write('\t MI(z*, z) = {:.3f}\n'.format(mi))
    print('MI(z*, z) = {:.3f}\n'.format(mi))

    # Per dimension
    f.write('\t Per dimension: \n')
    print('Per dimension:')
    MI = np.zeros((num_latent, num_latent))
    s = ""
    for i in range(num_latent):
        s += "\t {}: ".format(i)
        print(" {}: ".format(i), end="")
        for j in range(num_latent):
            statistics_network = nn.Sequential(
                nn.Linear(1 + 1, 100),
                nn.ReLU(), nn.Linear(100, 100),
                nn.ReLU(), nn.Linear(100, 1))

            mine = Mine(
                T = statistics_network,
                loss = 'mine', 
                method = 'concat').to(device)
            mi = mine.optimize(true_z[:,i:i+1], inferred_z[:,j:j+1], iters=20, batch_size=100)
            s += "({}: {:.3f}) ".format(j, mi)
            MI[i,j] = max(0.0,mi)
            print("({}: {:.3f}) ".format(j, mi), end="")
        s += "\n"
        print()
    
    f.write(s)
    with open(path + '/MI_true-z-vs-inferred-z.npy', 'wb') as np_f:
        np.save(np_f, MI)


def between_latents_and_obs_of_model(path, samples, f, device):
    f.write("-- Between x, z ~ p(x|z)p(z) --\n")
    x = samples['model_x'].float().to(device)
    z = samples['model_z'].float().to(device)

    num_latent = z.shape[1]
    num_obs = x.shape[1]

    # Between latents of the model
    f.write('\t MI(z, z) per dimension: \n')
    print("-- Between x, z ~ p(x|z)p(z) --")
    print("MI(z, z) per dimension:")
    MI = np.zeros((num_latent, num_latent))
    s = ""
    for i in range(num_latent):
        s += "\t {}: ".format(i)
        print(" {}: ".format(i), end="")
        for j in range(i+1, num_latent):
            statistics_network = nn.Sequential(
                nn.Linear(1 + 1, 100),
                nn.ReLU(), nn.Linear(100, 100),
                nn.ReLU(), nn.Linear(100, 1))

            mine = Mine(
                T = statistics_network,
                loss = 'mine', #mine_biased, fdiv
                method = 'concat').to(device)
            mi = mine.optimize(z[:,i:i+1], z[:,j:j+1], iters=20, batch_size=200)
            s += "({}: {:.3f}) ".format(j, mi)
            MI[i,j] = max(0.0,mi)
            print("({}: {:.3f}) ".format(j, mi), end="")
        s += "\n"
        print()

    f.write(s)

    with open(path + '/MI-model-z-vs-model-x__z.npy', 'wb') as np_f:
        np.save(np_f, MI)

    # Between latents and obs of the model
    f.write('\t MI(z, x) per dimension: \n')
    print("MI(z, x) per dimension:")
    MI = np.zeros((num_latent, num_obs))
    s = ""
    for i in range(num_latent):
        s += "\t {}: ".format(i)
        print(" {}: ".format(i), end="")
        for j in range(num_obs):
            statistics_network = nn.Sequential(
                nn.Linear(1 + 1, 100),
                nn.ReLU(), nn.Linear(100, 100),
                nn.ReLU(), nn.Linear(100, 1))

            mine = Mine(
                T = statistics_network,
                loss = 'mine', #mine_biased, fdiv
                method = 'concat').to(device)
            mi = mine.optimize(z[:,i:i+1], x[:,j:j+1], iters=20, batch_size=100)
            s += "({}: {:.3f}) ".format(j, mi)
            MI[i,j] = max(0.0,mi)
            print("({}: {:.3f}) ".format(j, mi), end="")
        s += "\n"
        print()

    f.write(s)

    with open(path + '/MI-model-z-vs-model-x__x.npy', 'wb') as np_f:
        np.save(np_f, MI)


def between_true_latents_and_obs(path, samples, f, device):
    f.write("-- Between x, z ~ p*(x|z)p*(z) --\n")
    x = samples['true_x'].float().to(device)
    z = samples['true_z'].float().to(device)
    num_latent = z.shape[1]
    num_obs = x.shape[1]

    # Between latents of the model
    f.write('\t MI(z, z) per dimension: \n')
    print("-- Between x, z ~ p*(x|z)p*(z) --")
    print("MI(z, z) per dimension:")
    MI = np.zeros((num_latent, num_latent))
    s = ""
    for i in range(num_latent):
        s += "\t {}: ".format(i)
        print(" {}: ".format(i), end="")
        for j in range(i+1, num_latent):
            statistics_network = nn.Sequential(
                nn.Linear(1 + 1, 100),
                nn.ReLU(), nn.Linear(100, 100),
                nn.ReLU(), nn.Linear(100, 1))

            mine = Mine(
                T = statistics_network,
                loss = 'mine', #mine_biased, fdiv
                method = 'concat').to(device)
            mi = mine.optimize(z[:,i:i+1], z[:,j:j+1], iters=20, batch_size=100)
            s += "({}: {:.3f}) ".format(j, mi)
            MI[i,j] = max(0.0,mi)
            print("({}: {:.3f}) ".format(j, mi), end="")
        s += "\n"
        print()

    f.write(s)
    with open(path + '/MI-true-z-vs-true-x__z.npy', 'wb') as np_f:
        np.save(np_f, MI)

    # Between latents and obs of the model
    f.write('\t MI(z, x) per dimension: \n')
    print("MI(z, x) per dimension:")
    MI = np.zeros((num_latent, num_obs))
    s = ""
    for i in range(num_latent):
        s += "\t {}: ".format(i)
        print(" {}: ".format(i), end="")
        for j in range(num_obs):
            statistics_network = nn.Sequential(
                nn.Linear(1 + 1, 100),
                nn.ReLU(), nn.Linear(100, 100),
                nn.ReLU(), nn.Linear(100, 1))

            mine = Mine(
                T = statistics_network,
                loss = 'mine', #mine_biased, fdiv
                method = 'concat').to(device)
            mi = mine.optimize(x[:,j:j+1], z[:,i:i+1], iters=20, batch_size=100)
            s += "({}: {:.3f}) ".format(j, mi)
            MI[i,j] = max(0.0,mi)
            print("({}: {:.3f}) ".format(j, mi), end="")
        s += "\n"
        print()

    f.write(s)

    with open(path + '/MI-true-z-vs-true-x__x.npy', 'wb') as np_f:
        np.save(np_f, MI)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("path")
    parser.add_argument("--estimate_between_true_and_inferred_latents", action='store_true')
    parser.add_argument("--estimate_between_model_vars", action='store_true')
    parser.add_argument("--estimate_between_true_vars", action='store_true')


    args = parser.parse_args()

    path = args.path

    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    samples = torch.load(path + 'samples.pt', map_location=torch.device(device))
    f = open(path + "MI.txt", "w")
    f.write("[MUTUAL INFORMATION RESULTS]\n")
    f.write('True model: p*(x|z)p*(z)\n')
    f.write('Learned model: p(x|z)p(z)\n')
    f.write('Inference model: q(z|x)\n')

    # Mutual information between true and inferred latents
    if args.estimate_between_true_and_inferred_latents:
        between_true_and_inferred_latents(path, samples, f, device)

    # Mutual information between latents and observeds of model
    if args.estimate_between_model_vars:
        between_latents_and_obs_of_model(path, samples, f, device)

    # Mutual information between true latents and observeds
    if args.estimate_between_true_vars:
        between_true_latents_and_obs(path, samples, f, device)

    f.close()    