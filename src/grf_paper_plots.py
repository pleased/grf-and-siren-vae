import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import torch
import matplotlib.colors as mc
import colorsys
import json
import os

from graph.belief_network import ArithmeticCircuit


def memory_usage_small():
    # All for SMALL models
    bn = ['Arithmetic Circuit']*5 + ['Tree']*5 + ['Protein']*5
    flow = [r'GRF$_S$ (Grad at end)', r'GRF$_S$ (Grad during forward)', r'GNF-M$_S$', r'GNF-A$_S$', r'SCCNF$_S$'] * 3
    mem_usage = np.array([
        # Arithmetic Circuit
        18.002432, # GRF
        7.906304, # GRF (eff)
        7.90784, # Monotonic, 
        2.444288, # Affine
        2.509312, # SCCNF
        # Tree
        22.400512, # GRF
        7.335424, # GRF (eff)
        6.957568, # Monotonic
        2.320896, # Affine
        2.375168, # SCCNF
        # Protein
        19.663872, # GRF
        8.287232, # GRF (eff)
        8.848384, # Monotonic
        2.1632, # Affine
        2.909696, # SCCNF
    ])

    df = pd.DataFrame(data={'Dataset':bn, 'Flow':flow, 'Memory usage (MB)':mem_usage})
    plt.rcParams.update({'font.size': 13})
    fig, ax = plt.subplots()
    fig.set_size_inches(8, 4.0)
    sns.set_palette("coolwarm_r")
    g = sns.barplot(ax=ax, x="Dataset", y="Memory usage (MB)", hue="Flow", data=df)
    g.set(xlabel=None)
    plt.savefig('./experiment_logs/figures/mem-usage-small.pdf',
                 bbox_inches='tight')
    plt.show()


def memory_usage_large():
    # All for LARGE models
    bn = ['Arithmetic Circuit']*5 + ['Tree']*5 + ['Protein']*5
    flow = [r'GRF$_L$ (Grad at end)', r'GRF$_L$ (Grad during forward)', r'GNF-M$_L$', r'GNF-A$_L$', r'SCCNF$_L$'] * 3
    mem_usage = np.array([
        # Arithmetic Circuit
        76.413952, # GRF
        18.852864, # GRF (eff)
        13.142528, # Monotonic, 
        7.659008, # Affine
        16.871424, # SCCNF
        # Tree
        67.31264, # GRF
        15.719936, # GRF (eff)
        11.63008, # Monotonic
        7.284736, # Affine
        16.738304, # SCCNF
        # Protein
        55.872, # GRF
        16.978432, # GRF (eff)
        14.72512, # Monotonic
        6.027264, # Affine
        17.273856, # SCCNF
    ])

    df = pd.DataFrame(data={'Dataset':bn, 'Flow':flow, 'Memory usage (MB)':mem_usage})
    plt.rcParams.update({'font.size': 13})
    fig, ax = plt.subplots()
    fig.set_size_inches(8, 4.0)
    sns.set_palette("coolwarm_r")
    g = sns.barplot(ax=ax, x="Dataset", y="Memory usage (MB)", hue="Flow", data=df)
    g.set(xlabel=None)
    plt.savefig('./experiment_logs/figures/mem-usage-large.pdf',
                 bbox_inches='tight')
    plt.show()


def memory_usage_thesis():
    # All for LARGE models
    bn = ['Arithmetic Circuit']*5 + ['Tree']*5 + ['Protein']*5 + ['EColi']*5
    flow = [r'GRF (Grad at end)', r'GRF (Grad during forward)', r'GNF-M', r'GNF-A', r'SCCNF'] * 4
    mem_usage = np.array([
        # Arithmetic Circuit
        76.413952, # GRF
        18.852864, # GRF (eff)
        13.142528, # Monotonic, 
        7.659008, # Affine
        16.871424, # SCCNF
        # Tree
        67.31264, # GRF
        15.719936, # GRF (eff)
        11.63008, # Monotonic
        7.284736, # Affine
        16.738304, # SCCNF
        # Protein
        55.872, # GRF
        16.978432, # GRF (eff)
        14.72512, # Monotonic
        6.027264, # Affine
        17.273856, # SCCNF
        # EColi
        212.425216, # GRF
        63.919616, # GRF (eff)
        30.950912, # Monotonic
        10.540544, # Affine
        47.125504 # SCCNF
    ])

    df = pd.DataFrame(data={'Dataset':bn, 'Flow':flow, 'Memory usage (MB)':mem_usage})
    plt.rcParams.update({'font.size': 13})
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 4.0)
    sns.set_palette("coolwarm_r")
    g = sns.barplot(ax=ax, x="Dataset", y="Memory usage (MB)", hue="Flow", data=df)
    g.set(xlabel=None)
    plt.savefig('./experiment_logs/figures/mem-usage.png', dpi=600, 
                    bbox_inches='tight')
    plt.show()


def memory_usage_thesis():
    # All for LARGE models
    bn = ['Arithmetic Circuit']*2 + ['Tree']*2 + ['Protein']*2 + ['EColi']*2
    flow = [r'GRF (Grad at end)', r'GRF (Grad during forward)']*4#, r'GNF-M', r'GNF-A', r'SCCNF'] * 4
    mem_usage = np.array([
        # Arithmetic Circuit
        76.413952, # GRF
        18.852864, # GRF (eff)
        # 13.142528, # Monotonic, 
        # 7.659008, # Affine
        # 16.871424, # SCCNF
        # Tree
        67.31264, # GRF
        15.719936, # GRF (eff)
        # 11.63008, # Monotonic
        # 7.284736, # Affine
        # 16.738304, # SCCNF
        # Protein
        55.872, # GRF
        16.978432, # GRF (eff)
        # 14.72512, # Monotonic
        # 6.027264, # Affine
        # 17.273856, # SCCNF
        # EColi
        212.425216, # GRF
        63.919616, # GRF (eff)
        # 30.950912, # Monotonic
        # 10.540544, # Affine
        # 47.125504 # SCCNF
    ])

    df = pd.DataFrame(data={'Dataset':bn, 'Flow':flow, 'Memory usage (MB)':mem_usage})
    plt.rcParams.update({'font.size': 13})
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 4.0)
    sns.set_palette("coolwarm_r")
    g = sns.barplot(ax=ax, x="Dataset", y="Memory usage (MB)", hue="Flow", data=df)
    g.set(xlabel=None)
    plt.savefig('./experiment_logs/figures/mem-usage.png', dpi=600, 
                    bbox_inches='tight')
    plt.show()


def adjust_lightness(color, amount=0.5):
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])


def banach_vs_newton_inversion():
    print('device:', device)
    def model_to_device(model):
        model = model.to(device)
        if device == 'cpu':
            for step in model.blocks:
                for mask in step.g.masks:
                    mask = mask.to(device)
                for l in step.g.layers:
                    l._weight = l._weight.to(device)
                    l._bias = l._bias.to(device)
        return model

    model_99 = model_to_device(torch.load('./experiment_logs/residual_nf_density_estimation/arithmetic-mul_graphical-lipschitz_99_lipswish_8_[125]/1/model.pt', map_location=torch.device(device)))
    bn = ArithmeticCircuit()
    z = bn.sample(100).double().to(device)
    z0, _ = model_99(z)

    sns.set_palette("coolwarm_r")
    plt.rcParams.update({'font.size': 15, 'figure.figsize': (10,5)})
    iters = range(0,200,10)

    # Invert using banach
    def inverse_banach(block, z, maxT):
        # Compute y st f(y|x) = z
        #   y0 = z
        #   y_{t+1} = z - g(y_i)      
        y = z.clone().detach()
        for _ in range(maxT):
            g, _ = block.g(y)
            y = z - g

        return y

    def invert_flow(model, z, maxT):
        with torch.no_grad():
            for block in reversed(model.blocks):
                z = inverse_banach(block, z, maxT)
        return z

    # 99 - NEWTON
    errs_newton_99 = []
    for max_iter in range(0,200,10):
        z_prime, _ = model_99.inverse(z0, maxT=max_iter, alpha=1.0)
        err = torch.mean(torch.norm(z-z_prime, dim=0)).item()
        errs_newton_99.append(max(1e-5,err))
    plt.plot(iters, errs_newton_99, c='indianred')

    # 99 - BANACH
    errs_banach_99 = []
    for max_iter in range(0,200,10):
        z_prime = invert_flow(model_99, z0, maxT=max_iter)
        err = torch.mean(torch.norm(z-z_prime, dim=0)).item()
        errs_banach_99.append(max(1e-5,err))
    plt.plot(iters, errs_banach_99, c=adjust_lightness('b', 0.4))

    model_85 = model_to_device(torch.load('./experiment_logs/residual_nf_density_estimation/arithmetic-mul_graphical-lipschitz_85_lipswish_8_[125]/1/model.pt', map_location=torch.device(device)))
    z0, _ = model_85(z)

    # 85 - NEWTON
    errs_newton_85 = []
    for max_iter in range(0,200,10):
        z_prime, _ = model_85.inverse(z0, maxT=max_iter, alpha=1.0)
        errs_newton_85.append(max(1e-5,torch.norm(z-z_prime).item()))
    plt.plot(iters, errs_newton_85, c=adjust_lightness('indianred', 1.2))

    # 85 - BANACH
    errs_banach_85 = []
    for max_iter in range(0,200,10):
        z_prime = invert_flow(model_85, z0, maxT=max_iter)
        err = torch.mean(torch.norm(z-z_prime, dim=0)).item()
        errs_banach_85.append(max(1e-5,err))
    plt.plot(iters, errs_banach_85, c=adjust_lightness('b', 1.2))
    
    model_7 = model_to_device(torch.load('./experiment_logs/residual_nf_density_estimation/arithmetic-mul_graphical-lipschitz_7_lipswish_8_[125]/1/model.pt', map_location=torch.device(device)))
    z0, _ = model_7(z)

    # 7 - NEWTON
    errs_newton_7 = []
    for max_iter in range(0,200,10):
        z_prime, _ = model_7.inverse(z0, maxT=max_iter, alpha=1.0)
        errs_newton_7.append(max(1e-5,torch.norm(z-z_prime).item()))
    plt.plot(iters, errs_newton_7, c=adjust_lightness('indianred', 1.6))

    # 7 - BANACH
    errs_banach_7 = []
    for max_iter in range(0,200,10):
        z_prime = invert_flow(model_7, z0, maxT=max_iter)
        err = torch.mean(torch.norm(z-z_prime, dim=0)).item()
        errs_banach_7.append(max(1e-5,err))
    plt.plot(iters, errs_banach_7, c=adjust_lightness('b', 1.6))

    plt.legend([r'Newton ($c=0.99$)', r'Banach ($c=0.99$)',  r'Newton ($c=0.85$)', r'Banach ($c=0.85$)', r'Newton ($c=0.70$)', r'Banach ($c=0.70$)'])
    plt.xlabel('Number of iterations per block')
    plt.ylabel('Reconstruction error')
    plt.yscale('log')
    xint= []
    for each in range(0,201,30):
        xint.append(int(each))
    plt.xticks(xint)
    plt.savefig('./experiment_logs/figures/inversion/newton-vs-banach2.pdf',
                 bbox_inches='tight')
    plt.show()


def nll_loss_curves(experiments, legend, _config, train_err_bars=True, test_err_bars=True, save_to=None):
    
    pass


def graph_topology():
    plt.rcParams.update({'font.size': 16})#, 'figure.figsize':[10,5]})
    experiments=[
        'residual_nf_density_estimation/arithmetic-mul-fc_graphical-lipschitz_99_lipmish_17_[200]/',
        'residual_nf_density_estimation/arithmetic-mul-random_graphical-lipschitz_99_lipmish_17_[200]/',
        'residual_nf_density_estimation/arithmetic-mul_graphical-lipschitz_99_lipmish_17_[200]/']
    legend=['fully-connected (28 edges)', 'random (8 edges)', 'faithful (8 edges)']
    _config={
            'num_runs':4,
            'iter':200,
            'ylims':[(0.5,14)],
            'palette':(adjust_lightness('b', 1.2), adjust_lightness('b', 1.6), 'indianred')
            }
    save_to='./experiment_logs/figures/graph_topology/arithmetic_circuit_small.pdf'

    plt.rcParams.update({'font.size': 16})
    num_runs = _config['num_runs']
    iter = _config['iter']
    ylims = _config['ylims']
    path = './experiment_logs/'
    exs_data = {'names':[]}
    exs_data_zoom = {'names':[]}
    num_experiments = len(experiments)
    palette = _config['palette'][:num_experiments]
    fig, ax = plt.subplots(figsize=[10, 5])

    for p in range(num_experiments):
        ex_name = experiments[p]
        exs_data['names'].append(ex_name)
        
        for i in range(1,num_runs+1):
            with open(os.path.join(path, experiments[p], str(i), 'metrics.json')) as json_file:
                data = json.load(json_file)
                
                if p == 0 and i == 1:
                    exs_data['iter'] = data['training.nll']['steps'][:200]
                    exs_data_zoom['iter'] = data['training.nll']['steps'][180:200]

                    num_iters = len(data['training.nll']['steps'][:200])
                    num_iters_zoom = len(data['training.nll']['steps'][180:200])

                    exs_data['train_nll'] = np.zeros((num_iters, num_runs*num_experiments))
                    exs_data_zoom['train_nll'] = np.zeros((num_iters_zoom, num_runs*num_experiments))

                    exs_data['test_nll'] = np.zeros((num_iters, num_runs*num_experiments))
                    exs_data_zoom['test_nll'] = np.zeros((num_iters_zoom, num_runs*num_experiments))

                exs_data['train_nll'][:,p*num_runs+i-1] = data['training.nll']['values'][:200]
                exs_data_zoom['train_nll'][:,p*num_runs+i-1] = data['training.nll']['values'][180:200]

                exs_data['test_nll'][:,p*num_runs+i-1] = data['test.nll']['values'][:200]
                exs_data_zoom['test_nll'][:,p*num_runs+i-1] = data['test.nll']['values'][180:200]

    columns = [[], []]
    for ex_name in exs_data['names']:
        columns[0] += [ex_name]*num_runs
        for i in range(1, num_runs+1):
            columns[1] += ['run_'+str(i)]
    columns = list(zip(*columns))
    columns = pd.MultiIndex.from_tuples(columns, names=["experiment", "runs"])

    # Training shifted reverse kl-divergence
    df = pd.DataFrame(exs_data['test_nll'], index=exs_data['iter'], columns=columns)
    df_zoom = pd.DataFrame(exs_data_zoom['test_nll'], index=exs_data_zoom['iter'], columns=columns)

    df = df.unstack(level=1).reset_index()
    df_zoom = df_zoom.unstack(level=1).reset_index()

    ci = 'sd'
    sns.lineplot(data=df, x='level_2', y=0, hue='experiment',
        palette=palette, ci=ci, ax=ax)
    ax.set_ylabel('Negative log-likelihood')
    ax.set_xlabel('epoch')
    if ylims is not None:
        ax.set_ylim(ylims[0])
    ax.legend(legend)

    # ZOOM faithful & fc
    axins = ax.inset_axes([0.55, 0.2, 0.35, 0.35])   
    sns.lineplot(data=df_zoom, x='level_2', y=0, hue='experiment',
        palette=palette, ci=ci, ax=axins)
    axins.get_legend().remove()
    axins.set_xlim(180, 200)
    axins.set_ylim(1.0, 1.5)
    axins.set_xticklabels([])
    axins.set_yticklabels([])  
    axins.set_xlabel('')
    axins.set_ylabel('') 

    ax.indicate_inset_zoom(axins, edgecolor="black")

    plt.savefig(save_to, bbox_inches='tight')
    plt.show()


if __name__ == '__main__':
    device = "cpu" #if not(torch.cuda.is_available()) else "cuda:0"
    # memory_usage_small()
    # memory_usage_large()
    memory_usage_thesis()
    # banach_vs_newton_inversion()
    # graph_topology()