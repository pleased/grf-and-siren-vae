import torch
import torch.nn as nn
import numpy as np
from math import log


class VAE(nn.Module):

    def __init__(self, inference_network, generative_network):
        super(VAE, self).__init__()
        self.inference_network = inference_network
        self.generative_network = generative_network

    
    def forward(self, x):
        '''Forward pass through the inference and generative 
        networks.

        Parameters
        ----------
        x : torch.tensor
            The observed variables of size [batch size, x_dim].

        Returns
        -------
        z : torch.tensor
            A posterior sample of size [batch size, z_dim] from the 
            approximate posterior q(z|x).
        log_q_z_given_x : float
            The density of the sample z.
        log_p_x_and_z : float
            The joint density of x and z under the given model.
        '''
        # Infer latent variable
        z, log_q_z_given_x = self.inference_network(x)

        # Compute joint under model
        log_p_x_given_z, log_p_z = self.generative_network(x, z)

        return z, log_q_z_given_x, log_p_x_given_z, \
                    log_p_z

    
    def loss(self,x, beta=1.0):
        '''Compute the negative ELBO:
            E_z~q(.) [b*(log q(z|x) - p(z)) - log p(x|z)]

            which for beta=1 is equivalent to :

            E_z~q(.) [log q(z|x) - log p(x,z)]
        ''' 

        if np.isscalar(beta):
            # Infer latent variable
            z, log_q_z_given_x = self.inference_network(x)

            # Compute joint under model
            log_p_x_given_z, log_p_z = self.generative_network(x, z)

            return torch.mean(beta*(log_q_z_given_x - log_p_z) - log_p_x_given_z)

        else:
            # Infer latent variable
            z, log_q_z_given_x = self.inference_network(x, sum_factors=False)

            # Compute joint under model
            log_p_x_given_z, log_p_z = self.generative_network(x, z, 
                            sum_factors=False)

            kl = torch.mm(log_q_z_given_x - log_p_z, torch.diag(beta)).sum(-1)
            return torch.mean(kl - log_p_x_given_z.sum(-1))



    def sample(self, batch_size, use_mu=False):
        '''Generate batch_size from the generative model.'''
        return self.generative_network.sample(batch_size, use_mu)


    def log_likelihood(self, x):
        '''Approximate log likelihood of the data under the model, calculated as
                log p(x) = log p(x,z) - log q(z|x)  
        '''
        # Approximate posterior
        z, log_q_z_given_x = self.inference_network(x)

        # Joint
        log_p_x_given_z, log_p_z = self.generative_network(x, z)

        return (log_p_x_given_z + log_p_z) - log_q_z_given_x


    def importance_sampled_log_likelihood(self, x, k):
        # Repeat x k times: [N x D] -> [N*k x D]
        N, _ = x.shape
        x = x.repeat((k,1))

        # Sample z ~ q and compute log q(z|x)
        z, log_q_z_given_x = self.inference_network(x)

        # Compute log p(x,z)
        log_p_x_given_z, log_p_z = self.generative_network(x, z)
        log_p_x_and_z = log_p_x_given_z + log_p_z

        # Reshape [N*k x 1] -> [N, k]
        log_q_z_given_x = log_q_z_given_x.T.view((k, N)).T
        log_p_x_and_z = log_p_x_and_z.T.view((k, N)).T


        log_w = log_p_x_and_z - log_q_z_given_x
        log_sum_exp_log_w = torch.logsumexp(log_w, dim=1)

        return log_sum_exp_log_w - log(k)


    def count_parameters(self):
        return self.inference_network.count_parameters() + \
            self.generative_network.count_parameters()