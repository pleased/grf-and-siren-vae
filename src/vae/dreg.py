from curses import use_default_colors
import torch
import torch.nn as nn
import numpy as np
import types

from math import log
from torch.distributions import Normal
from torch.nn.functional import softplus

from vae import VAE
from vae.vanilla.infer_network import VanillaInferNetwork


class MemoryEfficientLogJacobianDet(torch.autograd.Function):

    @staticmethod
    def forward(ctx, g_nn, eps, training, compute_jacdet, *g_params):
        ctx.training = training
        with torch.enable_grad():
            eps = eps.detach().requires_grad_(True)
            g, layer_outputs = g_nn(eps)
            ctx.g = g
            ctx.eps = eps
            logjacdet = torch.log(torch.abs(
                    compute_jacdet(layer_outputs))).sum(1) 
            
            if training:
                grad_eps, *grad_params = torch.autograd.grad(
                    logjacdet.sum(), (eps,) + g_params, retain_graph=True,
                    allow_unused=True
                )
                if grad_eps is None:
                    grad_eps = torch.zeros_like(eps)
                ctx.save_for_backward(grad_eps, *g_params, *grad_params)
        
        return safe_detach(g), safe_detach(logjacdet)


    @staticmethod
    def backward(ctx, grad_g, grad_logdetjac):
        training = ctx.training
        if not training:
            raise ValueError('Provide training=True if using backward.')

        with torch.enable_grad():
            grad_eps, *params_and_grad = ctx.saved_tensors
            g, eps = ctx.g, ctx.eps

            # Precomputed gradients.
            g_params = params_and_grad[:len(params_and_grad) // 2]
            grad_params = params_and_grad[len(params_and_grad) // 2:]
            dg_eps, *dg_params = torch.autograd.grad(g, [eps] + g_params, grad_g, allow_unused=True, retain_graph=True) 

        # Update based on gradient from logdetgrad.
        dL = grad_logdetjac[0].detach()
        with torch.no_grad():
            grad_eps.mul_(dL)
            grad_params = tuple([g.mul_(dL) if g is not None else None for g in grad_params])
        
        # Update based on gradient from g.
        with torch.no_grad():
            grad_eps.add_(dg_eps)
            grad_params = tuple([dg.add_(djac) if djac is not None else dg for dg, djac in zip(dg_params, grad_params)])

        #return (None, None, grad_eps, None, None, None, None) + grad_params
        return (None, grad_eps, None, None) + grad_params


def safe_detach(tensor):
        return tensor.detach().requires_grad_(tensor.requires_grad)

def _mem_efficient_forward(self, eps, sum_factors=True):
    g, logdetjac = MemoryEfficientLogJacobianDet.apply(
        self.g, eps, self.training, self._jacobian_det,
        *list(self.g.parameters())
    )
    return g, logdetjac

class DReG(VAE):

    def __init__(self, inference_network, generative_network, k):
        super(DReG, self).__init__(inference_network, 
                generative_network)
        self.inference_network = inference_network
        self.generative_network = generative_network
        self.k = k

        if not isinstance(self.inference_network, VanillaInferNetwork):
            for step in self.inference_network.nf.steps:
                if step.grad_in_forward:
                    step._mem_efficient_forward = types.MethodType(_mem_efficient_forward, step)
            for step in self.generative_network.nf.steps:
                if step.grad_in_forward:
                    step._mem_efficient_forward = types.MethodType(_mem_efficient_forward, step)



    def loss(self, x, beta=1.0):
        '''Compute:

            - E_z~q(.) [- log k + log(sum_k exp{log p(x,z_k) - q(z_k|x)})]
        '''    
        # print('Computing loss')
        if not self.training:
            return torch.mean(-self.importance_sampled_log_likelihood(x, self.k))

        N, _ = x.shape


        if isinstance(self.inference_network, VanillaInferNetwork):
            # Encode x
            loc, scale = self.inference_network.encode(x)

            # Sample z and compute posterior densities
            p_z = Normal(loc=torch.zeros_like(loc), scale=torch.ones_like(scale))
            q_z_given_x = Normal(loc=loc, scale=scale)
            stop_grad_q_z_given_x = Normal(loc=loc.detach(), 
                                           scale=scale.detach())

            eps = p_z.sample((self.k,))         # [k, N, latent_dim]
            z = loc + eps * scale               # [k, N, latent_dim]

            log_q_z_given_x = q_z_given_x.log_prob(z) # [k, N, latent_dim]
            stop_grad_log_q_z_given_x = stop_grad_q_z_given_x \
                                        .log_prob(z)  # [k, N, latent_dim]

            # Compute observation likelihood
            log_p_x_given_z, log_p_z = self.generative_network(x.repeat((self.k,
                    1)), z.view((N*self.k, -1))) # [k*N, D], [k*N, latent_dim]
                    #.reshape((self.k,N))

            if np.isscalar(beta):
                # Reshape [N*k x 1] -> [k, N]
                log_q_z_given_x = beta*log_q_z_given_x.sum(-1).view((self.k,N))
                stop_grad_log_q_z_given_x = beta*stop_grad_log_q_z_given_x.sum(-1).view((self.k,N))
                log_p_x_and_z = log_p_x_given_z.view((self.k, N)) + \
                        beta*log_p_z.view((self.k, N))

            else:
                raise Exception('Non-scalar beta not implemented for Vanilla inference network.')

        else:
            # Repeat x k times: [N x D] -> [N*k x D]
            x = x.repeat((self.k,1))

            # Sample z ~ q and compute log q(z|x)
            nl = self.inference_network.nf.latent_dim
            p_eps = Normal(loc=torch.zeros((x.shape[0], nl)).to(x.device), 
                        scale=torch.ones((x.shape[0], nl)).to(x.device))
            eps = p_eps.sample().to(x.device)

            if np.isscalar(beta):
                # Sample z ~ q and compute log q(z|x)
                z, log_q_z_given_x = self.inference_network(x, eps=eps)

                # Compute log p(x,z)
                log_p_x_given_z, log_p_z = self.generative_network(x, z)

                # Compute q using detached parameters
                self.eval()
                stop_grad_log_q_z_given_x = self.stop_grad_q_via_inverse(z, x)
                self.train()

                log_q_z_given_x = beta*log_q_z_given_x.view((self.k,N))
                stop_grad_log_q_z_given_x = beta*stop_grad_log_q_z_given_x.view((self.k,N))
                log_p_x_and_z = log_p_x_given_z.view((self.k, N)) + \
                        beta*log_p_z.view((self.k, N))

            else:
                z, log_q_z_given_x = self.inference_network(x, eps=eps,
                         sum_factors=False)

                # Compute log p(x,z)
                log_p_x_given_z, log_p_z = self.generative_network(x, z,
                         sum_factors=False)

                # Compute q using detached parameters
                self.eval()
                stop_grad_log_q_z_given_x = self.stop_grad_q_via_inverse(z, x, sum_factors=False)
                
                self.train()
                        
                log_q_z_given_x = torch.mm(log_q_z_given_x, 
                        torch.diag(beta)).sum(-1).view((self.k,N))
                stop_grad_log_q_z_given_x = torch.mm(stop_grad_log_q_z_given_x, 
                        torch.diag(beta)).sum(-1).view((self.k,N))
                log_p_x_and_z = log_p_x_given_z.sum(-1).view((self.k, N)) + \
                        torch.mm(log_p_z, torch.diag(beta)).sum(-1).view((self.k, N))

        # To compute gradient w.r.t. decoder parameters
        log_w = log_p_x_and_z - log_q_z_given_x     # [k, N]
        log_p_loss = -torch.mean(torch.logsumexp(log_w, dim=0) - \
                                    log(self.k)) 

        # To compute gradients w.r.t. encoder parameters
        stop_grad_log_w = log_p_x_and_z - stop_grad_log_q_z_given_x
        # weights here aren't backpropogated:
        normalized_weights = torch.exp((stop_grad_log_w - \
                torch.logsumexp(stop_grad_log_w, dim=0)).detach()) 
        log_q_loss = -torch.mean((normalized_weights.pow(2) * \
            stop_grad_log_w).sum(dim=0))

        return log_p_loss, log_q_loss


    # 20, 1e-5
    def stop_grad_q_via_inverse(self, z, x, maxT=10, sum_factors=True):
        j_total = 0.0

        for step in reversed(self.inference_network.nf.steps):
            z, j = self.stop_grad_q_via_inverse_step(z, x, step, maxT, 
                sum_factors=sum_factors)
            j_total += j

        q0 = Normal(loc=torch.zeros_like(z), 
                scale=torch.ones_like(z)).log_prob(z)
        if sum_factors: q0 = q0.sum(-1)

        return q0 - j_total


    
    def stop_grad_q_via_inverse_step(self, z, x, step, maxT, sum_factors=True):
        y = z
        with torch.no_grad():
            e = torch.norm(y)
        epsilon = 1e-4

        def lipmish(x, beta):
            b = softplus(beta)
            return x*torch.tanh(softplus(b*x))/1.088

        def lipmish_prime(x, beta):
            b = softplus(beta)
            return (torch.tanh(softplus(b*x)) + \
                b*x*torch.pow(1/torch.cosh(softplus(b*x)),2)*torch.sigmoid(b*x))/1.088

        # Invert to obtain y st z = y + g(y)
        for t in range(maxT):
            if e <= epsilon: break
            g = torch.cat((y, x), dim=1)

            # Compute: g, layer_outputs = self.g(yx)
            layer_outputs = []
            for l in range(len(step.g.layers)-1):
                W = step.g.layers[l]._weight.detach()
                g = torch.addmm(step.g.layers[l]._bias.detach(), g, W.transpose(0,1))

                layer_outputs.append(g)
                g = lipmish(g, step.g.activation_func.beta.detach())

            W = step.g.layers[-1]._weight.detach()
            g = torch.addmm(step.g.layers[-1]._bias.detach(), g, W.transpose(0,1))
            
            # Compute: diag_J = self._jacobian_det(layer_outputs)
            L = step.g.num_hidden
            N = layer_outputs[0].shape[0]
            W0 = step.g.layers[0]._weight.detach()[:,:step.out_dim]
            J = W0.expand(N,-1,-1)
            for l in range(1,L+1):
                W = step.g.layers[l]._weight.detach()
                h_prime = lipmish_prime(layer_outputs[l-1], step.g.activation_func.beta.detach()).unsqueeze(dim=1)
                J = h_prime*J.transpose(1,2)
                J = J.reshape(N*step.out_dim, step.hidden_dims[l-1]).transpose(0,1)
                J = torch.mm(W,J)
                J = torch.stack(J.chunk(N, dim=1), dim=0)       

            # log det J_f = trace(log(J_f))
            diag_J = torch.stack([torch.diag(j) for j in torch.unbind(J, dim=0)], dim=0)
            diag_J = diag_J + 1

            f = y + g
            y = y - 1.0*(f - z)/diag_J

            with torch.no_grad():
                e = torch.norm(f - z)

        logjacdet = torch.log(torch.abs(diag_J))
        if sum_factors: logjacdet = logjacdet.sum(1)

        return y, logjacdet