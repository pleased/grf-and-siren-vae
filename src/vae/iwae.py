import torch
import numpy as np

from math import log

from vae import VAE


class IWAE(VAE):

    def __init__(self, inference_network, generative_network, k):
        super(IWAE, self).__init__(inference_network, 
                generative_network)
        self.inference_network = inference_network
        self.generative_network = generative_network
        self.k = k


    def loss(self, x, beta=1.0):
        '''Compute:

            - E_z~q(.) [- log k + log(sum_k exp{log p(x,z_k) - q(z_k|x)})]
        '''      
        # Repeat x k times: [N x D] -> [N*k x D]
        N, _ = x.shape

        x = x.repeat((self.k,1))

        if np.isscalar(beta):
            # Sample z ~ q and compute log q(z|x)
            z, log_q_z_given_x = self.inference_network(x)

            # Compute log p(x,z)
            log_p_x_given_z, log_p_z = self.generative_network(x, z)

            log_q_z_given_x = beta*log_q_z_given_x
            log_p_x_and_z = log_p_x_given_z + beta*log_p_z

        else:
            # Sample z ~ q and compute log q(z|x)
            z, log_q_z_given_x = self.inference_network(x, sum_factors=False)

            # Compute log p(x,z)
            log_p_x_given_z, log_p_z = self.generative_network(x, z, 
                    sum_factors=False)

            log_q_z_given_x = torch.mm(log_q_z_given_x,torch.diag(beta)).sum(-1)
            log_p_x_and_z = log_p_x_given_z.sum(-1) + \
                            torch.mm(log_p_z, torch.diag(beta)).sum(-1)

        # Reshape [N*k x 1] -> [N, k]
        log_q_z_given_x = log_q_z_given_x.T.view((self.k, N)).T
        log_p_x_and_z = log_p_x_and_z.T.view((self.k, N)).T

        log_w = log_p_x_and_z - log_q_z_given_x
        log_sum_exp_log_w = torch.logsumexp(log_w, dim=1)

        importance_sampled_ll = log_sum_exp_log_w - log(self.k)

        return torch.mean(-importance_sampled_ll)