import torch
import torch.nn as nn

from torch.distributions import Normal

from modules import StandardNormalLogDensity
from normalizing_flow.discrete.resflow.residual_blocks.graphical_lipschitz import GraphicalLipschitzResidualBlock
from normalizing_flow.discrete.ignf import NormalizingFlowStep


class NFDiscreteTime(nn.Module):
    '''Normalizing mapping: z -> e
        where e ~ N(0,I)
    '''

    def __init__(self, steps, graph, device):
        super(NFDiscreteTime, self).__init__()
        self.graph = graph
        self.device = device
        self.latent_dim = graph.get_num_latent()
        self.eps_log_density = StandardNormalLogDensity()
        
        self.steps = nn.ModuleList()
        for step in steps:
            self.steps.append(step)
    

    def forward(self, z, x=None, sum_factors=True):
        j_total = 0.0

        for step in self.steps:
            if isinstance(step, GraphicalLipschitzResidualBlock):
                z, j = step(z, x, sum_factors=sum_factors)
            else:
                z, j = step(z, x)
            j_total = j_total + j

        eps = z
        q0 = self.eps_log_density(eps, sum_factors=sum_factors)
        log_q_z = q0 + j_total
        
        return eps, log_q_z

    
    def inverse(self, eps, x=None, maxT=15, epsilon=1e-5, alpha=1.0,
                 sum_factors=True):
        j_total = 0.0

        q0 = self.eps_log_density(eps, sum_factors=sum_factors)

        with torch.no_grad():
            for step in reversed(self.steps):
                if isinstance(step, GraphicalLipschitzResidualBlock):
                    eps, j = step.inverse(eps, x, maxT, epsilon, alpha, 
                            sum_factors=sum_factors)
                elif isinstance(step, NormalizingFlowStep):
                    eps, j = step.inverse_fixed_point(eps, x, maxT, epsilon)
                else:
                    raise Exception("Unknown flow step type: {}".format(type(step)))
                j_total = j_total + j

        z = eps
        log_q_z = q0 + j_total

        return z, log_q_z

