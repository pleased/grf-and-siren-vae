import torch
import torch.nn as nn

from torch.distributions import Normal

from modules import StandardNormalLogDensity

from normalizing_flow.discrete.resflow.residual_blocks.graphical_lipschitz import GraphicalLipschitzResidualBlock
from normalizing_flow.discrete.ignf import NormalizingFlowStep
from normalizing_flow.discrete.resflow.residual_blocks.graphical_lipschitz import GraphicalLipschitzResidualBlock


class GFDiscreteTime(nn.Module):
    '''Generative mapping: e -> z
        where e ~ N(0,I)
    '''

    def __init__(self, steps, graph, device):
        super(GFDiscreteTime, self).__init__()
        self.graph = graph
        self.device = device
        self.latent_dim = graph.get_num_latent()
        self.eps_log_density = StandardNormalLogDensity()
        
        self.steps = nn.ModuleList()
        for step in steps:
            self.steps.append(step)
    

    def forward(self, x=None, eps=None, sum_factors=True):
        j_total = 0.0

        # Sample from reference distribution
        if eps is None:
            eps = Normal(
                loc=torch.zeros(self.latent_dim, dtype=torch.float32), 
                scale=torch.ones(self.latent_dim, dtype=torch.float32)
                ).sample((x.shape[0],)).to(self.device)
            if hasattr(self.steps[0], 'g'):
                dtype = self.steps[0].g.layers[0]._weight.dtype
                if dtype == torch.float64:
                    eps = eps.double()

        q0 = self.eps_log_density(eps, sum_factors=sum_factors)

        for step in self.steps:
            if isinstance(step, GraphicalLipschitzResidualBlock):
                eps, j = step(eps, x, sum_factors=sum_factors)
            else:
                eps, j = step(eps, x)
            j_total = j_total + j

        z = eps
        log_q_z_given_x = q0 - j_total
        
        return z, log_q_z_given_x

    
    def inverse(self, z, x=None, maxT=50, epsilon=1e-5, sum_factors=True):
        j_total = 0.0

        with torch.no_grad():
            for step in reversed(self.steps):
                if isinstance(step, GraphicalLipschitzResidualBlock):
                    z, j = step.inverse(z, x, maxT, epsilon, 
                        sum_factors=sum_factors)
                elif isinstance(step, NormalizingFlowStep):
                    z, j = step.inverse_fixed_point(z, x, maxT, epsilon)
                else:
                    raise Exception("Unknown flow step type: {}".format(type(step)))
                j_total = j_total + j

        eps = z
        q0 = self.eps_log_density(eps, sum_factors=sum_factors)
        log_q_z_given_x = q0 - j_total

        return eps, log_q_z_given_x