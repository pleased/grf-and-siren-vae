import torch.nn as nn

from torchdiffeq import odeint_adjoint as odeint
from torch.distributions import Normal
from toolz import last


class NFContinuousTime(nn.Module):

    def __init__(self, odefunc, graph, x_shift, x_scale, 
                integration_times=[0.0, 1.0], solver='dopri5', 
                atol=1e-5, rtol=1e-5):
        super(NFContinuousTime, self).__init__()

        self.graph = graph
        self.num_latent = graph.get_num_latent()
        self.odefunc = odefunc
        self.solver = solver
        self.atol = atol
        self.rtol = rtol

        self.x_shift = x_shift
        self.x_scale = x_scale
    

    def forward(self, x):
        batch_size = x.shape[0]
        integration_times = [0.0, 1.0]
        
        # Normalize
        x = (x - self.x_shift)/self.x_scale
        self.odefunc.diffeq.condition = x

        # sample from reference distribution
        q0 = Normal(0.0, 1.0)  
        z0 = q0.sample((batch_size, self.num_latent)).to(x)
        log_q0 = q0.log_prob(z0).sum(dim=1).unsqueeze(1)

        # Refresh the odefunc statistics
        self.odefunc.before_odeint()

        state_t = odeint(
            self.odefunc,
            (z0, log_q0),
            integration_times.to(x),
            atol=self.atol,
            rtol=self.rtol,
            method=self.solver,
            options=self.solver_options,
        )
        z, delta_log_q = map(last, state_t[:2])

        log_q_z_given_x = log_q0 + delta_log_q 

        return z, log_q_z_given_x
