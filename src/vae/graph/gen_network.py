import torch
import torch.nn as nn

from torch.distributions import Normal

from modules import MaskedMLP, MLP
from graph.utils import graph_masks

class GraphGenNetwork(nn.Module):

    def __init__(self, graph, nf, hidden_dims, device, act_func=nn.ELU(), 
                    condition_sigma=True, z_as_unit=False, likelihood='gaussian'):
        super(GraphGenNetwork, self).__init__()
        self.graph = graph
        self.latent_dim = graph.get_num_latent()
        self.obs_dim = graph.get_num_obs()
        self.n = self.latent_dim + self.obs_dim
        self.device = device

        self.nf = nf

        if z_as_unit:
            if condition_sigma:
                self.decoder_net = MLP(in_size=(self.latent_dim),
                    hidden=hidden_dims, out_size=2*(self.obs_dim), act_f=act_func)
            else:
                self.decoder_net = MLP(in_size=(self.latent_dim),
                    hidden=hidden_dims, out_size=(self.obs_dim), act_f=act_func)
                self.log_sigma = torch.nn.Parameter(torch.zeros(1))
        else:
            if condition_sigma:
                masks = graph_masks(
                    g=graph.forward_graph, 
                    input_vars=[*range(self.latent_dim)],
                    output_vars=[*range(self.latent_dim,self.n)]*2,
                    hidden_dims=hidden_dims
                    )
                self.masks = [torch.from_numpy(mask).to(device) for mask in masks]
                self.decoder_net = MaskedMLP(in_size=(self.latent_dim),
                    hidden=hidden_dims, out_size=2*(self.obs_dim),
                    masks=self.masks, activation_func=act_func)
            else:
                masks = graph_masks(
                    g=graph.forward_graph, 
                    input_vars=[*range(self.latent_dim)],
                    output_vars=[*range(self.latent_dim,self.n)],
                    hidden_dims=hidden_dims
                    )
                self.masks = [torch.from_numpy(mask).to(device) for mask in masks]
                self.decoder_net = MaskedMLP(in_size=(self.latent_dim),
                    hidden=hidden_dims, out_size=(self.obs_dim),
                    masks=self.masks, activation_func=act_func)
                self.log_sigma = torch.nn.Parameter(torch.zeros(1))


    def forward(self, x, z, sum_factors=True):
        '''Forward pass through the generative network. 


        Parameters
        ----------
        x : torch.tensor
            The observed variables of size [batch size, x_dim].
        z : torch.tensor
            A sample of size [batch size, z_dim] of the latent 
            variables from the inference network.

        Returns
        -------
        p_x_and_z : torch.tensor
            The joint density of x and z under the given model.
        '''
        # Compute density of z
        _, log_p_z = self.nf(z, sum_factors=sum_factors)

        # Calculate mean and standard deviation 
        params = self.decoder_net(z)

        if params.shape[1] == 2*self.obs_dim:
            mu = params[:,:self.obs_dim]
            sigma = torch.exp(params[:,self.obs_dim:])#.clamp(-5,5))
        else:
            mu = params
            sigma = torch.exp(self.log_sigma)
        p = Normal(loc=mu, scale=sigma)

        # Compute the obs likelihood
        log_p_x_given_z = p.log_prob(x)
        if sum_factors: log_p_x_given_z = log_p_x_given_z.sum(dim=1)
        
        return log_p_x_given_z, log_p_z


    def log_likelihood(self, x, z):
        '''Forward pass through the generative network. 

        Parameters
        ----------
        x : torch.tensor
            The observed variables of size [batch size, x_dim].
        z : torch.tensor
            A sample of size [batch size, z_dim] of the latent 
            variables from the inference network.

        Returns
        -------
        p_x_given_z : torch.tensor
            The likelihood of x given z under the given model.
        '''
        # Calculate mean and standard deviation 
        params = self.decoder_net(z)

        if params.shape[1] == 2*self.obs_dim:
            mu = params[:,:self.obs_dim]
            sigma = torch.exp(params[:,self.obs_dim:])#.clamp(-5,5))
        else:
            mu = params
            sigma = torch.exp(self.log_sigma)
        p = Normal(loc=mu, scale=sigma)

        # Compute the obs likelihood
        log_p_x_given_z = p.log_prob(x).sum(-1)
        
        return log_p_x_given_z


    
    def sample(self, batch_size, use_mu=False):
        # Generate sample of latent variables
        eps = Normal(
            loc=torch.zeros(self.latent_dim, dtype=torch.float32), 
            scale=torch.ones(self.latent_dim, dtype=torch.float32)
            ).sample((batch_size,)).to(self.device)
        if hasattr(self.nf.steps[0], 'g'):
            dtype = self.nf.steps[0].g.layers[0]._weight.dtype
        elif hasattr(self.nf.steps[0], 'conditioner'):
            dtype = self.nf.steps[0].conditioner.embedding_net.layers[0]._weight.dtype
        if dtype == torch.float64:
            eps = eps.double()
        z, _ = self.nf.inverse(eps)

        # Calculate mean and standard deviation 
        params = self.decoder_net(z)

        if params.shape[1] == 2*self.obs_dim:
            mu = params[:,:self.obs_dim]
            sigma = torch.exp(params[:,self.obs_dim:])
        else:
            mu = params
            sigma = torch.exp(self.log_sigma)
        p = Normal(loc=mu, scale=sigma)

        if use_mu: return mu, z
        else:
            # Generate sample of observed variables:
            x = p.sample().to(self.device)
            return x, z

    
    def count_parameters(self):
        total = sum(p.numel() for p in self.parameters() if p.requires_grad)
        masked = 0
        # Masks in MLP
        for m in self.masks:
            masked += torch.numel(m) - torch.count_nonzero(m)

        # Masks in flow
        for s in self.nf.steps:
            for m in s.g.masks:
                masked += torch.numel(m) - torch.count_nonzero(m)
        
        return (total - masked).item()