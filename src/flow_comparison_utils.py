import matplotlib.pyplot as plt
import numpy as np
import os
import json
import torch
import matplotlib.colors as mc
import colorsys

from scipy.stats import ttest_rel
from tqdm import trange
from math import isnan, isinf

from graph.belief_network import ArithmeticCircuit, Tree, EColi70Adapted
from normalizing_flow.discrete.ignf.normalizers.affine import AffineNormalizer
from normalizing_flow.discrete.ignf.normalizers.monotonic import MonotonicNormalizer
from normalizing_flow.discrete.ignf.conditioners.dag import DAGConditioner
from normalizing_flow.discrete.ignf.conditioners.dag_masked import DAGMaskedConditioner
from normalizing_flow.discrete.ignf.discrete_nf import DiscreteNF
from normalizing_flow.continuous.sccnf.continuous_nf import ContinuousNF
from ex_utils import Timer
from normalizing_flow.discrete.resflow.graphical_residual_flow import GraphicalResidualFlow

global_colours = ['red','blue', 'green', 'darkorange', 'teal', 'purple']


def plot_loss(experiments, _config):
    plt.style.use('seaborn')
    plt.rc('axes', labelsize=15)
    plt.rc('legend', fontsize=15)
    plt.rcParams.update({'font.size': 15})

    iter = _config['iter']
    ylims = _config['ylims']
    log_scale = _config['log_scale']
    legend = _config['legend']
    path = './experiment_logs/'
    num_experiments = len(experiments)
    if 'palette' not in _config: 
        palette = global_colours[:num_experiments]
    else:
        palette = _config['palette']
    if 'linestyles' not in _config:
        linestyles = ['-']*num_experiments
    else:
        linestyles = _config['linestyles']

    fig, ax = plt.subplots(1,2, figsize=(15,5))
    for i, ex in enumerate(experiments):
        ex_path = path + ex
        test_losses = []
        train_losses = []
        for run in os.listdir(ex_path):
            if not run.isdigit(): continue
            ex_run_path = ex_path + '/{}/'.format(run)
            with open(ex_run_path+'metrics.json') as f:
                metrics = json.load(f)
            test_losses.append(metrics['val']['values'][:iter])
            train_losses.append(metrics['train']['values'][:iter])
        test_losses = np.array(test_losses)
        train_losses = np.array(train_losses)
        
        test_mean = test_losses.mean(axis=0)
        train_mean = train_losses.mean(axis=0)
        
        test_std = test_losses.std(axis=0)
        train_std = train_losses.std(axis=0)
        
        ax[0].plot(train_mean, label = legend[i], c=palette[i], 
            linestyle=linestyles[i])
        ax[1].plot(test_mean, label = legend[i], c=palette[i], 
            linestyle=linestyles[i])
        ax[0].fill_between(range(train_losses.shape[1]), 
            train_mean - train_std, train_mean + train_std, color=palette[i], alpha=0.3)
        ax[1].fill_between(range(test_losses.shape[1]), test_mean - test_std, 
            test_mean + test_std, color=palette[i], alpha=0.3)
        
        ax[0].set_xlabel('Epoch')
        ax[1].set_xlabel('Epoch')
        ax[0].set_ylabel('Train '+_config['objective'])
        ax[1].set_ylabel('Validation '+_config['objective'])
    
    ax[0].legend([],[], frameon=False)
    
    if ylims is not None:
        ax[0].set_ylim(ylims[0])
        ax[1].set_ylim(ylims[1])

    if log_scale:
        ax[0].set(yscale='log')
        ax[1].set(yscale='log')

 
    handles, labels = ax[1].get_legend_handles_labels()
    ax[1].legend(handles, labels)
       
    plt.show()


def set_model(model, double, device):
    model = model.to(device)
    if isinstance(model.bn, Tree):
        model.bn = Tree(device, double)

    if isinstance(model, GraphicalResidualFlow):
        if double:
            if not hasattr(model, 'scale') or model.scale is None:
                model.scale = torch.tensor([1.0]).double().to(device)
            else:
                model.scale = model.scale.double().to(device)
            if not hasattr(model, 'shift') or model.shift is None:
                model.shift = torch.tensor([0.0]).double().to(device)
            else:
                model.shift = model.shift.double().to(device)
        else:
            if not hasattr(model, 'scale') or model.scale is None:
                model.scale = torch.tensor([1.0]).float().to(device)
            else:
                model.scale = model.scale.float().to(device)
            if not hasattr(model, 'shift') or model.shift is None:
                model.shift = torch.tensor([0.0]).float().to(device)
            else:
                model.shift = model.shift.float().to(device)
        for block in model.blocks:
            for layer in block.g.layers:
                if double:
                    layer._weight = layer._weight.double().to(device)
                    layer._bias = torch.nn.Parameter(layer._bias.double().to(device))
                    layer.mask = layer.mask.double().to(device)
                else:
                    layer._weight = layer._weight.float().to(device)
                    layer._bias = torch.nn.Parameter(layer._bias.float().to(device))
                    layer.mask = layer.mask.float().to(device)

    elif isinstance(model, DiscreteNF):
        if double:
            if not hasattr(model, 'scale') or model.scale is None:
                model.scale = torch.tensor([1.0]).double().to(device)
            else:
                model.scale = model.scale.double().to(device)
            if not hasattr(model, 'shift') or model.shift is None:
                model.shift = torch.tensor([0.0]).double().to(device)
            else:
                model.shift = model.shift.double().to(device)
        else:
            if not hasattr(model, 'scale') or model.scale is None:
                model.scale = torch.tensor([1.0]).float().to(device)
            else:
                model.scale = model.scale.float().to(device)
            if not hasattr(model, 'shift') or model.shift is None:
                model.shift = torch.tensor([0.0]).float().to(device)
            else:
                model.shift = model.shift.float().to(device)
        if double:
            for step in model.steps:
                if isinstance(step.conditioner, DAGMaskedConditioner):
                    for layer in step.conditioner.embedding_net.layers:
                        layer.mask = layer.mask.double().to(device) 
                        layer._weight = torch.nn.Parameter(layer._weight.double().to(device))
                        layer._bias = torch.nn.Parameter(layer._bias.double().to(device))       

                elif isinstance(step.conditioner, DAGConditioner):
                    step.conditioner.A = step.conditioner.A.double().to(device)
                    step.conditioner.embedding_net.net = step.conditioner.embedding_net.net.double().to(device)

                if isinstance(step.normalizer, MonotonicNormalizer):
                    step.normalizer.integrand_net.net = step.normalizer.integrand_net.net.double().to(device)
        else:
            for step in model.steps:
                if isinstance(step.conditioner, DAGMaskedConditioner):
                    for layer in step.conditioner.embedding_net.layers:
                        layer.mask = layer.mask.float().to(device) 
                        layer._weight = torch.nn.Parameter(layer._weight.float().to(device))
                        layer._bias = torch.nn.Parameter(layer._bias.float().to(device))       

                elif isinstance(step.conditioner, DAGConditioner):
                    step.conditioner.A = step.conditioner.A.float().to(device)
                    step.conditioner.embedding_net.net = step.conditioner.embedding_net.net.float().to(device)

                if isinstance(step.normalizer, MonotonicNormalizer):
                    step.normalizer.integrand_net.net = step.normalizer.integrand_net.net.float().to(device)

    elif isinstance(model, ContinuousNF):        
        if double:
            if model.cond_shift is not None:
                model.cond_shift = model.cond_shift.double().to(device)
                model.cond_scale = model.cond_scale.double().to(device)
            model.z_shift = model.z_shift.double().to(device)
            model.z_scale = model.z_scale.double().to(device)
            for step in model.odefunc.diffeq.steps:
                step._weight_mask = step._weight_mask.double().to(device)
                step._weights = torch.nn.Parameter(step._weights.double().to(device))
                step._bias = torch.nn.Parameter(step._bias.double().to(device))
                step._hyper_bias = step._hyper_bias.double().to(device)
                step._hyper_gate = step._hyper_gate.double().to(device)
        else:
            if model.cond_shift is not None:
                model.cond_shift = model.cond_shift.float().to(device)
                model.cond_scale = model.cond_scale.float().to(device)
            model.z_shift = model.z_shift.float().to(device)
            model.z_scale = model.z_scale.float().to(device)
            for step in model.odefunc.diffeq.steps:
                step._weight_mask = step._weight_mask.float().to(device)
                step._weights = torch.nn.Parameter(step._weights.float().to(device))
                step._bias = torch.nn.Parameter(step._bias.float().to(device))
                step._hyper_bias = step._hyper_bias.float().to(device)
                step._hyper_gate = step._hyper_gate.float().to(device)
    
    return model


def nll(model, x, batch_size=100):
    nll = 0
    data = torch.utils.data.DataLoader(x, batch_size=batch_size)
    for batch in data:
        if isinstance(model, ContinuousNF):
            z0, j = model(batch, torch.tensor([0.0, 1.0]).to(x.device))
        else:
            z0, j = model(batch)
        nll += -model.ll(z0,j).item()
    return nll/(x.shape[0]//batch_size)



def print_nll(x, bn, experiments, device, seed=0):
    # experiments = [
    #    ('name', path)
    # ]
    torch.manual_seed(seed)
    np.random.seed(seed)

    x = x.to(device)
    print('Num test samples:',x.shape[0])

    path = './experiment_logs/'
    hline = '+----------------+---------------+'
    print(hline)
    print('| Model          | Avg NLL (std) |')
    print(hline)

    results = []
    for ex in experiments:
        line = '| {:<14} |'.format(ex[0])
        p = path + ex[1]
        nlls = []

        with torch.no_grad():
            for run in os.listdir(p):
                if not run.isdigit(): continue
                torch.manual_seed(seed)
                np.random.seed(seed)
                model = set_model(torch.load(p+'/{}/'.format(run)+'model.pt'.format(run), map_location=torch.device(device)), True, device)
                nlls.append(nll(model, x))
        results.append(nlls)
        line = line + " {0:4.3f} ({1:4.3f}) |".format(np.mean(nlls), np.std(nlls))
        print(line)
        print(hline)
    
    # if isinstance(bn, ArithmeticCircuit) or isinstance(bn,EColi70Adapted):
    #     print('* True log p(x) = {0:.3f}'.format(torch.mean(bn.log_joint(x[:,bn.get_num_latent():],x[:,:bn.get_num_latent()]))))
    # if isinstance(bn, Tree):
    #     x = x.float()
    #     print('* True log p(x) = {0:.3f}'.format(torch.mean(bn.log_joint(x[:,bn.get_num_latent():],x[:,:bn.get_num_latent()]))))
    

    return results


def reverse_kl(model, x, batch_size=100):
    kls = 0
    data = torch.utils.data.DataLoader(x, batch_size=batch_size)
    for batch in data:
        if isinstance(model, ContinuousNF):
            z0, z, j = model(batch, torch.tensor([0.0,1.0],dtype=torch.float64).to(batch.device))
        else:
            z0, z, j = model(cond=batch)
        kls += model.shifted_reverse_kl(batch, z, z0, j).item()
    return kls/(x.shape[0]//batch_size)



def print_reverse_kl(x, bn, experiments, device, seed=0):
    # experiments = [
    #    ('name', path)
    # ]
    x = x.to(device)
    print('Num test samples:',x.shape[0])

    path = './experiment_logs/'
    hline = '+----------------+----------------------+'
    print(hline)
    print('| Model          | Avg Reverse KL (std) |')
    print(hline)

    torch.manual_seed(seed)
    np.random.seed(seed)

    x = x[:,bn.get_num_latent():]

    results = []
    for ex in experiments:
        line = '| {:<14} |'.format(ex[0])
        p = path + ex[1]
        kls = []

        with torch.no_grad():
            for run in os.listdir(p):
                if not run.isdigit(): continue
                torch.manual_seed(seed)
                np.random.seed(seed)
                model = torch.load(p+'/{}/'.format(run)+'model.pt'.format(run), map_location=torch.device(device))
                model = set_model(model, True, device)
                kls.append(reverse_kl(model, x))
        results.append(kls)
        kl = "{0:4.3f} ({1:4.3f})".format(np.mean(kls), np.std(kls))
        line = line + " {:20} |".format(kl)
        print(line)
        print(hline)

    return results


def longest_shortest_path(bn):
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()
    g = bn.inverse_graph
    d = np.zeros((num_obs,num_latent))

    def _graph_distance(g, i, j):
        min_dist = len(g.get_vertices())
        for k in g.get_out_neighbours(i):
            if k == j:
                dist = 1
            else:
                d = _graph_distance(g, k, j)
                if d > -1: dist = 1 + d
                else: dist = -1

            if dist > -1 and dist < min_dist:
                min_dist = dist

        return min_dist if min_dist != len(g.get_vertices()) else -1

    for i in range(num_obs):
        for j in range(num_latent):
            d[i,j] = _graph_distance(g, i+num_latent,j)
    return np.max(d)



def t_test(samples):
    np.set_printoptions(precision=5, suppress=True)
    N = len(samples)
    p_values = np.zeros((N,N))

    for i in range(N):
        for j in range(i+1,N):
            stat, p = ttest_rel(samples[i],samples[j])
            p_values[i,j] = p

    return p_values



def convergence_test_discrete(model, z, save_to, maxT=50, flow_type='gnf', double=True, device='cpu'):
    model = set_model(model, double, device)
    
    z = z.to(device)
    if double:
        z = z.double()
    N = z.shape[0]
    z0, _ = model(z)

    alphas = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9]
    
    # initialize
    results = [None]*N
    for n in range(N):
        results[n] = {
            'converged':False
        }
    
    # For each sample
    epochs = trange(N, mininterval=1)
    for n in epochs:
        zn = z[n,:].reshape(1,-1)
        zn_0 = z0[n,:].reshape(1,-1)
        global_min = maxT
    
        # For each alpha
        for a in alphas:
            max_num_steps = 3
            converged = False
            
            while not converged and max_num_steps<=global_min:
                if flow_type == 'gnf':
                    z_prime, _ = model.inverse_fixed_point(zn_0, maxT=max_num_steps, alpha=a)
                elif flow_type == 'grf':
                    z_prime, _ = model.inverse(zn_0, maxT=max_num_steps, alpha=a)
                err = torch.norm(zn-z_prime).item()
               
                if err < 1e-4:
                    # If already converged previously
                    if results[n]['converged']:
                        if max_num_steps < results[n]['num_steps']:
                            results[n]['alpha'] = a
                            results[n]['num_steps'] = max_num_steps
                    else:
                        results[n]['converged'] = True
                        results[n]['alpha'] = a
                        results[n]['num_steps'] = max_num_steps
                    if max_num_steps < global_min:
                        global_min = max_num_steps
                    converged = True
                
                max_num_steps += 1
    
    # Save results
    with open('./experiment_logs/'+save_to+'/convergence_results.json', 'w') as fp:
        json.dump(results, fp,  indent=4)


def convergence_test_discrete_sequential(model, z, double=True, device='cpu', bs_x_max=20, bs_x_min=20, bs_range=20):
    def invert(model, z):
        for step in reversed(model.steps):
            z = invert_step(step, model.bn, z)
        z = z*model.scale + model.shift
        return z

    def invert_step(step, bn, z):
        x = torch.zeros_like(z)
        for i in range(bn.get_num_vertices()):
            h = step.conditioner(x)
            x_prev = x
            if isinstance(step.normalizer, AffineNormalizer):
                x,_ = step.normalizer.inverse(z, h)
            elif isinstance(step.normalizer, MonotonicNormalizer):
                x = inverse_transform_monotonic(step.normalizer, z, h)
            if torch.norm(x - x_prev) == 0.:
                break
        return x

    def inverse_transform_monotonic(normalizer, z, h):
        x_max = torch.ones_like(z) * bs_x_max
        x_min = -torch.ones_like(z) * bs_x_min
        z_max, _ = normalizer.forward(x_max, h)
        z_min, _ = normalizer.forward(x_min, h)
        for i in range(bs_range):
            x_middle = (x_max + x_min) / 2
            z_middle, _ = normalizer.forward(x_middle, h)
            left = (z_middle > z).float()
            right = 1 - left
            x_max = left * x_middle + right * x_max
            x_min = right * x_middle + left * x_min
            z_max = left * z_middle + right * z_max
            z_min = right * z_middle + left * z_min
        return (x_max + x_min) / 2

    timer = Timer()
    model = set_model(model, double, device)
    z = z.to(device)
    if double:
        z = z.double()
    z0, _ = model(z)

    timer.start()
    z_prime = invert(model,z0)
    elapsed_time = timer.stop()

    re = np.array(torch.norm(z_prime - z, dim=1).detach().cpu())
    print('Converged: {}%'.format(np.sum(re <= 1e-4)))
    print('Avg reconstruction error: {}'.format(np.mean(re)))
    print('Max reconstruction error: {}'.format(np.max(re)))
    print('Inversion Time {:.3f} ms ({:.3f} ms per step)'.format(elapsed_time, elapsed_time/len(model.steps)))


def convergence_test_continuous(model, z):
    model = set_model(model, True, 'cpu')
    z = z.double().to('cpu')
    torch.manual_seed(0)
    integration_time = torch.tensor([0.0,1.0], dtype=torch.float64).to(z.device)
    z0, _ = model(z, integration_time)

    z_prime, _ = model.inverse(z0, integration_time)
    re = np.array(torch.norm(z_prime - z, dim=1).detach().cpu())
    print('Converged: {}%'.format(np.sum(re <= 1e-4)))
    print('Avg reconstruction error: {}'.format(np.mean(re)))
    print('Max reconstruction error: {}'.format(np.max(re)))



def save_convergence_test_z(z, path):
    torch.save(z, './experiment_logs/'+path+'/convergence_test_z.pt')   

def load_convergence_test_results(path):
    with open('./experiment_logs/'+path+'/convergence_results.json', 'r') as f:
        return json.load(f)


def load_convergence_test_z(path):
    return torch.load('./experiment_logs/'+path+'/convergence_test_z.pt', map_location=torch.device('cpu'))


def process_convergence_test_results(path):
    results = load_convergence_test_results(path)
    num_converged = 0
    converged_num_steps = []
    converged_alpha = []
    for n in range(100):
        if results[n]['converged'] and results[n]['num_steps'] <= 50: 
            num_converged += 1
            converged_num_steps.append(results[n]['num_steps'])
            converged_alpha.append(results[n]['alpha'])
    print('Converged: {}%'.format(num_converged))
    print('Min/max num iterations required: {}/{}'.format(np.min(converged_num_steps), np.max(converged_num_steps)))
    print('Min/max alpha required: {}/{}'.format(np.min(converged_alpha), np.max(converged_alpha)))



def plot_alpha_vs_iter_vs_recon_err(model, z, alpha, maxT, ylim, skip_size=1,
         flow_type='gnf', save_to=None, device='cpu', double=False, title=None):
    model = set_model(model, double, device)
    z = z.to(device)

    plt.style.use('seaborn')
    plt.rc('axes', labelsize=18)
    plt.rc('legend', fontsize=18)
    plt.rcParams.update({'font.size': 18})
    fig, ax = plt.subplots(1,1, figsize=(10,5))
    legend =  alpha

    z0, _ = model(z)

    iters = range(1,maxT,skip_size)
    for a in alpha:
        errs = []
        for max_iter in range(1,maxT,skip_size):
            if flow_type == 'gnf':
                z_prime, _ = model.inverse_fixed_point(z0, maxT=max_iter, 
                            alpha=a)
            elif flow_type == 'grf':
                z_prime, _ = model.inverse(z0, maxT=max_iter, alpha=a)
            errs.append(torch.norm(z-z_prime).item())
        for i in range(1,len(errs)):
            if isnan(errs[i]) or isinf(errs[i]):
                errs[i] = errs[i-1]
        errs = np.clip(errs, a_min=1e-4, a_max=np.max(errs))
        
        ax.plot(iters, errs)
    ax.legend(legend, title=r'$\alpha$', loc='lower right')
    ax.set_xlabel('Number of iterations per flow step')
    ax.set_ylabel('Reconstruction error')
    ax.set_yscale('log')
    ax.set_ylim(ylim)
    xint = []
    for each in range(1,maxT+1, max(2, maxT//10)):
        xint.append(int(each))
    if xint[-1] != maxT: xint.append(maxT)
    ax.set_xticks(xint)
    ax.tick_params(axis='x', labelsize=18)
    ax.tick_params(axis='y', labelsize=18)

    if title is not None:
        ax.set_title(title, fontdict={'fontsize': 18})

    if save_to is not None:
        plt.savefig('./experiment_logs/figures/inversion/'+save_to,
                    dpi=600, bbox_inches='tight', facecolor='white') 
    plt.show()


def inversion_time(experiments, z, device):
    # experiments = [
    #    {
    #     'name':,
    #     'path':,
    #     'alpha':,
    #     'maxT':
    #    }
    # ]
    path = './experiment_logs/'
    hline = '+-------+---------------------------------'
    print(hline)
    print('| Model | Inverse pass ')
    print(hline)
    timer = Timer()

    with torch.no_grad():
        for ex in experiments:
            line = '| {:<5} |'.format(ex['name'])
            p = path + ex['path']

            model = torch.load(p+'/model.pt', map_location=torch.device(device)).to(device)
            model = set_model(model, True, device)
            model.eval()

            if isinstance(model, ContinuousNF):
                num_flow_steps = len(model.odefunc.diffeq.steps)
                z0, _ = model(z, torch.tensor([0.0, 1.0]).to(device))
                timer.start()
                model.inverse(z0,torch.tensor([0.0, 1.0]))
                elapsed_time = timer.stop()
            
            elif isinstance(model, DiscreteNF):
                num_flow_steps = len(model.steps)
                z0, _ = model(z)
                timer.start()
                model.inverse_fixed_point(z0, maxT=ex['maxT'], alpha=ex['alpha'])
                elapsed_time = timer.stop()

            else:
                num_flow_steps = len(model.blocks)
                z0, _ = model(z)
                timer.start()
                model.inverse(z0, maxT=ex['maxT'], alpha=ex['alpha'])
                elapsed_time = timer.stop()
                
            print(line + ' {:.3f} ms ({:.3f} ms per step) '.format(elapsed_time, elapsed_time/num_flow_steps))
            print(hline)


def banach_vs_newton_inversion(experiments, x, save_to=None, N=200, n=10):
    # Invert using banach
    def inverse_banach(block, z, maxT):
        # Compute y st f(y|x) = z
        #   y0 = z
        #   y_{t+1} = z - g(y_i)      
        y = z.clone().detach()
        for _ in range(maxT):
            g, _ = block.g(y)
            y = z - g

        return y

    def invert_flow(model, z, maxT):
        with torch.no_grad():
            for block in reversed(model.blocks):
                z = inverse_banach(block, z, maxT)
        return z

    def adjust_lightness(color, amount=0.5):
        try:
            c = mc.cnames[color]
        except:
            c = color
        c = colorsys.rgb_to_hls(*mc.to_rgb(c))
        return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])

    plt.rcParams.update({'font.size': 20, 'figure.figsize': (10,5)})
    palette = ['indianred', adjust_lightness('b', 0.4), adjust_lightness('indianred', 1.2), adjust_lightness('b', 1.2), adjust_lightness('indianred', 1.6), adjust_lightness('b', 1.6)]
    iters = range(0,N,n)

    x = x.double()
    path = './experiment_logs/residual_nf_density_estimation/'
    i = 0
    legend = []
    for ex in experiments:
        model = set_model(torch.load(path+ex['path']+'model.pt', map_location=torch.device('cpu')), double=True, device='cpu')
        legend.extend(['Newton (c={:.2f})'.format(ex['c']), 'Banach (c={:.2f})'.format(ex['c'])])
        x0, _ = model(x)

        errs = []
        for max_iter in range(0,N,n):
            x_prime, _ = model.inverse(x0, maxT=max_iter, alpha=1.0)
            err = torch.mean(torch.norm(x-x_prime, dim=0)).item()
            errs.append(max(1e-5,err))
        plt.plot(iters, errs, c=palette[i])
        i += 1

        errs = []
        for max_iter in range(0,N,n):
            x_prime = invert_flow(model, x0, maxT=max_iter)
            err = torch.mean(torch.norm(x-x_prime, dim=0)).item()
            errs.append(max(1e-5,err))
        plt.plot(iters, errs, c=palette[i])
        i += 1

    plt.legend(legend)
    plt.xlabel('Number of iterations performed per block')
    plt.ylabel('Reconstruction error')
    plt.yscale('log')
    xint= []
    for each in range(0,N+1,3*n):
        xint.append(int(each))
    plt.xticks(xint)
    if save_to is not None:
        plt.savefig('./experiment_logs/figures/inversion/'+save_to,
                    dpi=600, bbox_inches='tight', facecolor='white') 
    plt.show()

def convergence_test_banach(model, z, device, double=True, maxT=50):
    # Invert using banach
    def inverse_banach(block, z, maxT):
        # Compute y st f(y|x) = z
        #   y0 = z
        #   y_{t+1} = z - g(y_i)      
        y = z.clone().detach()
        for _ in range(maxT):
            g, _ = block.g(y)
            y = z - g

        return y

    def invert_flow(model, z, maxT):
        with torch.no_grad():
            for block in reversed(model.blocks):
                z = inverse_banach(block, z, maxT)
        return z

    # BANACH
    timer = Timer()
    model = set_model(model, double, device)
    z = z.to(device)
    if double:
        z = z.double()
    z0, _ = model(z)

    timer.start()
    z_prime = invert_flow(model, z0, maxT=maxT)
    elapsed_time = timer.stop()
    re = np.array(torch.norm(z_prime - z, dim=1).detach().cpu())
    print('-- Banach --')
    print('Converged: {}%'.format(np.sum(re <= 1e-4)))
    print('Avg reconstruction error: {}'.format(np.mean(re)))
    print('Inversion Time {:.3f} ms ({:.3f} ms per step)'.format(elapsed_time, elapsed_time/len(model.blocks)))


def plot_depth(experiments, _config):
    # experiments = {
    #   'flow': [
    #       (depth1, path1),
    #       (depth2, path2), ...
    #      ],
    #   ...
    # }

    plt.style.use('seaborn')

    plt.rc('axes', labelsize=24)
    plt.rc('legend', fontsize=24)
    plt.rcParams.update({'font.size': 24})

    fig, ax = plt.subplots(1,1, figsize=(10,5))

    path = './experiment_logs/'
    ylabel = _config['ylabel']
    xlabel = _config['xlabel']
    
    colours =  ['b',  'g', 'darkorange', 'r']

    all_depths = set()
    legend = []
    for i, activation in enumerate(experiments):
        legend.append(activation)
        depths = []
        losses = []

        for ex in experiments[activation]:
            depths.append(ex[0])

            ex_path = path + ex[1]
            loss = []
            for run in os.listdir(ex_path):
                if not run.isdigit(): continue
                ex_run_path = ex_path + '/{}/'.format(run)
                with open(ex_run_path+'metrics.json') as f:
                    metrics = json.load(f)
                loss.append(metrics['test']['values'][-1])
            losses.append(np.mean(loss))
        all_depths.update(depths)

        ax.plot(depths, losses, c=colours[i], marker='o')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(list(all_depths))
    ax.legend(legend)

    ax.tick_params(axis='x', labelsize=18)
    ax.tick_params(axis='y', labelsize=18)

    if 'save_to' in _config:
        plt.savefig('./experiment_logs/figures/flow_comparisons/'+_config['save_to'], dpi=600, bbox_inches='tight', facecolor='white')

    plt.show()
