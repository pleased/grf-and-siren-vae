import torch


class WeightConstraint():

    def __call__(self, module, inputs):
        setattr(module.layers[-1], '_weight', self.constrain_weight(module))


    def constrain_weight(self, module):
        # If there are K layers, let:   V = W_0 x ... x W_{k-1}
        # Then replace W_k with:
        #   W_k = W_K x sign(W_k) x sign(V^T)
        V = module.layers[0]._weight.data[:,:module.out_size]
        W = module.layers[-1]._weight
        W = W.mul(torch.sign(W)).mul(torch.sign(V.t()))
        return W


    @staticmethod
    def apply(module):
        wc = WeightConstraint()
        module.register_forward_pre_hook(wc)

        lk = module.layers[-1]
        W = lk._parameters['_weight']
        delattr(lk, '_weight')
        setattr(lk, '_weight', W.data)

        return wc


def weight_constraint(module):
    print('Applying constraint to weights of residual block.')
    WeightConstraint.apply(module)
    return module