from seaborn.palettes import dark_palette
import torch.nn as nn
import torch

from math import ceil, log
from warnings import warn

from normalizing_flow.discrete.resflow.residual_blocks.residual_block import *
from normalizing_flow.discrete.resflow.residual_blocks.spectral_norm import spectral_norm


class MaskedLayer(nn.Module):

    def __init__(self, in_size, out_size, mask):
        super(MaskedLayer, self).__init__()
        self.mask = mask
        self.layer = nn.Linear(in_size, out_size)
        self._weight = self.layer.weight
        self._bias = self.layer.bias

    
    def forward(self, x):
        W = self._weight
        x = torch.addmm(self._bias, x, W.transpose(0,1))
        return x


class MaskedSpecNormMLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, masks, activation_func, 
                    coeff, n_power_iterations, activation_func_prime):
        super(MaskedSpecNormMLP, self).__init__()
        self.masks = masks
        self.num_hidden = len(hidden)
        self.coeff = coeff
        self.n_power_iterations = n_power_iterations
        self.out_size = out_size

        in_size = in_size
        l1 = [in_size] + hidden
        l2 = hidden + [out_size]

        layers = []
        for m, l in enumerate(zip(l1, l2)):
            layers.append(self._spectral_norm(MaskedLayer(l[0], l[1], masks[m]).to(masks[0].device)))
        self.layers = nn.ModuleList(layers)
        self.activation_func = activation_func
        self.activation_func_prime = activation_func_prime

    
    def forward(self, x):
        layer_output = []

        for l in range(len(self.layers)-1):
            x = self.layers[l](x)
            layer_output.append(x)
            x = self.activation_func(x)
          
        x = self.layers[-1](x)
        return x, layer_output

    
    def _spectral_norm(self, layer):
        return spectral_norm(layer, self.coeff, self.n_power_iterations)

    
    def lipschitz(self):
        # Lip(g) = prod_i Lip(W_i @ x + b_i)
        #   where Lip(W_i @ x + b_i) = ||W_i||
        # NOTE: Lip(activation function) = 1
        L = 1
        for l in self.layers:
            L *= l.weight_sigma.item()
        return L


    def largest_singular_values(self):
        sigmas = []
        for l in self.layers:
            W = l._weight
            _, S, _ = torch.svd(W, compute_uv=False)
            sigmas.append(S[0].item())
        return sigmas


class MemoryEfficientLogJacobianDet(torch.autograd.Function):

    @staticmethod
    def forward(ctx, g_nn, eps, training, compute_jacdet, *g_params):
        ctx.training = training
        with torch.enable_grad():
            eps = eps.detach().requires_grad_(True)
            g, layer_outputs = g_nn(eps)
            ctx.g = g
            ctx.eps = eps
            logjacdet = torch.log(torch.abs(
                    compute_jacdet(layer_outputs))).sum(1) 
            
            if training:
                grad_eps, *grad_params = torch.autograd.grad(
                    logjacdet.sum(), (eps,) + g_params, retain_graph=True,
                    allow_unused=True
                )
                if grad_eps is None:
                    grad_eps = torch.zeros_like(eps)
                ctx.save_for_backward(grad_eps, *g_params, *grad_params)
        
        return safe_detach(g), safe_detach(logjacdet)


    @staticmethod
    def backward(ctx, grad_g, grad_logdetjac):
        training = ctx.training
        if not training:
            raise ValueError('Provide training=True if using backward.')

        with torch.enable_grad():
            grad_eps, *params_and_grad = ctx.saved_tensors
            g, eps = ctx.g, ctx.eps

            # Precomputed gradients.
            g_params = params_and_grad[:len(params_and_grad) // 2]
            grad_params = params_and_grad[len(params_and_grad) // 2:]
            dg_eps, *dg_params = torch.autograd.grad(g, [eps] + g_params, grad_g, allow_unused=True) 

        # Update based on gradient from logdetgrad.
        dL = grad_logdetjac[0].detach()
        with torch.no_grad():
            grad_eps.mul_(dL)
            grad_params = tuple([g.mul_(dL) if g is not None else None for g in grad_params])
        
        # Update based on gradient from g.
        with torch.no_grad():
            grad_eps.add_(dg_eps)
            grad_params = tuple([dg.add_(djac) if djac is not None else dg for dg, djac in zip(dg_params, grad_params)])

        #return (None, None, grad_eps, None, None, None, None) + grad_params
        return (None, grad_eps, None, None) + grad_params



def safe_detach(tensor):
        return tensor.detach().requires_grad_(tensor.requires_grad)


class GraphicalLipschitzResidualBlock(ResidualBlock):
    """ f(z) = z + g(z|x)
            where the weight matrices of g(z|x) are normalized st Lip(g) < 1.
    """

    def __init__(self, in_dim, hidden_dims, masks, cond_dim=0, 
            activation_function='elu', coeff=0.97, n_power_iterations=5, grad_in_forward=True):
        super(GraphicalLipschitzResidualBlock, self).__init__()
        self.in_dim = in_dim + cond_dim
        self.out_dim = in_dim

        self.grad_in_forward = grad_in_forward

        if len(hidden_dims) == 0:
            raise Exception("Residual block requires at least one hidden layer")
        self.hidden_dims = hidden_dims

        if activation_function == 'elu': 
            act_func = elu
            self.act_func_prime = elu_prime
        elif activation_function == 'tanh': 
            act_func = tanh
            self.act_func_prime = tanh_prime
        elif activation_function == 'sigmoid': 
            act_func = sigmoid
            self.act_func_prime = sigmoid_prime
        elif activation_function == 'linear':
            act_func = linear
            self.act_func_prime = linear_prime
        elif activation_function == 'lipswish':
            act_func = LipSwish()
            self.act_func_prime = act_func.prime
        elif activation_function == 'softplus':
            act_func = softplus
            self.act_func_prime = softplus_prime
        elif activation_function == 'lipmish':
            act_func = LipMish()
            self.act_func_prime = act_func.prime
        else: raise Exception("Activation function unknown: {}".format(activation_function))

        self.g = MaskedSpecNormMLP(self.in_dim, hidden_dims, self.out_dim, 
                    masks, act_func, coeff, n_power_iterations, self.act_func_prime)


    def forward(self, eps, x=None, sum_factors=True):
        if x is not None: inputs = torch.cat((eps, x), dim=1)
        else: inputs = eps

        if self.grad_in_forward and self.training:
            g, logdetjac = self._mem_efficient_forward(inputs, 
                    sum_factors=sum_factors)
        else:
            g, logdetjac = self._forward(inputs, sum_factors=sum_factors)
            
        return eps + g, logdetjac


    def _forward(self, eps, sum_factors=True):
        g, layer_outputs = self.g(eps)
        
        detjac = torch.abs(self._jacobian_det(layer_outputs))
        
        logdetjac = torch.log(detjac)
        if sum_factors: logdetjac = logdetjac.sum(1)  
        return g, logdetjac


    def _mem_efficient_forward(self, eps, sum_factors=True):
        g, logdetjac = MemoryEfficientLogJacobianDet.apply(
            self.g, eps, self.training, self._jacobian_det,
            *list(self.g.parameters())
        )
        return g, logdetjac


    def _jacobian_det(self, layer_outputs):
        L = self.g.num_hidden
        N = layer_outputs[0].shape[0]

        W0 = self.g.layers[0]._weight[:,:self.out_dim]
        J = W0.expand(N,-1,-1)
        for l in range(1,L+1):
            W = self.g.layers[l]._weight
            h_prime = self.act_func_prime(layer_outputs[l-1]).unsqueeze(dim=1)
            J = h_prime*J.transpose(1,2)
            J = J.reshape(N*self.out_dim, self.hidden_dims[l-1]).transpose(0,1)
            J = torch.mm(W,J)
            J = torch.stack(J.chunk(N, dim=1), dim=0)       

        # log det J_f = trace(log(J_f))
        diag_J = torch.stack([torch.diag(j) for j in torch.unbind(J, dim=0)], dim=0)

        # J_f(eps|x) = I + J_g(eps|x)
        diag_J = diag_J + 1
        return diag_J


    def inverse(self, z, x, maxT, epsilon, alpha=1.0, sum_factors=True):
        # Compute y st f(y|x) = z
        y = z

        for _ in range(maxT):
            if x is not None: inputs = torch.cat((y, x), dim=1)
            else: inputs = y
            g, layer_outputs = self.g(inputs)
            diag_J = self._jacobian_det(layer_outputs)

            f = y + g
            y = y - alpha*(f - z)/diag_J


        if x is not None: inputs = torch.cat((y, x), dim=1)
        else: inputs = y
        g, layer_outputs = self.g(inputs)
        detjac = torch.abs(self._jacobian_det(layer_outputs))
        j = torch.log(detjac)
        if sum_factors: j = j.sum(1) 
        return y, j


    def num_steps_required_for_inversion_accuracy_of(self, eps, z, x):
        # Banach fixed-point-theorem error bound:
        # ||y - y_n|| <= Lip(g)^n / (1-Lip(g)) * ||y_1 - y_0||
        # for ||y - y_n|| <= eps, require:
        #   n >= (log(eps) + log(1-Lip(g)) - log(||y_1 - y_0||)) / log(Lip(g))  
        y0 = z
        y1 = z - self.g(torch.cat((y0, x), dim=1))[0]
        step1 = torch.norm(y1-y0, dim=1).mean().item()
        L = self.g.lipschitz()

        return ceil((log(eps) + log(1-L) - log(step1))/log(L))


    def _vae_mem_efficient_forward(self):
        # for model loading purposes
        pass

