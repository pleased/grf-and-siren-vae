import torch.nn as nn
import torch

from torch.nn.functional import softplus as sp
from torch.nn.parameter import Parameter


class ResidualBlock(nn.Module):

    def __init__(self):
        super(ResidualBlock, self).__init__()

    def forward(self):
        pass


    def inverse(self):
        pass


    def jacobian_det(self):
        pass

#######################
# Activation functions
#######################
def tanh(x):
    return torch.tanh(x)

def tanh_prime(x):
    # tanh'(x) = sech^2(x) = 1 - tanh^2(x)
    return 1 - (torch.tanh(x))**2


def softplus(x):
    # f(x) = ln(1+e^x)
    return sp(x)

def softplus_prime(x):
    # f'(x) = 1 / (1 + e^(-x))
    return torch.sigmoid(x)


def elu(x):
    n = x < 0
    p = x >= 0
    return (p*x) + (torch.exp((n*x)) - 1)

def elu_prime(x):
    n = x < 0
    p = x >= 0
    return (torch.exp((n*x)))


def sigmoid(x):
    return torch.sigmoid(x)

def sigmoid_prime(x):
    return torch.sigmoid(x)*(1 - torch.sigmoid(x))


def linear(x):
    return x

def linear_prime(x):
    return torch.ones_like(x)


def swish(x, beta=1):
    return x*torch.sigmoid(beta*x)

def swish_prime(x, beta=1):
    return beta*swish(x, beta) + torch.sigmoid(beta*x)*(1 - beta*swish(x, beta))


class Swish(nn.Module):

    def __init__(self, beta=None):
        if beta is None:
            self.beta = Parameter(torch.tensor(1.0))
        else :
            self.beta = beta
    
    def forward(self, x):
        return x*torch.sigmoid(self.beta*x)
    
    def prime(self, x):
        return self.beta*self(x) + \
            torch.sigmoid(self.beta*x)*(1 - self.beta*self(x))


class LipSwish(nn.Module):

    def __init__(self, beta=None):
        super(LipSwish, self).__init__()
        if beta is None:
            self.beta = Parameter(torch.tensor(1.0))
        else :
            self.beta = beta
    
    def forward(self, x):
        b = softplus(self.beta)
        return x*torch.sigmoid(b*x)/1.1

    def prime(self, x):
        b = softplus(self.beta)
        return b*self(x) + torch.sigmoid(b*x)*(1/1.1 - b*self(x))


class LipMish(nn.Module):

    def __init__(self, beta=None):
        super(LipMish, self).__init__()
        if beta is None:
            self.beta = Parameter(torch.tensor(1.0))
        else :
            self.beta = beta
    
    def forward(self, x):
        b = softplus(self.beta)
        return x*torch.tanh(softplus(b*x))/1.088

    def prime(self, x):
        b = softplus(self.beta)
        return (torch.tanh(softplus(b*x)) + \
            b*x*torch.pow(1/torch.cosh(softplus(b*x)),2)*sigmoid(b*x))/1.088



class ELU(nn.Module):
    

    def __init__(self):
        super(ELU, self).__init__()
        self.alpha = Parameter(torch.tensor(1.0))
    
    def forward(self, x):
        # elu(x) = {
        #         z      if z >= 0
        #   alpha(e^x-1) if z < 0
        # }
        n = x < 0
        p = x >= 0
        x = (p*x) + self.alpha*(torch.exp((n*x)) - 1)
        return x 

    def prime(self, x):
        # elu'(x) = {
        #       1       if z >= 0
        #   alpha(e^z)  if z < 0
        # }
        n = x < 0
        p = x >= 0
        return p + (self.alpha*n)*(torch.exp((n*x)))

        