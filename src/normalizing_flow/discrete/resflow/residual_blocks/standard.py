"""
Adapted from:
Invertible Residual Networks http://proceedings.mlr.press/v97/behrmann19a/behrmann19a.pdf
"""

from seaborn.palettes import dark_palette
import torch.nn as nn
import torch
import numpy as np

from math import ceil, log
from warnings import warn

from normalizing_flow.discrete.resflow.residual_blocks.residual_block import *
from normalizing_flow.discrete.resflow.residual_blocks.spectral_norm import spectral_norm


class Layer(nn.Module):

    def __init__(self, in_size, out_size, device):
        super(Layer, self).__init__()
        self.layer = nn.Linear(in_size, out_size)
        self._weight = self.layer.weight
        self._bias = self.layer.bias
        self.mask = torch.ones(self._weight.shape).to(device)

    
    def forward(self, x):
        W = self._weight
        x = torch.addmm(self._bias, x, W.transpose(0,1))
        return x


class SpecNormMLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, activation_func, 
                    coeff, n_power_iterations, activation_func_prime, device):
        super(SpecNormMLP, self).__init__()
        self.num_hidden = len(hidden)
        self.coeff = coeff
        self.n_power_iterations = n_power_iterations
        self.out_size = out_size

        in_size = in_size
        l1 = [in_size] + hidden
        l2 = hidden + [out_size]

        layers = []
        for m, l in enumerate(zip(l1, l2)):
            layers.append(self._spectral_norm(Layer(l[0], l[1], device).to(device)))
        self.layers = nn.ModuleList(layers)
        self.activation_func = activation_func
        self.activation_func_prime = activation_func_prime

    
    def forward(self, x):

        for l in range(len(self.layers)-1):
            x = self.layers[l](x)
            x = self.activation_func(x)
          
        x = self.layers[-1](x)
        return x

    
    def _spectral_norm(self, layer):
        return spectral_norm(layer, self.coeff, self.n_power_iterations)

    
    def lipschitz(self):
        # Lip(g) = prod_i Lip(W_i @ x + b_i)
        #   where Lip(W_i @ x + b_i) = ||W_i||
        # NOTE: Lip(activation function) = 1
        L = 1
        for l in self.layers:
            L *= l.weight_sigma.item()
        return L


    def largest_singular_values(self):
        sigmas = []
        for l in self.layers:
            W = l._weight
            _, S, _ = torch.svd(W, compute_uv=False)
            sigmas.append(S[0].item())
        return sigmas




####################################

class StandardResidualBlock(ResidualBlock):
    """ f(z) = z + g(z|x)
            where the weight matrices of g(z|x) are normalized st Lip(g) < 1.
    """

    def __init__(self, in_dim, hidden_dims, cond_dim=0, 
            activation_function='elu', coeff=0.97, n_power_iterations=5, grad_in_forward=True, device='cpu'):
        super(StandardResidualBlock, self).__init__()
        self.in_dim = in_dim + cond_dim
        self.out_dim = in_dim

        self.grad_in_forward = True

        if len(hidden_dims) == 0:
            raise Exception("Residual block requires at least one hidden layer")
        self.hidden_dims = hidden_dims

        if activation_function == 'elu': 
            act_func = elu
            self.act_func_prime = elu_prime
        elif activation_function == 'tanh': 
            act_func = tanh
            self.act_func_prime = tanh_prime
        elif activation_function == 'sigmoid': 
            act_func = sigmoid
            self.act_func_prime = sigmoid_prime
        elif activation_function == 'linear':
            act_func = linear
            self.act_func_prime = linear_prime
        elif activation_function == 'lipswish':
            act_func = LipSwish()
            self.act_func_prime = act_func.prime
        elif activation_function == 'softplus':
            act_func = softplus
            self.act_func_prime = softplus_prime
        elif activation_function == 'lipmish':
            act_func = LipMish()
            self.act_func_prime = act_func.prime
        else: raise Exception("Activation function unknown: {}".format(activation_function))

        self.lamb = nn.Parameter(torch.tensor(2.0))
        self.n_samples = 1
        self.n_exact_terms = 2
        self.geom_p = nn.Parameter(torch.tensor(np.log(0.5) - np.log(1. - 0.5)))

        # store the samples of n.
        self.register_buffer('last_n_samples', torch.zeros(self.n_samples))
        self.register_buffer('last_firmom', torch.zeros(1))
        self.register_buffer('last_secmom', torch.zeros(1))

        self.g = SpecNormMLP(self.in_dim, hidden_dims, self.out_dim, 
                    act_func, coeff, n_power_iterations, self.act_func_prime, device)


    def forward(self, eps, x=None):
        if x is not None: inputs = torch.cat((eps, x), dim=1)
        else: inputs = eps

        g, logdetjac = self._logdetgrad(inputs)
            
        return eps + g, logdetjac


    def _logdetgrad(self, x):
        """Returns g(x) and logdet|d(x+g(x))/dx|."""

        with torch.enable_grad():
            # if (not self.training):
            #     ###########################################
            #     # Brute-force compute Jacobian determinant.
            #     ###########################################
            #     x = x.requires_grad_(True)
            #     g = self.g(x) 
            #     jac = batch_jacobian(g, x) 
            #     batch_dets = (jac[:, 0, 0] + 1) * (jac[:, 1, 1] + 1) - jac[:, 0, 1] * jac[:, 1, 0]
            #     return g, torch.log(torch.abs(batch_dets)).view(-1, 1)

            geom_p = torch.sigmoid(self.geom_p).item()
            sample_fn = lambda m: geometric_sample(geom_p, m) 
            rcdf_fn = lambda k, offset: geometric_1mcdf(geom_p, k, offset) 

            if self.training:
                # Unbiased estimation.
                lamb = self.lamb.item() 
                n_samples = sample_fn(self.n_samples)  
                n_power_series = max(n_samples) + self.n_exact_terms
                coeff_fn = lambda k: 1 / rcdf_fn(k, self.n_exact_terms) * \
                    sum(n_samples >= k - self.n_exact_terms) / len(n_samples)
            
            else:
                # Unbiased estimation with more exact terms.
                lamb = self.lamb.item()
                n_samples = sample_fn(self.n_samples)
                n_power_series = max(n_samples) + 20
                coeff_fn = lambda k: 1 / rcdf_fn(k, 20) * \
                    sum(n_samples >= k - 20) / len(n_samples)

            # if not self.exact_trace:
            #     ####################################
            #     # Power series with trace estimator.
            #     ####################################
            vareps = torch.randn_like(x)

            # Choose the type of estimator.
            # if self.training and self.neumann_grad:
            #     estimator_fn = neumann_logdet_estimator
            # else:
            estimator_fn = basic_logdet_estimator

            # Do backprop-in-forward to save memory.
            if self.training and self.grad_in_forward:
                g, logdetgrad = mem_eff_wrapper( 
                    estimator_fn, self.g, x, n_power_series, vareps, coeff_fn, self.training
                )
            else:
                x = x.requires_grad_(True)
                g = self.g(x)
                logdetgrad = estimator_fn(g, x, n_power_series, vareps, coeff_fn, self.training)
            # else:
            ############################################
            # Power series with exact trace computation.
            # ############################################
            # x = x.requires_grad_(True)
            # g = self.g(x)  # <--------------------------------------
            # jac = batch_jacobian(g, x)
            # logdetgrad = batch_trace(jac)
            # jac_k = jac
            # for k in range(2, n_power_series + 1):
            #     jac_k = torch.bmm(jac, jac_k)
            #     logdetgrad = logdetgrad + (-1)**(k + 1) / k * coeff_fn(k) * batch_trace(jac_k)

            if self.training:
                self.last_n_samples.copy_(torch.tensor(n_samples).to(self.last_n_samples)) # <--------------------------------------
                estimator = logdetgrad.detach()
                self.last_firmom.copy_(torch.mean(estimator).to(self.last_firmom)) # <--------------------------------------
                self.last_secmom.copy_(torch.mean(estimator**2).to(self.last_secmom)) # <--------------------------------------

            return g, logdetgrad.view(-1, 1)
        

def batch_jacobian(g, x):
    jac = []
    for d in range(g.shape[1]):
        jac.append(torch.autograd.grad(torch.sum(g[:, d]), x, create_graph=True)[0].view(x.shape[0], 1, x.shape[1]))
    return torch.cat(jac, 1)


def batch_trace(M):
    return M.view(M.shape[0], -1)[:, ::M.shape[1] + 1].sum(1)


def basic_logdet_estimator(g, x, n_power_series, vareps, coeff_fn, training):
    vjp = vareps
    logdetgrad = torch.tensor(0.).to(x)
    for k in range(1, n_power_series + 1):
        vjp = torch.autograd.grad(g, x, vjp, create_graph=training, retain_graph=True)[0]
        tr = torch.sum(vjp.view(x.shape[0], -1) * vareps.view(x.shape[0], -1), 1)
        delta = (-1)**(k + 1) / k * coeff_fn(k) * tr
        logdetgrad = logdetgrad + delta
    return logdetgrad


#####################
# Logdet Estimators
#####################
class MemoryEfficientLogDetEstimator(torch.autograd.Function):

    @staticmethod
    def forward(ctx, estimator_fn, gnet, x, n_power_series, vareps, coeff_fn, training, *g_params):
        ctx.training = training
        with torch.enable_grad():
            x = x.detach().requires_grad_(True)
            g = gnet(x)
            ctx.g = g
            ctx.x = x
            logdetgrad = estimator_fn(g, x, n_power_series, vareps, coeff_fn, training)

            if training:
                grad_x, *grad_params = torch.autograd.grad(
                    logdetgrad.sum(), (x,) + g_params, retain_graph=True, allow_unused=True
                )
                if grad_x is None:
                    grad_x = torch.zeros_like(x)
                ctx.save_for_backward(grad_x, *g_params, *grad_params)

        return safe_detach(g), safe_detach(logdetgrad)

    @staticmethod
    def backward(ctx, grad_g, grad_logdetgrad):
        training = ctx.training
        if not training:
            raise ValueError('Provide training=True if using backward.')

        with torch.enable_grad():
            grad_x, *params_and_grad = ctx.saved_tensors
            g, x = ctx.g, ctx.x

            # Precomputed gradients.
            g_params = params_and_grad[:len(params_and_grad) // 2]
            grad_params = params_and_grad[len(params_and_grad) // 2:]

            dg_x, *dg_params = torch.autograd.grad(g, [x] + g_params, grad_g, allow_unused=True)

        # Update based on gradient from logdetgrad.
        dL = grad_logdetgrad[0].detach()
        with torch.no_grad():
            grad_x.mul_(dL)
            grad_params = tuple([g.mul_(dL) if g is not None else None for g in grad_params])

        # Update based on gradient from g.
        with torch.no_grad():
            grad_x.add_(dg_x)
            grad_params = tuple([dg.add_(djac) if djac is not None else dg for dg, djac in zip(dg_params, grad_params)])

        return (None, None, grad_x, None, None, None, None) + grad_params



def mem_eff_wrapper(estimator_fn, gnet, x, n_power_series, vareps, coeff_fn, training):

    # We need this in order to access the variables inside this module,
    # since we have no other way of getting variables along the execution path.
    if not isinstance(gnet, nn.Module):
        raise ValueError('g is required to be an instance of nn.Module.')

    return MemoryEfficientLogDetEstimator.apply(
        estimator_fn, gnet, x, n_power_series, vareps, coeff_fn, training, *list(gnet.parameters())
    )



def safe_detach(tensor):
    return tensor.detach().requires_grad_(tensor.requires_grad)


# -------- Helper distribution functions --------
# These take python ints or floats, not PyTorch tensors.


def geometric_sample(p, n_samples):
    return np.random.geometric(p, n_samples)


def geometric_1mcdf(p, k, offset):
    if k <= offset:
        return 1.
    else:
        k = k - offset
    """P(n >= k)"""
    return (1 - p)**max(k - 1, 0)

