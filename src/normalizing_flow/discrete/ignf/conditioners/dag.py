# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch
import torch.nn as nn

from normalizing_flow.discrete.ignf.conditioners import Conditioner


class DAGMLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, cond_in=0):
        super(DAGMLP, self).__init__()
        in_size = in_size
        l1 = [in_size + cond_in] + hidden
        l2 = hidden + [out_size]
        layers = []
        for h1, h2 in zip(l1, l2):
            layers += [nn.Linear(h1, h2), nn.ReLU()]
        layers.pop()
        self.net = nn.Sequential(*layers)

    def forward(self, x):
        return self.net(x)


class DAGConditioner(Conditioner):
    """
    Implements the graphical conditioning function:
        c_i(x) = h_i(x*A_i, context)
    """
    
    def __init__(self, in_size, hidden, out_size, A, cond_in=0, hot_encoding=False):
        super(DAGConditioner, self).__init__()
        
        self.A = A.fill_diagonal_(0.0)
        self.in_size = in_size

        in_net = in_size*2 #if hot_encoding else in_size

        # Can either use custom embedding network (eg CNN)
        if issubclass(type(hidden), nn.Module):
            self.embedding_net = hidden
        # or use standard NN with hidden layers of size hidden, where
        # hidden [hidden_size1, hidden_size2, ...]
        else:
            self.embedding_net = DAGMLP(in_net, hidden, out_size, cond_in)

        self.hot_encoding = hot_encoding


    def forward(self, eps, context):
        batch_size = eps.shape[0]

        # Reshape eps [batch_size, in_size] -> [batch_size, num_latent, num_latent]
        eps_reshape = eps.unsqueeze(1).expand(-1, self.in_size, -1)

        # Reshape A [in_size, in_size] -> [batch_size, num_latent, n]
        A = self.A[:eps.shape[1], :]
        A_reshape = A.unsqueeze(0).expand(batch_size, -1, -1)
        
        # concat context -> [batch_size, num_latent, n]
        if context is not None:
            context_reshape = context.unsqueeze(1).expand(-1, self.in_size, -1)
            eps_reshape = torch.cat((eps_reshape, context_reshape), dim=2)

        # mask out variables that z_i is independet of
        # e -> [batch_size * num_latent, n]
        e = (eps_reshape * A_reshape).view(batch_size*self.in_size, -1)

        hot_encoding = torch.eye(self.in_size, device=self.A.device).unsqueeze(0).expand(batch_size, -1, -1)\
            .contiguous().view(-1, self.in_size)
        e  = torch.cat((e, hot_encoding), 1)

        # reshape output [batch_size*num_latent, output_size] -> [batch_size, input_size, output_size]
        e = self.embedding_net(e).view(batch_size, self.in_size, -1)

        return e


