# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch
import torch.nn as nn

from normalizing_flow.discrete.ignf.conditioners import Conditioner


class CouplingMLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, 
                    cond_in):
        super(CouplingMLP, self).__init__()
        l1 = [in_size + cond_in] + hidden
        l2 = hidden + [out_size]
        layers = []
        for h1, h2 in zip(l1, l2):
            layers += [nn.Linear(h1, h2), nn.Tanh()]
        layers.pop()
        self.net = nn.Sequential(*layers)

    def forward(self, x):
        return self.net(x)


class CouplingConditioner(Conditioner):
    '''
    c_i(z) = 0          if i <= k
           = h_i(z_i:k) if i > k
    '''

    def __init__(self, in_size, hidden, out_size, 
                    cond_in=0, reverse=False):
        super(CouplingConditioner, self).__init__()
        self.in_size = in_size
        self.out_size = out_size

        self.block1 = in_size//2
        self.block2 = in_size - self.block1

        self.reverse = reverse
        if not self.reverse:
            # block1 remains constant
            # block 2 depends on block 1
            self.embedding_net = CouplingMLP(self.block1, hidden, out_size*self.block2, cond_in)
            self.constants = torch.zeros((self.block1, out_size))

        else:
            # block 2 remains constant
            # block 1 depends on block 2
            self.embedding_net = CouplingMLP(self.block2, hidden, out_size*self.block1, cond_in)
            self.constants = torch.zeros((self.block2, out_size))
            

    
    def forward(self, eps, context=None):        
        if not self.reverse:
            h_block1 = self.constants.unsqueeze(0).expand(eps.shape[0], self.block1, self.out_size)
            if context is not None: input = torch.cat((eps[:, :self.block1], context), dim=1)
            else: input = eps[:, :self.block1]
            h_block2 = self.embedding_net(input).view(eps.shape[0], self.block2, self.out_size)

        else:
            if context is not None: input = torch.cat((eps[:, self.block1:], context), dim=1)
            else: input = eps[:, self.block1:]
            h_block1 = self.embedding_net(input).view(eps.shape[0], self.block1, self.out_size)
            h_block2 = self.constants.unsqueeze(0).expand(eps.shape[0], self.block2, self.out_size)
            
        return torch.cat((h_block1, h_block2), dim=1)