import torch
import torch.nn as nn

from normalizing_flow.discrete.ignf.conditioners import Conditioner


class MaksedLayer(nn.Module):

    def __init__(self, in_size, out_size, mask):
        super(MaksedLayer, self).__init__()
        self.mask = mask
        self.layer = nn.Linear(in_size, out_size)
        self._weight = self.layer.weight
        self._bias = self.layer.bias

    
    def forward(self, x, relu=True):
        w = torch.mul(self.mask, self._weight)
        x = torch.addmm(self._bias, x, w.transpose(0,1))
        if relu:
            x = torch.relu(x)
        return x


class MaskedMLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, masks, cond_in):
        super(MaskedMLP, self).__init__()
        self.masks = masks
        in_size = in_size
        l1 = [in_size + cond_in] + hidden
        l2 = hidden + [out_size]
        
        layers = []
        for m, l in enumerate(zip(l1, l2)):
            layers.append(MaksedLayer(l[0], l[1], masks[m]))
        self.layers = nn.ModuleList(layers)


    def forward(self, x):
        for l in range(len(self.layers)-1):
            x = self.layers[l](x)
        x = self.layers[-1](x, relu=False)
        return x


class AutoregressiveConditioner(Conditioner):

    def __init__(self, in_size, hidden, out_size, masks, cond_in=0, reverse=False):
        super(AutoregressiveConditioner, self).__init__()
        self.masks = masks
        self.in_size = in_size
        self.num_out_chunks = out_size
        self.embedding_net = MaskedMLP(in_size, hidden, out_size*in_size, masks, cond_in)
        self.reverse = reverse


    def forward(self, eps, context=None):
        if self.reverse:
            eps = reverse_ordering(eps)

        if context is not None: input = torch.cat((eps, context), dim=1)
        else: input = eps
        h = self.embedding_net(input)

        # change h shape: [batch_size, num_out_chunks*num_latent]
        #             -> [batch_size, num_latent, num_out_chunks]
        # and undo reversion if necessary
        if self.reverse:
            return torch.stack([reverse_ordering(x) for x in torch.chunk(h, self.num_out_chunks, dim=1)], dim=2)
        else:
            return torch.stack(torch.chunk(h, self.num_out_chunks, dim=1), dim=2)

    
def permute(eps, P=None):
    d = eps.shape[1]

    if P is None:
        P = torch.zeros((d,d))
        perm = torch.randperm(d)
        for i in range(d):
            P[i, perm[i]] = 1.0

        eps = torch.matmul(eps, P)
        return eps, P

    else:
        return torch.matmul(eps, P)


def reverse_ordering(eps):
    return torch.flip(eps, [1,0])
        