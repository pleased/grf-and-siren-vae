# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch.nn as nn

class Conditioner(nn.Module):
    def __init__(self):
        super(Conditioner, self).__init__()
        self.is_invertible = True


    def forward(self, eps, context=None):
        """Compute the conditioning factors to be used in the normalizer. 
        
        For example, if the affine normalizer is used, conditioning_factors 
        (cf) is a [batch_size, d, 2] tensor s.t. 
            z_bi = exp(cf_bi_0)*x_bi + cf_bi_1
            where b = 1,...,batch_size
                  i = 1,...,d

        Parameters
        ----------
        eps : tensor
            A [batch_size, d] tensor of the current value of the latent variables 
        context : tensor, optional
            A [batch_size, c] tensor, additional context (embedding of the observed variables x) 

        Returns
        -------
        conditioning_factors : tensor
            A [batch_size, d, h] tensor where h is the size of the embedding
        """

        pass