# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch
import torch.nn as nn

from torch.nn.functional import relu

from normalizing_flow.discrete.ignf.conditioners import Conditioner
from modules import MaskedMLP


class DAGMaskedConditioner(Conditioner):
    """
    Implements the graphical conditioning function:
        c_i(x) = h_i(x*A_i, context)
    """
    
    def __init__(self, in_size, hidden, out_size, masks, 
                cond_in=0):
        super(DAGMaskedConditioner, self).__init__()
        self.masks = masks
        self.in_size = in_size
        self.num_out_chunks = out_size
        self.embedding_net = MaskedMLP(in_size+cond_in, hidden, out_size*in_size, masks, relu)


    def forward(self, eps, context=None):
        if context is not None: input = torch.cat((eps, context), dim=1)
        else: input = eps
        h = self.embedding_net(input)
        # change h shape: [batch_size, num_out_chunks*num_latent] -> [batch_size, num_latent, num_out_chunks]
        h = torch.stack(torch.chunk(h, self.num_out_chunks, dim=1), dim=2)
        return h

    