# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch
from normalizing_flow.discrete.ignf.normalizers.normalizer import Normalizer


class AffineNormalizer(Normalizer):
    """
    Implements the affine transformation, g : R x R^2 -> R
        g(x; m, s) = x*exp(s) + m
    """

    def __init__(self):
        super(AffineNormalizer, self).__init__()


    def forward(self, eps, h):
        mu, sigma = h[:, :, 0], torch.exp(h[:, :, 1].clamp_(-2., 2.))
        eps = eps*sigma + mu
        return eps, sigma


    def inverse(self, eps, h):
        mu, sigma = h[:, :, 0], torch.exp(h[:, :, 1].clamp_(-2., 2.))
        eps = (eps - mu)/sigma
        return eps, sigma


    @staticmethod
    def num_params():
        return 2
