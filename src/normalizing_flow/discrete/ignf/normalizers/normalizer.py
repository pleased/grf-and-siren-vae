# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch.nn as nn


class Normalizer(nn.Module):
    def __init__(self):
        super(Normalizer, self).__init__()


    def forward(self, eps, h):
        """
        Parameters
        ----------
        eps : tensor
            A [batch_size, d] tensor of variables to transform
        h : tensor
            A [batch_size, d, h] tensor of embeddings

        
        Returns
        -------
        z : tensor
            A [batch_size, d] tensor representing x transformed by a one-to-one mapping conditioned on h.
        jac : tensor
            A [batch-size, d] tensor, the diagonal terms of the Jacobian.
        """

        pass

    @staticmethod
    def num_params():
        pass