# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

from typing import final
import torch
import torch.nn as nn

from UMNN import NeuralIntegral, ParallelNeuralIntegral

from normalizing_flow.discrete.ignf.normalizers import Normalizer


class ELUPlus(nn.Module):
    def __init__(self):
        super(ELUPlus, self).__init__()
        self.elu = nn.ELU()

    def forward(self, x):
        return self.elu(x) + 1.05


class SigmoidPlus(nn.Module):
    def __init__(self, coeff=1.5):
        super(SigmoidPlus, self).__init__()
        self.sigmoid = nn.Sigmoid()
        self.coeff = coeff

    def forward(self, x):
        return self.coeff*self.sigmoid(x) + 0.05


class TanhPlus(nn.Module):
    def __init__(self):
        super(TanhPlus, self).__init__()
        self.tanh = nn.Tanh()

    def forward(self, x):
        return self.tanh(x) + 1.05


class IntegrandNet(nn.Module):
    
    def __init__(self, hidden, cond_in, final_activation):
        super(IntegrandNet, self).__init__()
        l1 = [cond_in + 1] + hidden
        l2 = hidden + [1]
        layers = []
        for h1, h2 in zip(l1, l2):
            layers += [nn.Linear(h1, h2), nn.ReLU()]
        layers.pop()
        if final_activation == 'sigmoid':
            layers.append(SigmoidPlus())
        elif final_activation == 'tanh':
            layers.append(TanhPlus())
        elif final_activation == 'elu':
            layers.append(ELUPlus())
        else:
            raise Exception('Activation function not available: {}'.format(final_activation))
        
        self.net = nn.Sequential(*layers)

    
    def forward(self, x, h):
        batch_size, in_dim = x.shape
        x = torch.cat((x, h), dim=1)
        x_he = x.view(batch_size, -1, in_dim).transpose(1, 2).contiguous().view(batch_size*in_dim, -1)
        y = self.net(x_he).view(batch_size, -1)
        return y


class MonotonicNormalizer(Normalizer):
    """Implements the monotonic normalizer:

    There exist multiple methods to parameterize monotonic normalizers,
    but in this work we rely on Unconstrained Monotonic Neural Networks
    which can be expressed as

        g(x;c) = integrate from 0 to x f(t,c) dt + b(c)

        where c is embedding made by the conditioner and
        f : R^{|c|+1} -> R+, b : R^D -> R are two neural networks

    NFs built with autoregressive conditioners and monotonic 
    normalizers are universal density approximators of continuous
    random variables.
    """

    def __init__(self, int_net_hidden, cond_size, num_steps=20,
                    solver='CC', int_net_final_activation='sigmoid'):
        super(MonotonicNormalizer, self).__init__()
        
        self.integrand_net = IntegrandNet(int_net_hidden, cond_size, int_net_final_activation)
        self.solver = solver
        self.num_steps = num_steps

    
    def forward(self, eps, h):
        eps0 = torch.zeros(eps.shape).to(eps.device)
        epsT = eps

        b = h[:,:,0]
        h = h.permute(0,2,1).contiguous().view(eps.shape[0], -1)

        if self.solver == "CC":
            eps2 = NeuralIntegral.apply(
                eps0, epsT, self.integrand_net, 
                _flatten(self.integrand_net.parameters()),
                h, self.num_steps) 
            eps2 = eps2 + b

        elif self.solver == "CCParallel":
            eps2 = ParallelNeuralIntegral.apply(
                eps0, epsT, self.integrand_net,
                _flatten(self.integrand_net.parameters()),
                h, self.num_steps)
            eps2 = eps2 + b
        else:
            return None

        j = self.integrand_net(eps, h)
        return eps2, j


    @staticmethod
    def num_params():
        pass


def _flatten(sequence):
    flat = [p.contiguous().view(-1) for p in sequence]
    return torch.cat(flat) if len(flat) > 0 else torch.tensor([])
