# Adapted from: https://github.com/AWehenkel/Graphical-Normalizing-Flows

import torch
import torch.nn as nn
from .conditioners import Conditioner, DAGMaskedConditioner, CouplingConditioner, DAGConditioner
from .normalizers import Normalizer, AffineNormalizer


class NormalizingFlow(nn.Module):
    
    def __init__(self):
        super(NormalizingFlow, self).__init__()
    
    
    def forward(self, eps, context=None):
        """Returns transformed eps and log-determinant of the Jacobian of 
        the transformation"""
        pass

        
    def get_conditioner(self):
        """Get list of conditioners"""
        pass


    def get_normalizer(self):
        """Get list of normalizers"""
        pass


class NormalizingFlowStep(NormalizingFlow):

    def __init__(self, conditioner: Conditioner, normalizer: Normalizer):
        super(NormalizingFlowStep, self).__init__()
        self.conditioner = conditioner
        self.normalizer = normalizer


    def forward(self, eps, x):
        h = self.conditioner(eps, x)
        eps, j = self.normalizer(eps, h)
        return eps, torch.log(j).sum(1)


    def inverse(self, eps, x=None, order=None):
        if not isinstance(self.normalizer, AffineNormalizer):
            raise Exception("Flow inversion not implemented for normalizer type: {}".format(type(self.normalizer)))

        # Sequential sampling to compute correct conditioner params:
        # f: eps_{t,i} = normalizer(eps_{t-1,i} | c(eps_{t-1,pa(i)}))
        #
        # f^{-1}: 
        # eps_{t-1,i} = inv_normalizer(eps_{t,i} | c(eps_{t-1,pa(i)})) 

        # Graphical Conditioner:

        if isinstance(self.conditioner, DAGMaskedConditioner) or isinstance(self.conditioner, DAGConditioner):
            j = torch.zeros(eps.shape)
            eps_tmin1 = torch.zeros(eps.shape, dtype=torch.float64).to(eps.device)
            for i in order:
                h = self.conditioner(eps_tmin1, x)
                eps_tmp, j_tmp = self.normalizer.inverse(eps, h)
                if torch.isnan(eps_tmp).any() or torch.isinf(eps_tmp).any():
                    print('PROBLEM in eps_tmp: ')
                    print(eps_tmp)
                    print(j_tmp)
                    print(h)
                eps_tmin1[:,i] = eps_tmp[:,i]
                j[:,i] = j_tmp[:,i]

            return eps_tmin1, torch.log(j).sum(1) 
        
        # Coupling Conditioner
        elif isinstance(self.conditioner, CouplingConditioner):
            # Forward eps -> z:
            # z_1:d = eps_1:d
            # z_d:D = exp(s(eps_1:d))*eps_d:D + m(eps_1:d)
            #
            # Inverse z -> eps:
            # eps_1:d = z_1:d
            # eps_d:D = (z_d:D - m(eps_1:d))/exp(s(eps_1:d))
            h = self.conditioner(eps, x)
            eps, j = self.normalizer.inverse(eps, h)
            return eps, torch.log(j).sum(1)

    
        else:
            raise Exception("Flow inversion not implemented for conditioner type: {}".format(type(self.conditioner)))

    
    def inverse_fixed_point(self, z, x, maxT, epsilon, alpha=1.0):
        y = z.detach().clone()
        for _ in range(maxT):
            h = self.conditioner(y, x)
            f, diag_J = self.normalizer(y, h)
            y = y - alpha*(f - z)/diag_J

        h = self.conditioner(y, x)
        f, diag_J = self.normalizer(y, h)
        return y, torch.log(diag_J).sum(1)


    def get_conditioner(self):
        return self.conditioner


    def get_normalizer(self):
        return self.normalizer


