# Adapted from: https://github.com/plai-group/daphne

import torch
import torch.nn as nn

from torch.distributions import Normal
from torchdiffeq import odeint_adjoint as odeint
from toolz import last

from modules import StandardNormalLogDensity


class ContinuousNF(nn.Module):

    '''
    Implementation of the structured conditional continuous normalizing
    flow. To be used only in the fixed model setting.
    '''

    def __init__(self, odefunc, bn, cond_shift, cond_scale, z_shift,
            z_scale, solver='dopri5', atol=1e-5, rtol=1e-5, 
            generative=True):
        super(ContinuousNF, self).__init__()

        self.bn = bn
        self.num_latent = bn.get_num_latent()
        self.z0_log_density = StandardNormalLogDensity()
        self.odefunc = odefunc
        self.solver = solver
        self.atol = atol
        self.rtol = rtol

        self.cond_shift = cond_shift
        self.cond_scale = cond_scale
        self.z_shift = z_shift
        self.z_scale = z_scale

        # Generating Flow: z0 -> zT
        # Normalizing Flow: zT -> z0
        self.generative = generative

    
    def forward(self, *args, **kwargs):
        if self.generative:
            return self._forward_gen(*args, **kwargs)
        else:
            return self._forward_norm(*args, **kwargs)

    
    def _forward_gen(self, cond, integration_times, z0=None):
        ''' Generating Flow: z0 -> zT '''
        if cond is not None:
            batch_size = cond.shape[0]
            dev = cond.device
        else:
            batch_size = z0.shape[0]
            dev = z0.device
        
        # Normalize
        if cond is not None:
            cond = (cond - self.cond_shift)/self.cond_scale
        self.odefunc.diffeq.condition = cond

        # sample from reference distribution
        if z0 is None:
            q0 = Normal(0.0, 1.0)  
            z0 = q0.sample((batch_size, self.num_latent)).to(dev)
            if cond.dtype == torch.float64:
                z0 = z0.double()
        log_q0 = torch.zeros((batch_size, 1)).to(dev)

        # Refresh the odefunc statistics
        self.odefunc.before_odeint()

        state_t = odeint(
            self.odefunc,
            (z0, log_q0), 
            integration_times.to(dev),
            atol=self.atol,
            rtol=self.rtol,
            method=self.solver
        )
        z, delta_log_q = map(last, state_t[:2])

        # Denormalize
        z = z*self.z_scale + self.z_shift

        return z0, z, delta_log_q

    
    def _forward_norm(self, z, integration_times, cond=None):
        ''' Normalizing Flow: zT -> z0 '''
        # Normalize
        z_ = (z - self.z_shift)/self.z_scale
        if cond is not None:
            cond = (cond - self.cond_shift)/self.cond_scale
        self.odefunc.diffeq.condition = cond

        zero = torch.zeros((z.shape[0], 1)).to(z)

        # Refresh the odefunc statistics.
        self.odefunc.before_odeint()

        state_t = odeint(
            self.odefunc,
            (z_, zero),
            integration_times.to(z),
            atol=self.atol,
            rtol=self.rtol,
            method=self.solver
        )
        z0, delta_log_p = map(last, state_t[:2])

        return z0, delta_log_p


    def inverse(self, *args, **kwargs):
        if self.generative:
            return self._inverse_gen(*args, **kwargs)
        else:
            return self._inverse_norm(*args, **kwargs)


    def _inverse_gen(self, z, cond, integration_times):
        integration_times = _flip(integration_times, 0)
        z0, delta_log_p = self._forward_norm(z, integration_times, cond=cond)
        return z0, delta_log_p

    
    def _inverse_norm(self, z0, integration_times, cond=None):
        integration_times = _flip(integration_times, 0)
        _, z, delta_log_q = self._forward_gen(cond, integration_times, z0=z0)
        return z, delta_log_q


    def shifted_reverse_kl(self, x, z, z0, delta_log_q, inverted=False):
        # Shifted Reverse KL-divergence: 
        #   E_{z ~ q(.|x)} [ log(q(z|x)) - log(p(x,z)) ]
        #  where
        #   log(p(x,z)) = log(p(x|z)) + log(p(z)) 
        #   log(q(z|x)) = delta_log_q - log|ds/dz z|

        # change of variable induced by denormalizing
        log_zscale = torch.sum(torch.log(self.z_scale)) 
        log_p_x_z = self.bn.log_joint(x, z)

        delta_log_q = delta_log_q.squeeze(1)

        if self.generative:
            if not inverted:
                #log_q_z_given_x = delta_log_q - log_zscale 
                log_q_z_given_x = self.z0_log_density(z0) + delta_log_q - log_zscale
            else:
                log_q_z_given_x = self.z0_log_density(z0) - delta_log_q - log_zscale
            
        else:
            if not inverted:
                # log(q(z|x)) = log(q0(z0)) - delta - log|ds(z)/dz|
                log_q_z_given_x = self.z0_log_density(z0) - delta_log_q - log_zscale
            else:
                #log_q_z_given_x = delta_log_q - log_zscale 
                log_q_z_given_x = self.z0_log_density(z0) + delta_log_q - log_zscale

        shifted_reverse_kl = log_q_z_given_x - log_p_x_z
        return torch.mean(shifted_reverse_kl)


    def ll(self, z0, delta, inverted=False):
        # change of variable induced by denormalizing
        log_zscale = torch.sum(torch.log(self.z_scale))

        if self.generative:
            if not inverted:
                log_q_z_given_x = delta - log_zscale 
            else:
                log_q_z_given_x = self.z0_log_density(z0) - delta - log_zscale
        else:
            if not inverted:
                log_q_z_given_x = self.z0_log_density(z0) - delta - log_zscale
            else:
                log_q_z_given_x = delta - log_zscale 
        
        return torch.mean(log_q_z_given_x)


    def count_parameters(self):
        total = sum(p.numel() for p in self.parameters() if p.requires_grad)
        masked = 0
        for s in self.odefunc.diffeq.steps:
            m = s._weight_mask
            zeros = torch.numel(m) - torch.count_nonzero(m)
            masked += zeros
        return (total - masked).item()


def _flip(x, dim):
    indices = [slice(None)] * x.dim()
    indices[dim] = torch.arange(x.size(dim) - 1, -1, -1, dtype=torch.long, device=x.device)
    return x[tuple(indices)]

    