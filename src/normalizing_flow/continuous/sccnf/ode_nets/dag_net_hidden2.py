# Adapted from: Structured Condiitonal Continuous Normalizing Flows
# for Efficient Amortized Inference at at https://github.com/plai-group/daphne

# Applies MADE-like masks to weight matrices of each hidden layer.

import torch
import torch.nn as nn


class DAGStep(nn.Module):

    def __init__(self, in_dim, out_dim, mask, act_func, device):
        super(DAGStep, self).__init__()
        print('Initializing step:')
        print('In',in_dim,'Out', out_dim)

        self._weight_mask = mask
        self.act_func = torch.tanh

        lin = nn.Linear(in_dim, out_dim)
        self._weights = lin.weight
        self._bias = lin.bias

        self._hyper_bias = nn.Linear(1, out_dim, bias=False)
        self._hyper_gate = nn.Linear(1, out_dim)

        
    def forward(self, t, z):
        w = torch.mul(self._weight_mask, self._weights)
        res = torch.addmm(self._bias, z, w.transpose(0,1))

        # [(W*H)z + b]*hyper_gate(t) + hyper_bias(t)
        n1 = self._hyper_gate(t.view(1, 1))
        n2 =  self._hyper_bias(t.view(1, 1))
        return res * torch.sigmoid(n1) + n2



class DAGNetHidden2(nn.Module):
    """Full deterministic flow-defining NN, f, on latent particles z:
            f(z, t| x) = (h_l(.,t) o ... o h_1(.,t))(concat(z, x))

        Part of the neural ODE system:
            dz/dt = f(z, t| x)
    """

    def __init__(self, data_dim, cond_dim, hidden_dim, 
                 num_flow_steps, masks, device, act_func=torch.tanh):
        super(DAGNetHidden2, self).__init__()
        steps = [
                DAGStep(in_dim=(data_dim + cond_dim), out_dim=hidden_dim, mask=masks[0], act_func=act_func, device=device)
            ] + [
                DAGStep(in_dim=hidden_dim, out_dim=hidden_dim, mask=masks[i], act_func=act_func, device=device) 
                for i in range(1,num_flow_steps-1)
            ] + [
                DAGStep(in_dim=hidden_dim, out_dim=data_dim, mask=masks[-1], act_func=act_func, device=device)
            ]
        self.act_func = act_func
        self.steps = nn.ModuleList(steps)

    
    def forward(self, t, z):
        if self.condition is not None:
            dz = torch.cat([z, self.condition], dim=1)
        else:
            dz = z
        for l, step in enumerate(self.steps):
            # if not last layer, use nonlinearity
            if l < len(self.steps) - 1:
                if l == 0:
                    dz = self.act_func(step(t, dz))
                else:
                    dz = self.act_func(step(t, dz)) + dz

            else:
                dz = step(t, dz)
        
        return dz