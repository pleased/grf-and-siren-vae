# Adapted from: Structured Condiitonal Continuous Normalizing Flows
# for Efficient Amortized Inference at at https://github.com/plai-group/daphne

import torch
from torch._C import dtype
import torch.nn as nn


class LinearDAGStep(nn.Module):
    """Single step of the flow transformation:
            z_{l+1} = h_l(z_{l}, t)
        where
            h_l(z_l, t) = tanh[(W*H)z_l * n1(t) + b*n2(t)]
            W - Weight matrix of step l
            H - Adjacency matrix
            b - bias term
            n1 - time-dependent linear gating function
            n2 - time-dependent linear gating function
    """

    def __init__(self, in_dim, out_dim, adjacency, device):
        super(LinearDAGStep, self).__init__()

        self.in_dim = in_dim
        self.out_dim = out_dim

        _weight_mask = adjacency[:out_dim,:in_dim]
        self._weight_mask = _weight_mask.to(device)

        lin = nn.Linear(in_dim, out_dim)
        self._weights = lin.weight
        self._bias = lin.bias

        self._hyper_bias = nn.Linear(1, out_dim, bias=False)
        self._hyper_gate = nn.Linear(1, out_dim)

        
    def forward(self, t, z):
        w = torch.mul(self._weight_mask, self._weights)
        res = torch.addmm(self._bias, z, w.transpose(0,1))

        # [(W*H)z + b]*hyper_gate(t) + hyper_bias(t)
        n1 = self._hyper_gate(t.view(1, 1))
        n2 =  self._hyper_bias(t.view(1, 1))
        return res * torch.sigmoid(n1) + n2
        


class DAGNet(nn.Module):
    """Full deterministic flow-defining NN, f, on particles z:
            f(z, t| x) = (h_l(.,t) o ... o h_1(.,t))(concat(z, x))

        Part of the neural ODE system:
            dz/dt = f(z, t| x)
    """

    def __init__(self, data_dim, cond_dim, adjacency, device, 
                 num_flow_steps):
        super(DAGNet, self).__init__()
        print('data dim = ', data_dim)
        steps = [
                LinearDAGStep(data_dim + cond_dim, data_dim, 
                adjacency, device)
            ] + [
                LinearDAGStep(data_dim, data_dim, adjacency, device) 
                for _ in range(num_flow_steps-1)
            ]

        act_functions = [nn.Tanh()]*(num_flow_steps)
        self.steps = nn.ModuleList(steps)
        self.act_functions = nn.ModuleList(act_functions[:-1])

    
    def forward(self, t, z):
        if self.condition is not None:
            dz = torch.cat([z, self.condition], dim=1)
        else:
            dz = z

        for l, step in enumerate(self.steps):
            # if not last layer, use nonlinearity
            if l < len(self.steps) - 1:
                activation = step(t, dz)
                if l == 0:
                    dz = self.act_functions[l](activation)
                else:
                    dz = self.act_functions[l](activation) + dz
            else:
                dz = step(t, dz)

        return dz
