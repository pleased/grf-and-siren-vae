# Adapted from: https://github.com/plai-group/daphne

import torch
import torch.nn as nn


def divergence_bf(dz, z):
    sum_diag = 0.
    for i in range(z.shape[1]):
        sum_diag += torch.autograd.grad(
            dz[:, i].sum(), z, 
            create_graph=True)[0].contiguous()[:, i].contiguous()
    return sum_diag.contiguous()


def divergence_approx(dz, z, e=None):
    e_dzdx = torch.autograd.grad(dz, z, e, create_graph=True)[0]
    e_dzdx_e = e_dzdx * e
    approx_tr_dzdx = e_dzdx_e.view(z.shape[0], -1).sum(dim=1)
    return approx_tr_dzdx


def sample_gaussian_like(z):
    return torch.randn_like(z)


class ODEfunc(nn.Module):

    def __init__(self, diffeq):
        super(ODEfunc, self).__init__()
        self.diffeq = diffeq
        self.divergence_fn = divergence_approx

    def before_odeint(self, e=None):
        self._e = e

    
    def forward(self, t, states):
        z = states[0]

        # convert to tensor
        t = torch.clone(t).type_as(z).detach()
        batchsize = z.shape[0]

        # Sample and fix the noise.
        if self._e is None:
            self._e = sample_gaussian_like(z)

        with torch.set_grad_enabled(True):
            z.requires_grad_(True)
            t.requires_grad_(True)
            
            dz = self.diffeq(t, z)
            divergence = self.divergence_fn(dz, z, e=self._e).view(batchsize, 1)

        return tuple([dz, -divergence])