from graph.belief_network import GaussianBinaryTree

from sacred import Ingredient


bt_ingredient = Ingredient('bt_config')


@bt_ingredient.config
def cfg():
    depth = 5
    seed = 4


@bt_ingredient.capture
def create_tree(depth, _rnd):
    coeffs = GaussianBinaryTree.rand_coeffs(_rnd, depth)
    bt = GaussianBinaryTree(depth, coeffs)
    return bt