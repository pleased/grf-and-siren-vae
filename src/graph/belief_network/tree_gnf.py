import torch
import sys
import numpy as np
import sklearn

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import BeliefNetwork

class Tree(BeliefNetwork):

    def __init__(self):
        super(Tree, self).__init__()

        # graph
        self.forward_graph = ForwardGraph().initialize(*Tree._construct_graph())
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = 1


    def sample(self, batch_size, train=True):
        rng = np.random.RandomState()

        # Circles
        z0z1 = sklearn.datasets.make_circles(n_samples=batch_size, factor=.5, noise=0.08)[0]
        z0z1 *= 3
        z0z1 = z0z1/torch.tensor([1.6944685, 1.6935346])

        # 8 Gaussians
        z2z3 = self.sample_8gaussians(rng, batch_size)
        z2z3 = z2z3/torch.tensor([2.0310535, 2.0305095])

        # x4~ N(max(X0, X1),1)
        z4 = []
        for i in range(batch_size):
            z4.append(np.random.normal(loc=max(z0z1[i,0],z0z1[i,1]),scale=1.0))
        z4 = np.array([z4]).T

        # x5 ~ N(min(X2, X3),1)
        z5 = []
        for i in range(batch_size):
            z5.append(np.random.normal(loc=min(z2z3[i,0],z2z3[i,1]),scale=1.0))
        z5 = np.array([z5]).T

        # x6 ~ 0.5*N(sin(X4+X5),1) + 0.5*N(cos(X4+X5),1).
        x0 = 0.5*np.random.normal(loc=np.sin(z4+z5),scale=np.ones_like(z5)) + \
             0.5*np.random.normal(loc=np.cos(z4+z5),scale=np.ones_like(z5))

        data = np.concatenate([z0z1, z2z3, z4, z5, x0], axis=1)
        data = torch.tensor(data)

        return data


    def sample_8gaussians(self, rng, batch_size):
        scale = 4.
        centers = [(1,0), (-1,0), (0,1), (0,-1), (1./np.sqrt(2), 1./np.sqrt(2)),
            (1./np.sqrt(2), -1./np.sqrt(2)), (-1./np.sqrt(2), 1./np.sqrt(2)), 
            (-1./np.sqrt(2), -1./np.sqrt(2))]
        centers = [(scale*x, scale*y) for x, y in centers]
        data= []
        for i in range(batch_size):
            point = rng.randn(2)*0.5
            idx = rng.randint(8)
            center = centers[idx]
            point[0] += center[0]
            point[1] += center[1]
            data.append(point)
        data = np.array(data)
        data /= 1.414
        return data


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for EightPairs BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for EightPairs BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for EightPairs BN")


    def get_num_latent(self):
        return 6


    def get_num_obs(self):
        return 1


    def get_num_vertices(self):
        return self.get_num_latent() + self.get_num_obs()


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['z0','z1', 'z2','z3', 'z4','z5', 'x0']
        edges = [('z1','z0'), ('z2','z1'),('z3','z2'), ('z0','z4'), ('z1','z4'),
                 ('z2','z5'), ('z3','z5'), ('z4','x0'), 
                 ('z5','x0')]  
        observed = {'z0','z1', 'z2','z3', 'z4','z5', 'x0'}
        return vertices, edges, observed