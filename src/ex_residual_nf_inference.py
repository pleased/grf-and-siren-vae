"""
Approximate the posterior using a (graphical) residual flow.
"""

import torch
import numpy as np
import json

from sacred import Experiment
from sacred.observers import FileStorageObserver
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_residual_flow
from ex_binary_tree_config import bt_ingredient, create_tree
from graph.belief_network import *
from data.load_data import load_dataset


torch.set_printoptions(edgeitems=50)


ex = Experiment('residual_nf', ingredients=[bt_ingredient])
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"


def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler, device):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt', map_location=torch.device(device))
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network 
    bn = 'arithmetic-circuit2'
    # structure: [faithful, fully-connected, random]
    structure = 'faithful'

    # Params for flow with Lipschitz constraint
    coeff = 0.99
    n_power_iterations = 5

    num_blocks = 1
    hidden_dims = [100]
    # Activation [elu, alpha-elu, tanh, sigmoid, lipswish]
    activation = 'lipmish'
    grad_in_forward = True

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_epochs = 100
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size

    lr = 1e-2
    seed = 0
    patience = 20
    double = True

    run = 1 

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False

    # Add observer
    ex_name = 'residual_nf'
    sub_folder = '{}_{}_{}_{}_{}'.format(structure, str(coeff).split('.')[1], activation, num_blocks, hidden_dims)

    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _run):
    # Training info
    batch_size = _config['batch_size']
    lr = _config['lr'] 
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    checkpoint_n = _config['checkpoint_n']

    print('[CONFIG]',_config)
    print('[DEVICE]', device)

    # BN initialization
    if _config['bn'] == 'arithmetic-circuit':
        bn = ArithmeticCircuit()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'tree':
        bn = Tree(device, double=_config['double'])
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'arth150-synth':
        bn = Arth150()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'binary-tree':
        bn = create_tree()
        ex.info['tree_coeffs'] = bn.coeffs.tolist()
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))

    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # Fix structure
    if _config['structure'] == 'faithful':
        print('[ENCODED STRUCTURE] Using faithful BN structure.')

    elif _config['structure'] == 'fully-connected':
        print('[ENCODED STRUCTURE] Updating BN structure to be fully-connected.')
        bn = FCBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn)

    elif _config['structure'] == 'random':
        print('[ENCODED STRUCTURE] Randomizing the faithful BN structure.')
        num_edges = bn.get_num_edges()
        bn = RandomBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn,
            num_edges=num_edges, seed=_config['seed'])
    
    else:
        raise Exception("Unknown structure: {}".format(_config['structure']))


    scale = {
        'scale': torch.tensor([1.0]).to(device),
        'shift': torch.tensor([0.0]).to(device)
    }

    # Get static batch for visualization over learning
    if bn == 'binary-tree':
        static_tree = bn.sample(batch_size=5)
        # Log static batches
        ex.info['static_batches'] = static_tree.tolist()
        static_samples = [np.tile(static_tree[i,:], (batch_size,1)) for i in range(5)]
        # log-likelihoods of samples from the inference network evaluated on the
        # analytical posterior, given the 5 static samples - saved as metric
        ll_p_metric_names = ["ll_p_1", "ll_p_2", "ll_p_3", "ll_p_4", "ll_p_5"]

    # Initialize model and optimizer
    coeff = _config['coeff']
    n_power_iterations = _config['n_power_iterations']
    hidden_dims = _config['hidden_dims']
    num_blocks = _config['num_blocks']
    activation = _config['activation']
    grad_in_forward = _config['grad_in_forward']
    model = build_residual_flow(
        'graphical-lipschitz', num_blocks, bn, hidden_dims, activation, coeff,
        n_power_iterations, device, grad_in_forward, scale=scale).double().to(device)
    if _config['double']:
        model = model.double() 

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True,
             min_lr=1e-6, patience=_config['patience'])

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler, device)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)

    # Log model capacity
    ex.info['num model params'] = model.count_parameters()

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_shifted_kl = 0.0
        if bn == 'binary-tree':
            train_true_kl = 0.0

        model.train()
        for train_batch in trainloader:
            x = train_batch[:,num_latent:]
            eps0, z, j = model(cond=x)
            loss = model.shifted_reverse_kl(x, z, eps0, j)
            l = loss.detach().item()
            train_shifted_kl += l    

            if bn == 'binary-tree':
                with torch.no_grad(): 
                    p = bn.log_posterior(torch.cat((z,x), dim=1)) 
                    q = model.ll(eps0, j)
                    train_true_kl += (q - p).item() 

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # log training metrics - training loss, kl-divergence
        _run.log_scalar("train", value=train_shifted_kl/num_train_batches) 
        if bn == 'binary-tree':
            _run.log_scalar("train.true", value=train_true_kl/num_train_batches) 
        if device == 'cuda:0':
            _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)

        # Log val metrics - val loss, kl-divergence
        model.eval()
        with torch.no_grad():
            val_shifted_kl = 0.0
            for val_batch in valloader:
                x = val_batch[:,num_latent:]
                eps0, z, j = model(cond=x)
                val_shifted_kl += model.shifted_reverse_kl(x, z, eps0, j).item()
            _run.log_scalar("val", val_shifted_kl/num_val_batches)

            if bn == 'binary-tree':
                p = bn.log_posterior(torch.cat((z,x), dim=1))
                q = model.ll(eps0, j)
                _run.log_scalar("val.true", (q - p).item())

        # Decay learning rate every 100 epochs
        lr_scheduler.step(train_shifted_kl/num_train_batches)

        # Calculate log-likelihood of samples from the inference
        # network evaluated on the analytical posterior
            
        if bn == 'binary-tree':
            ll_p = [[],[],[],[],[]]
            for i in range(5):
                with torch.no_grad():
                    static_sample = torch.DoubleTensor(static_samples[i]).to(device)
                    x = static_sample[:,num_latent:]
                    _, z, _ = model(cond=x)
                    ll = bn.log_posterior(torch.cat((z, x), dim=1))
                    # log training metrics - log-likelihood
                    ll_p[i] = -ll.item()/num_latent
                    _run.log_scalar(ll_p_metric_names[i], ll_p[i])

            epochs.set_description('val: {}, train: shifted {}, true {}, post [{}, {}, {}, {}, {}]'.format(
                val_shifted_kl, 
                train_shifted_kl/num_train_batches, 
                train_true_kl/num_train_batches, 
                ll_p[0], ll_p[1], ll_p[2], ll_p[3], ll_p[4]))
        else:
            epochs.set_description('Train: {:.3f}, Val: {:.3f}'.format(
            train_shifted_kl/num_train_batches, val_shifted_kl/num_val_batches),
            refresh=False)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1
    
    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))
                    
    # Log test metrics - val loss, kl-divergence
    model.eval()
    with torch.no_grad():
        test_shifted_kl = 0

        num_test_batches = 0
        for test_batch in testloader:
            x = test_batch[:,num_latent:]
            eps0, z, j = model(cond=x)
            test_shifted_kl += model.shifted_reverse_kl(x, z, eps0, j).item()
            num_test_batches += 1
        # log test metrics 
        _run.log_scalar("test", value=test_shifted_kl/num_test_batches) 