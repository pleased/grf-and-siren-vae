import torch
import os
import numpy as np
import matplotlib.pyplot as plt
import json

from torch.nn.functional import softplus
from sacred import Experiment 

ex = Experiment('lipmish vs lipswish illustrations')

def lipswish(x):
    return x*torch.sigmoid(x)/1.1

def lipswish_first_der(x):
    return lipswish(x) + torch.sigmoid(x)*(1/1.1 - lipswish(x))

def lipswish_second_der(x):
    return (2*torch.sigmoid(x)*(1-torch.sigmoid(x)) + \
            x*torch.sigmoid(x)*(1-torch.sigmoid(x))**2 - \
            x*torch.sigmoid(x)**2*(1-torch.sigmoid(x)))/1.1

def lipmish(x, beta=0.54132):
    beta = torch.tensor([beta], dtype=torch.float32)
    return x*np.tanh(softplus(softplus(beta)*x))/1.088

def lipmish_first_der(x, beta=0.54132):
    beta = torch.tensor([beta], dtype=torch.float32)
    b = softplus(beta)
    return (torch.tanh(softplus(b*x)) + \
        b*x*torch.pow(1/torch.cosh(softplus(b*x)),2)*torch.sigmoid(b*x))/1.088

def lipmish_second_der(x, beta=0.54132):
    beta = torch.tensor([beta], dtype=torch.float32)
    b = softplus(beta)
    return (2*b*((1/torch.cosh(softplus(b*x)))**2)*torch.sigmoid(b*x) - \
      2*(b**2)*x*((1/torch.cosh(softplus(b*x)))**2)*torch.tanh(softplus(b*x))* \
      (torch.sigmoid(b*x)**2) + (b**2)*x*((1/torch.cosh(softplus(b*x)))**2)* \
            torch.sigmoid(b*x)*(1-torch.sigmoid(b*x)))/1.088


def mish_first_der(x):
    return torch.tanh(softplus(x)) + \
            x*torch.pow(1/torch.cosh(softplus(x)),2)*torch.sigmoid(x)

def mish_second_der(x):
    return 2*((1/torch.cosh(softplus(x)))**2)*torch.sigmoid(x) - \
            2*x*((1/torch.cosh(softplus(x)))**2)*torch.tanh(softplus(x))* \
            (torch.sigmoid(x)**2) + x*((1/torch.cosh(softplus(x)))**2)* \
            torch.sigmoid(x)*(1-torch.sigmoid(x))


def relu_first_der(x):
    return x >= 0

def relu_second_der(x):
    return torch.zeros_like(x)


def elu_first_der(x):
    n = x <= 0
    p = x > 0
    return (torch.exp((n*x)))

def elu_second_der(x):
    n = x <= 0
    p = x > 0
    return (torch.exp((n*x))) - p.long()


def softplus_first_der(x):
    return torch.sigmoid(x)

def softplus_second_der(x):
    return torch.sigmoid(x)*(1 - torch.sigmoid(x))


@ex.command(unobserved=True)
def plot_lipmish_vs_lipswish_derivatives():

    plt.rcParams.update({'font.size': 20})
    fig, ax = plt.subplots(1, figsize=(10,6))
    x = torch.tensor(np.linspace(-6,6,1000))

    ax.plot(x, lipswish_first_der(x), 'b', alpha=0.5, lw=3)
    ax.plot(x, lipmish_first_der(x), 'r', alpha=0.5, lw=3)
    ax.plot(x, lipswish_second_der(x), 'b:', lw=3)
    ax.plot(x, lipmish_second_der(x), 'r:', lw=3)

    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.2, 1.1))
    ax.set_xticks(range(-6,7,2))

    ax.legend([r'LipSwish$^\prime(x)$', r'LipMish$^{\prime}(x)$', r'LipSwish$^{\prime\prime}(x)$', r'LipMish$^{\prime\prime}(x)$'])

    ax.text(2.8, 0.07, r'softplus$(\beta)=1$',
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':5})

    ax.plot(x, torch.ones(1000), linestyle='dashed', c='gray', alpha=0.6,zorder=0)

    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/lipswish-vs-lipmish.png', dpi=600, bbox_inches='tight')
    plt.show()

@ex.command(unobserved=True)
def plot_lipmish_betas():
    plt.rcParams.update({'font.size': 20})
    fig, ax = plt.subplots(1, figsize=(10,6))

    x = torch.tensor(np.linspace(-6,6,1000))

    ax.plot(x, lipmish(x, beta=-1.050225), 'darkred', alpha=0.5, lw=3)
    ax.plot(x, lipmish_first_der(x, beta=-1.050225), 'r', alpha=0.5, lw=3)
    ax.plot(x, lipmish_second_der(x, beta=-1.050225), 'r:', lw=3)
    ax.legend([r'LipMish$(x)$', r'LipMish$^\prime(x)$', r'LipMish$^{\prime\prime}(x)$'])

    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.4, 1.1))
    ax.set_xticks(range(-6,7,2))

    ax.text(2.4, 0.66, r'softplus$(\beta)=0.3$',
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':5})

    ax.plot(x, torch.ones(1000), linestyle='dashed', c='gray', alpha=0.6,zorder=0)

    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/lipmish_betas1.png', dpi=600, bbox_inches='tight')
    plt.show()

    # -----

    fig, ax = plt.subplots(1, figsize=(10,6))

    x = torch.tensor(np.linspace(-6,6,1000))

    ax.plot(x, lipmish(x, beta=9.999954), 'darkred', alpha=0.5, lw=3)
    ax.plot(x, lipmish_first_der(x, beta=9.999954), 'r', alpha=0.5, lw=3)
    ax.plot(x, lipmish_second_der(x, beta=9.999954), 'r:', lw=3)
    ax.legend([r'LipMish$(x)$', r'LipMish$^\prime(x)$', r'LipMish$^{\prime\prime}(x)$'])

    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.4, 1.1))
    ax.set_xticks(range(-6,7,2))

    ax.text(2.5, 0.66, r'softplus$(\beta)=10$',
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':5})

    ax.plot(x, torch.ones(1000), linestyle='dashed', c='gray', alpha=0.6,zorder=0)

    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/lipmish_betas2.png', dpi=600, bbox_inches='tight')
    plt.show()


@ex.command(unobserved=True)
def plot_lipmish_derivatives():
    plt.rcParams.update({'font.size': 20})
    fig, ax = plt.subplots(1, figsize=(10,6))
    x = torch.tensor(np.linspace(-6,6,1000))
    y1 = -0.5*torch.ones(500)
    y2 = 1.5*torch.ones(500)

    ax.plot(x, lipmish(x), 'darkred', alpha=0.5, lw=3)
    ax.plot(x, lipmish_first_der(x), 'r', alpha=0.5, lw=3)
    ax.plot(x, lipmish_second_der(x), 'r:', lw=3)
    ax.legend([r'LipMish$(x)$', r'LipMish$^\prime(x)$', r'LipMish$^{\prime\prime}(x)$'])
    xx = torch.tensor(np.linspace(0.5,2.5,500))
    ax.fill_between(xx, y1, y2, color='grey', alpha=0.2)

    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.4, 1.1))
    ax.set_xticks(range(-6,7,2))


    ax.text(2.8, 0.65, r'softplus$(\beta)=1$',
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':5})

    ax.plot(x, torch.ones(1000), linestyle='dashed', c='gray', alpha=0.6,zorder=0)

    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/lipmish_derivatives.png', dpi=600, bbox_inches='tight')
    plt.show()


@ex.command(unobserved=True)
def plot_mish_derivatives():
    plt.rcParams.update({'font.size': 20})
    fig, ax = plt.subplots(1, figsize=(10,6))
    x = torch.tensor(np.linspace(-6,6,1000))

    ax.plot(x, mish_first_der(x), 'r', alpha=0.5, lw=3)
    ax.plot(x, mish_second_der(x), 'r:', lw=3)
    ax.legend([r'Mish$^\prime(x)$', r'Mish$^{\prime\prime}(x)$'])

    ax.set_yticks([0.5, 1])
    ax.set_xticks(range(-6,7,2))
    ax.set_ylim((-0.2, 1.2))

    plt.axhline(y=1, color='gray', linestyle='dashed', zorder=0)

    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_position('zero')
    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_color('none')

    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/mish_derivatives.png', dpi=600, bbox_inches='tight')
    plt.show()


@ex.command(unobserved=True)
def plot_mish_derivatives2():
    plt.rcParams.update({'font.size': 20})
    fig, ax = plt.subplots(1, figsize=(10,6))
    x = torch.tensor(np.linspace(-6,6,1000))

    ax.plot(x, mish_first_der(x), 'r', alpha=0.5, lw=3)
    ax.plot(x, mish_second_der(x), 'r:', lw=3)
    ax.legend([r'Mish$^\prime(x)$', r'Mish$^{\prime\prime}(x)$'])

    ax.set_yticks([0.5, 1])
    ax.set_xticks(range(-6,7,2))
    ax.set_ylim((-0.2, 1.2))

    # 1. Find point where second derivative = 0; i.e. y st mish"(y) = 0
    # 2. Value that Mish needs to be divided by is = mish'(y)
    plt.vlines(x=1.49057118, ymin=0, ymax=1.088, color='gray', linestyle='dashed')
    ax.plot([1.49057118], [0], 'ro', zorder=3)
    plt.hlines(y=1.088, xmin=0, xmax=1.49057118, color='gray', linestyle='dashed')
    ax.plot([0], [1.088], 'ro', zorder=3)
    ax.text(-1.37, 1.062, '1.088', style='italic', c='gray')
    ax.text(1.32, -0.22, '1.491', style='italic', c='gray', rotation=90)
    ax.text(-6.2, 0.75, r'softplus$(\beta)=1$',
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':5})

    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_position('zero')
    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_color('none')


    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/mish_derivatives_with_root.png', dpi=600, bbox_inches='tight')
    plt.show()


@ex.command(unobserved=True)
def plot_monotonic_derivatives():
    plt.rcParams.update({'font.size': 14})
    x = torch.tensor(np.linspace(-5,5,1000))
    y1 = -0.5*torch.ones(500)
    y2 = 1.5*torch.ones(500)

    # ReLU
    fig, ax = plt.subplots(1,1, figsize=(5,3))
    ax.plot(x, relu_first_der(x), 'r', alpha=0.5, lw=3)
    ax.plot(x, relu_second_der(x), 'r:', lw=3)
    ax.legend([r'ReLU$^\prime(x)$', r'ReLU$^{\prime\prime}(x)$'])
    xx = torch.tensor(np.linspace(0,5,500))
    ax.fill_between(xx, y1, y2, color='grey', alpha=0.2)
    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.1, 1.1))
    ax.set_title(r'ReLU$(x)=\max(0,x)$')
    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/monotonic_derivatives_relu.png', dpi=600, bbox_inches='tight')
    plt.show()

    # ELU
    fig, ax = plt.subplots(1,1, figsize=(5,3))
    ax.plot(x, elu_first_der(x), 'g', alpha=0.5, lw=3)
    ax.plot(x, elu_second_der(x), 'g:', lw=3)
    ax.legend([r'ELU$^\prime(x)$', r'ELU$^{\prime\prime}(x)$'])
    xx = torch.tensor(np.linspace(0,5,500))
    ax.fill_between(xx, y1, y2, color='grey', alpha=0.2)
    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.1, 1.1))
    ax.set_title(r'ELU$(x)=x$ if $x>0$ else $\exp(x)-1$')
    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/monotonic_derivatives_elu.png', dpi=600, bbox_inches='tight')
    plt.show()

    # Softplus
    fig, ax = plt.subplots(1,1, figsize=(5,3))
    ax.plot(x, softplus_first_der(x), 'b', alpha=0.5, lw=3)
    ax.plot(x, softplus_second_der(x), 'b:', lw=3)
    ax.legend([r'Softplus$^\prime(x)$', r'Softplus$^{\prime\prime}(x)$'])
    xx = torch.tensor(np.linspace(4.0,5,500))
    ax.fill_between(xx, y1, y2, color='grey', alpha=0.2)
    ax.set_yticks([0, 0.5, 1])
    ax.set_ylim((-0.1, 1.1))
    ax.set_title(r'Softplus$(x) = \log(1+\exp(x))$')
    plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/monotonic_derivatives_softplus.png', dpi=600, bbox_inches='tight')
    plt.show()

@ex.command(unobserved=True)
def plot_depth(experiments, _config):
    # experiments = {
    #   'activation_function': [
    #       (depth1, path1),
    #       (depth2, path2), ...
    #      ],
    #   ...
    # }

    plt.style.use('seaborn')

    plt.rc('axes', labelsize=24)
    plt.rc('legend', fontsize=24)
    plt.rcParams.update({'font.size': 24})

    fig, ax = plt.subplots(1,1, figsize=(10,5))

    path = './experiment_logs/'
    ylabel = _config['ylabel']
    xlabel = _config['xlabel']
    
    colours =  ['b',  'g', 'darkorange', 'r']

    
    legend = []
    for i, activation in enumerate(experiments):
        legend.append(activation)
        depths = []
        losses = []

        for ex in experiments[activation]:
            depths.append(ex[0])

            ex_path = path + ex[1]
            loss = []
            for run in os.listdir(ex_path):
                if not run.isdigit(): continue
                ex_run_path = ex_path + '/{}/'.format(run)
                with open(ex_run_path+'metrics.json') as f:
                    metrics = json.load(f)
                loss.append(metrics['val']['values'][-1])
            losses.append(np.mean(loss))

        ax.plot(depths, losses, c=colours[i], marker='o')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticks(depths)
    ax.legend(legend)

    ax.tick_params(axis='x', labelsize=18)
    ax.tick_params(axis='y', labelsize=18)

    if 'save_to' in _config:
        plt.savefig('./experiment_logs/figures/lipmish_vs_lipswish/'+_config['save_to'], dpi=600, bbox_inches='tight', facecolor='white')

    plt.show()


@ex.config
def cfg():
    beta= 0.0

@ex.automain
def run(_config, _run):
    pass
