import torch
import json

from sacred import Experiment
from sacred.observers import FileStorageObserver
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_maf_vae
from ex_utils import sample_batch
from graph.belief_network import *
from data.load_data import *

ex = Experiment('maf_vae')
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"

def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler, device):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt', map_location=torch.device(device))
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network [arithmetic-circuit2, ecoli70-alt, arth150-synth]
    bn = 'arithmetic-circuit2'

    num_flow_steps = 3
    flow_hidden_dims = [100]
    decoder_hidden_dims = [100]
    condition_sigma = True

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    num_epochs = 100

    normalize = False
    lr = 1e-2
    patience = 20
    seed = 0

    # For plotting 
    run = 1

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False
    save_checkpoints = False

    # Add observer
    ex_name = 'maf_vae'
    sub_folder = '{}_{}_{}'.format(num_flow_steps, flow_hidden_dims,  decoder_hidden_dims)
    # if iw:
    #     ex_name = 'iw_' + ex_name
    #     sub_folder += '_{}k'.format(k)   

    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _rnd, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr']
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    checkpoint_n = _config['checkpoint_n']

    # BN initialization
    real = False
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    elif _config['bn'] == 'mehra-real':
        bn = Mehra()
        print('[LOADING DATA] Mehra')
        trainloader, valloader, _ = load_mehra()

        if _config['num_train_samples'] < len(trainloader.dataset):
            X = trainloader.dataset[:_config['num_train_samples']]
            trainloader = torch.utils.data.DataLoader(X, batch_size=_config['batch_size'])

        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))
    num_latent = bn.get_num_latent()

    # Normalization
    if _config['normalize']:
        min_std = 1e-5
        batch = sample_batch(bn.sample, 10000)
        x = batch[:,num_latent:]
        z = batch[:,:num_latent]
        x_std = torch.std(x, dim=0).to(device)
        x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
        x_shift = torch.mean(x, dim=0).to(device)
        z_std = torch.std(z, dim=0).to(device)
        z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
        z_shift = torch.mean(z, dim=0).to(device)
    else:
        x_scale = 1.0
        z_scale = 1.0
        x_shift = 0.0
        z_shift = 0.0

    if not real:
        train = bn.sample(_config['num_train_samples'])
        val = bn.sample(_config['num_val_samples'])
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)

    # Initialize model and optimizer
    model = build_maf_vae(bn, _config['decoder_hidden_dims'], 
                _config['num_flow_steps'], _config['flow_hidden_dims'], condition_sigma=_config['condition_sigma'], device=device).double().to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True,
                     min_lr=1e-6, patience=_config['patience'])

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler, device)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_elbo = 0.0

        model.train()
        for train_batch in trainloader:
            x = train_batch[:,num_latent:].double().to(device)
            x = (x - x_shift)/x_scale

            loss = model.loss(x, beta=1.0)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            train_elbo += loss.detach().item()
            
        # log training metrics - kl-divergence
        _run.log_scalar("train", value=train_elbo/num_train_batches) 

        # Log test metrics - kl-divergence
        model.eval()
        with torch.no_grad():
            val_elbo = 0.0
            for val_batch in valloader:
                x = val_batch[:,num_latent:].double().to(device)
                x = (x - x_shift)/x_scale
                val_elbo += model.loss(x).item()
            _run.log_scalar("val", val_elbo/num_val_batches)

        # Print progress
        epochs.set_description('Train -ELBO: {:.3f}, Val -ELBO: {:.3f}'.format(
            train_elbo/num_train_batches, val_elbo/num_val_batches),
            refresh=False)
        # Decay learning rate every 100 epochs
        lr_scheduler.step(train_elbo/num_train_batches)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            if _config['save_checkpoints']:
                torch.save(model, path+'/{}/model_{}.pt'.format(_run._id, t))
            else:
                torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1

    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))