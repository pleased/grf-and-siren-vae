import torch
import numpy as np
import matplotlib.pyplot as plt

from sacred import Experiment 
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau
from torch.utils.data import DataLoader, random_split
from tqdm import trange

from data.load_data import load_2_spirals


ex = Experiment('finite flow illustration')

@ex.config
def cfg():
    # Training info
    batch_size = 100
    num_train_samples = 5000
    num_val_samples = 1000
    num_epochs = 100
    lr = 1e-2
    seed = 0
    patience = 10

    # Add observer
    ex_name = 'finite_flow_illustration'
    path = './experiment_logs/{}'.format(ex_name)
    ex.observers.append(FileStorageObserver(path))

@ex.automain
def run(_config, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr']

    num_train_samples = _config['num_train_samples']
    num_val_samples = _config['num_val_samples']
    X = load_2_spirals(num_train_samples+num_val_samples)
    plt.scatter(X[:,0], X[:,1])
    plt.show()
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    X_train, X_val = random_split(X, [num_train_samples, num_val_samples],
                generator=torch.Generator().manual_seed(_config['seed']))

    
