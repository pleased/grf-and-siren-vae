import torch
import sys
import numpy as np

if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph.invert as invert

from sklearn import datasets

from graph.belief_network import BeliefNetwork

class TwoSpirals(BeliefNetwork):

    def __init__(self):
        super(TwoSpirals, self).__init__()

        # graph
        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph()
            self.inverse_graph = _WrapperGraph(inverse=True)
        else:
            self.forward_graph = ForwardGraph().initialize(*TwoSpirals._construct_graph())
            self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 2 


    def sample(self, batch_size, train=True):
        rng = np.random.RandomState()
        n = np.sqrt(rng.rand(batch_size//2, 1))*540*(2*np.pi)/360
        d1x = -np.cos(n)*n + rng.rand(batch_size//2, 1)*0.5
        d1y = np.sin(n)*n + rng.rand(batch_size//2, 1)*0.5
        spirals = np.vstack((np.hstack((d1x, d1y)), np.hstack((-d1x, -d1y))))/3
        spirals += rng.randn(*spirals.shape)*0.1

        return torch.tensor(spirals) 


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for TwoSpirals BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for TwoSpirals BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for TwoSpirals BN")


    def get_num_latent(self):
        return 0


    def get_num_obs(self):
        return 2


    def get_num_vertices(self):
        return 2


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['x0','x1']
        edges = [('x1','x0')]  
        observed = {'x0','x1'}
        return vertices, edges, observed