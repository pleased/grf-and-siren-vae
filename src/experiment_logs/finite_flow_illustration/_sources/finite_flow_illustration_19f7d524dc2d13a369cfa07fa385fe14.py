import torch
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from sacred import Experiment 
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau
from torch.utils.data import DataLoader, random_split
from tqdm import trange

from data.load_data import load_2_spirals
from graph.belief_network.two_spirals import TwoSpirals
from modules.factory import build_discrete_nf_density_estimation


ex = Experiment('finite flow illustration')

@ex.config
def cfg():
    # Training info
    batch_size = 100
    num_train_samples = 5000
    num_epochs = 100
    lr = 1e-2
    seed = 0
    patience = 10

    # Add observer
    ex_name = 'finite_flow_illustration'
    path = './experiment_logs/{}'.format(ex_name)
    ex.observers.append(FileStorageObserver(path))


@ex.command(unobserved=True)
def plot():
    fig, ax = plt.subplots(1,5, figsize=(20,4), sharex=True, sharey=True)
    X = load_2_spirals(10000).cpu().numpy()
    df = pd.DataFrame({'one':X[:,0],'two':X[:,1]})
    sns.kdeplot(x=df.one, y=df.two, cmap="Blues", shade=True, bw_adjust=.5, ax=ax[0])  
    ax[0].set_title('Data')

    T = [1,2,4,8]
    for t in range(4):
        ax[t+1].set_title(r'$T=${}'.format(T[t]))

    for axi in ax:
        axi.axes.xaxis.set_visible(False)
        axi.axes.yaxis.set_visible(False)

    plt.subplots_adjust(wspace=0.05, hspace=0.05)
    plt.savefig('./experiment_logs/figures/finite_flow_illustration.png')
    plt.show()


@ex.automain
def run(_config, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr']
    bn = TwoSpirals()

    num_train_samples = _config['num_train_samples']
    X = load_2_spirals(num_train_samples)
    num_train_batches = num_train_samples//batch_size
    train_batcher = DataLoader(X, batch_size=batch_size) 

    # For 1 - 5 flow steps
    T = [1,2,4,8]
    for t in T:
        # Build model
        model = build_discrete_nf_density_estimation(
            t, bn, [50], 'dag-masked', 'affine', device
        ).to(device)

        optimizer = torch.optim.Adam(model.parameters(), lr=lr)
        lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True,        
                    min_lr=1e-6, patience=_config['patience'])

        # Train
        epochs = trange(_config['num_epochs'], mininterval=1) 
        for epoch in epochs:
            train_loss = 0.0
            
            for x in train_batcher:
                z0, j = model(x.to(device))
                loss = -model.ll(z0, j)
                train_loss += loss.detach().item()

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            lr_scheduler.step(train_loss/num_train_batches)
            epochs.set_description('Train ELBO: {:.3f}'.format(train_loss/num_train_batches), refresh=False)

        # Save the model
        path = _config['path']
        torch.save(model, path+'/{}/model_{}t.pt'.format(_run._id, t))


        
