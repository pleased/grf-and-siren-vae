import torch
import torchvision
import torchvision.transforms as transforms
import numpy as np


def load_onehot(num_samples):
    torch.manual_seed(0)
    p = torch.tensor([1/3]*3)
    sample = torch.stack(
         [torch.zeros_like(p).scatter_(0, torch.multinomial(p,1), 1.) 
        for _ in range(num_samples)])
    return sample


def load_mnist():
    print('Loading MNIST:')
    flatten = transforms.Compose([transforms.ToTensor(),
                transforms.Lambda(lambda img: img.view(-1))])
    mnist_train_dataset = torchvision.datasets.MNIST(root='./data/datasets/',
                train=True, download=True, transform=flatten)
    mnist_test_dataset = torchvision.datasets.MNIST(root='./data/datasets/', 
                train=False, download=True, transform=flatten)
    print('Data dimension: ', len(mnist_train_dataset[0][0]))
    print('Train dataset size: ', len(mnist_train_dataset))
    print('Test dataset size: ', len(mnist_test_dataset))
    return mnist_train_dataset, mnist_test_dataset


def load_2_spirals(num_samples):
    rng = np.random.RandomState()
    n = np.sqrt(rng.rand(num_samples//2, 1))*540*(2*np.pi)/360
    d1x = -np.cos(n)*n + rng.rand(num_samples//2, 1)*0.5
    d1y = np.sin(n)*n + rng.rand(num_samples//2, 1)*0.5
    spirals = np.vstack((np.hstack((d1x, d1y)), np.hstack((-d1x, -d1y))))/3
    spirals += rng.randn(*spirals.shape)*0.1

    return torch.tensor(spirals) 