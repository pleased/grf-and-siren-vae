INFO - residual_nf - Running command 'run'
INFO - residual_nf - Started run with ID "10"
{'bn': 'protein', 'res_type': 'graphical-lipschitz', 'coeff': 0.99, 'n_power_iterations': 5, 'num_blocks': 9, 'hidden_dims': [100], 'activation': 'lipmish', 'grad_in_forward': True, 'batch_size': 100, 'num_train_samples': 10000, 'num_test_samples': 5000, 'num_steps': 200, 'num_train_batches': 100, 'num_test_batches': 50, 'lr': 0.01, 'lr_decay': 0.1, 'lr_decay_step_size': 20, 'seed': 13552, 'ex_name': 'residual_nf_density_estimation', 'sub_folder': 'protein_graphical-lipschitz_99_lipmish_9_[100]', 'path': './experiment_logs/residual_nf_density_estimation/protein_graphical-lipschitz_99_lipmish_9_[100]', 'bt_config': {'depth': 5, 'seed': 4}}
[  1]: train nll: 1.12025; test nll: -2.36019
[  2]: train nll: -3.32599; test nll: -2.69832
[  3]: train nll: -3.70590; test nll: -3.10541
[  4]: train nll: -3.93827; test nll: -3.29796
[  5]: train nll: -4.03948; test nll: -3.36050
[  6]: train nll: -4.16410; test nll: -3.47061
[  7]: train nll: -4.26245; test nll: -3.61938
[  8]: train nll: -4.34055; test nll: -3.63495
[  9]: train nll: -4.39630; test nll: -3.66836
[ 10]: train nll: -4.46898; test nll: -3.80515
[ 11]: train nll: -4.50738; test nll: -3.85739
[ 12]: train nll: -4.55302; test nll: -3.89053
[ 13]: train nll: -4.59124; test nll: -3.92612
[ 14]: train nll: -4.63216; test nll: -3.96483
[ 15]: train nll: -4.68084; test nll: -3.96997
[ 16]: train nll: -4.69519; test nll: -4.06256
[ 17]: train nll: -4.74000; test nll: -4.05954
[ 18]: train nll: -4.75027; test nll: -4.08590
[ 19]: train nll: -4.76933; test nll: -4.12990
[ 20]: train nll: -4.80567; test nll: -4.15834
[ 21]: train nll: -4.80048; test nll: -4.17686
[ 22]: train nll: -4.82162; test nll: -4.24044
[ 23]: train nll: -4.83531; test nll: -4.27776
[ 24]: train nll: -4.84536; test nll: -4.25749
[ 25]: train nll: -4.86517; test nll: -4.29246
[ 26]: train nll: -4.87477; test nll: -4.28423
[ 27]: train nll: -4.88974; test nll: -4.31891
[ 28]: train nll: -4.89659; test nll: -4.31789
[ 29]: train nll: -4.91181; test nll: -4.33804
[ 30]: train nll: -4.92000; test nll: -4.32121
[ 31]: train nll: -4.93504; test nll: -4.30884
[ 32]: train nll: -4.94206; test nll: -4.33557
[ 33]: train nll: -4.94951; test nll: -4.33748
[ 34]: train nll: -4.95743; test nll: -4.33470
[ 35]: train nll: -4.96043; test nll: -4.36342
[ 36]: train nll: -4.97829; test nll: -4.34394
[ 37]: train nll: -4.98835; test nll: -4.38895
[ 38]: train nll: -4.98302; test nll: -4.36334
[ 39]: train nll: -4.99798; test nll: -4.38804
[ 40]: train nll: -5.00607; test nll: -4.41847
[ 41]: train nll: -5.00658; test nll: -4.38140
[ 42]: train nll: -5.00563; test nll: -4.40551
[ 43]: train nll: -5.02061; test nll: -4.39177
[ 44]: train nll: -5.03637; test nll: -4.40462
[ 45]: train nll: -5.03193; test nll: -4.39867
[ 46]: train nll: -5.04723; test nll: -4.41505
[ 47]: train nll: -5.04543; test nll: -4.41691
[ 48]: train nll: -5.05363; test nll: -4.42660
[ 49]: train nll: -5.06871; test nll: -4.44291
[ 50]: train nll: -5.06971; test nll: -4.43999
[ 51]: train nll: -5.07726; test nll: -4.46739
[ 52]: train nll: -5.08334; test nll: -4.43132
[ 53]: train nll: -5.08677; test nll: -4.44032
[ 54]: train nll: -5.08645; test nll: -4.48943
[ 55]: train nll: -5.10015; test nll: -4.44868
[ 56]: train nll: -5.10610; test nll: -4.45724
[ 57]: train nll: -5.09304; test nll: -4.47889
[ 58]: train nll: -5.11031; test nll: -4.47605
[ 59]: train nll: -5.11511; test nll: -4.49705
[ 60]: train nll: -5.10862; test nll: -4.47566
[ 61]: train nll: -5.11614; test nll: -4.47791
[ 62]: train nll: -5.13320; test nll: -4.51898
[ 63]: train nll: -5.12778; test nll: -4.48438
[ 64]: train nll: -5.12467; test nll: -4.51041
[ 65]: train nll: -5.13639; test nll: -4.49755
[ 66]: train nll: -5.14158; test nll: -4.53794
[ 67]: train nll: -5.14896; test nll: -4.51787
[ 68]: train nll: -5.14253; test nll: -4.51264
[ 69]: train nll: -5.15275; test nll: -4.50569
[ 70]: train nll: -5.14288; test nll: -4.51152
[ 71]: train nll: -5.15212; test nll: -4.52706
[ 72]: train nll: -5.14463; test nll: -4.51858
[ 73]: train nll: -5.16290; test nll: -4.50177
[ 74]: train nll: -5.15421; test nll: -4.53448
[ 75]: train nll: -5.15869; test nll: -4.54105
[ 76]: train nll: -5.17460; test nll: -4.52111
[ 77]: train nll: -5.15810; test nll: -4.53632
[ 78]: train nll: -5.16973; test nll: -4.57052
[ 79]: train nll: -5.17413; test nll: -4.53898
[ 80]: train nll: -5.16911; test nll: -4.54396
[ 81]: train nll: -5.17939; test nll: -4.55188
[ 82]: train nll: -5.18010; test nll: -4.54012
[ 83]: train nll: -5.17011; test nll: -4.51368
[ 84]: train nll: -5.18095; test nll: -4.55678
[ 85]: train nll: -5.18689; test nll: -4.55349
[ 86]: train nll: -5.18310; test nll: -4.55220
[ 87]: train nll: -5.18995; test nll: -4.55333
[ 88]: train nll: -5.18621; test nll: -4.54329
[ 89]: train nll: -5.19466; test nll: -4.58306
[ 90]: train nll: -5.19819; test nll: -4.55839
[ 91]: train nll: -5.20081; test nll: -4.57122
[ 92]: train nll: -5.19797; test nll: -4.56029
[ 93]: train nll: -5.20401; test nll: -4.58760
[ 94]: train nll: -5.20666; test nll: -4.58580
[ 95]: train nll: -5.19710; test nll: -4.56163
[ 96]: train nll: -5.20469; test nll: -4.60620
[ 97]: train nll: -5.21435; test nll: -4.57685
[ 98]: train nll: -5.21668; test nll: -4.60115
[ 99]: train nll: -5.20458; test nll: -4.58686
[100]: train nll: -5.22377; test nll: -4.60474
[101]: train nll: -5.22652; test nll: -4.59705
[102]: train nll: -5.21710; test nll: -4.58615
[103]: train nll: -5.21922; test nll: -4.64279
[104]: train nll: -5.22664; test nll: -4.59841
[105]: train nll: -5.22557; test nll: -4.60340
[106]: train nll: -5.23256; test nll: -4.57962
[107]: train nll: -5.23360; test nll: -4.63223
[108]: train nll: -5.23629; test nll: -4.59821
[109]: train nll: -5.23154; test nll: -4.60074
[110]: train nll: -5.23037; test nll: -4.60367
[111]: train nll: -5.23748; test nll: -4.61704
[112]: train nll: -5.24229; test nll: -4.63357
[113]: train nll: -5.24608; test nll: -4.61619
[114]: train nll: -5.24797; test nll: -4.60778
[115]: train nll: -5.24661; test nll: -4.60290
[116]: train nll: -5.24158; test nll: -4.62763
[117]: train nll: -5.24441; test nll: -4.61155
[118]: train nll: -5.25185; test nll: -4.63948
[119]: train nll: -5.25066; test nll: -4.64935
[120]: train nll: -5.25349; test nll: -4.61900
[121]: train nll: -5.25538; test nll: -4.63023
[122]: train nll: -5.25764; test nll: -4.64355
[123]: train nll: -5.25988; test nll: -4.61467
[124]: train nll: -5.26012; test nll: -4.62852
[125]: train nll: -5.25481; test nll: -4.64511
[126]: train nll: -5.26590; test nll: -4.61970
[127]: train nll: -5.26033; test nll: -4.65507
[128]: train nll: -5.26261; test nll: -4.65852
[129]: train nll: -5.26437; test nll: -4.63917
[130]: train nll: -5.26403; test nll: -4.63331
[131]: train nll: -5.27210; test nll: -4.62131
[132]: train nll: -5.27415; test nll: -4.63760
[133]: train nll: -5.26807; test nll: -4.66750
[134]: train nll: -5.27058; test nll: -4.63299
[135]: train nll: -5.26634; test nll: -4.66093
[136]: train nll: -5.27226; test nll: -4.63965
[137]: train nll: -5.27179; test nll: -4.63562
[138]: train nll: -5.26915; test nll: -4.66374
[139]: train nll: -5.27149; test nll: -4.63540
[140]: train nll: -5.27152; test nll: -4.64205
[141]: train nll: -5.27722; test nll: -4.64143
[142]: train nll: -5.28018; test nll: -4.65463
[143]: train nll: -5.27388; test nll: -4.64586
[144]: train nll: -5.28189; test nll: -4.64541
[145]: train nll: -5.27222; test nll: -4.64012
[146]: train nll: -5.28381; test nll: -4.64906
[147]: train nll: -5.27687; test nll: -4.65733
[148]: train nll: -5.27050; test nll: -4.63995
[149]: train nll: -5.27428; test nll: -4.63363
[150]: train nll: -5.28284; test nll: -4.66597
[151]: train nll: -5.29242; test nll: -4.63239
[152]: train nll: -5.28292; test nll: -4.63966
[153]: train nll: -5.28300; test nll: -4.66530
[154]: train nll: -5.28785; test nll: -4.63592
[155]: train nll: -5.29311; test nll: -4.63725
[156]: train nll: -5.28205; test nll: -4.65883
[157]: train nll: -5.28868; test nll: -4.62260
[158]: train nll: -5.28548; test nll: -4.67042
[159]: train nll: -5.27989; test nll: -4.64239
[160]: train nll: -5.28555; test nll: -4.64626
[161]: train nll: -5.29344; test nll: -4.64795
[162]: train nll: -5.29237; test nll: -4.64952
[163]: train nll: -5.29188; test nll: -4.66056
[164]: train nll: -5.29240; test nll: -4.65501
[165]: train nll: -5.29408; test nll: -4.64157
[166]: train nll: -5.29634; test nll: -4.65224
[167]: train nll: -5.29688; test nll: -4.65729
[168]: train nll: -5.30340; test nll: -4.64976
[169]: train nll: -5.30142; test nll: -4.66458
[170]: train nll: -5.29416; test nll: -4.64017
[171]: train nll: -5.29329; test nll: -4.66771
[172]: train nll: -5.29444; test nll: -4.66291
[173]: train nll: -5.29550; test nll: -4.65843
[174]: train nll: -5.30276; test nll: -4.65231
[175]: train nll: -5.30070; test nll: -4.64661
[176]: train nll: -5.30053; test nll: -4.65433
[177]: train nll: -5.31134; test nll: -4.66862
[178]: train nll: -5.30561; test nll: -4.66124
[179]: train nll: -5.30585; test nll: -4.66590
[180]: train nll: -5.30674; test nll: -4.67148
[181]: train nll: -5.30690; test nll: -4.65735
[182]: train nll: -5.30347; test nll: -4.65142
[183]: train nll: -5.30866; test nll: -4.68010
[184]: train nll: -5.31795; test nll: -4.67634
[185]: train nll: -5.31486; test nll: -4.67947
[186]: train nll: -5.30697; test nll: -4.65269
[187]: train nll: -5.31261; test nll: -4.67564
[188]: train nll: -5.30890; test nll: -4.67359
[189]: train nll: -5.30549; test nll: -4.65238
[190]: train nll: -5.31442; test nll: -4.64789
[191]: train nll: -5.31764; test nll: -4.66359
[192]: train nll: -5.32123; test nll: -4.67108
[193]: train nll: -5.31953; test nll: -4.65380
[194]: train nll: -5.31514; test nll: -4.64669
[195]: train nll: -5.31932; test nll: -4.67596
[196]: train nll: -5.31456; test nll: -4.67094
[197]: train nll: -5.31493; test nll: -4.69012
[198]: train nll: -5.31220; test nll: -4.66014
[199]: train nll: -5.33011; test nll: -4.68442
[200]: train nll: -5.32255; test nll: -4.67275

-- Invertibility Verification --
Largest singular values of the weight matrices of each layer of residual block i:
For invertibility: sigma_max < 1 and will approximately = 0.99
[0] [0.990564486081301, 0.9952622636034284]
[1] [0.9935223276733219, 0.9951425948115655]
[2] [0.9929497293672671, 0.9915954333418644]
[3] [0.9906095181295934, 1.0094165034923384]
[4] [0.9985735761365142, 1.0005006696207257]
[5] [0.9946758657912573, 1.0076191888860688]
[6] [0.9937336330731721, 1.002639819258512]
[7] [0.9901141070603209, 0.997385766623791]
[8] [0.9972152212135171, 0.990573597595676]
ex_residual_nf_density_estimation.py:240: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
  plt.figure()
