INFO - residual_nf - Running command 'run'
INFO - residual_nf - Started run with ID "3"
{'bn': 'protein', 'res_type': 'graphical-lipschitz', 'coeff': 0.99, 'n_power_iterations': 5, 'num_blocks': 22, 'hidden_dims': [125], 'activation': 'lipswish', 'grad_in_forward': True, 'batch_size': 100, 'num_train_samples': 10000, 'num_test_samples': 5000, 'num_steps': 200, 'num_train_batches': 100, 'num_test_batches': 50, 'lr': 0.01, 'lr_decay': 0.1, 'lr_decay_step_size': 20, 'seed': 2, 'ex_name': 'residual_nf_density_estimation', 'sub_folder': 'protein_graphical-lipschitz_99_lipswish_22_[125]', 'path': './experiment_logs/residual_nf_density_estimation/protein_graphical-lipschitz_99_lipswish_22_[125]', 'bt_config': {'depth': 5, 'seed': 4}}
[  1]: train nll: 0.63114; test nll: -2.86005
[  2]: train nll: -3.91213; test nll: -4.05509
[  3]: train nll: -4.36692; test nll: -4.61606
[  4]: train nll: -4.74530; test nll: -4.63537
[  5]: train nll: -4.92416; test nll: -4.65686
[  6]: train nll: -5.04670; test nll: -4.72755
[  7]: train nll: -5.12234; test nll: -4.76534
[  8]: train nll: -5.19654; test nll: -4.70204
[  9]: train nll: -5.34191; test nll: -4.61286
[ 10]: train nll: -5.42369; test nll: -4.97124
[ 11]: train nll: -5.46780; test nll: -4.70505
[ 12]: train nll: -5.53895; test nll: -5.20059
[ 13]: train nll: -5.58579; test nll: -5.13120
[ 14]: train nll: -5.61708; test nll: -5.26415
[ 15]: train nll: -5.53658; test nll: -5.33935
[ 16]: train nll: -5.68474; test nll: -5.47974
[ 17]: train nll: -5.79351; test nll: -5.51883
[ 18]: train nll: -5.83974; test nll: -5.59125
[ 19]: train nll: -5.93351; test nll: -5.53503
[ 20]: train nll: -5.93510; test nll: -5.60580
[ 21]: train nll: -5.92768; test nll: -5.55799
[ 22]: train nll: -5.97491; test nll: -5.73498
[ 23]: train nll: -6.01375; test nll: -5.27481
[ 24]: train nll: -6.05318; test nll: -5.69071
[ 25]: train nll: -6.05094; test nll: -5.72795
[ 26]: train nll: -6.09809; test nll: -5.84510
[ 27]: train nll: -6.14022; test nll: -5.84411
[ 28]: train nll: -6.10335; test nll: -5.80108
[ 29]: train nll: -6.12953; test nll: -5.89274
[ 30]: train nll: -6.15531; test nll: -5.89296
[ 31]: train nll: -6.24254; test nll: -5.91530
[ 32]: train nll: -6.29445; test nll: -5.91267
[ 33]: train nll: -6.31445; test nll: -5.79528
[ 34]: train nll: -6.26016; test nll: -5.73800
[ 35]: train nll: -6.29470; test nll: -5.72931
[ 36]: train nll: -6.28585; test nll: -5.93971
[ 37]: train nll: -6.30996; test nll: -5.56099
[ 38]: train nll: -6.34261; test nll: -5.91979
[ 39]: train nll: -6.30771; test nll: -5.88191
[ 40]: train nll: -6.32778; test nll: -6.04304
[ 41]: train nll: -6.28662; test nll: -5.74438
[ 42]: train nll: -6.36731; test nll: -5.78238
[ 43]: train nll: -6.41142; test nll: -6.06237
[ 44]: train nll: -6.37722; test nll: -6.02945
[ 45]: train nll: -6.41786; test nll: -6.15591
[ 46]: train nll: -6.34514; test nll: -5.98041
[ 47]: train nll: -6.36875; test nll: -6.10421
[ 48]: train nll: -6.41419; test nll: -6.02986
[ 49]: train nll: -6.49513; test nll: -6.18567
[ 50]: train nll: -6.38351; test nll: -6.16871
[ 51]: train nll: -6.44101; test nll: -6.13891
[ 52]: train nll: -6.41308; test nll: -6.07433
[ 53]: train nll: -6.40465; test nll: -6.17325
[ 54]: train nll: -6.42597; test nll: -6.04068
[ 55]: train nll: -6.49853; test nll: -6.06672
[ 56]: train nll: -6.55678; test nll: -6.06106
[ 57]: train nll: -6.50348; test nll: -6.04337
[ 58]: train nll: -6.48511; test nll: -5.92995
[ 59]: train nll: -6.49167; test nll: -6.16392
[ 60]: train nll: -6.49675; test nll: -6.07640
[ 61]: train nll: -6.51382; test nll: -6.18451
[ 62]: train nll: -6.55849; test nll: -6.19353
[ 63]: train nll: -6.40122; test nll: -5.79174
[ 64]: train nll: -6.48843; test nll: -6.25132
[ 65]: train nll: -6.53202; test nll: -5.94346
[ 66]: train nll: -6.52627; test nll: -6.15154
[ 67]: train nll: -6.53174; test nll: -5.93242
[ 68]: train nll: -6.57061; test nll: -6.16578
[ 69]: train nll: -6.58383; test nll: -6.31201
[ 70]: train nll: -6.57364; test nll: -6.11784
[ 71]: train nll: -6.58305; test nll: -6.10576
[ 72]: train nll: -6.63597; test nll: -6.26830
[ 73]: train nll: -6.58605; test nll: -6.32642
[ 74]: train nll: -6.59691; test nll: -6.26133
[ 75]: train nll: -6.58603; test nll: -6.21055
[ 76]: train nll: -6.62766; test nll: -6.09448
[ 77]: train nll: -6.51005; test nll: -5.86637
[ 78]: train nll: -6.58929; test nll: -6.24337
[ 79]: train nll: -6.62347; test nll: -5.94861
[ 80]: train nll: -6.52757; test nll: -6.08275
[ 81]: train nll: -6.68271; test nll: -5.98908
[ 82]: train nll: -6.63251; test nll: -6.10093
[ 83]: train nll: -6.62855; test nll: -6.07866
[ 84]: train nll: -6.63803; test nll: -6.04963
[ 85]: train nll: -6.69301; test nll: -6.22305
[ 86]: train nll: -6.50170; test nll: -6.13418
[ 87]: train nll: -6.67725; test nll: -6.02278
[ 88]: train nll: -6.54140; test nll: -6.10775
[ 89]: train nll: -6.57676; test nll: -5.86339
[ 90]: train nll: -6.61747; test nll: -6.04656
[ 91]: train nll: -6.58767; test nll: -5.83625
[ 92]: train nll: -6.67634; test nll: -6.22047
[ 93]: train nll: -6.65017; test nll: -6.08845
[ 94]: train nll: -6.67884; test nll: -5.77484
[ 95]: train nll: -6.63190; test nll: -6.12957
Epoch    96: reducing learning rate of group 0 to 1.0000e-03.
[ 96]: train nll: -6.61287; test nll: -5.98684
[ 97]: train nll: -7.04331; test nll: -6.42916
[ 98]: train nll: -7.13020; test nll: -6.39554
[ 99]: train nll: -7.14581; test nll: -6.39450
[100]: train nll: -7.15712; test nll: -6.37218
[101]: train nll: -7.16498; test nll: -6.34920
[102]: train nll: -7.17484; test nll: -6.22217
[103]: train nll: -7.18132; test nll: -6.34627
[104]: train nll: -7.18581; test nll: -6.35678
[105]: train nll: -7.19147; test nll: -6.11025
[106]: train nll: -7.19521; test nll: -6.33932
[107]: train nll: -7.20214; test nll: -6.36966
[108]: train nll: -7.21295; test nll: -6.33995
[109]: train nll: -7.21500; test nll: -6.33620
[110]: train nll: -7.22481; test nll: -6.28413
[111]: train nll: -7.23164; test nll: -6.31140
[112]: train nll: -7.23718; test nll: -6.29490
[113]: train nll: -7.24323; test nll: -6.32971
[114]: train nll: -7.24652; test nll: -6.31217
[115]: train nll: -7.24605; test nll: -6.27220
[116]: train nll: -7.25402; test nll: -6.33925
[117]: train nll: -7.25864; test nll: -6.31573
[118]: train nll: -7.25466; test nll: -6.30007
[119]: train nll: -7.25502; test nll: -6.31988
[120]: train nll: -7.25973; test nll: -6.28350
[121]: train nll: -7.26055; test nll: -6.28487
[122]: train nll: -7.26820; test nll: -6.26630
[123]: train nll: -7.27057; test nll: -6.27044
[124]: train nll: -7.26540; test nll: -6.25675
[125]: train nll: -7.25885; test nll: -6.24519
[126]: train nll: -7.26928; test nll: -6.24605
[127]: train nll: -7.27744; test nll: -6.24996
[128]: train nll: -7.28398; test nll: -6.28278
[129]: train nll: -7.28412; test nll: -6.29279
[130]: train nll: -7.28632; test nll: -6.25092
[131]: train nll: -7.28208; test nll: -6.27505
[132]: train nll: -7.28320; test nll: -6.30665
[133]: train nll: -7.29931; test nll: -6.29584
[134]: train nll: -7.29650; test nll: -6.27547
[135]: train nll: -7.30307; test nll: -6.27339
[136]: train nll: -7.30957; test nll: -6.09609
[137]: train nll: -7.31122; test nll: -6.24884
[138]: train nll: -7.31010; test nll: -6.28811
[139]: train nll: -7.31855; test nll: -5.96164
[140]: train nll: -7.31489; test nll: -6.19729
[141]: train nll: -7.32862; test nll: -6.24391
[142]: train nll: -7.32585; test nll: -6.18694
[143]: train nll: -7.33280; test nll: -6.23277
[144]: train nll: -7.33906; test nll: -6.22145
[145]: train nll: -7.32773; test nll: -6.25916
[146]: train nll: -7.33866; test nll: -6.27368
[147]: train nll: -7.33554; test nll: -6.30732
[148]: train nll: -7.33428; test nll: -6.29160
[149]: train nll: -7.33131; test nll: -6.24500
[150]: train nll: -7.33255; test nll: -6.30426
[151]: train nll: -7.34211; test nll: -6.28626
[152]: train nll: -7.34260; test nll: -6.28388
[153]: train nll: -7.34984; test nll: -6.29422
[154]: train nll: -7.33653; test nll: -6.29051
[155]: train nll: -7.32896; test nll: -6.26161
[156]: train nll: -7.36263; test nll: -6.27407
[157]: train nll: -7.35899; test nll: -6.20860
[158]: train nll: -7.35604; test nll: -6.28402
[159]: train nll: -7.34690; test nll: -6.25068
[160]: train nll: -7.36649; test nll: -6.24906
[161]: train nll: -7.37828; test nll: -6.27930
[162]: train nll: -7.38124; test nll: -6.30036
[163]: train nll: -7.37109; test nll: -6.17554
[164]: train nll: -7.33004; test nll: -6.25476
[165]: train nll: -7.39117; test nll: -6.27526
[166]: train nll: -7.38752; test nll: -6.25590
[167]: train nll: -7.38481; test nll: -6.29965
[168]: train nll: -7.37552; test nll: -6.18116
[169]: train nll: -7.38398; test nll: -6.28336
[170]: train nll: -7.37920; test nll: -6.09254
[171]: train nll: -7.34601; test nll: -6.19156
[172]: train nll: -7.37224; test nll: -6.27841
[173]: train nll: -7.40194; test nll: -5.95272
[174]: train nll: -7.37630; test nll: -6.25641
[175]: train nll: -7.38232; test nll: -6.28208
[176]: train nll: -7.37156; test nll: -6.23945
[177]: train nll: -7.39548; test nll: -6.23209
[178]: train nll: -7.34755; test nll: -6.24673
[179]: train nll: -7.38382; test nll: -6.26023
[180]: train nll: -7.39326; test nll: -6.25779
[181]: train nll: -7.40383; test nll: -6.25377
[182]: train nll: -7.40221; test nll: -6.24061
[183]: train nll: -7.40789; test nll: -6.25156
[184]: train nll: -7.35714; test nll: -6.25563
[185]: train nll: -7.40542; test nll: -6.19904
[186]: train nll: -7.39039; test nll: -6.25968
[187]: train nll: -7.42719; test nll: -6.25421
[188]: train nll: -7.41953; test nll: -6.25774
[189]: train nll: -7.41370; test nll: -6.23389
[190]: train nll: -7.36088; test nll: -6.20999
[191]: train nll: -7.38312; test nll: -6.20791
[192]: train nll: -7.42312; test nll: -6.26409
[193]: train nll: -7.40450; test nll: -6.23565
[194]: train nll: -7.33622; test nll: -6.22200
[195]: train nll: -7.37794; test nll: -6.12738
[196]: train nll: -7.37974; test nll: -6.21027
[197]: train nll: -7.42830; test nll: -6.22973
[198]: train nll: -7.42839; test nll: -6.17266
[199]: train nll: -7.43205; test nll: -6.21904
[200]: train nll: -7.33337; test nll: -6.26380

-- Invertibility Verification --
Largest singular values of the weight matrices of each layer of residual block i:
For invertibility: sigma_max < 1 and will approximately = 0.99
[0] [0.9910392987301245, 0.9909016459446399]
[1] [0.9912104136989174, 0.991750164176041]
[2] [0.9909685603894367, 0.9915351615372265]
[3] [0.9910591362241542, 0.9912487803473637]
[4] [0.990691521800621, 0.9908901613680925]
[5] [0.9901276159904311, 0.9912530079637757]
[6] [0.9916305758834089, 0.9924368540086477]
[7] [0.9915325057990727, 0.9924147751262731]
[8] [0.9906966269818708, 0.9902563596091738]
[9] [0.9901812995811108, 0.9901545083494389]
[10] [0.9907815837270213, 0.99185908223936]
[11] [0.9903747249769563, 0.990000036788409]
[12] [0.990849343171738, 0.9911507692104381]
[13] [0.9901578752762897, 0.9965773808700924]
[14] [0.9912812459575541, 0.9913157920045373]
[15] [0.9915271659711292, 0.9927166792521476]
[16] [0.9911401505833111, 0.9900088594476469]
[17] [0.9909557915079141, 0.9912845332598116]
[18] [0.9940043386306219, 0.9912436915804258]
[19] [0.992665358910063, 0.9921287499757478]
[20] [0.9932020701082209, 0.9949579635622774]
[21] [0.9904284375481476, 0.9901714731773603]
ex_residual_nf_density_estimation.py:236: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
  plt.figure()
