import torch
import torch.distributions as dist
import numpy as np
import matplotlib.pyplot as plt
import json

from sacred import Experiment
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_residual_flow_density_estimation
from ex_utils import batch_iterator, count_parameters, sample_batch
from ex_binary_tree_config import bt_ingredient, create_tree
from graph.belief_network import *
from data.load_data import load_protein,load_mehra

torch.set_printoptions(precision=5, sci_mode=False, edgeitems=5)


ex = Experiment('residual_nf', ingredients=[bt_ingredient])
device = "cpu" #if not(torch.cuda.is_available()) else "cuda:0"


@ex.command(unobserved=True)
def diagnostics(_config):
    # BN + true samples
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    path = './experiment_logs/residual_nf_density_estimation/'
    model = torch.load(_config['path']+'/{}/model.pt'.format(_config['run'])).double().to(device)

    # Inversion verification
    print('\n-- Invertibility Verification (GRF_n) --')
    print('Largest singular values of the weight matrices of each layer of residual block i:')
    print('For invertibility: sigma_max < 1 and will approximately = {}'.format(0.99))
    for i, block in enumerate(model.blocks):
        sigmas = block.g.largest_singular_values()
        print('[{}] {}'.format(i, sigmas))


def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network 
    bn = 'arithmetic-circuit2'
    # structure: [faithful, fully-connected, random]
    structure = 'faithful'
    
    # Params for flow with Lipschitz constraint
    coeff = 0.99
    n_power_iterations = 5

    num_blocks = 1
    hidden_dims = [100]
    # Activation [elu, alpha-elu, tanh, sigmoid, lipswish, lipmish]
    activation = 'lipmish'
    grad_in_forward = True

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_epochs = 100
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size

    lr = 1e-2
    seed = 0

    patience = 20

    run = 1

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False

    # Add observer
    ex_name = 'residual_nf_density_estimation'
    sub_folder = '{}_{}_{}_{}_{}'.format(structure, str(coeff).split('.')[1], activation, num_blocks, hidden_dims)

    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _run):
    # Training info
    batch_size = _config['batch_size']
    lr = _config['lr'] 
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    checkpoint_n = _config['checkpoint_n']

    print('[CONFIG]',_config)
    print('[DEVICE]', device)

    # BN initialization
    real = False
    if _config['bn'] == 'arithmetic-circuit':
        bn = ArithmeticCircuit()
    elif _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif _config['bn'] == 'tree':
        bn = Tree(device)
    elif _config['bn'] == 'protein':
        bn = Protein()
        print('[LOADING DATA] Protein')
        # 9000 train samples, 1000 validation samples, batch_size=100
        trainloader, valloader, testloader = load_protein(batch_size)
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    elif _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif _config['bn'] == 'arth150-synth':
        bn = Arth150()
    # Mehra
    elif _config['bn'] == 'mehra-real':
        bn = Mehra()
        print('[LOADING DATA] Mehra')
        trainloader, valloader, testloader = load_mehra()
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    elif _config['bn'] == 'binary-tree':
        bn = create_tree()
        ex.info['tree_coeffs'] = bn.coeffs.tolist()
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))

    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # Fix structure
    if _config['structure'] == 'faithful':
        print('[ENCODED STRUCTURE] Using faithful BN structure.')

    elif _config['structure'] == 'fully-connected':
        print('[ENCODED STRUCTURE] Updating BN structure to be fully-connected.')
        bn = FCBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn)

    elif _config['structure'] == 'random':
        print('[ENCODED STRUCTURE] Randomizing the faithful BN structure.')
        num_edges = bn.get_num_edges()
        bn = RandomBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn,
            num_edges=num_edges, seed=_config['seed'])
    
    else:
        raise Exception("Unknown structure: {}".format(_config['structure']))


    # Draw train and val sets and return minibatch iterators
    if not real:
        train = bn.sample(_config['num_train_samples'])
        val = bn.sample(_config['num_val_samples'])
        test = bn.sample(_config['num_val_samples'])
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
        testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)

    # # Joint space normalization
    # min_std = 1e-5
    # batch = sample_batch(bn.sample, 10)#10000)
    # z = batch[:,:num_latent]
    # x = batch[:,num_latent:]
    # z_std = torch.std(z, dim=0).to(device)
    # x_std = torch.std(x, dim=0).to(device)
    # scale = {
    #     'z_scale': torch.maximum(z_std, torch.ones_like(z_std)*min_std),
    #     'z_shift': torch.mean(z, dim=0).to(device),
    #     'x_scale': torch.maximum(x_std, torch.ones_like(x_std)*min_std),
    #     'x_shift': torch.mean(x, dim=0).to(device)
    # }
    # del batch
   
    # Initialize model and optimizer
    coeff = _config['coeff']
    n_power_iterations = _config['n_power_iterations']
    hidden_dims = _config['hidden_dims']
    num_blocks = _config['num_blocks']
    activation = _config['activation']
    grad_in_forward = _config['grad_in_forward']
    model = build_residual_flow_density_estimation('graphical-lipschitz',
        num_blocks, bn, hidden_dims, activation, coeff,
        n_power_iterations, device, grad_in_forward).double().to(device)

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True, 
                min_lr=1e-6, patience=_config['patience'])


    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)


    # Log model capacity
    ex.info['num model params'] = model.count_parameters()

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_nll = 0.0

        model.train()
        for train_batch in trainloader:
            z0, j = model(train_batch.double().to(device))
            loss = -model.ll(z0, j)

            l = loss.detach().item()
            train_nll += l    

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # log training metrics 
        _run.log_scalar("train", value=train_nll/num_train_batches) 
        if device == 'cuda:0':
            _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)

        # Log val metrics - val loss, kl-divergence
        model.eval()
        with torch.no_grad():
            val_nll = 0
            # true_nll = 0
            for val_batch in valloader:
                z0, j = model(val_batch.double().to(device))
                val_nll += -model.ll(z0, j).item()

                # if bn == 'arithmetic-mul':
                #     z = val_batch[:,:num_latent]
                #     x = val_batch[:,num_latent:]
                #     true_nll += (-graph.log_joint(x, z)).mean().item()
            # log val metrics 
            _run.log_scalar("val", value=val_nll/num_val_batches) 

        # Decay learning rate every 100 epochs
        lr_scheduler.step(train_nll)

        epochs.set_description('Train NLL: {:.3f}, Val NLL: {:.3f}'.format(
            train_nll/num_train_batches, val_nll/num_val_batches),
            refresh=False)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1
    
    # Peak memory usage
    if device == 'cuda:0':
        ex.info['memory usage (MB)'] = torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000 
    
    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))

    # Log test metrics - val loss, kl-divergence
    model.eval()
    with torch.no_grad():
        test_nll = 0

        num_test_batches = 0
        for test_batch in testloader:
            z0, j = model(test_batch.double().to(device))
            test_nll += -model.ll(z0, j).item()
            num_test_batches += 1
        # log test metrics 
        _run.log_scalar("test", value=test_nll/num_test_batches) 

   
    # model.train()
    # Inversion verification
    # print('\n-- Invertibility Verification --')
    # print('Largest singular values of the weight matrices of each layer of residual block i:')
    # print('For invertibility: sigma_max < 1 and will approximately = {}'.format(coeff))
    # for i, block in enumerate(model.blocks):
    #     sigmas = block.g.largest_singular_values()
    #     print('[{}] {}'.format(i, sigmas))

    # model.eval()

    # Some plots
    #with torch.no_grad():
    # if True:
    #     batch = sample_batch(graph.sample, 10000)
    #     true_sample = (batch).double().to(device)

    #     z0 = dist.Normal(
    #             loc=torch.zeros(graph.get_num_vertices(), dtype=torch.float64),
    #             scale=torch.ones(graph.get_num_vertices(), dtype=torch.float64)
    #         ).sample((10000,)).to(device)
    #     model_sample, _ = model.inverse(z0)

    #     model_z0,_ = model(true_sample)

    #     label = ['True','Flow']
    
    #     for i in range(graph.get_num_vertices()):
    #         ztrue = true_sample[:,i].cpu().tolist()
    #         zflow = model_sample[:,i].cpu().tolist()
    #         plt.figure()
    #         plt.hist(x=[ztrue, zflow], bins=50, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red'], edgecolor='none',
    #                     label=label)
    #         plt.xlabel("Node "+str(i))
    #         plt.ylabel("Density")
    #         plt.legend()
    #         plt.savefig(path+'/{}/{}'.format(_run._id,i))

    #         ztrue = z0[:,i].cpu().tolist()
    #         zflow = model_z0[:,i].cpu().tolist()
    #         plt.figure()
    #         plt.hist(x=[ztrue, zflow], bins=50, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red'], edgecolor='none',
    #                     label=label)
    #         plt.xlabel("Node "+str(i))
    #         plt.ylabel("Density")
    #         plt.legend()
    #         plt.savefig(path+'/{}/z0_{}'.format(_run._id,i))