import torch
import sys
import numpy as np
import sklearn

if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import BeliefNetwork

class TwoPairs(BeliefNetwork):

    def __init__(self):
        super(TwoPairs, self).__init__()

        # graph
        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph()
            self.inverse_graph = _WrapperGraph(inverse=True)
        else:
            self.forward_graph = ForwardGraph().initialize(*TwoPairs._construct_graph())
            self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 4


    def sample(self, batch_size, train=True):
        rng = np.random.RandomState()
        # 2 Spirals:
        data1 = self.sample_2spirals(rng,batch_size)

        # 8 Gaussians
        data2 = self.sample_8gaussians(rng, batch_size)

        std = torch.tensor([1.604934, 1.584863, 2.0310535, 2.0305095])
        data = np.concatenate([data1, data2], axis=1)
        data = torch.tensor(data)

        return data/std


    def sample_8gaussians(self, rng, batch_size):
        scale = 4.
        centers = [(1,0), (-1,0), (0,1), (0,-1), (1./np.sqrt(2), 1./np.sqrt(2)),
            (1./np.sqrt(2), -1./np.sqrt(2)), (-1./np.sqrt(2), 1./np.sqrt(2)), 
            (-1./np.sqrt(2), -1./np.sqrt(2))]
        centers = [(scale*x, scale*y) for x, y in centers]
        data= []
        for i in range(batch_size):
            point = rng.randn(2)*0.5
            idx = rng.randint(8)
            center = centers[idx]
            point[0] += center[0]
            point[1] += center[1]
            data.append(point)
        data = np.array(data)
        data /= 1.414
        return data
        
    
    def sample_pinwheel(self, rng, batch_size):
        radial_std = 0.3
        tangential_std = 0.1
        num_classes = 5
        num_per_class = batch_size//5
        rate = 0.25
        rads = np.linspace(0, 2*np.pi, num_classes, endpoint=False)

        features = rng.randn(num_classes*num_per_class, 2) \
            * np.array([radial_std, tangential_std])
        features[:, 0] += 1.
        labels = np.repeat(np.arange(num_classes), num_per_class)

        angles = rads[labels] + rate*np.exp(features[:, 0])
        rotations = np.stack([np.cos(angles), -np.sin(angles), np.sin(angles), np.cos(angles)])
        rotations = np.reshape(rotations.T, (-1, 2, 2))

        return 2*rng.permutation(np.einsum("ti,tij->tj", features, rotations))


    def sample_2spirals(self, rng, batch_size):
        n = np.sqrt(rng.rand(batch_size//2, 1))*540*(2*np.pi)/360
        d1x = -np.cos(n)*n + rng.rand(batch_size//2, 1)*0.5
        d1y = np.sin(n)*n + rng.rand(batch_size//2, 1)*0.5
        x = np.vstack((np.hstack((d1x, d1y)), np.hstack((-d1x, -d1y))))/3
        x += rng.randn(*x.shape)*0.1
        return x


    def sample_checkerboard(self, batch_size):
        x1 = np.random.rand(batch_size)*4 - 2
        x2_ = np.random.rand(batch_size) - np.random.randint(0, 2, batch_size)*2
        x2 = x2_ + (np.floor(x1)%2)
        return np.concatenate([x1[:, None], x2[:, None]], 1)*2

    def sample_line(self, rng, batch_size):
        x = rng.rand(batch_size)
        x = x*5 - 2.5
        y = x
        return np.stack((x, y), 1)


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for EightPairs BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for EightPairs BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for EightPairs BN")


    def get_num_latent(self):
        return 0


    def get_num_obs(self):
        return 4


    def get_num_vertices(self):
        return 4


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['x0','x1', 'x2','x3']
        edges = [('x1','x0'), ('x3','x2')]  
        observed = {'x0','x1', 'x2','x3'}
        return vertices, edges, observed