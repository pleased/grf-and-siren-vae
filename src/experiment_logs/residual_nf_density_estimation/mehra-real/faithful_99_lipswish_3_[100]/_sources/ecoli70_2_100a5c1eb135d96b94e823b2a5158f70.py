import torch
import sys
import numpy as np
import random

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal, Laplace

from graph.belief_network import BeliefNetwork


class EColi70_2(BeliefNetwork):

    def __init__(self):
        super(EColi70_2, self).__init__()

        # graph
        graph = EColi70_2._construct_graph()
        self.vertices = graph[0]
        self.edges = graph[1]
        self.forward_graph = ForwardGraph().initialize(*graph)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 9
        self.num_obs = 37

    
    def aceB(self, icdA):
        return Normal(0.1324257 + 1.0463576*icdA, 0.2921168)

    def asnA(self, ygcE):
        return Normal(0.3494150 + 0.7974541*ygcE, 0.3022864)
    
    def atpD(self, sucA, ygcE):
        return Normal(-0.04025017 + 0.26029355*sucA - 0.72522014*ygcE, 0.6427111)

    def atpG(self, sucA):
        return Normal(-0.8907914 + 0.6179975*sucA, 0.6279111)

    def b1191(self):
        return Normal(1.272989, 0.7801026)

    def b1583(self, lacA, lacZ, yceP):
        return Normal(1.3819549 - 0.2456687*lacA + 0.3421805*lacZ + 0.2407019*yceP, 1.052861)

    def b1963(self, yheI):
        return Normal(0.9649125 + 1.0375753*yheI, 0.6138791)

    def cchB(self, fixC):
        return Normal(1.0695227 + 0.6179876*fixC, 0.8003899)

    def cspA(self, cspG):
        return Normal(-0.4264853 + 0.2887115*cspG, 1.239623)

    def cspG(self):
        return Normal(2.026078, 1.037081)

    def dnaG(self, ycgX, yheI):
        return Normal(0.1170132 + 0.5992179*ycgX + 0.1954802*yheI, 0.3608203)

    def dnaJ(self, sucA):
        return Normal(0.1209770 - 0.8084874*sucA, 0.7585577)

    def dnaK(self, yheI):
        return Normal(-0.2469326 + 1.0797222*yheI, 0.5392409)

    def eutG(self):
        return Normal(1.2654, 0.8313535)

    def fixC(self, b1191):
        return Normal(0.3164756 + 0.9406052*b1191, 1.063448)

    def flgD(self, sucA):
        return Normal(-0.5166705 + 0.6361945*sucA, 0.6268255)

    def folK(self, yheI):
        return Normal(0.5640817 + 0.8180528*yheI, 0.3666615)

    def ftsJ(self, mopB):
        return Normal(0.6737551 + 0.9241186*mopB, 0.3954749)

    def gltA(self, sucA):
        return Normal(-0.9572130 + 0.3789963*sucA,  0.8303468)

    def hupB(self, cspA, yfiA):
        return Normal(-0.1820572 - 0.2984208*cspA + 1.3866871*yfiA, 0.3403716)

    def ibpB(self, eutG, yceP):
        return Normal(-0.4226813 + 1.4471465*eutG + 0.1249285*yceP, 0.678909)

    def icdA(self, asnA, ygcE):
        return Normal(-0.4154791 + 0.5228393*asnA - 1.0584814*ygcE, 0.5638465)

    def lacA(self, asnA, cspG):
        return Normal(0.4468699 + 0.2723904*asnA + 0.2539059*cspG, 1.699123)

    def lacY(self, asnA, cspG, eutG, lacA):
        return Normal(-0.1148751 - 0.2039735*asnA - 0.2240817*cspG +
            0.3536548*eutG + 1.0462075* lacA, 0.2453465)

    def lacZ(self, asnA, lacA, lacY):
        return Normal(0.19996029 - 0.01611601*asnA + 1.36838439*lacA - 
            0.43756791*lacY, 0.5838168)

    def lpdA(self, yedE):
        return Normal(-0.1007488 + 0.9609477*yedE, 0.3740323)

    def mopB(self, dnaK, lacZ):
        return Normal(0.03568437 + 0.89579537*dnaK - 0.05902883*lacZ, 0.560237)

    def nmpC(self, pspA):
        return Normal(0.1688145 - 0.7690484*pspA, 0.568521)

    def nuoM(self, lacY):
        return Normal(-2.0175694 + 0.4082615*lacY, 0.9018575)

    def pspA(self, cspG, pspB, yedE):
        return Normal(-0.1900509 + 0.1079258*cspG + 0.1399003*pspB - 
            0.7455691*yedE, 0.4151538)

    def pspB(self, cspG, yedE):
        return Normal(-0.2490257 + 0.2758657*cspG - 0.9886109*yedE, 0.5201657)

    def sucA(self, eutG):
        return Normal(0.0242884 - 1.0894048*eutG, 0.811525)

    def sucD(self, sucA):
        return Normal(-0.5907153 + 0.6829936*sucA, 0.6600159)

    def tnaA(self, b1191, fixC, sucA):
        return Normal(-0.3861436 - 0.5926134*b1191 - 0.2441913*fixC + 
            0.1105793*sucA, 0.3102185)

    def yaeM(self, cspG, lacA, lacZ):
        return Normal(0.3665120 + 1.4721822*cspG - 0.7172945*lacA + 
            0.7203860*lacZ, 0.8100231)

    def yceP(self, eutG, fixC):
        return Normal(-0.1280449 + 1.1408675*eutG - 0.3267433*fixC, 0.4090936)

    def ycgX(self, fixC, yheI):
        return Normal(0.1583832 - 0.2716239*fixC + 1.2448325*yheI, 0.5054622)

    def yecO(self, cspG):
        return Normal(0.2719168 + 0.7948663*cspG, 0.474221)

    def yedE(self, cspG):
        return Normal(-0.1606039 - 0.6420267*cspG, 0.526442)

    def yfaD(self, eutG, sucA, yceP):
        return Normal(0.1628253 + 0.2875757*eutG - 0.2437410*sucA + 
            0.3177723*yceP, 0.4324096)

    def yfiA(self, cspA):
        return Normal(-1.1928196 + 0.8572262*cspA, 0.5617983)

    def ygbD(self, fixC):
        return Normal(1.3504359 + 0.6606881*fixC, 0.8602441)

    def ygcE(self, b1191, sucA):
        return Normal(0.5239508 + 1.8815081*b1191 + 0.6326535*sucA, 0.5856137)

    def yhdM(self, sucA):
        return Normal(0.2084980 - 0.7770235*sucA, 0.7669168)

    def yheI(self, atpD, yedE):
        return Normal(-0.2137443 - 0.9633228*atpD + 0.3381502*yedE, 0.3749968)

    def yjbO(self, fixC):
        return Normal(1.59101726 - 0.07064048*fixC, 1.360589)


    def sample(self, batch_size, train=True):
        sample = torch.empty((batch_size, self.num_latent + self.num_obs))

        b1191 = self.b1191().sample((batch_size,1))
        sample[:, 0:1] = b1191
        eutG = self.eutG().sample((batch_size,1))
        sample[:, 1:2] = eutG
        fixC = self.fixC(b1191).sample()
        sample[:, 2:3] = fixC
        sucA = self.sucA(eutG).sample()
        sample[:, 3:4] = sucA
        yceP = self.yceP(eutG, fixC).sample()
        sample[:, 4:5] = yceP
        ygcE = self.ygcE(b1191,sucA).sample()
        sample[:, 5:6] = ygcE
        asnA = self.asnA(ygcE).sample()
        sample[:, 6:7] = asnA
        cspG = self.cspG().sample((batch_size,1))
        sample[:, 7:8] = cspG
        atpD = self.atpD(sucA, ygcE).sample()
        sample[:, 8:9] = atpD
        icdA = self.icdA(asnA,ygcE).sample()
        sample[:, 9:10] = icdA
        lacA = self.lacA(asnA, cspG).sample()
        sample[:,10:11] = lacA
        cspA = self.cspA(cspG).sample()
        sample[:,11:12] = cspA
        yedE = self.yedE(cspG).sample()
        sample[:,12:13] = yedE
        lacY = self.lacY(asnA, cspG, eutG, lacA).sample()
        sample[:,13:14] = lacY
        yfiA = self.yfiA(cspA).sample()
        sample[:,14:15] = yfiA
        pspB = self.pspB(cspG, yedE).sample()
        sample[:,15:16] = pspB
        yheI = self.yheI(atpD, yedE).sample()
        sample[:,16:17] = yheI
        lacZ = self.lacZ(asnA, lacA, lacY).sample()
        sample[:,17:18] = lacZ
        pspA = self.pspA(cspG, pspB, yedE).sample()
        sample[:,18:19] = pspA
        ycgX = self.ycgX(fixC, yheI).sample()
        sample[:,19:20] = ycgX
        dnaK = self.dnaK(yheI).sample()
        sample[:,20:21] = dnaK
        mopB = self.mopB(dnaK, lacZ).sample()
        sample[:,21:22] = mopB
        sample[:,22:23] = self.ygbD(fixC).sample()
        sample[:,23:24] = self.yjbO(fixC).sample()
        sample[:,24:25] = self.cchB(fixC).sample()
        sample[:,25:26] = self.tnaA(b1191, fixC, sucA).sample()
        sample[:,26:27] = self.dnaJ(sucA).sample()
        sample[:,27:28] = self.flgD(sucA).sample()
        sample[:,28:29] = self.gltA(sucA).sample()
        sample[:,29:30] = self.sucD(sucA).sample()
        sample[:,30:31] = self.yhdM(sucA).sample()
        sample[:,31:32] = self.atpG(sucA).sample()
        sample[:,32:33] = self.ibpB(eutG, yceP).sample()
        sample[:,33:34] = self.yfaD(eutG, sucA, yceP).sample()
        sample[:,34:35] = self.yecO(cspG).sample()
        sample[:,35:36] = self.aceB(icdA).sample()
        sample[:,36:37] = self.lpdA(yedE).sample()
        sample[:,37:38] = self.nuoM(lacY).sample()
        sample[:,38:39] = self.hupB(cspA, yfiA).sample()
        sample[:,39:40] = self.b1963(yheI).sample()
        sample[:,40:41] = self.folK(yheI).sample()
        sample[:,41:42] = self.b1583(lacA, lacZ, yceP).sample()
        sample[:,42:43] = self.yaeM(cspG, lacA, lacZ).sample()
        sample[:,43:44] = self.nmpC(pspA).sample()
        sample[:,44:45] = self.dnaG(ycgX, yheI).sample()
        sample[:,45:46] = self.ftsJ(mopB).sample()

        return sample


    def log_likelihood(self, x, z):
        # p(x|z) = p(x0|z3)p(x1|z5)
        asnA = z[:,6]
        cspG = z[:,7]
        lacA = x[:,10-self.num_latent]
        cspA = x[:,11-self.num_latent]
        yedE = x[:,12-self.num_latent]
        atpD = z[:,8]
        lacY = x[:,13-self.num_latent]
        pspB = x[:,15-self.num_latent]
        yheI = x[:,16-self.num_latent]
        dnaK = x[:,20-self.num_latent]
        lacZ = x[:,17-self.num_latent]
        fixC = z[:, 2]
        b1191 = z[:, 0]
        sucA = z[:, 3]
        eutG = z[:, 1]
        yceP = z[:, 4]
        cspG = z[:, 7]
        icdA = x[:, 9-self.num_latent]
        yedE = x[:,12-self.num_latent]
        lacY = x[:,13-self.num_latent]
        cspA = x[:,11-self.num_latent]
        yfiA = x[:,14-self.num_latent]
        yheI = x[:,16-self.num_latent]
        lacZ = x[:,17-self.num_latent]
        pspA = x[:,18-self.num_latent]
        ycgX = x[:,19-self.num_latent]
        mopB = x[:,21-self.num_latent]
        ygcE = z[:,5]

        log_p_x = self.icdA(asnA,ygcE).log_prob(x[:,9-self.num_latent])
        log_p_x += self.lacA(asnA, cspG).log_prob(x[:,10-self.num_latent])
        log_p_x += self.cspA(cspG).log_prob(x[:,11-self.num_latent])
        log_p_x += self.yedE(cspG).log_prob(x[:,12-self.num_latent])
        log_p_x += self.lacY(asnA, cspG, eutG, lacA).log_prob(x[:,13-self.num_latent])
        log_p_x += self.yfiA(cspA).log_prob(x[:,14-self.num_latent])
        log_p_x += self.pspB(cspG, yedE).log_prob(x[:,15-self.num_latent])
        log_p_x += self.yheI(atpD, yedE).log_prob(x[:,16-self.num_latent])
        log_p_x += self.lacZ(asnA, lacA, lacY).log_prob(x[:,17-self.num_latent])
        log_p_x += self.pspA(cspG, pspB, yedE).log_prob(x[:,18-self.num_latent])
        log_p_x += self.ycgX(fixC, yheI).log_prob(x[:,19-self.num_latent])
        log_p_x += self.dnaK(yheI).log_prob(x[:,20-self.num_latent])
        log_p_x += self.mopB(dnaK, lacZ).log_prob(x[:,21-self.num_latent])

        log_p_x += self.ygbD(fixC).log_prob(x[:,22-self.num_latent])
        log_p_x += self.yjbO(fixC).log_prob(x[:,23-self.num_latent])
        log_p_x += self.cchB(fixC).log_prob(x[:,24-self.num_latent])
        log_p_x += self.tnaA(b1191, fixC, sucA).log_prob(x[:,25-self.num_latent])
        log_p_x += self.dnaJ(sucA).log_prob(x[:,26-self.num_latent])
        log_p_x += self.flgD(sucA).log_prob(x[:,27-self.num_latent])
        log_p_x += self.gltA(sucA).log_prob(x[:,28-self.num_latent])
        log_p_x += self.sucD(sucA).log_prob(x[:,29-self.num_latent])
        log_p_x += self.yhdM(sucA).log_prob(x[:,30-self.num_latent])
        log_p_x += self.atpG(sucA).log_prob(x[:,31-self.num_latent])
        log_p_x += self.ibpB(eutG, yceP).log_prob(x[:,32-self.num_latent])
        log_p_x += self.yfaD(eutG, sucA, yceP).log_prob(x[:,33-self.num_latent])
        log_p_x += self.yecO(cspG).log_prob(x[:,34-self.num_latent])
        log_p_x += self.aceB(icdA).log_prob(x[:,35-self.num_latent])
        log_p_x += self.lpdA(yedE).log_prob(x[:,36-self.num_latent])
        log_p_x += self.nuoM(lacY).log_prob(x[:,37-self.num_latent])
        log_p_x += self.hupB(cspA, yfiA).log_prob(x[:,38-self.num_latent])
        log_p_x += self.b1963(yheI).log_prob(x[:,39-self.num_latent])
        log_p_x += self.folK(yheI).log_prob(x[:,40-self.num_latent])
        log_p_x += self.b1583(lacA, lacZ, yceP).log_prob(x[:,41-self.num_latent])
        log_p_x += self.yaeM(cspG, lacA, lacZ).log_prob(x[:,42-self.num_latent])
        log_p_x += self.nmpC(pspA).log_prob(x[:,43-self.num_latent])
        log_p_x += self.dnaG(ycgX, yheI).log_prob(x[:,44-self.num_latent])
        log_p_x += self.ftsJ(mopB).log_prob(x[:,45-self.num_latent])

        return log_p_x


    def log_prior(self, z):
        # p(z) = p(z0) x p(z1) x p(z2|z0,z1) x p(z3|z0,z1) x
        #            p(z4) x p(z5|z3,z4)
        b1191 = z[:,0]
        eutG = z[:,1]
        fixC = z[:,2]
        sucA = z[:,3]
        ygcE = z[:,5]

        log_p_z = self.b1191().log_prob(z[:,0])
        log_p_z += self.eutG().log_prob(z[:,1])
        log_p_z += self.fixC(b1191).log_prob(z[:,2])
        log_p_z += self.sucA(eutG).log_prob(z[:,3])
        log_p_z += self.yceP(eutG, fixC).log_prob(z[:,4])
        log_p_z += self.ygcE(b1191,sucA).log_prob(z[:,5])
        log_p_z += self.asnA(ygcE).log_prob(z[:,6])
        log_p_z += self.cspG().log_prob(z[:,7])
        log_p_z += self.atpD(sucA, ygcE).log_prob(z[:,8])
        

        return log_p_z 


    def log_joint(self, x, z):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z)
        return log_lik + log_prior


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = [
                    # Latents
                    "b1191", "eutG", "fixC", "sucA", "yceP", "ygcE",
                    "asnA", "cspG", "atpD", 
                    # Observed
                    "icdA", "lacA", "cspA", 
                    "yedE", "lacY", "yfiA", "pspB", "yheI", "lacZ",
                    "pspA", "ycgX", "dnaK", "mopB", 
                    "ygbD", "yjbO", "cchB", "tnaA", "dnaJ", "flgD", 
                    "gltA", "sucD", "yhdM", "atpG", "ibpB", "yfaD", 
                    "yecO", "aceB", "lpdA", "nuoM", "hupB", "b1963",
                    "folK", "b1583", "yaeM", "nmpC", "dnaG", "ftsJ"
                ]

        observed = {
                    "icdA", "lacA", "cspA", 
                    "yedE", "lacY", "yfiA", "pspB", "yheI", "lacZ",
                    "pspA", "ycgX", "dnaK", "mopB", 
                    "ygbD", "yjbO", "cchB", "tnaA", "dnaJ", "flgD", 
                    "gltA", "sucD", "yhdM", "atpG", "ibpB", "yfaD", 
                    "yecO", "aceB", "lpdA", "nuoM", "hupB", "b1963",
                    "folK", "b1583", "yaeM", "nmpC", "dnaG", "ftsJ"
                }

        edges = [
            ("asnA","icdA"),("asnA","lacA"),("asnA","lacY"),("asnA","lacZ"),

            ("atpD","yheI"),

            ("b1191","fixC"),("b1191","tnaA"),("b1191","ygcE"),

            ("cspA","hupB"), ("cspA","yfiA"),

            ("cspG","cspA"),("cspG","lacA"),("cspG","lacY"),("cspG","pspA"),
            ("cspG","pspB"),("cspG","yaeM"),("cspG","yecO" ),("cspG","yedE"),

            ("dnaK","mopB"),

            ("eutG","ibpB"),("eutG","lacY" ),("eutG","sucA"),("eutG","yceP"),
            ("eutG","yfaD"),

            ("fixC","cchB"),("fixC","tnaA"),("fixC","yceP"),("fixC","ycgX"),
            ("fixC","ygbD"),("fixC","yjbO"),

            ("icdA","aceB"),

            ("lacA","b1583"),("lacA","lacY"),("lacA","lacZ"),("lacA","yaeM"),

            ("lacY","lacZ"),("lacY","nuoM"),

            ("lacZ","b1583"),("lacZ","mopB"),("lacZ","yaeM"),

            ("mopB","ftsJ"),

            ("pspA","nmpC"),

            ("pspB","pspA"),

            ("sucA","atpD"),("sucA","atpG"),("sucA","dnaJ"),("sucA","flgD"),
            ("sucA","gltA"),("sucA","sucD"),("sucA","tnaA"),("sucA","yfaD"),
            ("sucA","ygcE"),("sucA","yhdM"),

            ("yceP","b1583"),("yceP","ibpB"),("yceP","yfaD"),

            ("ycgX","dnaG"),

            ("yedE","lpdA"),("yedE","pspA"),("yedE","pspB"),("yedE","yheI"),

            ("yfiA","hupB"),

            ("ygcE","asnA"),("ygcE","atpD"),("ygcE","icdA"),

            ("yheI","b1963"),("yheI","dnaG"),("yheI","dnaK"),("yheI","folK"),
            ("yheI","ycgX")
            ]  
    

        return vertices, edges, observed