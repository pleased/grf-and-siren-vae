import torch
import torchvision
import torchvision.transforms as transforms
import numpy as np
import pandas as pd
import random

from math import ceil
from sklearn.neighbors import NearestNeighbors


def load_protein(batch_size):
    train = torch.load('./data/datasets/human_protein/X_train.pkt')
    test = torch.load('./data/datasets/human_protein/X_valid.pkt')

    mu, sigma = train.mean(0), train.std(0)
    train = (train - mu)/sigma
    test = (test - mu)/sigma

    train, val = torch.utils.data.random_split(train, [9000, 1000])

    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=1672, shuffle=True)

    return trainloader, valloader, testloader


def load_ecoli70():
    # Load data
    train = torch.load('./data/datasets/ecoli/upsampled_ecoli_train.pt')
    test = torch.load('./data/datasets/ecoli/upsampled_ecoli_test.pt')

    # train, val = torch.utils.data.random_split(train, [72, 20])

    trainloader = torch.utils.data.DataLoader(train, batch_size=72, shuffle=True)
    # valloader = torch.utils.data.DataLoader(val, batch_size=20, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=30, shuffle=True)
    # print('Data dimension: ', len(train[0]))
    # print('Train dataset size: ', 72)
    # print('Validation dataset size: ', 20)
    # print('Test dataset size: ', 30)
    return trainloader, testloader


def load_arth150():
    # Load data
    # 210 upsampled training points
    # 3 test samples 
    # 4 val samples 
    train = torch.load('./data/datasets/arabidopsis/upsampled_arth150_train.pt')
    val = torch.load('./data/datasets/arabidopsis/arth150_val.pt')
    test = torch.load('./data/datasets/arabidopsis/arth150_test.pt')

    mu, sigma = train.mean(0), train.std(0)
    train = (train - mu)/sigma
    val = (val - mu)/sigma
    test = (test - mu)/sigma

    trainloader = torch.utils.data.DataLoader(train, batch_size=70, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=4, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=3, shuffle=True)
    return trainloader, valloader, testloader

    # TO UPSAMPLE ARTH150
    # import pandas as pd
    # data = pd.read_csv('./data/datasets/arabidopsis/arth150.csv')
    # data = torch.tensor(data.values)
    # train, val, test = torch.utils.data.random_split(data, [15, 4, 3])
    # data, upsamples = upsample(train.dataset[train.indices], 15)
    # print(upsamples.shape)
    # torch.save(test.dataset[test.indices], './data/datasets/arabidopsis/arth150_test.pt')
    # torch.save(val.dataset[val.indices], './data/datasets/arabidopsis/arth150_val.pt')
    # torch.save(upsamples, './data/datasets/arabidopsis/upsampled_arth150_train.pt')
    # np.savetxt("upsampled_arth150.csv", upsamples, delimiter=",")


def load_mehra():
    # 6000 training samples (5000, 1000 val)
    # 885 test samples
    df_train = pd.read_csv('./data/datasets/mehra/mehra_train.csv')
    df_test = pd.read_csv('./data/datasets/mehra/mehra_test.csv')

    train = torch.tensor(df_train.values)
    test = torch.tensor(df_test.values)

    train, val = torch.utils.data.random_split(train, [5000, 1000])
    train = train.dataset[train.indices]
    val = val.dataset[val.indices]

    # Normalize
    mu, sigma = train.mean(0), train.std(0)
    train = (train - mu)/sigma
    val = (val - mu)/sigma
    test = (test - mu)/sigma

    trainloader = torch.utils.data.DataLoader(train, batch_size=100, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=100, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=885, shuffle=True)
    
    return trainloader, valloader, testloader


def load_onehot(num_samples):
    torch.manual_seed(0)
    p = torch.tensor([1/5]*5)
    sample = torch.stack(
         [torch.zeros_like(p).scatter_(0, torch.multinomial(p,1), 1.) 
        for _ in range(num_samples)])
    return sample


def load_mnist():
    print('Loading MNIST:')
    flatten = transforms.Compose([transforms.ToTensor(),
                transforms.Lambda(lambda img: img.view(-1))])
    train_data = torchvision.datasets.MNIST(root='./data/datasets/',
                train=True, download=True, transform=flatten)
    test_data = torchvision.datasets.MNIST(root='./data/datasets/', 
                train=False, download=True, transform=flatten)
    print('Data dimension: ', len(train_data[0][0]))
    print('Train dataset size: ', len(train_data))
    print('Test dataset size: ', len(test_data))
    return train_data, test_data


def load_binary_mnist():
    print('Loading binary MNIST:')
    transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.1307,), (0.3081,)),
                lambda x: x>0,
                lambda x: x.float(),
                transforms.Lambda(lambda img: img.view(-1))
            ])
    train_data = torchvision.datasets.MNIST('./pytorch/data/', 
                train=True, download=True, transform=transform)
    test_data = torchvision.datasets.MNIST('./pytorch/data/', 
                train=False, download=True, transform=transform)
    print('Data dimension: ', len(train_data[0][0]))
    print('Train dataset size: ', len(train_data))
    print('Test dataset size: ', len(test_data))
    return train_data, test_data


def load_2_spirals(num_samples):
    rng = np.random.RandomState()
    n = np.sqrt(rng.rand(num_samples//2, 1))*540*(2*np.pi)/360
    d1x = -np.cos(n)*n + rng.rand(num_samples//2, 1)*0.5
    d1y = np.sin(n)*n + rng.rand(num_samples//2, 1)*0.5
    spirals = np.vstack((np.hstack((d1x, d1y)), np.hstack((-d1x, -d1y))))/3
    spirals += rng.randn(*spirals.shape)*0.1

    return torch.tensor(spirals, dtype=torch.float32) 


def load_pinwheel(num_samples):
    rng = np.random.RandomState()
    radial_std = 0.3
    tangential_std = 0.1
    num_classes = 5
    num_per_class = num_samples//5
    rate = 0.25
    rads = np.linspace(0, 2*np.pi, num_classes, endpoint=False)

    features = rng.randn(num_classes*num_per_class, 2) \
            * np.array([radial_std, tangential_std])
    features[:, 0] += 1.
    labels = np.repeat(np.arange(num_classes), num_per_class)

    angles = rads[labels] + rate*np.exp(features[:, 0])
    rotations = np.stack([np.cos(angles), -np.sin(angles), np.sin(angles), np.cos(angles)])
    rotations = np.reshape(rotations.T, (-1, 2, 2))

    wheels = 2*rng.permutation(np.einsum("ti,tij->tj", features, rotations))

    return torch.tensor(wheels, dtype=torch.float32)


def upsample(data, factor):
    N, D = data.shape
    n = (factor-1)*N
    n = ceil(n/5)
    upsamples =[]
    
    # Find all nearest neighbours
    neighbors = NearestNeighbors(n_neighbors=6)
    neighbors.fit(data)
    
    for i in range(n):
        # Pick random data point
        point = data[random.randint(0, N-1)].reshape(1,-1)
        
        # Find 5 nearest neighbours
        nbrs_idx = neighbors.kneighbors(point, return_distance=False).squeeze()
        
        # For each neighbour
        for nbr_idx in nbrs_idx[1:]:
            # pick random point along joining line
            nbr = data[nbr_idx]
            b = random.random()
            new_point = point + b*(nbr - point)
        
            # Add point to upsamples
            upsamples.append(new_point)
    
    return data, torch.cat(upsamples, dim=0)