import torch
import json


import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal

from graph.belief_network import BeliefNetwork


class Synthetic1(BeliefNetwork):

    def __init__(self):
        super(Synthetic1, self).__init__()

        # graph
        self.vertices, self.edges, self.observed = Synthetic1._construct_graph()
        self.intercepts = [0.349415, -0.04025017, 1.272989, -0.4264853, 2.026078, -0.2469326, 1.2654]
        self.coefficients = [[       0.0,       0.0,        0.0,        0.0],
                             [ 0.3536548,       0.0,        0.0,        0.0],
                             [-1.0584814,       0.0,        0.0,        0.0],
                             [ 0.2723900,       0.0,        0.0,        0.0],
                             [       0.0, 0.9241186,        0.0,        0.0],
                             [       0.0,       0.0, -0.9633228,        0.0],
                             [       0.0,       0.0,        0.0, -0.6420267]]
        self.stds = [0.3022864, 0.6427111, 0.7801026, 1.239623, 1.037081, 0.5392409, 0.8313535]
        self.forward_graph = ForwardGraph().initialize(self.vertices, 
                self.edges, self.observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = len(self.vertices) - len(self.observed)
        self.num_obs = len(self.observed)

    
    def get_normal(self, v, X):
        c = torch.tensor(self.coefficients[v]).to(X.device)
        mean = self.intercepts[v] + (c*X).sum(-1)
        return Normal(mean, self.stds[v])
    
    
    def sample(self, batch_size, train=True, observed_only=False):
        sample = torch.zeros((batch_size, self.num_latent + self.num_obs))

        for v in range(self.num_latent+self.num_obs):
            sample[:,v] = self.get_normal(v, sample[:,:self.num_latent]).sample()

        if observed_only:
            return sample[:,self.num_latent:]
        else:
            return sample
        

    def log_likelihood(self, x, z):
        # log p(x|z)
        log_p_x = 0

        for v in range(self.num_latent, self.num_latent + self.num_obs):
            log_p_x += self.get_normal(v, z).log_prob(x[:,v-self.num_latent])    

        return log_p_x


    def log_prior(self, z):
        # log p(z)
        log_p_z = 0

        for v in range(self.num_latent):
            log_p_z += self.get_normal(v, z).log_prob(z[:,v])
        return log_p_z 


    def log_joint(self, x, z):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z)
        return log_lik + log_prior


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['z0','z1','z2','z3','x0','x1','x2']
        observed = {'x0','x1','x2'}
        edges = [('z0','z1'),('z0','z2'),('z0','z3'),
                 ('z1','x0'),('z2','x1'),('z3','x2')]

        return vertices, edges, observed