import torch
import sys
import numpy as np

if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph
import graph.invert as invert

from torch.distributions import Normal, Laplace

from graph.belief_network import BeliefNetwork

class _WrapperGraph():

    def __init__(self, inverse=False):
        super(_WrapperGraph, self).__init__()
        self.is_inverse = inverse
        self.n = 8 + 3*6


    def num_vertices(self):
        return self.n


    def topological_order(self):
        if self.is_inverse:
            # NOTE: Below from SCCNF paper
            # return [7, 6, 3, 5, 4, 1, 2, 0]
            return [25, 24, 21, 22, 23, 5, 15, 16, 17, 3, 18, 19, 20, 4, 12, 13, 14, 2, 9, 10, 11, 1, 6, 7, 8, 0]
        else:
            return np.arange(self.n).tolist()

    
    def get_in_neighbours(self, i):
        if self.is_inverse:
            # NOTE: Below from SCCNF paper
            # if i==0:
            #     return [1,2,3]
            # elif i==1:
            #     return [3]
            # elif i==2:
            #     return [1,3]
            # elif i==3:
            #     return [6,7]
            # elif i==4:
            #     return [3,5]
            # elif i==5:
            #     return [3,7]
            # else:
            #     return []
            if i==0:
                return [1,9,10,11,
                            2,12,13,14,
                            3,15,16,17,
                            6,7,8]
            elif i==6:
                return [1,9,10,11,
                            2,12,13,14,
                            3,15,16,17]
            elif i==7:
                return [1,9,10,11,
                            2,12,13,14,
                            3,15,16,17, 
                            6]
            elif i==8:
                return [1,9,10,11,
                            2,12,13,14,
                            3,15,16,17, 
                            6,7]

            elif i==1:
                return [2,12,13,14,
                            3,15,16,17, 
                            9,10,11]
            elif i==9:
                return [2,12,13,14,
                            3,15,16,17]
            elif i==10:
                return [2,12,13,14,
                            3,15,16,17,
                            9]
            elif i==11:
                return [2,12,13,14,
                            3,15,16,17,
                            9,10]

            elif i==2:
                return [3,15,16,17,
                            12,13,14]
            elif i==12:
                return [3,15,16,17]
            elif i==13:
                return [3,15,16,17, 
                            12]
            elif i==14:
                return [3,15,16,17,
                            12,13]

            elif i==3:
                return [5,21,22,23,
                            24, 
                            15,16,17]
            elif i==15:
                return [5,21,22,23,
                            24]
            elif i==16:
                return [5,21,22,23,
                            24, 
                            15]
            elif i==17:
                return [5,21,22,23,
                            24,
                            15,16]

            elif i==4:
                return [3,15,16,17,
                            5,21,22,23, 
                            18,19,20]
            elif i==18:
                return [3,15,16,17,
                            5,21,22,23]
            elif i==19:
                return [3,15,16,17,
                            5,21,22,23,
                            18]
            elif i==20:
                return [3,15,16,17,
                            5,21,22,23,
                            18,19]

            elif i==5:
                return [24,25, 
                            21,22,23]
            elif i==21:
                return [24,25]
            elif i==22:
                return [24,25,
                            21]
            elif i==23:
                return [24,25,
                            21,22]
                
            else:
                return []
            
        else:
            if i==2:
                return [0,1]
            elif i==3:
                return [0,1]
            elif i==5:
                return [3,4]
            elif i==6:
                return [3]
            elif i==7:
                return [5]
            else:
                return []


    def get_out_neighbours(self, i):
        if self.is_inverse:
            # NOTE: Below from SCCNF paper
            # if i==1:
            #     return [0,2]
            # elif i==2:
            #     return [0]
            # elif i==3:
            #     return [0,1,2,4,5]
            # elif i==5:
            #     return [4]
            # elif i==6:
            #     return [3]
            # elif i==7:
            #     return [3,5]
            # else:
            #     return []
            if i==0:
                return []
            elif i==6:
                return [0, 7,8]
            elif i==7:
                return [0, 8]
            elif i==8:
                return [0]

            elif i==1:
                return [0, 6,7,8]
            elif i==9:
                return [1, 10,11,
                            0,6,7,8]
            elif i==10:
                return [1, 11, 
                            0,6,7,8]
            elif i==11:
                return [1,
                            0,6,7,8]
                    
            elif i==2:
                return [0, 6,7,8, 1, 9,10,11]
            elif i==12:
                return [2,13,14,
                            0,6,7,8,
                            1,9,10,11]
            elif i==13:
                return [2,14,
                            0,6,7,8,
                            1,9,10,11]
            elif i==14:
                return [2,
                            0,6,7,8,
                            1,9,10,11]
                    
            elif i==3:
                return [0, 6,7,8, 1, 9,10,11, 2, 12,13,14, 4, 18,19,20]
            elif i==15:
                return [3, 16,17,
                            0,6,7,8,
                            1,9,10,11,
                            2,12,13,14,
                            4,18,19,20]
            elif i==16:
                return [3, 17,
                            0,6,7,8,
                            1,9,10,11,
                            2,12,13,14,
                            4,18,19,20]
            elif i==17:
                return [3,
                            0,6,7,8,
                            1,9,10,11,
                            2,12,13,14,
                            4,18,19,20]

            elif i==4:
                return []
            elif i==18:
                return [4, 19,20]
            elif i==19:
                return [4, 20]
            elif i==20:
                return [4]
                    
            elif i==5:
                return [3, 15,16,17, 4, 18,19,20]
            elif i==21:
                return [5, 22,23,
                            3,15,16,17,
                            4,18,19,20]
            elif i==22:
                return [5, 23,
                            3,15,16,17,
                            4,18,19,20]
            elif i==23:
                return [5,
                            3,15,16,17,
                            4,18,19,20]
                    
            elif i==6:
                return [3, 15,16,17, 5, 21,22,23]

            elif i==7:
                return [5, 21,22,23]

            else:
                return []
         
        else:
            if i==0:
                return [2,3]
            elif i==1:
                return [2,3]
            elif i==3:
                return [5,6]
            elif i==4:
                return [5]
            elif i==5:
                return [7]
            else:
                return []

    
    def get_vertices(self):
        return np.arange(self.n)


class AugmentedArithmeticCircuit(BeliefNetwork):

    def __init__(self, mul=False):
        super(AugmentedArithmeticCircuit, self).__init__()
        self.mul = mul
        
        # Node distributions
        self.z0 = Laplace(5,1)
        self.z1 = Laplace(-2,1)
        self.z2 = self.z2tmp
        self.z3 = self.z3tmp
        self.z4 = Normal(7,2)
        self.z5 = self.z5tmp

        self.x0 = self.x0tmp
        self.x1 = self.x1tmp

        # Augmentation
        self.a = Normal(0,1)
        self.num_augment = 3*6

        # graph
        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph()
            self.inverse_graph = _WrapperGraph(inverse=True)
        else:
            self.forward_graph = ForwardGraph().initialize(*AugmentedArithmeticCircuit._construct_graph())
            self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = 2


    def z2tmp(self, z0, z1):
        return Normal(torch.tanh(z0 + z1 - 2.8), 0.1)

    def z3tmp(self, z0, z1): 
        if self.mul:
            return Normal(z0*z1, 0.1)
        else:
            return Normal(z0+z1, 0.1)

    def z5tmp(self, z3, z4): 
        return Normal(torch.tanh(z3 + z4), 0.1)

    def x0tmp(self, z3): 
        return Normal(z3, 0.1)

    def x1tmp(self, z5): 
        return Normal(z5, 0.1)


    def sample(self, batch_size, train=True):
        z0 = self.z0.sample((batch_size,1))
        z1 = self.z1.sample((batch_size,1))
        z2 = self.z2(z0, z1).sample()
        z3 = self.z3(z0, z1).sample()
        z4 = self.z4.sample((batch_size,1))
        z5 = self.z5(z3, z4).sample()

        a = self.a.sample((batch_size, self.num_augment))

        x0 = self.x0(z3).sample()
        x1 = self.x1(z5).sample()

        sample = torch.cat([z0,z1,z2,z3,z4,z5,a,x0,x1], dim=1)
        return sample


    def log_likelihood(self, x, z):
        # p(x|z) = p(x0|z3)p(x1|z5)
        assert x.shape[1] == 2
        log_p_x0 = self.x0(z[:,3]).log_prob(x[:,0])
        log_p_x1 = self.x1(z[:,5]).log_prob(x[:,1])
        return log_p_x0 + log_p_x1


    def log_prior(self, z, a):
        # p(z) = p(z0) x p(z1) x p(z2|z0,z1) x p(z3|z0,z1) x
        #            p(z4) x p(z5|z3,z4)
        log_p_z = self.z0.log_prob(z[:,0])
        log_p_z += self.z1.log_prob(z[:,1])
        log_p_z += self.z2(z[:,0], z[:,1]).log_prob(z[:,2])
        log_p_z += self.z3(z[:,0], z[:,1]).log_prob(z[:,3])
        log_p_z += self.z4.log_prob(z[:,4])
        log_p_z += self.z5(z[:,3], z[:,4]).log_prob(z[:,5])

        log_p_a = self.a.log_prob(a).sum(dim=1)

        return log_p_z + log_p_a


    def log_joint(self, x, z, a):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z, a)
        return log_lik + log_prior


    def get_num_latent(self):
        return 24


    def get_num_obs(self):
        return 2


    def get_num_vertices(self):
        return 26


    def topological_order(self,):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['z0', 'z1', 'z2', 'z3', 'z4', 'z5', 
                    'x0', 'x1']
        edges = [('z0','z2'), ('z0','z3'), ('z1','z2'),
                 ('z1','z3'), ('z3','z5'), ('z3','x0'), 
                 ('z4','z5'), ('z5','x1')]  
        observed = {'x0', 'x1'}
        return vertices, edges, observed