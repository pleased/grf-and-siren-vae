import torch
import sys
import numpy as np

if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph.invert as invert

from sklearn import datasets

from graph.belief_network import BeliefNetwork

class SwissRoll(BeliefNetwork):

    def __init__(self):
        super(SwissRoll, self).__init__()

        # graph
        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph()
            self.inverse_graph = _WrapperGraph(inverse=True)
        else:
            self.forward_graph = ForwardGraph().initialize(*SwissRoll._construct_graph())
            self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 2


    def sample(self, batch_size, train=True):
        sr = (datasets.make_swiss_roll(n_samples=batch_size, noise=1.0)[0])[:, [0, 2]]
        sr /= 5
        sr= torch.tensor(sr, dtype=torch.float32)

        return sr


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for SwissRoll BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for SwissRoll BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for SwissRoll BN")


    def get_num_latent(self):
        return 0


    def get_num_obs(self):
        return 2


    def get_num_vertices(self):
        return 2


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['x0','x1']
        edges = [('x1','x0')]  
        observed = {'x0','x1'}
        return vertices, edges, observed