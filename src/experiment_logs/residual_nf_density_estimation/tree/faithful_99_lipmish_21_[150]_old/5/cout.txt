INFO - residual_nf - Running command 'run'
INFO - residual_nf - Started run with ID "5"
{'bn': 'tree2', 'res_type': 'graphical-lipschitz', 'coeff': 0.99, 'n_power_iterations': 5, 'num_blocks': 21, 'hidden_dims': [150], 'activation': 'lipmish', 'grad_in_forward': True, 'batch_size': 100, 'num_train_samples': 10000, 'num_test_samples': 5000, 'num_steps': 200, 'num_train_batches': 100, 'num_test_batches': 50, 'lr': 0.01, 'lr_decay': 0.1, 'lr_decay_step_size': 20, 'seed': 4, 'ex_name': 'residual_nf_density_estimation', 'sub_folder': 'tree2_graphical-lipschitz_99_lipmish_21_[150]', 'path': './experiment_logs/residual_nf_density_estimation/tree2_graphical-lipschitz_99_lipmish_21_[150]', 'bt_config': {'depth': 5, 'seed': 4}}
[  1]: train nll: 9.99873; test nll: 9.81660
[  2]: train nll: 9.57730; test nll: 9.22956
[  3]: train nll: 9.01015; test nll: 8.78653
[  4]: train nll: 8.82759; test nll: 8.82802
[  5]: train nll: 8.84997; test nll: 8.73820
[  6]: train nll: 8.78747; test nll: 8.78527
[  7]: train nll: 8.74264; test nll: 8.73372
[  8]: train nll: 8.78435; test nll: 8.70748
[  9]: train nll: 8.73596; test nll: 8.74167
[ 10]: train nll: 8.72452; test nll: 8.67908
[ 11]: train nll: 8.74895; test nll: 8.73690
[ 12]: train nll: 8.74602; test nll: 8.71657
[ 13]: train nll: 8.73182; test nll: 8.74565
[ 14]: train nll: 8.71647; test nll: 8.66570
[ 15]: train nll: 8.71966; test nll: 8.67154
[ 16]: train nll: 8.72869; test nll: 8.76749
[ 17]: train nll: 8.69697; test nll: 8.71829
[ 18]: train nll: 8.70973; test nll: 8.67116
[ 19]: train nll: 8.69244; test nll: 8.66951
[ 20]: train nll: 8.68905; test nll: 8.68236
[ 21]: train nll: 8.73538; test nll: 8.65952
[ 22]: train nll: 8.73998; test nll: 8.70719
[ 23]: train nll: 8.69837; test nll: 8.71157
[ 24]: train nll: 8.69882; test nll: 8.69546
[ 25]: train nll: 8.70774; test nll: 8.65566
[ 26]: train nll: 8.68363; test nll: 8.70115
[ 27]: train nll: 8.68061; test nll: 8.64039
[ 28]: train nll: 8.71615; test nll: 8.73439
[ 29]: train nll: 8.69987; test nll: 8.70428
[ 30]: train nll: 8.70985; test nll: 8.63990
[ 31]: train nll: 8.69556; test nll: 8.66625
[ 32]: train nll: 8.70181; test nll: 8.68118
[ 33]: train nll: 8.68028; test nll: 8.69459
[ 34]: train nll: 8.68481; test nll: 8.72085
[ 35]: train nll: 8.69345; test nll: 8.72781
[ 36]: train nll: 8.69071; test nll: 8.66254
[ 37]: train nll: 8.66466; test nll: 8.65560
[ 38]: train nll: 8.67822; test nll: 8.68512
[ 39]: train nll: 8.72426; test nll: 8.69055
[ 40]: train nll: 8.67038; test nll: 8.72435
[ 41]: train nll: 8.69707; test nll: 8.61868
[ 42]: train nll: 8.67339; test nll: 8.65790
[ 43]: train nll: 8.68885; test nll: 8.70021
[ 44]: train nll: 8.68923; test nll: 8.74303
[ 45]: train nll: 8.68785; test nll: 8.74597
[ 46]: train nll: 8.69313; test nll: 8.66279
[ 47]: train nll: 8.68603; test nll: 8.78523
Epoch    48: reducing learning rate of group 0 to 1.0000e-03.
[ 48]: train nll: 8.70898; test nll: 8.66927
[ 49]: train nll: 8.67150; test nll: 8.62023
[ 50]: train nll: 8.61597; test nll: 8.66022
[ 51]: train nll: 8.62016; test nll: 8.62057
[ 52]: train nll: 8.63382; test nll: 8.67769
[ 53]: train nll: 8.62185; test nll: 8.63742
[ 54]: train nll: 8.62012; test nll: 8.61098
[ 55]: train nll: 8.62353; test nll: 8.61781
[ 56]: train nll: 8.62416; test nll: 8.61670
[ 57]: train nll: 8.62579; test nll: 8.54864
[ 58]: train nll: 8.64993; test nll: 8.60166
[ 59]: train nll: 8.64085; test nll: 8.62587
[ 60]: train nll: 8.61442; test nll: 8.63250
[ 61]: train nll: 8.66031; test nll: 8.63124
[ 62]: train nll: 8.59663; test nll: 8.60348
[ 63]: train nll: 8.62130; test nll: 8.56988
[ 64]: train nll: 8.63758; test nll: 8.67355
[ 65]: train nll: 8.62168; test nll: 8.59801
[ 66]: train nll: 8.63890; test nll: 8.64611
[ 67]: train nll: 8.63657; test nll: 8.58715
[ 68]: train nll: 8.61278; test nll: 8.63506
[ 69]: train nll: 8.62952; test nll: 8.61615
[ 70]: train nll: 8.63703; test nll: 8.62944
[ 71]: train nll: 8.65862; test nll: 8.62006
[ 72]: train nll: 8.65392; test nll: 8.64762
Epoch    73: reducing learning rate of group 0 to 1.0000e-04.
[ 73]: train nll: 8.61649; test nll: 8.65547
[ 74]: train nll: 8.61479; test nll: 8.61275
[ 75]: train nll: 8.59361; test nll: 8.65750
[ 76]: train nll: 8.59009; test nll: 8.62182
[ 77]: train nll: 8.65352; test nll: 8.64545
[ 78]: train nll: 8.61908; test nll: 8.62163
[ 79]: train nll: 8.63120; test nll: 8.62089
[ 80]: train nll: 8.63629; test nll: 8.60437
[ 81]: train nll: 8.58137; test nll: 8.65130
[ 82]: train nll: 8.60910; test nll: 8.60700
[ 83]: train nll: 8.63282; test nll: 8.61626
[ 84]: train nll: 8.62281; test nll: 8.62966
[ 85]: train nll: 8.61856; test nll: 8.64145
[ 86]: train nll: 8.62576; test nll: 8.58506
[ 87]: train nll: 8.58430; test nll: 8.61931
[ 88]: train nll: 8.59864; test nll: 8.60629
[ 89]: train nll: 8.61230; test nll: 8.57189
[ 90]: train nll: 8.58265; test nll: 8.63147
[ 91]: train nll: 8.62351; test nll: 8.65552
Epoch    92: reducing learning rate of group 0 to 1.0000e-05.
[ 92]: train nll: 8.58131; test nll: 8.59544
[ 93]: train nll: 8.64395; test nll: 8.57021
[ 94]: train nll: 8.60560; test nll: 8.62963
[ 95]: train nll: 8.63919; test nll: 8.60913
[ 96]: train nll: 8.60977; test nll: 8.63636
[ 97]: train nll: 8.62920; test nll: 8.62733
[ 98]: train nll: 8.60635; test nll: 8.59924
[ 99]: train nll: 8.61364; test nll: 8.57381
[100]: train nll: 8.61350; test nll: 8.62727
[101]: train nll: 8.60810; test nll: 8.57938
[102]: train nll: 8.60115; test nll: 8.62919
Epoch   103: reducing learning rate of group 0 to 1.0000e-06.
[103]: train nll: 8.59546; test nll: 8.67107
[104]: train nll: 8.59589; test nll: 8.58666
[105]: train nll: 8.59803; test nll: 8.60383
[106]: train nll: 8.62271; test nll: 8.67696
[107]: train nll: 8.62119; test nll: 8.62921
[108]: train nll: 8.62694; test nll: 8.66322
[109]: train nll: 8.61295; test nll: 8.60849
[110]: train nll: 8.61609; test nll: 8.60904
[111]: train nll: 8.61486; test nll: 8.61801
[112]: train nll: 8.60249; test nll: 8.61297
[113]: train nll: 8.61104; test nll: 8.60692
[114]: train nll: 8.60318; test nll: 8.60270
[115]: train nll: 8.61559; test nll: 8.56493
[116]: train nll: 8.60378; test nll: 8.62128
[117]: train nll: 8.58114; test nll: 8.63013
[118]: train nll: 8.60493; test nll: 8.61188
[119]: train nll: 8.63007; test nll: 8.57296
[120]: train nll: 8.55281; test nll: 8.59385
[121]: train nll: 8.60985; test nll: 8.60960
[122]: train nll: 8.57328; test nll: 8.61325
[123]: train nll: 8.59198; test nll: 8.56351
[124]: train nll: 8.61249; test nll: 8.64454
[125]: train nll: 8.59155; test nll: 8.65828
[126]: train nll: 8.60542; test nll: 8.59260
[127]: train nll: 8.58154; test nll: 8.60157
[128]: train nll: 8.59695; test nll: 8.59566
[129]: train nll: 8.64931; test nll: 8.57366
[130]: train nll: 8.59736; test nll: 8.60420
[131]: train nll: 8.61681; test nll: 8.58361
[132]: train nll: 8.63646; test nll: 8.59930
[133]: train nll: 8.58563; test nll: 8.65052
[134]: train nll: 8.62820; test nll: 8.59192
[135]: train nll: 8.60784; test nll: 8.59451
[136]: train nll: 8.65163; test nll: 8.61168
[137]: train nll: 8.61224; test nll: 8.62651
[138]: train nll: 8.65868; test nll: 8.63776
[139]: train nll: 8.63373; test nll: 8.64695
[140]: train nll: 8.60313; test nll: 8.60921
[141]: train nll: 8.63508; test nll: 8.61095
[142]: train nll: 8.59863; test nll: 8.61177
[143]: train nll: 8.62138; test nll: 8.62855
[144]: train nll: 8.62311; test nll: 8.60329
[145]: train nll: 8.62059; test nll: 8.61169
[146]: train nll: 8.59898; test nll: 8.63295
[147]: train nll: 8.62238; test nll: 8.61391
[148]: train nll: 8.61794; test nll: 8.63338
[149]: train nll: 8.60240; test nll: 8.60797
[150]: train nll: 8.59798; test nll: 8.61394
[151]: train nll: 8.60049; test nll: 8.60039
[152]: train nll: 8.62002; test nll: 8.61196
[153]: train nll: 8.60763; test nll: 8.65239
[154]: train nll: 8.62677; test nll: 8.58784
[155]: train nll: 8.62962; test nll: 8.57818
[156]: train nll: 8.62797; test nll: 8.62807
[157]: train nll: 8.62502; test nll: 8.61863
[158]: train nll: 8.62417; test nll: 8.61482
[159]: train nll: 8.58764; test nll: 8.61976
[160]: train nll: 8.60382; test nll: 8.60614
[161]: train nll: 8.60568; test nll: 8.58274
[162]: train nll: 8.61727; test nll: 8.58545
[163]: train nll: 8.61419; test nll: 8.65705
[164]: train nll: 8.62649; test nll: 8.63531
[165]: train nll: 8.61588; test nll: 8.59292
[166]: train nll: 8.62427; test nll: 8.62055
[167]: train nll: 8.62253; test nll: 8.66146
[168]: train nll: 8.58890; test nll: 8.62850
[169]: train nll: 8.62071; test nll: 8.62714
[170]: train nll: 8.65044; test nll: 8.59474
[171]: train nll: 8.62904; test nll: 8.57368
[172]: train nll: 8.61781; test nll: 8.59621
[173]: train nll: 8.60002; test nll: 8.58849
[174]: train nll: 8.60042; test nll: 8.60240
[175]: train nll: 8.61134; test nll: 8.60232
[176]: train nll: 8.60182; test nll: 8.64167
[177]: train nll: 8.59514; test nll: 8.59227
[178]: train nll: 8.63587; test nll: 8.61117
[179]: train nll: 8.61303; test nll: 8.59474
[180]: train nll: 8.62757; test nll: 8.61643
[181]: train nll: 8.59187; test nll: 8.55046
[182]: train nll: 8.61866; test nll: 8.59113
[183]: train nll: 8.61394; test nll: 8.61071
[184]: train nll: 8.61449; test nll: 8.63656
[185]: train nll: 8.59269; test nll: 8.60284
[186]: train nll: 8.61746; test nll: 8.59814
[187]: train nll: 8.59688; test nll: 8.63545
[188]: train nll: 8.61181; test nll: 8.61218
[189]: train nll: 8.61979; test nll: 8.62111
[190]: train nll: 8.63015; test nll: 8.62825
[191]: train nll: 8.63121; test nll: 8.65287
[192]: train nll: 8.63862; test nll: 8.64287
[193]: train nll: 8.56699; test nll: 8.62916
[194]: train nll: 8.60310; test nll: 8.63609
[195]: train nll: 8.62263; test nll: 8.65646
[196]: train nll: 8.59824; test nll: 8.58615
[197]: train nll: 8.60099; test nll: 8.63902
[198]: train nll: 8.59900; test nll: 8.58026
[199]: train nll: 8.59540; test nll: 8.64350
[200]: train nll: 8.63578; test nll: 8.59043

-- Invertibility Verification --
Largest singular values of the weight matrices of each layer of residual block i:
For invertibility: sigma_max < 1 and will approximately = 0.99
[0] [0.9899999685865837, 0.990000044449166]
[1] [0.9899999514959139, 0.9900583516859262]
[2] [0.9899999656419095, 0.9899999814369559]
[3] [0.9900000449263513, 0.9900000147739192]
[4] [0.9899999748887836, 0.989999975347319]
[5] [0.9900000137865312, 0.9899999728176972]
[6] [0.989999966254869, 0.9900000361646871]
[7] [0.9899999781136299, 0.9900000025247887]
[8] [0.9899999899569426, 0.8074784928876122]
[9] [0.9900000010200125, 0.9899999817316366]
[10] [0.9899999798360861, 0.989999991196392]
[11] [0.9899999994149288, 0.9899999901310846]
[12] [0.9900000006879623, 0.23647527232342083]
[13] [0.9900000214145265, 0.49212668194332543]
[14] [0.9899999667651909, 0.9900000089233103]
[15] [0.9900000356310489, 0.34921757910489876]
[16] [0.9900000183385654, 0.33223949689237686]
[17] [0.9900000420101688, 0.9673746719562231]
[18] [0.9900000183127413, 0.5295563911399863]
[19] [0.9899999753068313, 0.4281809233951815]
[20] [0.9900479598409507, 0.9899999632715814]
