import torch
import graph_tool.all as gt
import graph.invert as invert

from torch.distributions import Normal
from graph.belief_network import BeliefNetwork
from graph.forward_graph import ForwardGraph

class LargerBN1(BeliefNetwork):

    def __init__(self):
        super(LargerBN1, self).__init__()

        self.forward_graph = ForwardGraph().initialize(*LargerBN1._construct_graph())
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 17
        self.num_obs = 5


    
    def sample(self, batch_size, train=True):
        """Draw samples from the forward graph g."""
        z0 = Normal(0, 5).sample((batch_size,1))
        z1 = Normal(z0, 0.1).sample()
        z2 = Normal(z1, 0.1).sample()
        z3 = Normal(z1, 0.1).sample()
        z4 = Normal(z2, 0.1).sample()
        z5 = Normal(z2, 0.1).sample()
        z6 = Normal(z3, 0.1).sample()
        z7 = Normal(z4 + z5, 0.1).sample()
        z8 = Normal(z6 + z5, 0.1).sample()
        z9 = Normal(z0 + z6, 0.1).sample()
        z10 = Normal(z7 + z8, 0.1).sample()
        z11 = Normal(z7 + z8 + z5, 0.1).sample()
        z12 = Normal(z6 + z8 + z5, 0.1).sample()
        z13 = Normal(z9 + z6, 0.1).sample()
        z14 = Normal(z11 + z10, 0.1).sample()
        z15 = Normal(z2 + z12 + z11, 0.1).sample()
        z16 = Normal(z13 + z12, 0.1).sample()

        x0 = Normal(z14, 0.1).sample()
        x1 = Normal(z15, 0.1).sample()
        x2 = Normal(z15, 0.1).sample()
        x3 = Normal(z16, 0.1).sample()
        x4 = Normal(z9, 0.1).sample()

        return torch.cat([z0, z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, 
                          z14, z15, z16, x0, x1, x2, x3, x4], dim=1)


    def log_likelihood(self, x, z):
        """Compute the log-likelihood log(p(x|z))"""
        log_p_x_z =  Normal(z[:,14], 0.1).log_prob(x[:,0])
        log_p_x_z +=  Normal(z[:,15], 0.1).log_prob(x[:,1])
        log_p_x_z +=  Normal(z[:,15], 0.1).log_prob(x[:,2])
        log_p_x_z +=  Normal(z[:,16], 0.1).log_prob(x[:,3])
        log_p_x_z +=  Normal(z[:,9], 0.1).log_prob(x[:,4])

        return log_p_x_z


    def log_prior(self, z):
        """Compute the log-prior log(p(z))"""
        log_p_z =  Normal(0, 5).log_prob(z[:, 0])
        log_p_z += Normal(z[:,0], 0.1).log_prob(z[:,1])
        log_p_z += Normal(z[:,1], 0.1).log_prob(z[:,2])
        log_p_z += Normal(z[:,1], 0.1).log_prob(z[:,3])
        log_p_z += Normal(z[:,2], 0.1).log_prob(z[:,4])
        log_p_z += Normal(z[:,2], 0.1).log_prob(z[:,5])
        log_p_z += Normal(z[:,3], 0.1).log_prob(z[:,6])
        log_p_z += Normal(z[:,4] + z[:,5], 0.1).log_prob(z[:,7])
        log_p_z += Normal(z[:,6] + z[:,5], 0.1).log_prob(z[:,8])
        log_p_z += Normal(z[:,0] + z[:,6], 0.1).log_prob(z[:,9])
        log_p_z += Normal(z[:,7] + z[:,8], 0.1).log_prob(z[:,10])
        log_p_z += Normal(z[:,7] + z[:,8] + z[:,5], 0.1).log_prob(z[:,11])
        log_p_z += Normal(z[:,6] + z[:,8] + z[:,5], 0.1).log_prob(z[:,12])
        log_p_z += Normal(z[:,9] + z[:,6], 0.1).log_prob(z[:,13])
        log_p_z += Normal(z[:,11] + z[:,10], 0.1).log_prob(z[:,14])
        log_p_z += Normal(z[:,2] + z[:,12] + z[:,11], 0.1).log_prob(z[:,15])
        log_p_z += Normal(z[:,13] + z[:,12], 0.1).log_prob(z[:,16])
        
        return log_p_z


    def log_joint(self, x, z):
        """Compute the log-joint log(p(x,z))"""
        log_p_x = self.log_prior(z)
        log_p_x_z = self.log_likelihood(x,z)
        return log_p_x + log_p_x_z


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_obs + self.num_latent


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['z{}'.format(i) for i in range(17)] +\
                   ['x{}'.format(i) for i in range(5)]
        edges = [('z0','z1'), ('z0','z9'), ('z1','z2'), ('z1','z3'), ('z2','z4'),
                 ('z2','z5'), ('z2','z15'), ('z3','z6'), ('z4','z7'), ('z5','z7'),
                 ('z5','z8'), ('z5','z11'), ('z5','z12'), ('z6','z8'), ('z6','z9'),
                 ('z6','z12'), ('z6','z13'), ('z7','z10'), ('z7','z11'), ('z8','z10'),
                 ('z8','z11'), ('z8','z12'), ('z9','z13'), ('z9','x4'), ('z10','z14'),
                 ('z11','z14'), ('z11','z15'), ('z12','z15'), ('z12','z16'), ('z13','z16'),
                 ('z14','x0'), ('z15','x1'), ('z15','x2'), ('z16','x3')]  
        observed = {'x{}'.format(i) for i in range(5)}
        return vertices, edges, observed