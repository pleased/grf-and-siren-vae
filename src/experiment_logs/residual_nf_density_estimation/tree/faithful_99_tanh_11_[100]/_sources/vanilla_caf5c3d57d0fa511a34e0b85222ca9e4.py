import graph_tool.all as gt

from graph.forward_graph import ForwardGraph
from graph.belief_network import BeliefNetwork

class VanillaBN(BeliefNetwork):
    def __init__(self, num_latent, num_obs):
        super(VanillaBN, self).__init__()
        
        self.num_latent = num_latent
        self.num_obs = num_obs
        
        forward_graph = VanillaBN._construct_forward_graph(self.num_latent, self.num_obs)
        inverse_graph = VanillaBN._construct_inverse_graph(self.num_latent, self.num_obs)
        self.forward_graph = ForwardGraph().initialize(*forward_graph)
        self.inverse_graph = ForwardGraph().initialize(*inverse_graph)
    
    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 
        
        
    @staticmethod
    def _construct_forward_graph(num_latent, num_obs):
        vertices = ['z{}'.format(i) for i in range(num_latent)] +\
                   ['x{}'.format(i) for i in range(num_obs)]
        
        observed = {'x{}'.format(i) for i in range(num_obs)}
        
        edges = [('z{}'.format(i),'x{}'.format(j)) for i in range(num_latent) 
                    for j in range(num_obs)]
        
        return vertices, edges, observed
    
    @staticmethod
    def _construct_inverse_graph(num_latent, num_obs):
        vertices = ['z{}'.format(i) for i in range(num_latent)] +\
                   ['x{}'.format(i) for i in range(num_obs)]
        
        observed = {'x{}'.format(i) for i in range(num_obs)}
        
        edges = [('x{}'.format(j),'z{}'.format(i)) for i in range(num_latent) 
                    for j in range(num_obs)]
        
        return vertices, edges, observed