INFO - residual_nf - Running command 'run'
INFO - residual_nf - Started run with ID "4"
{'bn': 'arithmetic-mul-fc', 'res_type': 'graphical-lipschitz', 'coeff': 0.99, 'n_power_iterations': 50, 'num_blocks': 17, 'hidden_dims': [200], 'activation': 'lipmish', 'grad_in_forward': True, 'batch_size': 100, 'num_train_samples': 10000, 'num_test_samples': 5000, 'num_steps': 200, 'num_train_batches': 100, 'num_test_batches': 50, 'lr': 0.01, 'lr_decay': 0.1, 'lr_decay_step_size': 20, 'seed': 135505, 'ex_name': 'residual_nf_density_estimation', 'sub_folder': 'arithmetic-mul-fc_graphical-lipschitz_99_lipmish_17_[200]', 'path': './experiment_logs/residual_nf_density_estimation/arithmetic-mul-fc_graphical-lipschitz_99_lipmish_17_[200]', 'bt_config': {'depth': 5, 'seed': 4}}
[  1]: train nll: 15.16325; test nll: 8.44085
[  2]: train nll: 7.36308; test nll: 6.58458
[  3]: train nll: 5.76960; test nll: 5.00293
[  4]: train nll: 4.69714; test nll: 5.71477
[  5]: train nll: 4.01481; test nll: 3.33089
[  6]: train nll: 3.50677; test nll: 3.12966
[  7]: train nll: 3.33177; test nll: 2.97372
[  8]: train nll: 2.98974; test nll: 2.73497
[  9]: train nll: 3.02113; test nll: 2.74657
[ 10]: train nll: 2.83307; test nll: 2.76382
[ 11]: train nll: 2.84546; test nll: 2.53899
[ 12]: train nll: 2.75851; test nll: 2.33502
[ 13]: train nll: 2.71128; test nll: 2.37945
[ 14]: train nll: 2.54818; test nll: 2.40940
[ 15]: train nll: 2.52218; test nll: 1.97504
[ 16]: train nll: 2.47856; test nll: 2.11957
[ 17]: train nll: 2.39259; test nll: 2.34006
[ 18]: train nll: 2.27857; test nll: 2.04646
[ 19]: train nll: 2.35398; test nll: 2.37206
[ 20]: train nll: 2.30238; test nll: 1.94452
[ 21]: train nll: 2.25980; test nll: 2.03338
[ 22]: train nll: 2.51549; test nll: 2.44006
[ 23]: train nll: 2.15241; test nll: 2.12440
[ 24]: train nll: 2.19573; test nll: 2.10795
[ 25]: train nll: 2.12641; test nll: 2.48761
[ 26]: train nll: 2.10830; test nll: 2.31444
[ 27]: train nll: 2.13064; test nll: 1.72621
[ 28]: train nll: 2.25648; test nll: 1.82856
[ 29]: train nll: 2.08745; test nll: 1.95024
[ 30]: train nll: 1.97662; test nll: 2.02872
[ 31]: train nll: 2.09470; test nll: 1.85728
[ 32]: train nll: 2.00229; test nll: 2.25536
[ 33]: train nll: 2.01618; test nll: 1.92613
[ 34]: train nll: 1.97591; test nll: 1.82146
[ 35]: train nll: 1.96935; test nll: 1.97965
[ 36]: train nll: 2.05623; test nll: 2.03129
[ 37]: train nll: 2.01447; test nll: 1.97486
[ 38]: train nll: 1.89438; test nll: 1.84744
[ 39]: train nll: 1.87682; test nll: 1.72119
[ 40]: train nll: 1.84842; test nll: 1.72562
[ 41]: train nll: 1.90426; test nll: 1.63023
[ 42]: train nll: 1.97922; test nll: 1.91761
[ 43]: train nll: 1.93257; test nll: 1.72582
[ 44]: train nll: 2.33387; test nll: 1.78583
[ 45]: train nll: 1.89979; test nll: 1.64095
[ 46]: train nll: 1.90585; test nll: 1.57174
[ 47]: train nll: 1.95941; test nll: 1.75333
[ 48]: train nll: 1.93794; test nll: 1.74834
[ 49]: train nll: 1.82930; test nll: 1.78923
[ 50]: train nll: 1.91975; test nll: 1.62388
[ 51]: train nll: 1.79107; test nll: 2.03861
[ 52]: train nll: 1.93646; test nll: 1.58098
[ 53]: train nll: 1.83012; test nll: 1.61430
[ 54]: train nll: 1.78378; test nll: 1.70065
[ 55]: train nll: 1.83117; test nll: 1.70661
[ 56]: train nll: 1.77873; test nll: 1.75930
[ 57]: train nll: 1.74616; test nll: 1.66870
[ 58]: train nll: 1.71182; test nll: 1.52273
[ 59]: train nll: 1.76269; test nll: 1.68807
[ 60]: train nll: 1.68097; test nll: 1.58373
[ 61]: train nll: 1.83806; test nll: 1.76055
[ 62]: train nll: 1.82320; test nll: 1.71508
[ 63]: train nll: 1.75635; test nll: 1.48358
[ 64]: train nll: 1.75964; test nll: 1.68870
[ 65]: train nll: 1.77779; test nll: 1.74189
[ 66]: train nll: 1.79985; test nll: 1.50093
[ 67]: train nll: 1.75916; test nll: 1.63651
[ 68]: train nll: 1.78891; test nll: 1.57639
[ 69]: train nll: 1.66271; test nll: 1.59402
[ 70]: train nll: 1.75822; test nll: 1.62640
[ 71]: train nll: 1.67589; test nll: 2.05895
[ 72]: train nll: 1.73582; test nll: 1.47879
[ 73]: train nll: 1.79017; test nll: 1.91445
[ 74]: train nll: 1.69907; test nll: 1.77479
[ 75]: train nll: 1.71451; test nll: 1.72655
[ 76]: train nll: 1.71731; test nll: 1.79333
[ 77]: train nll: 1.70420; test nll: 1.56691
[ 78]: train nll: 1.68605; test nll: 1.60381
[ 79]: train nll: 1.81066; test nll: 1.64608
Epoch    80: reducing learning rate of group 0 to 1.0000e-03.
[ 80]: train nll: 1.67809; test nll: 1.66144
[ 81]: train nll: 1.33681; test nll: 1.24352
[ 82]: train nll: 1.33276; test nll: 1.36246
[ 83]: train nll: 1.27409; test nll: 1.34498
[ 84]: train nll: 1.29066; test nll: 1.27521
[ 85]: train nll: 1.31111; test nll: 1.22528
[ 86]: train nll: 1.28142; test nll: 1.32282
[ 87]: train nll: 1.33610; test nll: 1.31921
[ 88]: train nll: 1.28426; test nll: 1.23579
[ 89]: train nll: 1.31241; test nll: 1.40432
[ 90]: train nll: 1.27181; test nll: 1.31394
[ 91]: train nll: 1.28288; test nll: 1.30963
[ 92]: train nll: 1.29586; test nll: 1.23581
[ 93]: train nll: 1.29654; test nll: 1.25017
[ 94]: train nll: 1.28289; test nll: 1.39433
[ 95]: train nll: 1.28480; test nll: 1.26655
[ 96]: train nll: 1.27881; test nll: 1.27759
[ 97]: train nll: 1.27890; test nll: 1.31032
[ 98]: train nll: 1.27487; test nll: 1.40851
[ 99]: train nll: 1.31749; test nll: 1.30527
[100]: train nll: 1.24665; test nll: 1.23745
[101]: train nll: 1.29235; test nll: 1.29015
[102]: train nll: 1.30009; test nll: 1.24595
[103]: train nll: 1.29757; test nll: 1.25233
[104]: train nll: 1.33605; test nll: 1.31158
[105]: train nll: 1.24547; test nll: 1.23230
[106]: train nll: 1.27503; test nll: 1.24027
[107]: train nll: 1.26038; test nll: 1.28073
[108]: train nll: 1.24099; test nll: 1.31710
[109]: train nll: 1.26600; test nll: 1.25890
[110]: train nll: 1.34409; test nll: 1.24750
[111]: train nll: 1.25770; test nll: 1.32548
[112]: train nll: 1.28324; test nll: 1.37677
[113]: train nll: 1.26627; test nll: 1.30709
[114]: train nll: 1.27405; test nll: 1.27979
[115]: train nll: 1.28286; test nll: 1.27510
[116]: train nll: 1.25629; test nll: 1.28456
[117]: train nll: 1.26703; test nll: 1.24415
[118]: train nll: 1.26546; test nll: 1.28322
Epoch   119: reducing learning rate of group 0 to 1.0000e-04.
[119]: train nll: 1.30027; test nll: 1.26911
[120]: train nll: 1.20844; test nll: 1.19141
[121]: train nll: 1.21474; test nll: 1.13302
[122]: train nll: 1.23284; test nll: 1.22518
[123]: train nll: 1.20782; test nll: 1.17691
[124]: train nll: 1.24216; test nll: 1.19726
[125]: train nll: 1.23969; test nll: 1.23429
[126]: train nll: 1.23069; test nll: 1.17687
[127]: train nll: 1.22590; test nll: 1.20095
[128]: train nll: 1.23373; test nll: 1.18141
[129]: train nll: 1.19655; test nll: 1.20220
[130]: train nll: 1.20680; test nll: 1.21773
[131]: train nll: 1.19730; test nll: 1.23416
[132]: train nll: 1.20883; test nll: 1.19600
[133]: train nll: 1.21933; test nll: 1.16739
[134]: train nll: 1.20216; test nll: 1.23985
[135]: train nll: 1.20219; test nll: 1.20702
[136]: train nll: 1.22547; test nll: 1.19971
[137]: train nll: 1.22354; test nll: 1.16229
[138]: train nll: 1.27160; test nll: 1.16161
[139]: train nll: 1.22019; test nll: 1.26846
Epoch   140: reducing learning rate of group 0 to 1.0000e-05.
[140]: train nll: 1.19853; test nll: 1.21710
[141]: train nll: 1.19758; test nll: 1.22820
[142]: train nll: 1.22251; test nll: 1.13436
[143]: train nll: 1.18345; test nll: 1.21414
[144]: train nll: 1.15900; test nll: 1.18268
[145]: train nll: 1.22476; test nll: 1.23181
[146]: train nll: 1.22742; test nll: 1.19096
[147]: train nll: 1.16156; test nll: 1.23986
[148]: train nll: 1.21724; test nll: 1.28588
[149]: train nll: 1.23476; test nll: 1.18031
[150]: train nll: 1.27187; test nll: 1.24197
[151]: train nll: 1.21744; test nll: 1.13297
[152]: train nll: 1.18830; test nll: 1.25187
[153]: train nll: 1.17412; test nll: 1.12678
[154]: train nll: 1.22030; test nll: 1.23623
Epoch   155: reducing learning rate of group 0 to 1.0000e-06.
[155]: train nll: 1.23185; test nll: 1.17650
[156]: train nll: 1.15816; test nll: 1.21793
[157]: train nll: 1.18480; test nll: 1.15268
[158]: train nll: 1.21130; test nll: 1.20735
[159]: train nll: 1.21429; test nll: 1.22611
[160]: train nll: 1.17176; test nll: 1.16645
[161]: train nll: 1.20964; test nll: 1.19437
[162]: train nll: 1.16088; test nll: 1.20326
[163]: train nll: 1.19412; test nll: 1.18770
[164]: train nll: 1.16518; test nll: 1.18962
[165]: train nll: 1.19283; test nll: 1.18856
[166]: train nll: 1.18812; test nll: 1.20980
[167]: train nll: 1.22390; test nll: 1.18332
[168]: train nll: 1.20313; test nll: 1.19394
[169]: train nll: 1.16826; test nll: 1.17222
[170]: train nll: 1.16961; test nll: 1.18090
[171]: train nll: 1.18532; test nll: 1.27311
[172]: train nll: 1.18287; test nll: 1.21314
[173]: train nll: 1.21861; test nll: 1.14273
[174]: train nll: 1.19843; test nll: 1.22762
[175]: train nll: 1.16228; test nll: 1.25117
[176]: train nll: 1.19164; test nll: 1.16357
[177]: train nll: 1.19805; test nll: 1.20220
[178]: train nll: 1.23962; test nll: 1.18128
[179]: train nll: 1.19838; test nll: 1.24088
[180]: train nll: 1.19331; test nll: 1.19778
[181]: train nll: 1.21502; test nll: 1.23795
[182]: train nll: 1.23167; test nll: 1.23426
[183]: train nll: 1.22181; test nll: 1.27992
[184]: train nll: 1.15605; test nll: 1.16059
[185]: train nll: 1.19255; test nll: 1.14811
[186]: train nll: 1.20135; test nll: 1.20682
[187]: train nll: 1.16149; test nll: 1.19664
[188]: train nll: 1.25330; test nll: 1.17267
[189]: train nll: 1.24692; test nll: 1.22271
[190]: train nll: 1.20303; test nll: 1.23834
[191]: train nll: 1.19293; test nll: 1.18437
[192]: train nll: 1.21866; test nll: 1.18913
[193]: train nll: 1.21446; test nll: 1.20378
[194]: train nll: 1.16458; test nll: 1.20993
[195]: train nll: 1.21095; test nll: 1.21039
[196]: train nll: 1.20422; test nll: 1.14470
[197]: train nll: 1.22213; test nll: 1.19288
[198]: train nll: 1.21143; test nll: 1.15083
[199]: train nll: 1.16755; test nll: 1.19558
[200]: train nll: 1.19897; test nll: 1.19185

-- Invertibility Verification --
Largest singular values of the weight matrices of each layer of residual block i:
For invertibility: sigma_max < 1 and will approximately = 0.99
[0] [0.9900000334967773, 0.98999997560763]
[1] [0.9899999729149451, 0.9900000319273708]
[2] [0.9900000027809625, 0.9900000157021385]
[3] [0.9900000206055882, 0.9899999889039517]
[4] [0.9900000143759586, 0.9899999666803517]
[5] [0.9899999688021686, 0.990000011091954]
[6] [0.9900000129666247, 0.9900000541650323]
[7] [0.9900003524213562, 0.990000020642572]
[8] [0.9900000021519632, 0.9899999884113121]
[9] [0.9900000040713752, 0.9900000284363217]
[10] [0.9900006699427519, 0.9900220015070796]
[11] [0.9900195131991366, 0.9900021245322898]
[12] [0.9900000277558858, 0.990000041005288]
[13] [0.9900004114891996, 0.9900477873338351]
[14] [0.9900189443767661, 0.9900240144967303]
[15] [0.9900000366989937, 0.9899999905864486]
[16] [0.9900036438764682, 0.9900000274450825]
