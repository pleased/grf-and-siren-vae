import graph_tool.all as gt
import graph.invert as invert
import numpy as np

from graph.forward_graph import ForwardGraph
from graph.belief_network import BeliefNetwork


class FCBN(BeliefNetwork):
    def __init__(self, num_latent, num_obs, true_bn=None):
        super(FCBN, self).__init__()
        
        self.num_latent = num_latent
        self.num_obs = num_obs
        self.true_bn = true_bn
        
        vertices, edges, observed = FCBN._construct_graph(self.num_latent, self.num_obs)
        self.vertices = vertices
        self.edges = edges
        self.observed = observed
        self.forward_graph = ForwardGraph().initialize(vertices, edges, observed)
        self.inverse_graph = invert.properly(self.forward_graph)


    def sample(self, batch_size, train=True):
        return self.true_bn.sample(batch_size, train)


    def log_joint(self, x, z):
        return self.true_bn.log_joint(x, z)
    
    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 
        
        
    @staticmethod
    def _construct_graph(num_latent, num_obs):
        vertices = ['z{}'.format(i) for i in range(num_latent)] +\
                   ['x{}'.format(i) for i in range(num_obs)]
        
        observed = {'x{}'.format(i) for i in range(num_obs)}
        

        edges = [('z{}'.format(i),'z{}'.format(j)) for i in range(num_latent)
                    for j in range(i+1,num_latent)] +\
                [('z{}'.format(i),'x{}'.format(j)) for i in range(num_latent) 
                    for j in range(num_obs)]
                
        return vertices, edges, observed