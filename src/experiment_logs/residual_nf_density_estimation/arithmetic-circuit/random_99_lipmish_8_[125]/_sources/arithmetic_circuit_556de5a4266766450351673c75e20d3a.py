import torch
import sys
import numpy as np

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal, Laplace

from graph.belief_network import BeliefNetwork


class ArithmeticCircuit(BeliefNetwork):

    def __init__(self, mul=False, structure='faithful'):
        super(ArithmeticCircuit, self).__init__()
        self.mul = mul
        
        # Node distributions
        self.z0 = Laplace(5,1)
        self.z1 = Laplace(-2,1)
        self.z2 = self.z2tmp
        self.z3 = self.z3tmp
        self.z4 = Normal(7,2)
        self.z5 = self.z5tmp

        self.x0 = self.x0tmp
        self.x1 = self.x1tmp

        # graph
        self.forward_graph = ForwardGraph().initialize(*ArithmeticCircuit._construct_graph(structure))
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = 2


    def z2tmp(self, z0, z1):
        return Normal(torch.tanh(z0 + z1 - 2.8), 0.1)

    def z3tmp(self, z0, z1): 
        if self.mul:
            return Normal(z0*z1, 0.1)
        else:
            return Normal(z0+z1, 0.1)

    def z5tmp(self, z3, z4): 
        return Normal(torch.tanh(z3 + z4), 0.1)

    def x0tmp(self, z3): 
        return Normal(z3, 0.1)

    def x1tmp(self, z5): 
        return Normal(z5, 0.1)


    def sample(self, batch_size, train=True):
        z0 = self.z0.sample((batch_size,1))
        z1 = self.z1.sample((batch_size,1))
        z2 = self.z2(z0, z1).sample()
        z3 = self.z3(z0, z1).sample()
        z4 = self.z4.sample((batch_size,1))
        z5 = self.z5(z3, z4).sample()

        x0 = self.x0(z3).sample()
        x1 = self.x1(z5).sample()

        sample = torch.cat([z0,z1,z2,z3,z4,z5,x0,x1], dim=1)
        return sample


    def log_likelihood(self, x, z):
        # p(x|z) = p(x0|z3)p(x1|z5)
        assert x.shape[1] == 2
        log_p_x0 = self.x0(z[:,3]).log_prob(x[:,0])
        if torch.isnan(log_p_x0).any():
            print('log_p_x0 contains nan')
            print(torch.min(z[:,3]))
        log_p_x1 = self.x1(z[:,5]).log_prob(x[:,1])
        if torch.isnan(log_p_x1).any():
            print('log_p_x1 contains nan')
            print(torch.min(z[:,5]))
        return log_p_x0 + log_p_x1


    def log_prior(self, z):
        # p(z) = p(z0) x p(z1) x p(z2|z0,z1) x p(z3|z0,z1) x
        #            p(z4) x p(z5|z3,z4)

        log_p_z = self.z0.log_prob(z[:,0]) 
        if torch.isnan(log_p_z).any():
            print('log_p_z (1) contains nan')
            print(torch.min(z[:,0]))
        log_p_z += self.z1.log_prob(z[:,1])
        if torch.isnan(log_p_z).any():
            print('log_p_z (2) contains nan')
            print(torch.min(z[:,1]))
        log_p_z += self.z2(z[:,0], z[:,1]).log_prob(z[:,2]) 
        if torch.isnan(log_p_z).any():
            print('log_p_z (3) contains nan')
            print(torch.min(z[:,0]))
            print(torch.min(z[:,1]))
            print(torch.min(z[:,2]))
        log_p_z += self.z3(z[:,0], z[:,1]).log_prob(z[:,3]) 
        if torch.isnan(log_p_z).any():
            print('log_p_z (4) contains nan')
            print(torch.min(z[:,0]))
            print(torch.min(z[:,3]))
        log_p_z += self.z4.log_prob(z[:,4]) 
        if torch.isnan(log_p_z).any():
            print('log_p_z (5) contains nan')
            print(torch.min(z[:,4]))
        log_p_z += self.z5(z[:,3], z[:,4]).log_prob(z[:,5])
        if torch.isnan(log_p_z).any():
            print('log_p_z (6) contains nan')
            print(torch.min(z[:,3]))
            print(torch.min(z[:,4]))
            print(torch.min(z[:,5]))

        return log_p_z 


    def log_joint(self, x, z):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z)
        return log_lik + log_prior

    
    def sample_base_prior(self, batch_size):
        z0 = Normal(0,1).sample((batch_size,1))
        z1 = Normal(0,1).sample((batch_size,1))
        z2 = Normal(0,1).sample((batch_size,1))
        z3 = Normal(0,1).sample((batch_size,1))
        z4 = Normal(0,1).sample((batch_size,1))
        z5 = Normal(0,1).sample((batch_size,1))

        return torch.cat([z0,z1,z2,z3,z4,z5], dim=1)

    def log_base_prior(self, z):
        # p(z) = p(z0) x p(z1) x p(z2|z0,z1) x p(z3|z0,z1) x
        #            p(z4) x p(z5|z3,z4)

        log_p_z = Normal(0,1).log_prob(z[:,0])
        log_p_z += Normal(0,1).log_prob(z[:,1])
        log_p_z += Normal(0,1).log_prob(z[:,2])
        log_p_z += Normal(0,1).log_prob(z[:,3])
        log_p_z += Normal(0,1).log_prob(z[:,4])
        log_p_z += Normal(0,1).log_prob(z[:,5])

        return log_p_z 


    def get_num_latent(self):
        return 6


    def get_num_obs(self):
        return 2


    def get_num_vertices(self):
        return 8


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph(structure):
        vertices = ['z0', 'z1', 'z2', 'z3', 'z4', 'z5', 
                        'x0', 'x1']
        observed = {'x0', 'x1'}

        if structure == 'faithful':
            edges = [('z0','z2'), ('z0','z3'), ('z1','z2'),
                    ('z1','z3'), ('z3','z5'), ('z3','x0'), 
                    ('z4','z5'), ('z5','x1')]  
        
        elif structure == 'fully-connected':
            edges = [
            ('z0','z1'), ('z0','z2'), ('z0','z3'), ('z0','z4'), ('z0','z5'),
             ('z0','x0'), ('z0','x1'),
            ('z1','z2'), ('z1','z3'), ('z1','z4'), ('z1','z5'), ('z1','x0'),
             ('z1','x1'),
            ('z2','z3'), ('z2','z4'), ('z2','z5'), ('z2','x0'), ('z2','x1'),
            ('z3','z4'), ('z3','z5'), ('z3','x0'), ('z3','x1'),
            ('z4','z5'), ('z4','x0'), ('z4','x1'),
            ('z5','x0'), ('z5','x1'),
            ('x0','x1')]

        elif structure == 'random':
            edges = [('z0','x0'), ('z2','z0'), ('z2','z1'), ('z3','z1'),
            ('z3','z5'), ('z4','z2'), ('z4','x1'), ('x1','z5')]
        
        return vertices, edges, observed
