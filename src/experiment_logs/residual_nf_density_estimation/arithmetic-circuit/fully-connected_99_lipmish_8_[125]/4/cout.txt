INFO - residual_nf - Running command 'run'
INFO - residual_nf - Started run with ID "4"
{'bn': 'arithmetic-mul-fc', 'res_type': 'graphical-lipschitz', 'coeff': 0.99, 'n_power_iterations': 50, 'num_blocks': 8, 'hidden_dims': [125], 'activation': 'lipmish', 'grad_in_forward': True, 'batch_size': 100, 'num_train_samples': 10000, 'num_test_samples': 5000, 'num_steps': 200, 'num_train_batches': 100, 'num_test_batches': 50, 'lr': 0.01, 'lr_decay': 0.1, 'lr_decay_step_size': 20, 'seed': 135504, 'ex_name': 'residual_nf_density_estimation', 'sub_folder': 'arithmetic-mul-fc_graphical-lipschitz_99_lipmish_8_[125]', 'path': './experiment_logs/residual_nf_density_estimation/arithmetic-mul-fc_graphical-lipschitz_99_lipmish_8_[125]', 'bt_config': {'depth': 5, 'seed': 4}}
[  1]: train nll: 17.97685; test nll: 9.27392
[  2]: train nll: 7.71235; test nll: 6.02443
[  3]: train nll: 5.80095; test nll: 4.95317
[  4]: train nll: 4.68076; test nll: 4.04171
[  5]: train nll: 4.21279; test nll: 4.17312
[  6]: train nll: 4.04625; test nll: 3.54784
[  7]: train nll: 3.48406; test nll: 3.10745
[  8]: train nll: 3.36510; test nll: 2.97674
[  9]: train nll: 3.27929; test nll: 3.22801
[ 10]: train nll: 3.24892; test nll: 3.01866
[ 11]: train nll: 2.98234; test nll: 2.93017
[ 12]: train nll: 3.01527; test nll: 3.02671
[ 13]: train nll: 2.96744; test nll: 2.56837
[ 14]: train nll: 3.00181; test nll: 2.82783
[ 15]: train nll: 2.80972; test nll: 2.40885
[ 16]: train nll: 2.76071; test nll: 2.52735
[ 17]: train nll: 2.75063; test nll: 2.53004
[ 18]: train nll: 2.70768; test nll: 2.36170
[ 19]: train nll: 2.73350; test nll: 2.40446
[ 20]: train nll: 2.52671; test nll: 2.46486
[ 21]: train nll: 2.58663; test nll: 2.73539
[ 22]: train nll: 2.62173; test nll: 2.41139
[ 23]: train nll: 2.50419; test nll: 2.36792
[ 24]: train nll: 2.39746; test nll: 2.44620
[ 25]: train nll: 2.59479; test nll: 2.29999
[ 26]: train nll: 2.48180; test nll: 2.46045
[ 27]: train nll: 2.46057; test nll: 2.29970
[ 28]: train nll: 2.44787; test nll: 2.23796
[ 29]: train nll: 2.37843; test nll: 2.27545
[ 30]: train nll: 2.31485; test nll: 2.29990
[ 31]: train nll: 2.37956; test nll: 2.19658
[ 32]: train nll: 2.30011; test nll: 2.14028
[ 33]: train nll: 2.43916; test nll: 2.44893
[ 34]: train nll: 2.30713; test nll: 2.08783
[ 35]: train nll: 2.41343; test nll: 2.50516
[ 36]: train nll: 2.34821; test nll: 2.12375
[ 37]: train nll: 2.36292; test nll: 2.10330
[ 38]: train nll: 2.25188; test nll: 2.44047
[ 39]: train nll: 2.22962; test nll: 1.91997
[ 40]: train nll: 2.20218; test nll: 2.09645
[ 41]: train nll: 2.35768; test nll: 2.15551
[ 42]: train nll: 2.23608; test nll: 1.98276
[ 43]: train nll: 2.21663; test nll: 2.06535
[ 44]: train nll: 2.20492; test nll: 2.57991
[ 45]: train nll: 2.14234; test nll: 1.92742
[ 46]: train nll: 2.17581; test nll: 2.00042
[ 47]: train nll: 2.20059; test nll: 2.34390
[ 48]: train nll: 2.15572; test nll: 2.14654
[ 49]: train nll: 2.15194; test nll: 1.90631
[ 50]: train nll: 2.20168; test nll: 1.91286
[ 51]: train nll: 2.06327; test nll: 2.01881
[ 52]: train nll: 2.09298; test nll: 2.20364
[ 53]: train nll: 2.09704; test nll: 1.86494
[ 54]: train nll: 2.23567; test nll: 2.30534
[ 55]: train nll: 2.16158; test nll: 1.92435
[ 56]: train nll: 2.06412; test nll: 2.01954
[ 57]: train nll: 2.14053; test nll: 1.96982
[ 58]: train nll: 2.05738; test nll: 2.09950
[ 59]: train nll: 2.11978; test nll: 2.11916
[ 60]: train nll: 2.12161; test nll: 2.05330
[ 61]: train nll: 2.08162; test nll: 2.12293
[ 62]: train nll: 2.09049; test nll: 1.98659
[ 63]: train nll: 2.03270; test nll: 2.43524
[ 64]: train nll: 1.98740; test nll: 1.88286
[ 65]: train nll: 2.06274; test nll: 2.16310
[ 66]: train nll: 2.09583; test nll: 1.94749
[ 67]: train nll: 1.97941; test nll: 1.84466
[ 68]: train nll: 2.07291; test nll: 2.08555
[ 69]: train nll: 2.08086; test nll: 1.80638
[ 70]: train nll: 2.00438; test nll: 1.85245
[ 71]: train nll: 2.04709; test nll: 2.03665
[ 72]: train nll: 2.08832; test nll: 2.05329
[ 73]: train nll: 2.05720; test nll: 1.83304
[ 74]: train nll: 1.99021; test nll: 2.26723
[ 75]: train nll: 2.00701; test nll: 2.02906
[ 76]: train nll: 1.98705; test nll: 1.89002
[ 77]: train nll: 2.09381; test nll: 2.15577
Epoch    78: reducing learning rate of group 0 to 1.0000e-03.
[ 78]: train nll: 2.04874; test nll: 1.95197
[ 79]: train nll: 1.66668; test nll: 1.70194
[ 80]: train nll: 1.64208; test nll: 1.65204
[ 81]: train nll: 1.62433; test nll: 1.57788
[ 82]: train nll: 1.60643; test nll: 1.64055
[ 83]: train nll: 1.58770; test nll: 1.53506
[ 84]: train nll: 1.64754; test nll: 1.53752
[ 85]: train nll: 1.58288; test nll: 1.57931
[ 86]: train nll: 1.57263; test nll: 1.61017
[ 87]: train nll: 1.58202; test nll: 1.56147
[ 88]: train nll: 1.62416; test nll: 1.62857
[ 89]: train nll: 1.59735; test nll: 1.57228
[ 90]: train nll: 1.51457; test nll: 1.52395
[ 91]: train nll: 1.62626; test nll: 1.56994
[ 92]: train nll: 1.56275; test nll: 1.58537
[ 93]: train nll: 1.60508; test nll: 1.59896
[ 94]: train nll: 1.57946; test nll: 1.54036
[ 95]: train nll: 1.52789; test nll: 1.54559
[ 96]: train nll: 1.58601; test nll: 1.64405
[ 97]: train nll: 1.60239; test nll: 1.56765
[ 98]: train nll: 1.55750; test nll: 1.57574
[ 99]: train nll: 1.53999; test nll: 1.61480
[100]: train nll: 1.60063; test nll: 1.59697
Epoch   101: reducing learning rate of group 0 to 1.0000e-04.
[101]: train nll: 1.59076; test nll: 1.59551
[102]: train nll: 1.51886; test nll: 1.57494
[103]: train nll: 1.52302; test nll: 1.50978
[104]: train nll: 1.54817; test nll: 1.47732
[105]: train nll: 1.51976; test nll: 1.57358
[106]: train nll: 1.55908; test nll: 1.55546
[107]: train nll: 1.55302; test nll: 1.51291
[108]: train nll: 1.56045; test nll: 1.50367
[109]: train nll: 1.52610; test nll: 1.53739
[110]: train nll: 1.52686; test nll: 1.47167
[111]: train nll: 1.58093; test nll: 1.51504
Epoch   112: reducing learning rate of group 0 to 1.0000e-05.
[112]: train nll: 1.55935; test nll: 1.52359
[113]: train nll: 1.53342; test nll: 1.57139
[114]: train nll: 1.53222; test nll: 1.46764
[115]: train nll: 1.54679; test nll: 1.47798
[116]: train nll: 1.58896; test nll: 1.48771
[117]: train nll: 1.57825; test nll: 1.52216
[118]: train nll: 1.55047; test nll: 1.52841
[119]: train nll: 1.53351; test nll: 1.47161
[120]: train nll: 1.49850; test nll: 1.56619
[121]: train nll: 1.54385; test nll: 1.57257
[122]: train nll: 1.57691; test nll: 1.46259
[123]: train nll: 1.51615; test nll: 1.55246
[124]: train nll: 1.55123; test nll: 1.53211
[125]: train nll: 1.49883; test nll: 1.60866
[126]: train nll: 1.51232; test nll: 1.51546
[127]: train nll: 1.51454; test nll: 1.51828
[128]: train nll: 1.55074; test nll: 1.48631
[129]: train nll: 1.51389; test nll: 1.55849
[130]: train nll: 1.52285; test nll: 1.51423
Epoch   131: reducing learning rate of group 0 to 1.0000e-06.
[131]: train nll: 1.53045; test nll: 1.50865
[132]: train nll: 1.52668; test nll: 1.42732
[133]: train nll: 1.51739; test nll: 1.60950
[134]: train nll: 1.48721; test nll: 1.51258
[135]: train nll: 1.53416; test nll: 1.50852
[136]: train nll: 1.53485; test nll: 1.44972
[137]: train nll: 1.47762; test nll: 1.51717
[138]: train nll: 1.50605; test nll: 1.48587
[139]: train nll: 1.55047; test nll: 1.53719
[140]: train nll: 1.51275; test nll: 1.47553
[141]: train nll: 1.50554; test nll: 1.47739
[142]: train nll: 1.53704; test nll: 1.54415
[143]: train nll: 1.49428; test nll: 1.54919
[144]: train nll: 1.50798; test nll: 1.50132
[145]: train nll: 1.56852; test nll: 1.50300
[146]: train nll: 1.56339; test nll: 1.51428
[147]: train nll: 1.51288; test nll: 1.53226
[148]: train nll: 1.52002; test nll: 1.48918
[149]: train nll: 1.45135; test nll: 1.54797
[150]: train nll: 1.51442; test nll: 1.49979
[151]: train nll: 1.48394; test nll: 1.55748
[152]: train nll: 1.49494; test nll: 1.50937
[153]: train nll: 1.52483; test nll: 1.52977
[154]: train nll: 1.52231; test nll: 1.51915
[155]: train nll: 1.56654; test nll: 1.51495
[156]: train nll: 1.54544; test nll: 1.53190
[157]: train nll: 1.56574; test nll: 1.50077
[158]: train nll: 1.56811; test nll: 1.57822
[159]: train nll: 1.50838; test nll: 1.52360
[160]: train nll: 1.50375; test nll: 1.50992
[161]: train nll: 1.58688; test nll: 1.52930
[162]: train nll: 1.51789; test nll: 1.54296
