import torch
import sys
import numpy as np
import pandas as pd

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from graph.belief_network import BeliefNetwork

class Mehra(BeliefNetwork):

    def __init__(self):
        super(Mehra, self).__init__()

        # graph
        vertices, edges, observed = Mehra._construct_graph()
        self.edges = edges
        self.vertices = vertices
        self.observed = observed
        self.forward_graph = ForwardGraph().initialize(vertices, edges, observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 3
        self.num_obs = 7

        # train, test = self.load_data()
        # np.random.shuffle(train)
        # np.random.shuffle(test)
        # self.train_idx = 0
        # self.test_idx = 0
        # self.train = train.astype(np.float32)
        # self.test = test.astype(np.float32)
        # self.train_N = self.train.shape[0]
        # self.test_N = self.test.shape[0]

    def load_data(self):
        dir_f = './data/datasets/mehra/'

        train = pd.read_csv(dir_f + "mehra_train.csv").values
        mu, sigma = train.mean(0), train.std(0)
        test =  pd.read_csv(dir_f + "mehra_test.csv").values
 
        return (train - mu)/sigma, (test - mu)/sigma

    
    def sample(self, batch_size, train=True):
        if train:
            return self._sample_train(batch_size)
        else:
            return self._sample_test(batch_size)


    def _sample_train(self, batch_size):
        sample = self.train.take(
            range(self.train_idx,self.train_idx + batch_size),
            axis=0, mode='wrap')
        self.train_idx = (self.train_idx + batch_size)%self.train_N
        return torch.tensor(sample)


    def _sample_test(self, batch_size):
        sample = self.test.take(
            range(self.test_idx, self.test_idx + batch_size),
            axis=0, mode='wrap')
        self.test_idx = (self.test_idx + batch_size)%self.test_N
        return torch.tensor(sample)


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for EightPairs BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for EightPairs BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for EightPairs BN")


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs

    
    def get_num_edges(self):
        return 10


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['wd', 'co', 'pm10', 't2m', 'ws', 'blh', 'ssr', 'no2', 'so2',
                    'pm2.5']

        observed = {'t2m', 'ws', 'blh', 'ssr', 'no2', 'so2', 'pm2.5'}

        edges = [('wd','t2m'),('wd','ws'),('wd','blh'),('wd','ssr'),
                 ('co','no2'),('co','so2'),('co','ssr'),
                 ('pm10','no2'),('pm10','ssr'),('pm10','pm2.5')]  
            
       
        return vertices, edges, observed