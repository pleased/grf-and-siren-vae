import torch
import numpy as np
import matplotlib.pyplot as plt
import torch.distributions as dist
import json

from sacred import Experiment 
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange
from scipy.stats import kde

from ex_binary_tree_config import bt_ingredient, create_tree
from modules.factory import build_discrete_nf_density_estimation
from ex_utils import batch_iterator, count_parameters, sample_batch, kolmogorov_smirnov
from graph.belief_network import *
from data.load_data import load_protein, load_mehra, load_dataset


ex = Experiment('discrete_nf', ingredients=[bt_ingredient])
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"

@ex.command(unobserved=True)
def plot_samples(_config):
     # BN initialization
    if _config['bn'] == 'arithmetic-circuit':
        bn = ArithmeticCircuit()
    elif _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif _config['bn'] == 'tree':
        bn = Tree(device)
    elif _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif _config['bn'] == 'arth150-synth':
        bn = Arth150()
    elif _config['bn'] == 'binary-tree':
        bn = create_tree()
        ex.info['tree_coeffs'] = bn.coeffs.tolist()
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))

    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # Load model
    model = torch.load(_config['path']+'/{}/model.pt'.format(_config['run'])).to(device)
    if _config['double']:
        model = model.double()
    
    # Some plots
    with torch.no_grad():
        batch = sample_batch(bn.sample, 100000)
        true_sample = (batch).double().to(device)

        z0 = dist.Normal(
                loc=torch.zeros(n, dtype=torch.float64),
                scale=torch.ones(n, dtype=torch.float64)
            ).sample((100000,)).to(device)
        if _config['normalizer_type'] == 'affine':
            model_sample, _ = model.inverse(z0)
        else:
            model_sample, _ = model.inverse_fixed_point(z0)

        z, j = model(model_sample)
        print('log p(x) = ', model.ll(z, j).item())

        label = ['True','Flow']
        
        for i in range(n):
            ztrue = true_sample[:,i].cpu().tolist()
            zflow = model_sample[:,i].cpu().tolist()
            plt.figure()
            plt.hist(x=[ztrue, zflow], bins=50, alpha=0.5, 
                            histtype='stepfilled', density=True,
                            color=['steelblue', 'red'], edgecolor='none',
                            label=label)
            plt.xlabel("Node "+str(i))
            plt.ylabel("Density")
            plt.legend()
            plt.savefig(_config['path']+'/{}/{}'.format(_config['run'],i))



def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network 
    bn = 'arithmetic-circuit2'
    # Conditioner [dag-masked, dag]
    conditioner_type = 'dag-masked'
    # Normalizer [affine, monotonic]
    normalizer_type = 'affine'
    # Monotonic normalizer args
    int_net_hidden = [100]
    int_net_final_activation = 'elu'
    cond_size = 10
    mono_num_steps = 15
    mono_solver = 'CC'  # or 'CCParallel'

    num_flow_steps = 1
    hidden_dims = [100]

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_epochs = 100
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size

    lr = 1e-2
    seed = 0
    patience = 20
    run = 1
    double = False

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False

    # Add observer
    ex_name = 'discrete_nf_density_estimation'
    sub_folder = '{}_{}_{}_{}'.format(conditioner_type, normalizer_type, num_flow_steps, hidden_dims)
    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _run):
    # Training info
    batch_size = _config['batch_size']
    lr = _config['lr'] 
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    hidden_dims = _config['hidden_dims']
    num_flow_steps = _config['num_flow_steps']
    checkpoint_n = _config['checkpoint_n']

    print('[CONFIG]',_config)
    print('[DEVICE]', device)

    # BN initialization
    real = False
    if _config['bn'] == 'arithmetic-circuit':
        bn = ArithmeticCircuit()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'tree':
        bn = Tree(device)
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'protein':
        bn = Protein()
        print('[LOADING DATA] Protein')
        # 9000 train samples, 1000 validation samples, batch_size=100
        trainloader, valloader, testloader = load_protein(batch_size, device, double=_config['double'], seed=_config['seed'])
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        batch_size = trainloader.batch_size
        real = True
    elif _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    # Mehra
    elif _config['bn'] == 'mehra-real':
        bn = Mehra()
        print('[LOADING DATA] MEHRA')
        trainloader, valloader, testloader = load_mehra(device, double=_config['double'])
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        batch_size = trainloader.batch_size
        real = True
    elif _config['bn'] == 'binary-tree':
        bn = create_tree()
        ex.info['tree_coeffs'] = bn.coeffs.tolist()
        train = bn.sample(_config['num_train_samples']).to(device)
        val = bn.sample(_config['num_val_samples']).to(device)
        test = bn.sample(_config['num_val_samples']).to(device)
        if _config['double']:
            train = train.double()
            val = val.double()
            test = test.double()
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
        testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)
    elif _config['bn'] == 'arth150-synth':
        bn = Arth150()
        train = bn.sample(_config['num_train_samples']).to(device)
        val = bn.sample(_config['num_val_samples']).to(device)
        test = bn.sample(_config['num_val_samples']).to(device)
        if _config['double']:
            train = train.double()
            val = val.double()
            test = test.double()
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
        testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)
    elif _config['bn'] == 'swiss-roll':
        bn = SwissRoll()
        train = bn.sample(_config['num_train_samples']).to(device)
        val = bn.sample(_config['num_val_samples']).to(device)
        test = bn.sample(_config['num_val_samples']).to(device)
        if _config['double']:
            train = train.double()
            val = val.double()
            test = test.double()
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
        testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)
    elif _config['bn'] == 'two-spirals':
        bn = TwoSpirals()
        train = bn.sample(_config['num_train_samples']).to(device)
        val = bn.sample(_config['num_val_samples']).to(device)
        test = bn.sample(_config['num_val_samples']).to(device)
        if _config['double']:
            train = train.double()
            val = val.double()
            test = test.double()
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
        testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))
    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # # Draw train, val and test sets and return minibatch iterators
    # if not real:
    #     train = bn.sample(_config['num_train_samples']).to(device)
    #     val = bn.sample(_config['num_val_samples']).to(device)
    #     test = bn.sample(_config['num_val_samples']).to(device)
    #     if _config['double']:
    #         train = train.double()
    #         val = val.double()
    #         test = test.double()
    #     trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    #     valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    #     testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)

    min_std = 1e-5
    x = torch.cat([batch for batch in trainloader], dim=0)
    x_std = torch.std(x, dim=0).to(device)
    scale = {
        'scale': torch.maximum(x_std, torch.ones_like(x_std)*min_std).to(device),
        'shift': torch.mean(x, dim=0).to(device)
    }
    del x

    # Initialize model and optimizer
    conditioner_type = _config['conditioner_type']
    normalizer_type = _config['normalizer_type']
    if normalizer_type == 'monotonic':
        mono_args = {
            'int_net_hidden': _config['int_net_hidden'],
            'int_net_final_activation':_config['int_net_final_activation'],
            'cond_size': _config['cond_size'],
            'mono_num_steps': _config['mono_num_steps'],
            'mono_solver': _config['mono_solver']
        }
        model = build_discrete_nf_density_estimation(
            num_flow_steps, bn, hidden_dims, conditioner_type, 
            normalizer_type, device, mono_args=mono_args, scale=scale).to(device)
    elif normalizer_type == 'affine':
        model = build_discrete_nf_density_estimation(
            num_flow_steps, bn, hidden_dims, conditioner_type, 
            normalizer_type, device, scale=scale).to(device)
    else:
        raise Exception("Unknown Normalizer type: {}".format(normalizer_type))
    if _config['double']:
        model = model.double() 

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True, 
                min_lr=1e-6, patience=_config['patience'])

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)
    
    # Log model capacity
    ex.info['num model params'] = model.count_parameters()

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_nll = 0.0

        model.train()
        for train_batch in trainloader:
            z0, j = model(train_batch)
            loss = -model.ll(z0, j)

            l = loss.detach().item()
            train_nll += l    

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # log training metrics - training loss, kl-divergence
        _run.log_scalar("train", value=train_nll/num_train_batches) 
        _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)


        # Log val metrics - val loss, kl-divergence
        model.eval()
        with torch.no_grad():
            val_nll = 0
            # true_nll = 0
            for val_batch in valloader:
                z0, j = model(val_batch)
                val_nll += -model.ll(z0, j).item()

                # z = val_batch[:,:num_latent]
                # x = val_batch[:,num_latent:]
                # if bn == 'arithmetic-mul':
                #     true_nll += (-graph.log_joint(x, z)).mean().item()
            # log val metrics 
            _run.log_scalar("val", value=val_nll/num_val_batches) 

        # Decay learning rate every 100 epochs
        # lr_scheduler.step()
        lr_scheduler.step(val_nll)

        epochs.set_description('Train NLL: {:.3f}, Val NLL: {:.3f}'.format(
            train_nll/num_train_batches, val_nll/num_val_batches),
            refresh=False)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1

    # Peak memory usage
    ex.info['memory usage (MB)'] = torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000 
    
    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))

    # Log test metrics - val loss, kl-divergence
    model.eval()
    with torch.no_grad():
        test_nll = 0

        num_test_batches = 0
        for test_batch in testloader:
            z0, j = model(test_batch)
            test_nll += -model.ll(z0, j).item()
            num_test_batches += 1
        # log test metrics 
        _run.log_scalar("test", value=test_nll/num_test_batches) 

    # # Density plots
    # with torch.no_grad():
    #     if bn == 'arithmetic-mul':
    #         sample= sample_batch(graph.sample, 1).double().to(device)
    #         npts = 100
    #         # True
    #         z3 = sample[:,3:4]
    #         z5 = sample[:,5:6]
    #         x = graph.x0(z3).sample((1,1000)).squeeze().cpu().numpy()
    #         y = graph.x1(z5).sample((1,1000)).squeeze().cpu().numpy()

    #         nbins=300
    #         k = kde.gaussian_kde([x,y])
    #         xmin = x.min()
    #         xmax = x.max()
    #         ymin = y.min()
    #         ymax = y.max()
    #         xi, yi = np.mgrid[xmin:xmax:nbins*1j, ymin:ymax:nbins*1j]
    #         zi = k(np.vstack([xi.flatten(), yi.flatten()]))
    #         plt.pcolormesh(xi, yi, zi.reshape(xi.shape), shading='auto')
    #         plt.savefig(path+'/{}/true'.format(_run._id))
    #         plt.show()
    #         plt.clf()


    #         # Model
    #         xside = np.linspace(xmin, xmax, npts)
    #         yside = np.linspace(ymin, ymax, npts)
    #         xx, yy = np.meshgrid(xside, yside)
    #         z = np.hstack([xx.reshape(-1, 1), yy.reshape(-1, 1)])
    #         z = torch.DoubleTensor(z).to(device)
    #         z = torch.cat(((sample[:,:num_latent]).expand(npts*npts,-1), z), dim=1)
    #         z0, j = model(z)
    #         ll = (model.eps_log_density(z0) + j).cpu().numpy()
    #         p = np.exp(ll).reshape(npts, npts)

    #         fig =  plt.figure()
    #         ax = fig.add_subplot(111)
    #         ax.pcolormesh(xx, yy, p, shading='auto')
    #         ax.invert_yaxis()
    #         # ax.get_xaxis().set_ticks([])
    #         # ax.get_yaxis().set_ticks([])
    #         plt.savefig(path+'/{}/model'.format(_run._id))
    #         plt.show()
    #         plt.clf()

    