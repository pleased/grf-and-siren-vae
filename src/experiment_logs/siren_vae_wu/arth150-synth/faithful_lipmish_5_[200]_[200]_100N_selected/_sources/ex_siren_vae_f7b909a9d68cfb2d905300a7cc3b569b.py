"""Model learning and inference using VAE-like encoder and 
    decoder. Encoder implemented using a normalzing flow."""
from sre_constants import IN
import torch
import numpy as np
import matplotlib.pyplot as plt
import torch.distributions as dist

from sacred import Experiment
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange
from sklearn.feature_selection import mutual_info_regression

from modules.factory import build_graph_vae, build_iw_graph_vae, \
                             build_dreg_iw_graph_vae
from ex_utils import batch_iterator, sample_batch, kolmogorov_smirnov
from graph.belief_network import *
from ex_plots import elbo_curves
from data.load_data import load_ecoli70

ex = Experiment('siren_vae')
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
print('Device: ', device)

@ex.command(unobserved=True)
def plot_z_distributions(_config):
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    # Load model
    path = _config['path']
    model = torch.load(path + '/{}/model.pt'.format(_config['run'])).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 20000).double().to(device)
        true_x = X[:,num_latent:]
        true_z = X[:,:num_latent]

        model_x, model_prior_z = model.generative_network.sample(20000)
        model_post_z, _, _ = model(true_x)

        for i in range(num_latent):
            x = [
                model_prior_z[:,i].cpu().tolist(),
                model_post_z[:,i].cpu().tolist(),
                true_z[:,i].cpu().tolist()
            ]
            plt.hist(x=x, bins=50, alpha=0.5, histtype='stepfilled',
                        density=True, color=['steelblue', 'red', 'gold'], edgecolor='none', 
                        label=[r'$p_\theta(z)$', r'$q(z|x)$', r'$p(z)$']) 
            plt.xlabel(r'$z_{}$'.format(i))
            plt.legend()
            plt.savefig(path + '/{}/z{}.png'.format(_config['run'], i), dpi=600, bbox_inches='tight')
            plt.clf()


@ex.command(unobserved=True)
def plot_x_distributions(_config):
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    if _config['normalize']:
        min_std = 1e-5
        batch = sample_batch(bn.sample, 10000)
        x = batch[:,num_latent:]
        z = batch[:,:num_latent]
        x_std = torch.std(x, dim=0).to(device)
        x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
        x_shift = torch.mean(x, dim=0).to(device)
        z_std = torch.std(z, dim=0).to(device)
        z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
        z_shift = torch.mean(z, dim=0).to(device)
    else:
        x_scale = 1.0
        z_scale = 1.0
        x_shift = 0.0
        z_shift = 0.0


    # Load model
    path = _config['path']
    model = torch.load(path + '/{}/model.pt'.format(_config['run'])).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 20000).double().to(device)
        true_x = X[:,num_latent:]
        true_x = (true_x - x_shift)/x_scale
        model_x, _ = model.generative_network.sample(20000)

        for i in range(num_obs):
            x = [
                model_x[:,i].cpu().tolist(),
                true_x[:,i].cpu().tolist()
            ]
            ks, _ = kolmogorov_smirnov(x[0], x[1])
            plt.hist(x=x, bins=50, alpha=0.5, histtype='stepfilled',
                        density=True, color=['steelblue', 'red'], edgecolor='none', 
                        label=[r'$p_\theta(x|z)$', r'$p(x,z)$']) 
            plt.xlabel(r'$x_{}$'.format(i))
            # plt.text(0.0,0.,"ks={:.3f}".format(ks), 
            #         # transform=ax[i,j].transAxes, 
            #         bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))

            plt.legend()
            plt.subplots_adjust(wspace=0.15, hspace=0.15)
            plt.savefig(path + '/{}/x{}.png'.format(_config['run'], i), dpi=600, bbox_inches='tight')
            plt.clf()

@ex.command(unobserved=True)
def diagnostics(_config):
    # Initialize BN
    model_path = _config['model_path']
    model_specs = model_path.split('_')
    if model_specs[0] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    path = './experiment_logs/siren_vae/'
    model = torch.load(path + model_path + '/1/model.pt').double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 1000).double().to(device)
        x = X[:,num_latent:]
        z = X[:,:num_latent]

    # Inversion verification
    print('\n-- Invertibility Verification (GRF_n) --')
    print('Largest singular values of the weight matrices of each layer of residual block i:')
    print('For invertibility: sigma_max < 1 and will approximately = {}'.format(0.99))
    for i, block in enumerate(model.generative_network.nf.steps):
        sigmas = block.g.largest_singular_values()
        print('[{}] {}'.format(i, sigmas))

    z0, _ = model.generative_network.nf(z)
    z_prime, _ = model.generative_network.nf.inverse(z0)
    print('Reconstruction error: {}'.format(torch.norm(z-z_prime).item()))

    # Inversion verification
    print('\n-- Invertibility Verification (GRF_g) --')
    print('Largest singular values of the weight matrices of each layer of residual block i:')
    print('For invertibility: sigma_max < 1 and will approximately = {}'.format(0.99))
    for i, block in enumerate(model.inference_network.nf.steps):
        sigmas = block.g.largest_singular_values()
        print('[{}] {}'.format(i, sigmas))

    eps = dist.Normal(
        loc=torch.zeros(num_latent, dtype=torch.float64),
        scale=torch.ones(num_latent, dtype=torch.float64)
    ).sample((1000,)).to(device)
    z, _ = model.inference_network.nf(x=x, eps=eps)
    eps_prime, _ = model.inference_network.nf.inverse(z, x=x)
    print('Reconstruction error: {}'.format(torch.norm(eps-eps_prime).item()))


@ex.command(unobserved=True)
def samples(_config):
    # BN + true samples
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    true_z = torch.zeros((10000, num_latent))
    true_x = torch.zeros((10000, num_obs))
    model_z = torch.zeros((10000, num_latent))
    model_x = torch.zeros((10000, num_obs))
    inferred_z = torch.zeros((10000, num_latent))

    # Model + infer
    model = torch.load(_config['path']+'/{}/model.pt'.format(_config['run'])).double().to(device)

    for i in range(5):
        t = sample_batch(bn.sample, 2000)
        true_z[i*2000:i*2000+2000,:] = t[:,:num_latent]
        true_x[i*2000:i*2000+2000,:] = t[:,num_latent:]
        
        iz, _ = model.inference_network(t[:,num_latent:].double().to(device))
        inferred_z[i*2000:i*2000+2000,:] = iz.cpu()

        mx, mz = model.sample(2000)
        model_z[i*2000:i*2000+2000,:] = mz
        model_x[i*2000:i*2000+2000,:] = mx

    samples = {
        'true_x': true_x,
        'model_x': model_x,
        'true_z': true_z,
        'model_z': model_z,
        'inferred_z': inferred_z
    }
    torch.save(samples, _config['path']+'/{}/samples.pt'.format(_config['run']))


# @ex.command(unobserved=True)
# def latent_mi(_config):
#     if _config['bn'] == 'arithmetic-circuit2':
#         bn = ArithmeticCircuit2()
#     num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()
#     num_vars = num_latent + num_obs
#     sample = sample_batch(bn.sample, 1000).cpu().numpy()

#     fig, ax = plt.subplots(1,3, figsize=(15,5), sharey=True)
#     MI = [np.zeros((num_vars, num_vars)) for i in range(3)]

#     # True
#     for v in range(num_vars):
#         mi = mutual_info_regression(sample, sample[:,v], 
#                 discrete_features=False)
#         MI[0][v] = mi
#     ax[0].set_title('Data')
#     ax[0].set_yticks(range(num_vars))
#     ax[0].set_yticklabels(['z0', 'z1', 'z2', 'z3', 'z4', 
#                     'x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9'])

#     # faithful
#     path = './experiment_logs/siren_vae/'
#     model = torch.load(path + _config['model_path'][0]+'/{}/model.pt'.format(_config['run'])).double().to(device)
#     x,z = model.sample(1000)
#     sample = torch.cat((z,x), dim=1).cpu().numpy()
#     for v in range(num_vars):
#         mi = mutual_info_regression(sample, sample[:,v], 
#                 discrete_features=False)
#         MI[1][v] = mi
#     ax[1].set_title('Faithful')

#     # fully-connected
#     path = './experiment_logs/siren_vae/'
#     model = torch.load(path + _config['model_path'][1]+'/{}/model.pt'.format(_config['run'])).double().to(device)
#     x,z = model.sample(1000)
#     sample = torch.cat((z,x), dim=1).cpu().numpy()
#     for v in range(num_vars):
#         mi = mutual_info_regression(sample, sample[:,v], 
#                 discrete_features=False)
#         MI[2][v] = mi
#     ax[2].set_title('Fully-connected')

#     for i in range(3):
#         MI[i] = MI[i] - np.diag(np.diag(MI[i]))
#     vmin = min(np.min(MI[0]), np.min(MI[1]), np.min(MI[2]))
#     vmax = max(np.max(MI[0]), np.max(MI[1]), np.max(MI[2]))
#     for i in range(3):
#         ax[i].imshow(MI[i], vmin=vmin, vmax=vmax)

#     h = np.array(range(num_vars+1))-0.5
#     for i in range(3):
#         ax[i].plot(h,(num_latent)*np.ones_like(h)-0.5,color='red',ls='dashed')
#         ax[i].plot((num_latent)*np.ones_like(h)-0.5, h,color='red',ls='dashed')
#         ax[i].set_xticks(range(num_vars))
#         ax[i].set_xticklabels(['z0', 'z1', 'z2', 'z3', 'z4', 'x0', 'x1', 'x2',
#                                'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9'])

#     plt.subplots_adjust(wspace=0.05)
#     plt.savefig('./experiment_logs/figures/siren-vae/{}/MI.png'.format(_config['bn']), dpi=600, bbox_inches='tight')
#     plt.show()


@ex.command(unobserved=True)
def plot_loss(_config):
    experiments = _config['model_path'][:]
    for i in range(len(experiments)):
        experiments[i] = 'siren_vae/'+experiments[i]
    legend = _config['legend']
    info = {
        'num_runs':1,
        'iter':_config['num_epochs'],
        'ylims':_config['ylims'],
        'log_scale':False,
        'palette':['steelblue', 'red']
    }
    elbo_curves(experiments, legend, info)


@ex.config
def cfg():
    # Belief network [arithmetic-circuit2, ecoli70-synth, ecoli70-alt, 
    #   ecoli70-complex, arth150-synth]
    bn = 'arithmetic-circuit2'
    # Encoded structure [faithful, fully-connected, independent, random]
    structure = 'faithful'

    # Resdual Flow params
    coeff = 0.99
    n_power_iterations = 5
    # Activatn func [elu, tanh, sigmoid, lipswish, lipmish] 
    activation = 'lipmish'

    num_flow_steps = 5
    flow_hidden_dims = [100]
    decoder_hidden_dims = [100]

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    num_epochs = 100
    normalize = False

    patience = 20
    lr = 1e-2
    seed = 0

    # normalize = False
    iw = False      # importance-weighted samples
    k = 1
    dreg = False    # doubly reparameterized gradients
    wu = False      # warm-up
    wu_N = 1    

    # Add observer
    ex_name = 'siren_vae'
    sub_folder = '{}_{}_{}_{}_{}'.format(structure, activation, num_flow_steps, flow_hidden_dims, decoder_hidden_dims)
    if iw and not dreg:
        ex_name = 'iw_' + ex_name
        sub_folder += '_{}k'.format(k)   
    elif dreg:
        ex_name = 'iw_' + ex_name + '_dreg'
        sub_folder += '_{}k'.format(k)   

    if wu:
        ex_name += '_wu'
        sub_folder += '_{}N'.format(wu_N)
        
    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))

    # For commands
    model_path = 'arithmetic-circuit2_faithful_lipmish_5_[100]_[100]'
    legend = [r'SIReN-VAE$_faith$', r'SIReN-VAE$_fc$']
    ylims = [(9,14), (9,14)]
    run = 1


@ex.automain
def run(_config, _rnd, _run):
    # Training info
    batch_size = _config['batch_size']
    lr = _config['lr']
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']

    # BN initialization
    real = False
    if _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
        apply_wu = [True, True, False, False, False]
    elif _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'ecoli70-real':
        bn = EColi70()
        print('[LOADING DATA] EColi70')
        trainloader, valloader = load_ecoli70()
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
        apply_wu = np.array([False]*40)
        apply_wu[[0,3,5,7,11,12,14,18,23,27,28,30,32,34,39]] =True
    elif  _config['bn'] == 'arth150-real':
        bn = Arth150()
        print('[LOADING DATA] Arth150')
        trainloader, valloader = load_arth150()
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    else:
        raise Exception("Unknown belief network: {}".format(bn))
    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # Fix structure
    if _config['structure'] == 'faithful':
        print('[ENCODED STRUCTURE] Using faithful BN structure.')
        x_as_unit = False
        z_as_unit = False

    elif _config['structure'] == 'fully-connected':
        print('[ENCODED STRUCTURE] Updating BN structure to be fully-connected.')
        bn = FCBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn)
        x_as_unit = True
        z_as_unit = True

    elif _config['structure'] == 'independent':
        print('[ENCODED STRUCTURE] Updating BN structure to be independent.')
        bn = IndependentBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn)
        x_as_unit = True
        z_as_unit = True

    elif _config['structure'] == 'random':
        print('[ENCODED STRUCTURE] Randomizing the faithful BN structure.')
        num_edges = bn.get_num_edges()
        bn = RandomBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn,
            num_edges=num_edges, seed=_config['seed'])
        x_as_unit = False
        z_as_unit = False
    
    else:
        raise Exception("Unknown structure: {}".format(_config['structure']))

    # Normalization
    # if _config['normalize']:
    #     min_std = 1e-5
    #     batch = sample_batch(bn.sample, 10000)
    #     x = batch[:,num_latent:]
    #     z = batch[:,:num_latent]
    #     x_std = torch.std(x, dim=0).to(device)
    #     x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
    #     x_shift = torch.mean(x, dim=0).to(device)
    #     z_std = torch.std(z, dim=0).to(device)
    #     z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
    #     z_shift = torch.mean(z, dim=0).to(device)
    # else:
    #     x_scale = 1.0
    #     z_scale = 1.0
    #     x_shift = 0.0
    #     z_shift = 0.0

    # Draw train and test sets and return minibatch iterators
    # train = torch.zeros((_config['num_train_samples'], n), dtype=torch.float64).to(device)
    # test = torch.zeros((_config['num_val_samples'], n), dtype=torch.float64).to(device)
    # get_data = partial(bn.sample)
    # iterators = partial(batch_iterator, get_data, train, test, batch_size)
    if not real:
        train = bn.sample(_config['num_train_samples'])
        val = bn.sample(_config['num_val_samples'])
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)

    # Initialize model and optimizer
    infer_args = {
        'type': 'resflow',
        'num_flow_steps': _config['num_flow_steps'], 
        'hidden_dims': _config['flow_hidden_dims'],
        'coeff': _config['coeff'],
        'n_power_iterations': _config['n_power_iterations'],
        'activation_function': _config['activation']
    }
    gen_args = {
        'type':'resflow',
        'num_flow_steps': _config['num_flow_steps'],
        'flow_hidden_dims': _config['flow_hidden_dims'],
        'decoder_hidden_dims': _config['decoder_hidden_dims'],
        'coeff': _config['coeff'],
        'n_power_iterations': _config['n_power_iterations'],
        'activation_function': _config['activation']
    }

    
    if _config['iw'] and not _config['dreg']:
        k = _config['k']
        print('[MODEL TYPE] IW-SIReN-VAE (k={})'.format(k))
        model = build_iw_graph_vae(bn, k, infer_args, gen_args, device, 
                x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True).double().to(device)
    elif _config['dreg']:
        k = _config['k']
        print('[MODEL TYPE] IW-SIReN-VAE with DReG (k={})'.format(k))
        model = build_dreg_iw_graph_vae(bn, k, infer_args, gen_args, device, 
                x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True).double().to(device)
    else:
        model = build_graph_vae(bn, infer_args, gen_args, device, 
                x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True).double().to(device)

    # print('GRF_n masks')
    # for l in model.generative_network.nf.steps[0].g.layers:
    #     print(l.mask)

    # print('Decoder masks')
    # for m in model.generative_network.masks:
    #     print(m)

    # print('GRF_g masks')
    # for l in model.inference_network.nf.steps[0].g.layers:
    #     print(l.mask)

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_schedular = ReduceLROnPlateau(optimizer, 'min', verbose=True,
                     min_lr=1e-6, patience=_config['patience'])

    # Warm-up
    wu = _config['wu']
    if wu: wu_N = _config['wu_N']
    def beta(epoch):
        if wu:
            b = torch.ones(num_latent)
            b[apply_wu] = min((1.0/wu_N)*epoch,1.0)
            return torch.tensor(b).double().to(device)
        else:
            return torch.ones(num_latent).double().to(device)
    # beta = lambda epoch: min((1.0/wu_N)*epoch,1.0) if wu else 1.0

    # Train
    epochs = trange(_config['num_epochs'], mininterval=1)
    t = 1
    for epoch in epochs:
        train_elbo = 0.0

        model.train()
        for train_batch in trainloader:
            x = train_batch[:,num_latent:].double().to(device)
            # x = (x - x_shift)/x_scale

            if not _config['dreg']:
                loss = model.loss(x, beta=beta(epoch))

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            else:
                optimizer.zero_grad()
                log_p_loss, log_q_loss = model.loss(x)

                # Gradient of generative network
                grad_dec = torch.autograd.grad(log_p_loss, 
                    model.generative_network.parameters(), retain_graph=True) 
                for i,p in enumerate(model.generative_network.parameters()):
                    p.grad = grad_dec[i].clone()

                # Gradient of inference network
                grad_enc = torch.autograd.grad(log_q_loss, model.
                    inference_network.parameters())
                for i,p in enumerate(model.inference_network.parameters()):
                    p.grad = grad_enc[i].clone()

                loss = log_p_loss
                optimizer.step()

            train_elbo += loss.detach().item()
            

        # log training metrics - kl-divergence
        _run.log_scalar("train", value=train_elbo/num_train_batches) 

        # Log test metrics - kl-divergence
        model.eval()
        with torch.no_grad():
            val_elbo = 0.0
            for val_batch in valloader:
                x = val_batch[:,num_latent:].double().to(device)
                # x = (x - x_shift)/x_scale
                val_elbo += model.loss(x, beta=torch.ones(num_latent).double().to(device)).item()

            _run.log_scalar("val", val_elbo/num_val_batches)

        
        # Print progress
        epochs.set_description('Train -ELBO: {:.3f}, Val -ELBO: {:.3f}'.format(
            train_elbo/num_train_batches, val_elbo/num_val_batches),
            refresh=False)
        
        if wu:
            if epoch >= wu_N:
                lr_schedular.step(train_elbo/num_train_batches)
        else:
            lr_schedular.step(train_elbo/num_train_batches)

        
        # Save the model
        if t % 10 == 0: 
            path = _config['path']
            torch.save(model, path+'/{}/model_{}.pt'.format(_run._id, t))
        t += 1
        
    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))