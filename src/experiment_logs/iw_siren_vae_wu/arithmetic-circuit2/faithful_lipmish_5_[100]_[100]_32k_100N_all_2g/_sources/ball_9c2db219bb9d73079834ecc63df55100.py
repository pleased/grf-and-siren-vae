import torch
import sys
import numpy as np
import random

from PIL import Image, ImageDraw
from math import sin, cos, sqrt

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph
from matplotlib.patches import Circle

import graph.invert as invert

from torch.distributions import Normal, Laplace, Uniform

from graph.belief_network import BeliefNetwork


class Ball(BeliefNetwork):

    def __init__(self):
        super(Ball, self).__init__()
        
        # Node distributions
        self.ls_br = Uniform(0.0, 1.0)
        self.ls_x = Normal(0.0, 2.0)
        self.ls_y = Normal(0.0, 2.0)
        # self.z1 = Laplace(-2,1)
        # self.z2 = self.z2tmp
        # self.z3 = self.z3tmp
        # self.z4 = Normal(7,2)
        # self.z5 = self.z5tmp

        # self.x0 = self.x0tmp
        # self.x1 = self.x1tmp

        # graph
        vertices, edges, observed = Ball._construct_graph()
        self.vertices = vertices
        self.edges = edges
        self.observed = observed 
        # self.forward_graph = ForwardGraph().initialize(vertices, edges, observed)
        # self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = 28*28


    # def z2tmp(self, z0, z1):
    #     return Normal(torch.tanh(z0 + z1 - 2.8), 0.1)

    # def z3tmp(self, z0, z1): 
    #     return Normal(z0*z1, 0.1)


    # def z5tmp(self, z3, z4): 
    #     return Normal(torch.tanh(z3 + z4), 0.1)

    # def x0tmp(self, z3): 
    #     return Normal(z3, 0.1)

    # def x1tmp(self, z5): 
    #     return Normal(z5, 0.1)

    def get_rect(self,x, y, width, height, angle):
        rect = np.array([(0, 0), (width, 0), (width, height), (0, height), (0, 0)])
        theta = (np.pi / 180.0) * angle
        R = np.array([[np.cos(theta), -np.sin(theta)],
                    [np.sin(theta), np.cos(theta)]])
        offset = np.array([x, y])
        transformed_rect = np.dot(rect, R) + offset
        return transformed_rect

    def sample(self, batch_size, train=True):
        H = 128
        X = np.zeros((H, H))

        def get_circ(x,y, radius):
            circ = np.array([
                (x + radius*cos(a), y + radius*sin(a)) for a in range(360)
            ])
            return circ

        ls_br = self.ls_br.sample()
        ls_x = Normal(H//2, sqrt(H//2)).sample()
        ls_y = Normal(H//2, sqrt(H//2)).sample()

        # Convert the numpy array to an Image object.
        img = Image.fromarray(X)

        # Draw a rotated rectangle on the image.
        draw = ImageDraw.Draw(img)
        circ = self.get_circ(x=ls_x, y=ls_y, radius=10)
        draw.polygon([tuple(p) for p in circ], fill='white')
        # Convert the Image data to a numpy array.
        X = np.asarray(img)

        # Display the result using matplotlib.  (`img.show()` could also be used.)



        # z0 = self.z0.sample((batch_size,1))
        # z1 = self.z1.sample((batch_size,1))
        # z2 = self.z2(z0, z1).sample()
        # z3 = self.z3(z0, z1).sample()
        # z4 = self.z4.sample((batch_size,1))
        # z5 = self.z5(z3, z4).sample()

        # x0 = self.x0(z3).sample()
        # x1 = self.x1(z5).sample()

        # sample = torch.cat([z0,z1,z2,z3,z4,z5,x0,x1], dim=1)

        return X


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for Ball BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for Ball BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for Ball BN")


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        N = 28
        vertices = ['ls_br', 'ls_y', 'ls_x', 'b_y', 'b_x', 'b_br'] + \
                        ['x_{}'.format(i) for i in range(N*N)]
        observed = {'x_{}'.format(i) for i in range(N*N)}

        edges = [('ls_br', 'b_br')] + \
                [('ls_br', 'x_{}'.format(i)) for i in range(N*N)] + \
                [('ls_y', 'b_x'), ('ls_y', 'b_y'), ('ls_y', 'b_br')] + \
                [('ls_y', 'x_{}'.format(i)) for i in range(N*N)] + \
                [('ls_x', 'b_x'), ('ls_x', 'b_y'), ('ls_x', 'b_br')] + \
                [('ls_x', 'x_{}'.format(i)) for i in range(N*N)] + \
                [('b_x', 'b_br')] + \
                [('b_x', 'x_{}'.format(i)) for i in range(N*N)] + \
                [('b_y', 'b_br')] + \
                [('b_y', 'x_{}'.format(i)) for i in range(N*N)] + \
                [('b_br', 'x_{}'.format(i)) for i in range(N*N)]

        return vertices, edges, observed