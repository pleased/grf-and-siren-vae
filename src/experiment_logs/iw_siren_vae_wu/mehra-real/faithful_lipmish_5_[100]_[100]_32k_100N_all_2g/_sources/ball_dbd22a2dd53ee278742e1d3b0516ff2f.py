import torch
import sys
import numpy as np
import random

from PIL import Image, ImageDraw
from math import sin, cos, sqrt, acos, asin

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph
from matplotlib.patches import Circle

import graph.invert as invert

from torch.distributions import Normal, Laplace, Uniform

from graph.belief_network import BeliefNetwork


class Ball(BeliefNetwork):

    def __init__(self, H):
        super(Ball, self).__init__()
        
        self.H = H

        # graph
        vertices, edges, observed = Ball._construct_graph(H)
        self.vertices = vertices
        self.edges = edges
        self.observed = observed 
        self.forward_graph = ForwardGraph().initialize(vertices, edges, observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = H*H


    def sample(self, batch_size):
        H = self.H
        X = np.zeros((batch_size, H, H))
        ls_r = round(0.1*H) 
        b_r = round(0.2*H)

        def get_circ(x,y, radius):
            # circ = np.array([
            #     (x + radius*cos(a), y + radius*sin(a)) for a in range(360)
            # ])
            circ = np.array([
                (x-radius,y+radius), (x-radius,y-radius), (x+radius, y-radius), (x+radius, y+radius)
            ])
            return circ

        def linear_gradient(i, poly, p1, p2, c1, c2):

            # Draw initial polygon, alpha channel only, on an empty canvas of image size
            ii = Image.new('RGBA', i.size, (0, 0, 0, 0))
            draw = ImageDraw.Draw(ii)
            draw.polygon(poly, fill=(0, 0, 0, 255), outline=None)

            # Calculate angle between point 1 and 2
            p1 = np.array(p1)
            p2 = np.array(p2)
            angle = np.arctan2(p2[1] - p1[1], p2[0] - p1[0]) / np.pi * 180

            # Rotate and crop shape
            temp = ii.rotate(angle, expand=True)
            temp = temp.crop(temp.getbbox())
            wt, ht = temp.size

            # Create gradient from color 1 to 2 of appropriate size
            gradient = np.linspace(c1, c2, wt, True).astype(np.uint8)
            gradient = np.tile(gradient, [2 * H, 1, 1])
            gradient = Image.fromarray(gradient)

            # Paste gradient on blank canvas of sufficient size
            temp = Image.new('RGBA', (max(i.size[0], gradient.size[0]),
                                    max(i.size[1], gradient.size[1])), (0, 0, 0, 0))
            temp.paste(gradient)
            gradient = temp

            # Rotate and translate gradient appropriately
            x = np.sin(angle * np.pi / 180) * ht
            y = np.cos(angle * np.pi / 180) * ht
            gradient = gradient.rotate(-angle, center=(0, 0),
                                    translate=(p1[0] + x, p1[1] - y))

            # Paste gradient on temporary image
            ii.paste(gradient.crop((0, 0, ii.size[0], ii.size[1])), mask=ii)

            # Paste temporary image on actual image
            i.paste(ii, mask=ii)

            return i

        # light source brightness
        ls_br = Uniform(0.5,1).sample((batch_size,1))

        # light source position
        ls_x = Uniform(b_r, H-b_r).sample((batch_size,1))
        ls_y = Uniform(b_r, H-b_r).sample((batch_size,1))

        # ball position
        b_x = Normal(H - ls_x, sqrt(H//4)).sample()
        b_y = Normal(H - ls_y, sqrt(H//4)).sample()

        # ball brightness
        d = torch.sqrt((ls_x-b_x)**2 + (ls_y-b_y)**2)
        max_d = sqrt(2)*H
        b_br = ((max_d-d)/max_d)*ls_br 


        for n in range(batch_size):
            # Convert the numpy array to an Image object.
            img = Image.fromarray(X[n])
            # Draw a circle on the image.
            draw = ImageDraw.Draw(img)

            # light source
            ls_yn = ls_y[n].item()
            ls_xn = ls_x[n].item()
            b_xn = b_x[n].item()
            b_yn = b_y[n].item()
            circ = get_circ(x=ls_xn, y=ls_yn, radius=ls_r/2)
            draw.polygon([tuple(p) for p in circ], fill=ls_br[n]*255)

            # ball
            circ = get_circ(x=b_xn, y=b_yn, radius=b_r/2)
            draw.polygon([tuple(p) for p in circ], fill=b_br[n]*255)

            # ball with gradient
            beta = asin(abs(ls_yn - b_yn)/d[n])
            y,x = (b_r/2)*sin(beta), (b_r/2)*cos(beta)

            if ls_xn < b_xn: x = -x
            if ls_yn < b_yn: y = -y
            
            img = linear_gradient(img, [tuple(p) for p in circ], (b_xn+x, b_yn+y), (b_xn-x, b_yn-y), (b_br[n]*255,b_br[n]*255,b_br[n]*255), (10,10,10))

            # Convert the Image data to a numpy array.
            X[n] = np.asarray(img)

        return (X.reshape((batch_size, H*H)))/255


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for Ball BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for Ball BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for Ball BN")


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph(H):
        vertices = ['ls_br', 'ls_y', 'ls_x', 'b_y', 'b_x', 'b_br'] + \
                        ['x_{}'.format(i) for i in range(H**2)]
        observed = {'x_{}'.format(i) for i in range(H**2)}

        edges = [('ls_br', 'b_br')] + \
                [('ls_br', 'x_{}'.format(i)) for i in range(H**2)] + \
                [('ls_y', 'b_x'), ('ls_y', 'b_y'), ('ls_y', 'b_br')] + \
                [('ls_y', 'x_{}'.format(i)) for i in range(H**2)] + \
                [('ls_x', 'b_x'), ('ls_x', 'b_y'), ('ls_x', 'b_br')] + \
                [('ls_x', 'x_{}'.format(i)) for i in range(H**2)] + \
                [('b_x', 'b_br')] + \
                [('b_x', 'x_{}'.format(i)) for i in range(H**2)] + \
                [('b_y', 'b_br')] + \
                [('b_y', 'x_{}'.format(i)) for i in range(H**2)] + \
                [('b_br', 'x_{}'.format(i)) for i in range(H**2)]

        return vertices, edges, observed