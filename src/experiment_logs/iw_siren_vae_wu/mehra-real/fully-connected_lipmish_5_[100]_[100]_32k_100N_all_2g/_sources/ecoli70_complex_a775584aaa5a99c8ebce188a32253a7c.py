import torch
import numpy as np
import json


import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal

from graph.belief_network import EColi70Adapted


class EColi70Complex(EColi70Adapted):

    def __init__(self):
        super(EColi70Complex, self).__init__()

        # graph
        with open('./graph/belief_network/bnlearn/ecoli70_adapted.json') as f:
            bn = json.load(f)
        self.vertices = bn['vertices']
        self.edges = bn['edges']
        self.observed = set(bn['observed'])
        self.intercepts = bn['intercepts']
        self.coefficients = bn['coefficients']
        self.stds = bn['stds']
        self.forward_graph = ForwardGraph().initialize(self.vertices, 
                self.edges, self.observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = len(self.vertices) - len(self.observed)
        self.num_obs = len(self.observed)

    
    def get_normal(self, v, X):
        c = torch.tensor(self.coefficients[v]).to(X.device)
        mean = self.intercepts[v] + (c*X).sum(-1)
        if v == 0 or v == 6 or v == 10 or v == 12:
            return Normal(torch.cos(mean) + torch.sin(mean), self.stds[v])
        elif v == 5 or v == 15 or v == 3:
            return Normal(torch.tanh(mean), self.stds[v])
        elif v == 20:
            return Normal(torch.tanh(mean-0.4), self.stds[v])
        elif v == 25:
            return Normal(torch.tanh(mean+0.25), self.stds[v])
        elif v == 30:
            return Normal(torch.tanh(mean+2), self.stds[v])
        elif v == 35:
            return Normal(torch.tanh(mean-3), self.stds[v])
        elif v == 40:
            return Normal(torch.tanh(mean+1), self.stds[v])
        else:
            #print(mean)
            return Normal(mean, self.stds[v])