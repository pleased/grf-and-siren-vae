# Train -ELBO: 35.126, Val -ELBO: 35.102
python ./ex_siren_vae.py  samples with bn='ecoli70-alt' num_epochs=400  iw=True k=8 dreg=True lr=1e-3 patience=10 wu=True wu_N=100

# Train -ELBO: 35.011, Val -ELBO: 34.993
python ./ex_siren_vae.py  samples with bn='ecoli70-alt' num_epochs=400  iw=True k=8 dreg=True lr=1e-3 patience=20 wu=True wu_N=50 
