import torch
import sys
import numpy as np
import sklearn

from torch.distributions import Normal
from math import log

if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import BeliefNetwork

class Tree2(BeliefNetwork):

    def __init__(self):
        super(Tree2, self).__init__()

        # graph
        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph()
            self.inverse_graph = _WrapperGraph(inverse=True)
        else:
            self.forward_graph = ForwardGraph().initialize(*Tree2._construct_graph())
            self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = 1

        self.gauss_8 = [
            Normal(loc=torch.tensor([0.0, 1.5]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([1.0, 1.0]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([1.5, 0.0]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([1.0,-1.0]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([0.0,-1.5]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([-1.0,-1.0]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([-1.5,0.0]), scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([-1.0,1.0]), scale=torch.tensor([0.2,0.2]))
        ]
        self.gauss_8_p = [0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 
                                    0.125, 0.125]

        self.gauss_2 = [
            Normal(loc=torch.tensor([1.0, 1.0]),scale=torch.tensor([0.2,0.2])),
            Normal(loc=torch.tensor([-1.0, -1.0]),scale=torch.tensor([0.2,0.2]))
        ]
        self.gauss_2_p = [0.5, 0.5]


    def z4tmp(self, z0, z1):
        return Normal(loc = torch.max(z0, z1), scale=1.0)


    def z5tmp(self, z2, z3):
        return Normal(loc = torch.min(z2, z3), scale=1.0)


    def x0tmp(self, z4, z5):
        loc =  0.5*torch.sin(z4+z5) + 0.5*torch.cos(z4+z5)
        return Normal(loc=loc, scale=1.0)


    def sample(self, batch_size, train=True):
        rng = np.random.RandomState()

        # 2 Gaussians
        z0,z1 = self.sample_2gaussians(rng, batch_size)

        # 8 Gaussians
        z2,z3 = self.sample_8gaussians(rng, batch_size)

        # x4~ N(max(X0, X1),1)
        z4 = self.z4tmp(z0, z1).sample()

        # x5 ~ N(min(X2, X3),1)
        z5 = self.z5tmp(z2, z3).sample()

        # x6 ~ 0.5*N(sin(X4+X5),1) + 0.5*N(cos(X4+X5),1).
        x0 = self.x0tmp(z4, z5).sample()

        sample = torch.cat([z0, z1, z2, z3, z4, z5, x0], dim=1)

        return sample


    def sample_8gaussians(self, rng, batch_size):
        data= []
        for i in range(batch_size):
            c = rng.choice(8, p=self.gauss_8_p)
            xy = self.gauss_8[c].sample((1,))
            data.append(xy)
        data = torch.cat(data, dim=0)
        return data[:,0].reshape(-1,1), data[:,1].reshape(-1,1)


    def sample_2gaussians(self, rng, batch_size):
        data= []
        for i in range(batch_size):
            c = rng.choice(2, p=self.gauss_2_p)
            xy = self.gauss_2[c].sample((1,))
            data.append(xy)
        data =  torch.cat(data, dim=0)
        return data[:,0].reshape(-1,1), data[:,1].reshape(-1,1)


    def log_likelihood(self, x, z):
        # Compute p(x|z)
        # => p(x0|z4, z5)
        return self.x0tmp(z[:,4],z[:,5]).log_prob(x)


    def log_prior(self, z):
        # Compute p(z)
        # => p(z5|z2,z3)p(z4|z0,z1)p(z2,z3)p(z0,z1)
        log_p = 0
        log_p += self.z5tmp(z[:,2],z[:,3]).log_prob(z[:,5])
        log_p += self.z4tmp(z[:,0],z[:,1]).log_prob(z[:,4])

        print(z.device)
        print(self.gauss_8[0])
        log_p += torch.logsumexp(
            [log(self.gauss_8_p[c])+self.gauss_8[c].log_prob(z[:,2:4]) 
                for c in range(8)])

        log_p += torch.logsumexp(
            [log(self.gauss_2_p[c])+self.gauss_2[c].log_prob(z[:,0:2]) 
                for c in range(2)])
        
        return log_p


    def log_joint(self, x, z):
        return self.log_likelihood(x, z) + self.log_prior(z)


    def get_num_latent(self):
        return 6


    def get_num_obs(self):
        return 1


    def get_num_vertices(self):
        return self.get_num_latent() + self.get_num_obs()


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['z0','z1', 'z2','z3', 'z4','z5', 'x0']
        edges = [('z0','z1') ,('z2','z3'), ('z0','z4'), ('z1','z4'),
                 ('z2','z5'), ('z3','z5'), ('z4','x0'), 
                 ('z5','x0')]  
        observed = {'x0'}
        return vertices, edges, observed