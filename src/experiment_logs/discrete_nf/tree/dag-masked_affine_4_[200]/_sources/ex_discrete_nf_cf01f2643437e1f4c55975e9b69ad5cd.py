"""
Approximate the posterior using an inverse graphical normalizing flow.
"""

import torch
import numpy as np
import matplotlib.pyplot as plt

from sacred import Experiment 
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau

from modules.factory import build_discrete_nf
from ex_binary_tree_config import bt_ingredient, create_tree
from ex_utils import batch_iterator, count_parameters, sample_batch, kolmogorov_smirnov
from graph.belief_network import ArithmeticCircuit, LargerBN1, Tree, Tree2


ex = Experiment('discrete_nf', ingredients=[bt_ingredient])


@ex.config
def cfg():
    # Belief network [arithmetic, arithmetic-mul, binary-tree, ternary-tree, largerbn1]
    bn = 'binary-tree'
    # Conditioner [dag-masked, coupling, autoregressive]
    conditioner_type = 'dag-masked'
    # Normalizer [affine, monotonic]
    normalizer_type = 'affine'
    # Monotonic normalizer args
    int_net_hidden = [100]
    int_net_final_activation = 'elu'
    cond_size = 10
    mono_num_steps = 15
    mono_solver = 'CC'  # or 'CCParallel'

    num_flow_steps = 5
    hidden_dims = [100]

    batch_size = 100
    num_train_samples = 10000
    num_test_samples = 5000
    num_steps = 100
    num_train_batches = num_train_samples//batch_size
    num_test_batches = num_test_samples//batch_size

    lr = 1e-2
    lr_decay = 0.1
    lr_decay_step_size = 40
    seed = 6#8#9#10#67#87#109#99#186#4#

    # Add observer
    ex_name = 'discrete_nf'
    sub_folder = '{}_{}_{}_{}_{}'.format(bn, conditioner_type, normalizer_type, num_flow_steps, hidden_dims)
    path = './experiment_logs/{}/{}'.format(ex_name, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr'] 
    lr_decay = _config['lr_decay']
    lr_decay_step_size = _config['lr_decay_step_size']
    num_train_batches = _config['num_train_batches']
    num_test_batches = _config['num_test_batches']
    hidden_dims = _config['hidden_dims']
    num_flow_steps = _config['num_flow_steps']

    # BN initialization
    bn = _config['bn']
    if bn == 'arithmetic':
        graph = ArithmeticCircuit()
    elif bn == 'arithmetic-mul':
        graph = ArithmeticCircuit(mul=True)
    elif bn == 'binary-tree':
        graph = create_tree()
        ex.info['tree_coeffs'] = graph.coeffs.tolist()
    elif bn == 'largerbn1':
        graph = LargerBN1()
    elif bn == 'tree':
        graph = Tree()
    elif bn == 'tree2':
        graph = Tree2(device)
    else:
        raise Exception("Unknown belief network: {}".format(bn))
    num_latent = graph.get_num_latent()
    n = graph.get_num_vertices()

    # Draw train and test sets and return minibatch iterators
    tree_train = np.zeros((_config['num_train_samples'], n))
    tree_test = np.zeros((_config['num_test_samples'], n))
    get_data = partial(graph.sample)
    iterators = partial(batch_iterator, get_data, tree_train, tree_test, batch_size)

    # Get static batch for visualization over learning
    if bn == 'binary-tree':
        static_tree = graph.sample(batch_size=5)
        # Log static batches
        ex.info['static_batches'] = static_tree.tolist()
        static_samples = [np.tile(static_tree[i,:], (batch_size,1)) for i in range(5)]
        # log-likelihoods of samples from the inference network evaluated on the
        # analytical posterior, given the 5 static samples - saved as metric
        ll_p_metric_names = ["ll_p_1", "ll_p_2", "ll_p_3", "ll_p_4", "ll_p_5"]

    # Initialize model and optimizer
    conditioner_type = _config['conditioner_type']
    normalizer_type = _config['normalizer_type']
    if normalizer_type == 'monotonic':
        mono_args = {
            'int_net_hidden': _config['int_net_hidden'],
            'cond_size': _config['cond_size'],
            'mono_num_steps': _config['mono_num_steps'],
            'mono_solver': _config['mono_solver'],
            'int_net_final_activation': _config['int_net_final_activation']
        }
        model = build_discrete_nf(
            num_flow_steps, graph, hidden_dims, conditioner_type, 
            normalizer_type, device, mono_args)
    elif normalizer_type == 'affine':
        model = build_discrete_nf(
            num_flow_steps, graph, hidden_dims, conditioner_type, 
            normalizer_type, device)
    else:
        raise Exception("Unknown Normalizer type: {}".format(normalizer_type))
    model.to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    # lr_scheduler = StepLR(optimizer, step_size=lr_decay_step_size, gamma=lr_decay)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min',verbose=True, min_lr=1e-6)

    # Log model capacity
    ex.info['num model params'] =  model.count_parameters()

    # Train
    for epoch in range(_config['num_steps']):
        train_batcher, test_batcher = iterators()
        train_shifted_kl = 0.0
        if bn == 'binary-tree':
            train_true_kl = 0.0

        for idx in range(num_train_batches):
            # train_batch = (sample_batch(graph.sample, batch_size)).double().to(device)
            train_batch = torch.tensor(train_batcher(), dtype=torch.float64).to(device)
            x = train_batch[:,num_latent:]
            eps0, z, j = model(cond=x)
            loss = model.shifted_reverse_kl(x, z, eps0, j)

            l = loss.detach().item()
            train_shifted_kl += l    

            if bn == 'binary-tree':
                with torch.no_grad(): 
                    p = graph.log_posterior(torch.cat((z,x), dim=1)) 
                    q = model.ll(eps0, j)
                    train_true_kl += (q - p).item() 

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # log training metrics - training loss, kl-divergence
        _run.log_scalar("training.shifted_kl", value=train_shifted_kl/num_train_batches) 
        if bn == 'binary-tree':
            _run.log_scalar("training.true_kl", value=train_true_kl/num_train_batches) 
        _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)

        # if epoch == 20 and train_shifted_kl/num_train_batches > 15:
        #     exit()

        # Log test metrics - test loss, kl-divergence
        with torch.no_grad():
            test_batch = torch.DoubleTensor(test_batcher()).to(device)
            x = test_batch[:,num_latent:]
            eps0, z, j = model(cond=x)
            test_shifted_kl = model.shifted_reverse_kl(x, z, eps0, j).item()
            _run.log_scalar("test.shifted_kl", test_shifted_kl)

            if bn == 'binary-tree':
                p = graph.log_posterior(torch.cat((z,x), dim=1))
                q = model.ll(eps0, j)
                _run.log_scalar("test.true_kl", (q - p).item())

        # Decay learning rate every 100 epochs
        lr_scheduler.step(train_shifted_kl)

        # Calculate log-likelihood of samples from the inference
        # network evaluated on the analytical posterior
        if epoch%1 == 0:
            
            if bn == 'binary-tree':
                ll_p = [[],[],[],[],[]]
                for i in range(5):
                    with torch.no_grad():
                        static_sample = torch.DoubleTensor(static_samples[i]).to(device)
                        x = static_sample[:,num_latent:]
                        _, z, _ = model(cond=x)
                        ll = graph.log_posterior(torch.cat((z, x), dim=1))
                        # log training metrics - log-likelihood
                        ll_p[i] = -ll.item()/num_latent
                        _run.log_scalar(ll_p_metric_names[i], ll_p[i])

                print('[{}]: test: {}, train: shifted {}, true {}, post [{}, {}, {}, {}, {}]'.format(
                    epoch+1, 
                    test_shifted_kl, 
                    train_shifted_kl/num_train_batches, 
                    train_true_kl/num_train_batches, 
                    ll_p[0], ll_p[1], ll_p[2], ll_p[3], ll_p[4]))
            else:
                print('[{}]: test {}, train {}'.format(
                    epoch+1, 
                    test_shifted_kl, 
                    train_shifted_kl/num_train_batches))

    # Peak memory usage
    ex.info['memory usage (MB)'] = torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000 

    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))
    
    # Log-likelihood of samples drawn from the true posterior
    if bn == 'binary-tree':
        true_ll = 0.0
        for i in range(5):
            static_sample = torch.DoubleTensor(static_samples[i]).to(device)
            x = static_sample[:,num_latent:]
            z = graph.sample_posterior(x)
            ll = graph.log_posterior(torch.cat((z, x), dim=1))
            true_ll += ll/5
        print('Neg Log-likelihood of samples drawn from the true posterior: -log(p(z|x)) = ', -(true_ll).item()/num_latent)

    # Some plots of the posterior distributions
    with torch.no_grad():
        sample= sample_batch(graph.sample, 100000).double().to(device)
        x = sample[:,num_latent:]
        eps0, z, j = model(cond=x)
        inference_network_samples = torch.cat((z, x), dim=1)
        if bn == 'binary-tree':
            bn_samples = graph.sample_posterior(x)
            label = ['True','Inference network']
        else:
            bn_samples = sample[:,:num_latent]
            label = ['Joint','Inference network']

        for i in range(num_latent):
            ztrue = bn_samples[:,i].cpu().tolist()
            zinf = inference_network_samples[:,i].cpu().tolist()
            ks, _ = kolmogorov_smirnov(zinf, ztrue)
            fig =  plt.figure()
            ax = fig.add_subplot(111)
            ax.hist(x=[ztrue, zinf], bins=50, alpha=0.5, 
                        histtype='stepfilled', density=True,
                        color=['steelblue', 'red'], edgecolor='none',
                        label=label)
            ax.set_xlabel("z_"+str(i))
            ax.set_ylabel("Density")
            ax.legend()
            ax.text(0.02,0.035,"ks={:.3f}".format(ks), transform=ax.transAxes, bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))
            plt.savefig(path+'/{}/z_{}'.format(_run._id,i))
            plt.clf()

    # # Inversion test
    # print('\n -- Inversion --')

    # sample = (sample_batch(graph.sample, 2)).double().to(device)
    # x = sample[:,num_latent:]

    # print('[iter] L2 norm')
    # for t in range(num_latent+20):
    #     eps0, z, j = model(cond=x)
    #     eps0_prime, j = model.inverse_fixed_point(z, cond=x, maxT=t, epsilon=1e-10)

    #     # Measure error
    #     error = torch.norm(eps0 - eps0_prime, dim=1).mean()
    #     print('[{}]'.format(t).rjust(6) + ' {}'.format(error))