"""
Approximate the posterior using an inverse graphical normalizing flow.
"""

import torch
import numpy as np
import matplotlib.pyplot as plt
import json

from sacred import Experiment 
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_discrete_nf
from ex_binary_tree_config import bt_ingredient, create_tree
from ex_utils import batch_iterator, count_parameters, sample_batch, kolmogorov_smirnov
from graph.belief_network import *
from data.load_data import load_protein, load_mehra, load_dataset


ex = Experiment('discrete_nf', ingredients=[bt_ingredient])
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"


@ex.command(unobserved=True)
def plot_z_distributions(_config):
    # BN initialization
    bn = _config['bn']
    if bn == 'arithmetic':
        graph = ArithmeticCircuit()
    elif bn == 'arithmetic-mul':
        graph = ArithmeticCircuit(mul=True)
    elif bn == 'binary-tree':
        graph = create_tree()
        ex.info['tree_coeffs'] = graph.coeffs.tolist()
    elif bn == 'largerbn1':
        graph = LargerBN1()
    elif bn == 'tree':
        graph = Tree()
    elif bn == 'tree2':
        graph = Tree2(device)
    elif bn == 'arithmetic-circuit2':
        graph = ArithmeticCircuit2()
    elif bn == 'ecoli70-synth':
        graph = EColi70()
    elif bn == 'arth150-synth':
        graph = Arth150()
    else:
        raise Exception("Unknown belief network: {}".format(bn))
    num_latent = graph.get_num_latent()
    n = graph.get_num_vertices()

    # Load model
    path = _config['path']
    run = _config['run']
    model = torch.load(path+'/{}/model.pt'.format(run))

    with torch.no_grad():
        sample= sample_batch(graph.sample, 10000).to(device)
        x = sample[:,num_latent:]
        eps0, z, j = model(cond=x)
        inference_network_samples = torch.cat((z, x), dim=1)
        if bn == 'binary-tree':
            bn_samples = graph.sample_posterior(x)
            label = ['True','Inference network']
        else:
            bn_samples = sample[:,:num_latent]
            label = ['Joint','Inference network']

        for i in range(num_latent):
            ztrue = bn_samples[:,i].cpu().tolist()
            zinf = inference_network_samples[:,i].cpu().tolist()
            ks, _ = kolmogorov_smirnov(zinf, ztrue)
            fig =  plt.figure()
            ax = fig.add_subplot(111)
            ax.hist(x=[ztrue, zinf], bins=50, alpha=0.5, 
                        histtype='stepfilled', density=True,
                        color=['steelblue', 'red'], edgecolor='none',
                        label=label)
            ax.set_xlabel("z_"+str(i))
            ax.set_ylabel("Density")
            ax.legend()
            ax.text(0.02,0.035,"ks={:.3f}".format(ks), transform=ax.transAxes, bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))
            plt.savefig(path+'/{}/z_{}'.format(run,i))
            plt.clf()


def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network 
    bn = 'arithmetic-circuit2'
    # Conditioner [dag-masked, coupling, autoregressive]
    conditioner_type = 'dag-masked'
    # Normalizer [affine, monotonic]
    normalizer_type = 'affine'
    # Monotonic normalizer args
    int_net_hidden = [100]
    int_net_final_activation = 'elu'
    cond_size = 10
    mono_num_steps = 15
    mono_solver = 'CC'  # or 'CCParallel'

    num_flow_steps = 5
    hidden_dims = [100]

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_epochs = 100
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size

    lr = 1e-2
    seed = 0
    patience = 20
    double = False

    run = 1

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False

    # Add observer
    ex_name = 'discrete_nf'
    sub_folder = '{}_{}_{}_{}'.format(conditioner_type, normalizer_type, num_flow_steps, hidden_dims)
    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _run):
    # Training info
    batch_size = _config['batch_size']
    lr = _config['lr'] 
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    hidden_dims = _config['hidden_dims']
    num_flow_steps = _config['num_flow_steps']
    checkpoint_n = _config['checkpoint_n']

    print('[CONFIG]',_config)
    print('[DEVICE]', device)

    # BN initialization
    if _config['bn'] == 'arithmetic-circuit':
        bn = ArithmeticCircuit()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'tree':
        bn = Tree(device)
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'arth150-synth':
        bn = Arth150()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'], device=device)
    elif _config['bn'] == 'binary-tree':
        bn = create_tree()
        ex.info['tree_coeffs'] = bn.coeffs.tolist()
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))

    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # min_std = 1e-5
    # x = torch.cat([batch for batch in trainloader], dim=0)
    # x_std = torch.std(x, dim=0).to(device)
    scale = {
        'scale': torch.tensor([1.0]).to(device),
        'shift': torch.tensor([0.0]).to(device)
    }
    # del x

    # Draw train and val sets and return minibatch iterators
    # train = bn.sample(_config['num_train_samples']).to(device)
    # val = bn.sample(_config['num_val_samples']).to(device)
    # test = bn.sample(_config['num_val_samples']).to(device)
    # if _config['double']:
    #     train = train.double()
    #     val = val.double()
    #     test = test.double()
    # trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    # valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    # testloader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)

    # Get static batch for visualization over learning
    if bn == 'binary-tree':
        static_tree = bn.sample(batch_size=5)
        # Log static batches
        ex.info['static_batches'] = static_tree.tolist()
        static_samples = [np.tile(static_tree[i,:], (batch_size,1)) for i in range(5)]
        # log-likelihoods of samples from the inference network evaluated on the
        # analytical posterior, given the 5 static samples - saved as metric
        ll_p_metric_names = ["ll_p_1", "ll_p_2", "ll_p_3", "ll_p_4", "ll_p_5"]

    # Initialize model and optimizer
    conditioner_type = _config['conditioner_type']
    normalizer_type = _config['normalizer_type']
    if normalizer_type == 'monotonic':
        mono_args = {
            'int_net_hidden': _config['int_net_hidden'],
            'cond_size': _config['cond_size'],
            'mono_num_steps': _config['mono_num_steps'],
            'mono_solver': _config['mono_solver'],
            'int_net_final_activation': _config['int_net_final_activation']
        }
        model = build_discrete_nf(
            num_flow_steps, bn, hidden_dims, conditioner_type, 
            normalizer_type, device, mono_args, scale=scale)
    elif normalizer_type == 'affine':
        model = build_discrete_nf(
            num_flow_steps, bn, hidden_dims, conditioner_type, 
            normalizer_type, device, scale=scale)
    else:
        raise Exception("Unknown Normalizer type: {}".format(normalizer_type))
    model.to(device)
    if _config['double']:
        model = model.double() 

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True, min_lr=1e-6, patience=_config['patience'])

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)

    # Log model capacity
    ex.info['num model params'] =  model.count_parameters()

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_shifted_kl = 0.0
        if bn == 'binary-tree':
            train_true_kl = 0.0

        model.train()
        for train_batch in trainloader:
            x = train_batch[:,num_latent:]
            eps0, z, j = model(cond=x)
            loss = model.shifted_reverse_kl(x, z, eps0, j)

            l = loss.detach().item()
            train_shifted_kl += l    

            if bn == 'binary-tree':
                with torch.no_grad(): 
                    p = bn.log_posterior(torch.cat((z,x), dim=1)) 
                    q = model.ll(eps0, j)
                    train_true_kl += (q - p).item() 

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # log training metrics - training loss, kl-divergence
        _run.log_scalar("train", value=train_shifted_kl/num_train_batches) 
        if bn == 'binary-tree':
            _run.log_scalar("train.true", value=train_true_kl/num_train_batches) 
        if device == 'cuda:0':
            _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)

        # Log val metrics - val loss, kl-divergence
        model.eval()
        with torch.no_grad():
            val_shifted_kl = 0.0
            for val_batch in valloader:
                x = val_batch[:,num_latent:]
                eps0, z, j = model(cond=x)
                val_shifted_kl += model.shifted_reverse_kl(x, z, eps0, j).item()
            _run.log_scalar("val", val_shifted_kl/num_val_batches)

            if bn == 'binary-tree':
                p = bn.log_posterior(torch.cat((z,x), dim=1))
                q = model.ll(eps0, j)
                _run.log_scalar("val.true", (q - p).item())

        # Decay learning rate every 100 epochs
        lr_scheduler.step(train_shifted_kl)

        # Calculate log-likelihood of samples from the inference
        # network evaluated on the analytical posterior
            
        if bn == 'binary-tree':
            ll_p = [[],[],[],[],[]]
            for i in range(5):
                with torch.no_grad():
                    static_sample = (static_samples[i]).to(device)
                    x = static_sample[:,num_latent:]
                    _, z, _ = model(cond=x)
                    ll = bn.log_posterior(torch.cat((z, x), dim=1))
                    # log training metrics - log-likelihood
                    ll_p[i] = -ll.item()/num_latent
                    _run.log_scalar(ll_p_metric_names[i], ll_p[i])

            epochs.set_description('val: {}, train: shifted {}, true {}, post [{}, {}, {}, {}, {}]'.format(
                val_shifted_kl/num_val_batches, 
                train_shifted_kl/num_train_batches, 
                train_true_kl/num_train_batches, 
                ll_p[0], ll_p[1], ll_p[2], ll_p[3], ll_p[4]))
        else:
            # Print progress
            epochs.set_description('Train: {:.3f}, Val: {:.3f}'.format(
            train_shifted_kl/num_train_batches, val_shifted_kl/num_val_batches),
            refresh=False)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1

    # Peak memory usage
    if device == 'cuda:0':
        ex.info['memory usage (MB)'] = torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000 

    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))
    
    # Log-likelihood of samples drawn from the true posterior
    if bn == 'binary-tree':
        true_ll = 0.0
        for i in range(5):
            static_sample = (static_samples[i]).to(device)
            x = static_sample[:,num_latent:]
            z = bn.sample_posterior(x)
            ll = bn.log_posterior(torch.cat((z, x), dim=1))
            true_ll += ll/5
        print('Neg Log-likelihood of samples drawn from the true posterior: -log(p(z|x)) = ', -(true_ll).item()/num_latent)

    # Some plots of the posterior distributions
    # with torch.no_grad():
    #     sample= sample_batch(graph.sample, 100000).to(device)
    #     x = sample[:,num_latent:]
    #     eps0, z, j = model(cond=x)
    #     inference_network_samples = torch.cat((z, x), dim=1)
    #     if bn == 'binary-tree':
    #         bn_samples = graph.sample_posterior(x)
    #         label = ['True','Inference network']
    #     else:
    #         bn_samples = sample[:,:num_latent]
    #         label = ['Joint','Inference network']

    #     for i in range(num_latent):
    #         ztrue = bn_samples[:,i].cpu().tolist()
    #         zinf = inference_network_samples[:,i].cpu().tolist()
    #         ks, _ = kolmogorov_smirnov(zinf, ztrue)
    #         fig =  plt.figure()
    #         ax = fig.add_subplot(111)
    #         ax.hist(x=[ztrue, zinf], bins=50, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red'], edgecolor='none',
    #                     label=label)
    #         ax.set_xlabel("z_"+str(i))
    #         ax.set_ylabel("Density")
    #         ax.legend()
    #         ax.text(0.02,0.035,"ks={:.3f}".format(ks), transform=ax.transAxes, bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))
    #         plt.savefig(path+'/{}/z_{}'.format(_run._id,i))
    #         plt.clf()

    # Inversion test
    # print('\n -- Inversion --')

    # sample = (sample_batch(graph.sample, 2)).to(device)
    # x = sample[:,num_latent:]

    # print('[iter] L2 norm')
    # for t in range(num_latent+20):
    #     eps0, z, j = model(cond=x)
    #     eps0_prime, j = model.inverse_fixed_point(z, cond=x, maxT=t, epsilon=1e-10)

    #     # Measure error
    #     error = torch.norm(eps0 - eps0_prime, dim=1).mean()
    #     print('[{}]'.format(t).rjust(6) + ' {}'.format(error))