import torch
import numpy as np
import json


import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal

from graph.belief_network import BeliefNetwork


class EColi70(BeliefNetwork):

    def __init__(self):
        super(EColi70, self).__init__()

        # graph
        with open('./graph/belief_network/bnlearn/ecoli70.json') as f:
            bn = json.load(f)
        self.vertices = bn['vertices']
        # for v in range(len(self.vertices)):
        #     print('[{}]'.format(v),self.vertices[v])
        self.edges = bn['edges']
        self.observed = set(bn['observed'])
        self.intercepts = bn['intercepts']
        self.coefficients = bn['coefficients']
        self.stds = bn['stds']
        self.forward_graph = ForwardGraph().initialize(self.vertices, 
                self.edges, self.observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        # for e in range(46):
        #     print('[{}]'.format(e),self.forward_graph.get_in_neighbours(e))
        
        # for e in range(46):
        #     print('[{}]'.format(e),self.inverse_graph.get_in_neighbours(e))

        self.num_latent = len(self.vertices) - len(self.observed)
        self.num_obs = len(self.observed)

    
    def get_normal(self, v, X):
        c = torch.tensor(self.coefficients[v]).to(X.device)
        mean = self.intercepts[v] + (c*X).sum(-1)
        return Normal(mean, self.stds[v])
    
    
    def sample(self, batch_size, train=True, observed_only=False):
        sample = torch.zeros((batch_size, self.num_latent + self.num_obs))

        for v in gt.topological_sort(self.forward_graph):
            sample[:,v] = self.get_normal(v, sample[:,:self.num_latent]).sample()

        if observed_only:
            return sample[:,self.num_latent:]
        else:
            return sample
        

    def log_likelihood(self, x, z):
        # log p(x|z)
        log_p_x = 0

        for v in range(self.num_latent, self.num_latent + self.num_obs):
            log_p_x += self.get_normal(v, z).log_prob(x[:,v-self.num_latent])    

        return log_p_x


    def log_prior(self, z):
        # log p(z)
        log_p_z = 0

        for v in range(self.num_latent):
            log_p_z += self.get_normal(v, z).log_prob(z[:,v])
        return log_p_z 


    def log_joint(self, x, z):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z)
        return log_lik + log_prior


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = [
                    # Latents
                    "b1191", "eutG", "fixC", "sucA", "yceP", "ygcE",
                    "asnA", "cspG", "atpD", "icdA", "lacA", "cspA", 
                    "yedE", "lacY", "yfiA", "pspB", "yheI", "lacZ",
                    "pspA", "ycgX", "dnaK", "mopB", 

                    # Observed
                    "ygbD", "yjbO", "cchB", "tnaA", "dnaJ", "flgD", 
                    "gltA", "sucD", "yhdM", "atpG", "ibpB", "yfaD", 
                    "yecO", "aceB", "lpdA", "nuoM", "hupB", "b1963",
                    "folK", "b1583", "yaeM", "nmpC", "dnaG", "ftsJ"
                ]

        observed = {
                    "ygbD", "yjbO", "cchB", "tnaA", "dnaJ", "flgD", 
                    "gltA", "sucD", "yhdM", "atpG", "ibpB", "yfaD", 
                    "yecO", "aceB", "lpdA", "nuoM", "hupB", "b1963",
                    "folK", "b1583", "yaeM", "nmpC", "dnaG", "ftsJ"
                }

        edges = [
            ("asnA","icdA"),("asnA","lacA"),("asnA","lacY"),("asnA","lacZ"),

            ("atpD","yheI"),

            ("b1191","fixC"),("b1191","tnaA"),("b1191","ygcE"),

            ("cspA","hupB"), ("cspA","yfiA"),

            ("cspG","cspA"),("cspG","lacA"),("cspG","lacY"),("cspG","pspA"),
            ("cspG","pspB"),("cspG","yaeM"),("cspG","yecO" ),("cspG","yedE"),

            ("dnaK","mopB"),

            ("eutG","ibpB"),("eutG","lacY" ),("eutG","sucA"),("eutG","yceP"),
            ("eutG","yfaD"),

            ("fixC","cchB"),("fixC","tnaA"),("fixC","yceP"),("fixC","ycgX"),
            ("fixC","ygbD"),("fixC","yjbO"),

            ("icdA","aceB"),

            ("lacA","b1583"),("lacA","lacY"),("lacA","lacZ"),("lacA","yaeM"),

            ("lacY","lacZ"),("lacY","nuoM"),

            ("lacZ","b1583"),("lacZ","mopB"),("lacZ","yaeM"),

            ("mopB","ftsJ"),

            ("pspA","nmpC"),

            ("pspB","pspA"),

            ("sucA","atpD"),("sucA","atpG"),("sucA","dnaJ"),("sucA","flgD"),
            ("sucA","gltA"),("sucA","sucD"),("sucA","tnaA"),("sucA","yfaD"),
            ("sucA","ygcE"),("sucA","yhdM"),

            ("yceP","b1583"),("yceP","ibpB"),("yceP","yfaD"),

            ("ycgX","dnaG"),

            ("yedE","lpdA"),("yedE","pspA"),("yedE","pspB"),("yedE","yheI"),

            ("yfiA","hupB"),

            ("ygcE","asnA"),("ygcE","atpD"),("ygcE","icdA"),

            ("yheI","b1963"),("yheI","dnaG"),("yheI","dnaK"),("yheI","folK"),
            ("yheI","ycgX")
            ]  
    

        return vertices, edges, observed