import graph_tool.all as gt
import graph.invert as invert
import numpy as np

from graph.forward_graph import ForwardGraph
from graph.belief_network import BeliefNetwork


class RandomBN(BeliefNetwork):
    def __init__(self, num_latent, num_obs, num_edges, true_bn=None, seed=0):
        super(RandomBN, self).__init__()
        
        self.num_latent = num_latent
        self.num_obs = num_obs
        self.true_bn = true_bn
        
        vertices, edges, observed = RandomBN._construct_graph(self.num_latent, self.num_obs, num_edges, seed)
        self.vertices = vertices
        self.edges = edges
        self.observed = observed
        self.forward_graph = ForwardGraph().initialize(vertices, edges, observed)
        self.inverse_graph = invert.properly(self.forward_graph)


    def sample(self, batch_size, train=True):
        return self.true_bn.sample(batch_size, train)


    def log_joint(self, x, z):
        return self.true_bn.log_joint(x, z)
    
    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 
        
        
    @staticmethod
    def _construct_graph(num_latent, num_obs, num_edges, seed):
        np.random.seed(seed)
        vertices = ['z{}'.format(i) for i in range(num_latent)] +\
                   ['x{}'.format(i) for i in range(num_latent,num_latent+num_obs)]
        
        observed = {'x{}'.format(i) for i in range(num_latent,num_latent+num_obs)}
        
        topo_order = np.arange(num_latent)
        np.random.shuffle(topo_order)
        topo_order = np.concatenate((topo_order, np.arange(num_latent,num_latent+num_obs)))
        print(topo_order)
        
        edges = []
        in_counts = np.zeros(num_latent+num_obs)
        p = np.array([5]*num_latent + [1]*num_obs)
        # Each latent should have at least 1 outgoing edge
        for i in range(num_latent):
            from_v = topo_order[i]
            to_v = np.random.choice(topo_order[i+1:], 1, p=p[i+1:]/np.sum(p[i+1:]))[0]
            edges.append(('z{}'.format(from_v), 'z{}'.format(to_v) if to_v < num_latent else 'x{}'.format(to_v)))
            in_counts[to_v] += 1
        
        # Each observed should have at least one incoming edge
        for to_v in range(num_latent, num_latent+num_obs):
            if in_counts[to_v] == 0:
                from_v = np.random.choice(topo_order[:num_latent], 1)[0]
#                 print('II')
#                 print(('z{}'.format(from_v), 'x{}'.format(to_v)))
                edges.append(('z{}'.format(from_v), 'x{}'.format(to_v)))
        
        # Randomly sample remaining edges
        if num_edges - len(edges) < 0:
            raise Exception('Too few edges allowed.')
        n = 0
        topo_order = topo_order.tolist()
        while n < num_edges - len(edges):
            from_v = np.random.choice(topo_order[:num_latent], 1)[0]
            idx = topo_order.index(from_v)
            to_v = np.random.choice(topo_order[idx+1:],1)[0]
            edge = ('z{}'.format(from_v), 'z{}'.format(to_v) if to_v < num_latent else 'x{}'.format(to_v))
            if edge not in edges:
                edges.append(edge)
                n += 1               
        
        print('Randomized BN:')
        print('Edges: ', edges)
        return vertices, edges, observed