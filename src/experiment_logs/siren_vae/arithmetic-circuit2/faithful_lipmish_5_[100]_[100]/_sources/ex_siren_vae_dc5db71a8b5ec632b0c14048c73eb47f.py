import torch
import numpy as np
import matplotlib.pyplot as plt
import torch.distributions as dist
import json

from sacred import Experiment
from sacred.observers import FileStorageObserver
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_graph_vae, build_iw_graph_vae, \
                             build_dreg_iw_graph_vae
from ex_utils import sample_batch, kolmogorov_smirnov
from graph.belief_network import *
from data.load_data import *
from siren_vae_explorations_utils import *

torch.cuda.empty_cache()

ex = Experiment('siren_vae')
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
print('Device: ', device)


@ex.command(unobserved=True)
def plot_z_distributions(_config):
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    # Load model
    path = _config['path']
    model = torch.load(path + '/{}/model.pt'.format(_config['run']), map_location=torch.device(device)).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 20000).double().to(device)
        true_x = X[:,num_latent:]
        true_z = X[:,:num_latent]

        model_x, model_prior_z = model.generative_network.sample(20000)
        model_post_z, _, _ = model(true_x)

        for i in range(num_latent):
            x = [
                model_prior_z[:,i].cpu().tolist(),
                model_post_z[:,i].cpu().tolist(),
                true_z[:,i].cpu().tolist()
            ]
            plt.hist(x=x, bins=50, alpha=0.5, histtype='stepfilled',
                        density=True, color=['steelblue', 'red', 'gold'], edgecolor='none', 
                        label=[r'$p_\theta(z)$', r'$q(z|x)$', r'$p(z)$']) 
            plt.xlabel(r'$z_{}$'.format(i))
            plt.legend()
            plt.savefig(path + '/{}/z{}.png'.format(_config['run'], i), dpi=600, bbox_inches='tight')
            plt.clf()


@ex.command(unobserved=True)
def plot_x_distributions(_config):
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    if _config['normalize']:
        min_std = 1e-5
        batch = sample_batch(bn.sample, 10000)
        x = batch[:,num_latent:]
        z = batch[:,:num_latent]
        x_std = torch.std(x, dim=0).to(device)
        x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
        x_shift = torch.mean(x, dim=0).to(device)
        z_std = torch.std(z, dim=0).to(device)
        z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
        z_shift = torch.mean(z, dim=0).to(device)
    else:
        x_scale = 1.0
        z_scale = 1.0
        x_shift = 0.0
        z_shift = 0.0


    # Load model
    path = _config['path']
    model = torch.load(path + '/{}/model.pt'.format(_config['run']), map_location=torch.device(device)).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 20000).double().to(device)
        true_x = X[:,num_latent:]
        true_x = (true_x - x_shift)/x_scale
        model_x, _ = model.generative_network.sample(20000)

        for i in range(num_obs):
            x = [
                model_x[:,i].cpu().tolist(),
                true_x[:,i].cpu().tolist()
            ]
            ks, _ = kolmogorov_smirnov(x[0], x[1])
            plt.hist(x=x, bins=50, alpha=0.5, histtype='stepfilled',
                        density=True, color=['steelblue', 'red'], edgecolor='none', 
                        label=[r'$p_\theta(x|z)$', r'$p(x,z)$']) 
            plt.xlabel(r'$x_{}$'.format(i))

            plt.legend()
            plt.subplots_adjust(wspace=0.15, hspace=0.15)
            plt.savefig(path + '/{}/x{}.png'.format(_config['run'], i), dpi=600, bbox_inches='tight')
            plt.clf()

@ex.command(unobserved=True)
def diagnostics(_config):
    # Initialize BN
    model_path = _config['model_path']
    model_specs = model_path.split('_')
    if model_specs[0] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    path = './experiment_logs/siren_vae/'
    model = torch.load(path + model_path + '/1/model.pt', map_location=torch.device(device)).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 1000).double().to(device)
        x = X[:,num_latent:]
        z = X[:,:num_latent]

    # Inversion verification
    print('\n-- Invertibility Verification (GRF_n) --')
    print('Largest singular values of the weight matrices of each layer of residual block i:')
    print('For invertibility: sigma_max < 1 and will approximately = {}'.format(0.99))
    for i, block in enumerate(model.generative_network.nf.steps):
        sigmas = block.g.largest_singular_values()
        print('[{}] {}'.format(i, sigmas))

    z0, _ = model.generative_network.nf(z)
    z_prime, _ = model.generative_network.nf.inverse(z0)
    print('Reconstruction error: {}'.format(torch.norm(z-z_prime).item()))

    # Inversion verification
    print('\n-- Invertibility Verification (GRF_g) --')
    print('Largest singular values of the weight matrices of each layer of residual block i:')
    print('For invertibility: sigma_max < 1 and will approximately = {}'.format(0.99))
    for i, block in enumerate(model.inference_network.nf.steps):
        sigmas = block.g.largest_singular_values()
        print('[{}] {}'.format(i, sigmas))

    eps = dist.Normal(
        loc=torch.zeros(num_latent, dtype=torch.float32),#64), DOUBLE
        scale=torch.ones(num_latent, dtype=torch.float32)#64)
    ).sample((1000,)).to(device)
    z, _ = model.inference_network.nf(x=x, eps=eps)
    eps_prime, _ = model.inference_network.nf.inverse(z, x=x)
    print('Reconstruction error: {}'.format(torch.norm(eps-eps_prime).item()))


@ex.command(unobserved=True)
def samples(_config):
    # BN + true samples
    # Initialize BN
    batch_size = 100
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
        trainloader, _, _ = load_dataset(_config['bn'], batch_size, _config['double'])
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
        trainloader, _, _ = load_dataset(_config['bn'], batch_size, _config['double'])
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
        trainloader, _, _ = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'mehra-real':
        bn = Mehra()
        trainloader, _, _ = load_mehra(device, double=_config['double'])
        batch_size = trainloader.batch_size

    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()
    N = len(trainloader.dataset)

    true_z = torch.zeros((N, num_latent))
    true_x = torch.zeros((N, num_obs))
    model_z = torch.zeros((N, num_latent))
    model_x = torch.zeros((N, num_obs))
    inferred_z = torch.zeros((N, num_latent))

    # Model + infer
    model = load_model(_config['path']+'/{}/model.pt'.format(_config['run']), device)

    for i, t in enumerate(trainloader):
        true_z[i*batch_size:i*batch_size+batch_size,:] = t[:,:num_latent]
        true_x[i*batch_size:i*batch_size+batch_size,:] = t[:,num_latent:]
        
        iz, _ = model.inference_network(t[:,num_latent:].double().to(device))
        inferred_z[i*batch_size:i*batch_size+batch_size,:] = iz.cpu()

        mx, mz = model.sample(batch_size)
        model_z[i*batch_size:i*batch_size+batch_size,:] = mz
        model_x[i*batch_size:i*batch_size+batch_size,:] = mx

    samples = {
        'true_x': true_x,
        'model_x': model_x,
        'true_z': true_z,
        'model_z': model_z,
        'inferred_z': inferred_z
    }
    torch.save(samples, _config['path']+'/{}/samples.pt'.format(_config['run']))


def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler, device):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt', map_location=torch.device(device))
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']



@ex.config
def cfg():
    # Belief network [arithmetic-circuit2, ecoli70-alt, arth150-synth]
    bn = 'arithmetic-circuit2'
    # Encoded structure [faithful, fully-connected, random]
    structure = 'faithful'

    # Resdual Flow params
    coeff = 0.99
    n_power_iterations = 5
    # Activatn func [elu, tanh, sigmoid, lipswish, lipmish] 
    activation = 'lipmish'
    grad_in_forward = False

    num_flow_steps = 5
    flow_hidden_dims = [100]
    decoder_hidden_dims = [100]

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    num_epochs = 100

    double = True
    patience = 20
    lr = 1e-2
    seed = 0

    # normalize = False
    iw = False      # importance-weighted samples
    k = 1
    dreg = False    # doubly reparameterized gradients
    wu = False      # warm-up
    wu_N = 1    
    wu_all = False

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False
    save_checkpoints = False

    # Add observer
    ex_name = 'siren_vae'
    sub_folder = '{}_{}_{}_{}_{}'.format(structure, activation, num_flow_steps, flow_hidden_dims, decoder_hidden_dims)
    if iw and not dreg:
        ex_name = 'iw_' + ex_name
        sub_folder += '_{}k'.format(k)   
    elif dreg:
        ex_name = 'iw_' + ex_name + '_dreg'
        sub_folder += '_{}k'.format(k)   

    if wu:
        ex_name += '_wu'
        sub_folder += '_{}N'.format(wu_N)
        if wu_all:
            sub_folder += '_all'
        else:
            sub_folder += '_selected'
        
    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))

    # For commands
    model_path = 'arithmetic-circuit2_faithful_lipmish_5_[100]_[100]'
    legend = [r'SIReN-VAE$_faith$', r'SIReN-VAE$_fc$']
    ylims = [(9,14), (9,14)]
    run = 1


@ex.automain
def run(_config, _rnd, _run):
    # Training info
    batch_size = _config['batch_size']
    lr = _config['lr']
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    checkpoint_n = _config['checkpoint_n']

    grad_in_forward = _config['grad_in_forward']
    wu = _config['wu']
    if grad_in_forward and wu:
        raise NotImplementedError("Cannot apply warm-up when computing gradients during the forward pass of GRF. Set grad_in_forward=False.")

    # BN initialization
    real = False
    # Arithmetic Circuit 2
    if _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
        if _config['wu_all']:
            apply_wu = [True, True, True, True, True]
        else:
            apply_wu = [True, True, False, True, False]
    # EColi70
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
        if _config['wu_all']:
            apply_wu = np.array([True]*15)
        else:
            apply_wu = np.array([False]*15)
            apply_wu[[1,2,13]] = True
    # Arth150
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
        if _config['wu_all']:
            apply_wu = np.array([True]*40)
        else:
            apply_wu = np.array([False]*40)
            apply_wu[[0,3,5,7,11,12,14,18,23,27,28,30,32,34,39]] = True
    # Mehra
    elif _config['bn'] == 'mehra-real':
        bn = Mehra()
        apply_wu = np.array([True]*3)
        print('[LOADING DATA] MEHRA')
        trainloader, valloader, _ = load_mehra()

        if _config['num_train_samples'] < len(trainloader.dataset):
            X = trainloader.dataset[:_config['num_train_samples']]
            trainloader = torch.utils.data.DataLoader(X, batch_size=_config['batch_size'])

        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    else:
        raise Exception("Unknown belief network: {}".format(bn))
    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    n = bn.get_num_vertices()

    # Fix structure
    if _config['structure'] == 'faithful':
        print('[ENCODED STRUCTURE] Using faithful BN structure.')
        x_as_unit = False
        z_as_unit = False

    elif _config['structure'] == 'fully-connected':
        print('[ENCODED STRUCTURE] Updating BN structure to be fully-connected.')
        bn = FCBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn)
        x_as_unit = True
        z_as_unit = True

    elif _config['structure'] == 'random':
        print('[ENCODED STRUCTURE] Randomizing the faithful BN structure.')
        num_edges = bn.get_num_edges()
        bn = RandomBN(num_latent=num_latent, num_obs=num_obs, true_bn=bn,
            num_edges=num_edges, seed=_config['seed'])
        x_as_unit = False
        z_as_unit = False
    
    else:
        raise Exception("Unknown structure: {}".format(_config['structure']))

    if not real:
        train = bn.sample(_config['num_train_samples'])
        val = bn.sample(_config['num_val_samples'])
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)

    # Initialize model and optimizer
    infer_args = {
        'type': 'resflow',
        'num_flow_steps': _config['num_flow_steps'], 
        'hidden_dims': _config['flow_hidden_dims'],
        'coeff': _config['coeff'],
        'n_power_iterations': _config['n_power_iterations'],
        'activation_function': _config['activation']
    }
    gen_args = {
        'type':'resflow',
        'num_flow_steps': _config['num_flow_steps'],
        'flow_hidden_dims': _config['flow_hidden_dims'],
        'decoder_hidden_dims': _config['decoder_hidden_dims'],
        'coeff': _config['coeff'],
        'n_power_iterations': _config['n_power_iterations'],
        'activation_function': _config['activation']
    }

    double = _config['double']
    if _config['iw'] and not _config['dreg']:
        k = _config['k']
        print('[MODEL TYPE] IW-SIReN-VAE (k={})'.format(k))
        if double:
            model = build_iw_graph_vae(bn, k, infer_args, gen_args, device, 
                    x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True, grad_in_forward=grad_in_forward).double().to(device)
        else:
            model = build_iw_graph_vae(bn, k, infer_args, gen_args, device, 
                    x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True, grad_in_forward=grad_in_forward).to(device)
    elif _config['dreg']:
        k = _config['k']
        print('[MODEL TYPE] IW-SIReN-VAE with DReG (k={})'.format(k))
        if double:
            model = build_dreg_iw_graph_vae(bn, k, infer_args, gen_args, 
                    device, x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True, grad_in_forward=grad_in_forward).double().to(device)
        else:
            model = build_dreg_iw_graph_vae(bn, k, infer_args, gen_args,    
                device, x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True, grad_in_forward=grad_in_forward).to(device)
    else:
        if double:
            model = build_graph_vae(bn, infer_args, gen_args, device, 
                    x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True, grad_in_forward=grad_in_forward).double().to(device)
        else:
            model = build_graph_vae(bn, infer_args, gen_args, device, 
                    x_as_unit=x_as_unit, z_as_unit=z_as_unit, condition_sigma=True, grad_in_forward=grad_in_forward).to(device)

    # Warm-up
    if wu:
        print('[WARM-UP] Applied to {}'.format(['z{}'.format(i) for i in range(num_latent) if apply_wu[i]]))
        wu_N = _config['wu_N']


    def beta(epoch):
        if wu:
            if double:
                b = torch.ones(num_latent, dtype=torch.float64) #DOUBLE
            else:
                b = torch.ones(num_latent, dtype=torch.float32) 
            b[apply_wu] = min((1.0/wu_N)*(epoch+1),1.0)
            return b.to(device)
        else:
            return 1.0

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True,
                     min_lr=1e-6, patience=_config['patience'])

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler, device)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        # model, optimizer, start_epoch = load_ckp(ckp_path, model, optimizer)
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_elbo = 0.0

        model.train()
        tb = 1
        for train_batch in trainloader:
            tb += 1
            if double:
                x = train_batch[:,num_latent:].double().to(device)
            else:
                x = train_batch[:,num_latent:].float().to(device)

            if not _config['dreg']:
                loss = model.loss(x, beta=beta(epoch))

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            else:
                optimizer.zero_grad()
                log_p_loss, log_q_loss = model.loss(x, beta=beta(epoch))

                # Gradient of generative network
                grad_dec = torch.autograd.grad(log_p_loss, 
                    model.generative_network.parameters(), retain_graph=True) 
                for i,p in enumerate(model.generative_network.parameters()):
                    p.grad = grad_dec[i].clone()

                # Gradient of inference network
                grad_enc = torch.autograd.grad(log_q_loss, model.
                    inference_network.parameters())
                for i,p in enumerate(model.inference_network.parameters()):
                    p.grad = grad_enc[i].clone()

                loss = log_p_loss.detach()
                optimizer.step()

            train_elbo += loss.detach().item()

        # log training metrics - kl-divergence
        _run.log_scalar("train", value=train_elbo/num_train_batches) 
        if (t == 1 or t%10 == 0) and device == 'cuda:0':
            _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)


        # Log test metrics - kl-divergence
        model.eval()
        with torch.no_grad():
            val_elbo = 0.0
            for val_batch in valloader:
                if double:
                    x = val_batch[:,num_latent:].double().to(device) #DOUBLE
                else:
                    x = val_batch[:,num_latent:].float().to(device)

                val_elbo += model.loss(x).item()

            _run.log_scalar("val", val_elbo/num_val_batches)

        
        # Print progress
        epochs.set_description('Train -ELBO: {:.5f}, Val -ELBO: {:.5f}'.format(
            train_elbo/num_train_batches, val_elbo/num_val_batches),
            refresh=False)
        
        if wu:
            if epoch >= wu_N:
                lr_scheduler.step(train_elbo/num_train_batches)
        else:
            lr_scheduler.step(train_elbo/num_train_batches)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            if _config['save_checkpoints']:
                torch.save(model, path+'/{}/model_{}.pt'.format(_run._id, t))
            else:
                torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1
        
    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))