import torch

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import Protein

class ProteinTransitiveMin(Protein):

    def __init__(self):
        super(ProteinTransitiveMin, self).__init__()

        # graph
        graph = ProteinTransitiveMin._construct_graph()
        self.edges = graph[1]
        self.forward_graph = ForwardGraph().initialize(*graph)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 11


    @staticmethod
    def _construct_graph():
        vertices = ['raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk']
        observed = {'raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk'}

        edges = [('plcg','pip3'),
                 ('pip3','pip2'),
                 ('pip2','pkc'),
                 ('pkc','pka'),
                 ('pka','jnk'),('pka','raf'),('pka','p38'),
                 ('raf','mek'),
                 ('mek','erk'),('erk','akt')]
                 
                
        return vertices, edges, observed