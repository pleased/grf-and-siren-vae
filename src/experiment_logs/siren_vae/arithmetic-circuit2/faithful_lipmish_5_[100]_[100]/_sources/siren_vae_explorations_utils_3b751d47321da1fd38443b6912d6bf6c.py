import os
import json
import torch
import math
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


from ex_utils import sample_batch
from matplotlib import gridspec
from matplotlib.lines import Line2D

from vae.vanilla.infer_network import VanillaInferNetwork
from vae.vanilla.gen_network import VanillaGenNetwork

# From https://gka.github.io/palettes/#/6|d|798bcf,00ff92,ffffe0|f3ff2c,ff0063,93003a|1|1 
global_colours = ['red','blue', 'green', 'darkorange', 'teal']


def plot_loss(experiments, _config, add_approx_marginal=True):
    num_runs = _config['num_runs']
    iter = _config['iter']
    ylims = _config['ylims']
    log_scale = _config['log_scale']
    legend = _config['legend']
    path = './experiment_logs/'
    exs_data = {'names':[]}
    num_experiments = len(experiments)
    palette = global_colours[:num_experiments]

    fig, ax = plt.subplots(1,2, figsize=(15,5))
    for p in range(num_experiments):
        ex_name = experiments[p]
        exs_data['names'].append(ex_name)
        
        for i in range(1,num_runs+1):
            with open(os.path.join(path, experiments[p], str(i), 'metrics.json')) as json_file:
                data = json.load(json_file)
                
                if p == 0 and i == 1:
                    exs_data['iter'] = data['train']['steps'][:iter]
                    num_iters = len(data['train']['steps'][:iter])
                    exs_data['elbo'] = np.zeros((num_iters, num_runs*num_experiments))
                    exs_data['val elbo'] = np.zeros((num_iters, num_runs*num_experiments))

                exs_data['elbo'][:,p*num_runs+i-1] = data['train']['values'][:iter]
                exs_data['val elbo'][:,p*num_runs+i-1] = data['val']['values'][:iter]

    columns = [[], []]
    for ex_name in exs_data['names']:
        columns[0] += [ex_name]*num_runs
        for i in range(1, num_runs+1):
            columns[1] += ['run_'+str(i)]
    columns = list(zip(*columns))
    columns = pd.MultiIndex.from_tuples(columns, names=["experiment", "runs"])

    # Training shifted reverse kl-divergence
    df = pd.DataFrame(exs_data['elbo'], index=exs_data['iter'], columns=columns)
    df = df.unstack(level=1).reset_index()
    s0 = sns.lineplot(data=df, x='level_2', y=0, hue='experiment',
        palette=palette, ax=ax[0])
    if log_scale:
        s0.set(yscale='log')
    ax[0].set_ylabel('Train Negative ELBO')
    ax[0].set_xlabel('epoch')
    if ylims is not None:
        ax[0].set_ylim(ylims[0])
    ax[0].legend([],[], frameon=False)

    # Test shifted reverse kl-divergence
    df = pd.DataFrame(exs_data['val elbo'], index=exs_data['iter'], columns=columns)
    df = df.unstack(level=1).reset_index()
    s1 = sns.lineplot(data=df, x='level_2', y=0, hue='experiment',
        palette=palette, ax=ax[1])
    if log_scale:
        s1.set(yscale='log')
    ax[1].set_ylabel('Validation Negative ELBO')
    ax[1].set_xlabel('epoch')
    if ylims is not None:
        if len(ylims) > 1:
            ax[1].set_ylim(ylims[1])
        else:
            ax[1].set_ylim(ylims[0])
    
    if add_approx_marginal:
        legend += [r'Approx $-\log p(x)$']
        _add_approx_marginal(_config['bn'], _config['fixed_model_path'], [s0, s1], _config['device'])
        s1.legend(legend)
        leg = s1.get_legend()
        leg.legendHandles[-1].set_color('grey')
        leg.legendHandles[-1].set_linestyle('--')
    else:
        s1.legend(legend)
    plt.show()
    

def plot_loss2(experiments, _config, add_approx_marginal=True):
    iter = _config['iter']
    ylims = _config['ylims']
    log_scale = _config['log_scale']
    legend = _config['legend']
    path = './experiment_logs/'
    num_experiments = len(experiments)
    if 'palette' not in _config: 
        palette = global_colours[:num_experiments]
    else:
        palette = _config['palette']
    if 'linestyles' not in _config:
        linestyles = ['-']*num_experiments
    else:
        linestyles = _config['linestyles']

    fig, ax = plt.subplots(1,2, figsize=(15,5))
    for i, ex in enumerate(experiments):
        ex_path = path + ex
        test_losses = []
        train_losses = []
        for run in os.listdir(ex_path):
            if not run.isdigit(): continue
            ex_run_path = ex_path + '/{}/'.format(run)
            with open(ex_run_path+'metrics.json') as f:
                metrics = json.load(f)
            test_losses.append(metrics['val']['values'][:iter])
            train_losses.append(metrics['train']['values'][:iter])
        test_losses = np.array(test_losses)
        train_losses = np.array(train_losses)
        
        test_mean = test_losses.mean(axis=0)
        train_mean = train_losses.mean(axis=0)
        
        test_std = test_losses.std(axis=0)
        train_std = train_losses.std(axis=0)
        
        ax[0].plot(train_mean, label = legend[i], c=palette[i], 
            linestyle=linestyles[i])
        ax[1].plot(test_mean, label = legend[i], c=palette[i], 
            linestyle=linestyles[i])
        ax[0].fill_between(range(train_losses.shape[1]), 
            train_mean - train_std, train_mean + train_std, color=palette[i], alpha=0.3)
        ax[1].fill_between(range(test_losses.shape[1]), test_mean - test_std, 
            test_mean + test_std, color=palette[i], alpha=0.3)
        
        ax[0].set_xlabel('Epoch')
        ax[1].set_xlabel('Epoch')
        ax[0].set_ylabel(r'Train $-$ELBO')
        ax[1].set_ylabel(r'VaLidation $-$ELBO')
    
    ax[0].legend([],[], frameon=False)
    
    if ylims is not None:
        ax[0].set_ylim(ylims[0])
        ax[1].set_ylim(ylims[1])

    if log_scale:
        ax[0].set(yscale='log')
        ax[1].set(yscale='log')

    if add_approx_marginal:
        legend += [r'Approx $-\log p(x)$']
        _add_approx_marginal(_config['bn'], _config['fixed_model_path'], ax, _config['device'])
        handles, labels = ax[1].get_legend_handles_labels()
        ax[1].legend(handles, labels)
    else:
        handles, labels = ax[1].get_legend_handles_labels()
        ax[1].legend(handles, labels)
       
    plt.show()



def _add_approx_marginal(bn, ex, ax, device):
    torch.manual_seed(0)
    np.random.seed(0)
    model = torch.load('./experiment_logs/{}/1/model.pt'.format(ex), map_location=torch.device(device)).double().to(device)
    num_latent = bn.get_num_latent()
    sample = sample_batch(bn.sample, 5000).double().to(device)
    x = sample[:,num_latent:]
    eps0, z, j = model(cond=x)
    p_x = model.shifted_reverse_kl(x, z, eps0, j).item()
    ax[0].axhline(y=p_x, color='grey', linestyle='--', label=r'Approx $-\log p(x)$')
    ax[1].axhline(y=p_x, color='grey', linestyle='--', label=r'Approx $-\log p(x)$')



def re(model, x):
    z, _ = model.inference_network(x)
    x_prime = model.generative_network.decoder_net(z)[:,:model.generative_network.obs_dim]
    return torch.norm(x-x_prime, dim=1).mean()


def log_lik(model, x, batch_size):
    # 1/N [log p(x,z) - log q(z|x)]
    # _, log_q_z_given_x, log_p_x_and_z = model(x)
    # return torch.mean(log_p_x_and_z - log_q_z_given_x)
    ll = 0
    data = torch.utils.data.DataLoader(x, batch_size=batch_size)
    with torch.no_grad():
        for batch in data:
            ll += torch.mean(model.importance_sampled_log_likelihood(batch, 50))
    return ll/(x.shape[0]//batch_size)


def sparsity(bn):
    l, o = bn.get_num_latent(), bn.get_num_obs()
    total = l*(l-1)/2 + l*o
    return bn.get_num_edges()/total * 100


def print_reconstruction_errors(experiments, device):
    # experiments = [
    #   (bn1,   
    #    {'vanilla': path,
    #     'fc':path,
    #     'graphical':path,
    #     'random':path
    #    }),
    #   (bn2: ...)
    # ]

    hline = "+"+"-"*22+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"

    print("+"+'-'*86+"+")
    print("|"+" "*33+"RECONSTRUCTION ERROR"+" "*33+"|")
    print("+"+'-'*86+"+")
    print("| Belief Network       | Vanilla       | FC            | True          | Random        |")
    print("+"+"="*22+"+"+"="*15+"+"+"="*15+"+"+"="*15+"+"+"="*15+"+")

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            torch.manual_seed(0)
            np.random.seed(0)
            num_latent = bn.get_num_latent()
            sample = sample_batch(bn.sample, 5000).double().to(device)
            x = sample[:,num_latent:]

            # VAE 1: vanilla
            r = []
            p = path + ex[1]['vanilla']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                        r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re1_m = np.mean(r)
                re1_s = np.std(r)
            except: 
                re1_m = 99
                re1_s = 99
            re1 = "{0:4.2f} ({1:4.2f})".format(re1_m, re1_s)
            
            # VAE 2: fully-connected
            r = []
            p = path + ex[1]['fc']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re2_m = np.mean(r)
                re2_s = np.std(r)
            except: 
                re2_m = 99
                re2_s = 99
            re2 = "{0:4.2f} ({1:4.2f})".format(re2_m, re2_s)
            
            # VAE 3: graphical
            r = []
            p = path + ex[1]['graphical']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re3_m = np.mean(r)
                re3_s = np.std(r)
            except: 
                re3_m = 99
                re3_s = 99
            re3 = "{0:4.2f} ({1:4.2f})".format(re3_m, re3_s)

            # VAE 4: random
            r = []
            p = path + ex[1]['random']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re4_m = np.mean(r)
                re4_s = np.std(r)
            except: 
                re4_m = 99
                re4_s = 99
            re4 = "{0:4.2f} ({1:4.2f})".format(re4_m, re4_s)
            
            print("| {0:<20} | {1:<13} | {2:<13} | {3:<13} | {4:<13} |".format(type(bn).__name__, re1, re2, re3, re4))
            print(hline)

def print_reconstruction_errors_real(experiments, device):
    hline = "+"+"-"*22+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"
    print(hline)

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            dataloader = ex[1]
            num_latent = bn.get_num_latent()
            sample = next(iter(dataloader)).to(device).double()
            x = sample[:,num_latent:]

            # VAE 1: vanilla
            r = []
            p = path + ex[2]['vanilla']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                        r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re1_m = np.mean(r)
                re1_s = np.std(r)
            except: 
                re1_m = 99
                re1_s = 99
            re1 = "{0:4.2f} ({1:4.2f})".format(re1_m, re1_s)
            
            # VAE 2: fully-connected
            r = []
            p = path + ex[2]['fc']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re2_m = np.mean(r)
                re2_s = np.std(r)
            except: 
                re2_m = 99
                re2_s = 99
            re2 = "{0:4.2f} ({1:4.2f})".format(re2_m, re2_s)
            
            # VAE 3: graphical
            r = []
            p = path + ex[2]['graphical']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re3_m = np.mean(r)
                re3_s = np.std(r)
            except: 
                re3_m = 99
                re3_s = 99
            re3 = "{0:4.2f} ({1:4.2f})".format(re3_m, re3_s)

            # VAE 4: random
            r = []
            p = path + ex[2]['random']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(re(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device('cpu')), x).cpu())
                re4_m = np.mean(r)
                re4_s = np.std(r)
            except: 
                re4_m = 99
                re4_s = 99
            re4 = "{0:4.2f} ({1:4.2f})".format(re4_m, re4_s)
            
            name = '{} (Real)'.format(type(bn).__name__)
            print("| {0:<20} | {1:<13} | {2:<13} | {3:<13} | {4:<13} |".format(name, re1, re2, re3, re4))
            print(hline)


def print_test_log_lik(experiments, device):
    # experiments = [
    #   (bn1,   
    #    {'vanilla': path,
    #     'fc':path,
    #     'graphical':path,
    #     'random':path
    #    }),
    #   (bn2: ...)
    # ]
    hline = "+"+"-"*22+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"

    print("+"+'-'*86+"+")
    print("|"+" "*31+"LOG MARGINAL LIKELIHOOD"+" "*32+"|")
    print("+"+'-'*86+"+")
    print("| Belief Network       | Vanilla       | FC            | True          | Random        |")
    print("+"+"="*22+"+"+"="*15+"+"+"="*15+"+"+"="*15+"+"+"="*15+"+")

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            torch.manual_seed(0)
            np.random.seed(0)
            num_latent = bn.get_num_latent()
            sample = sample_batch(bn.sample, 5000).double().to(device)
            x = sample[:,num_latent:]

            # VAE 1: vanilla
            r = []
            p = path + ex[1]['vanilla']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=50).cpu())
                re1_m = np.mean(r)
                re1_s = np.std(r)
            except: 
                re1_m = 99
                re1_s = 99
            re1 = "{0:4.2f} ({1:4.2f})".format(re1_m, re1_s)
            
            # VAE 2: fully-connected
            r = []
            p = path + ex[1]['fc']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=50).cpu())
                re2_m = np.mean(r)
                re2_s = np.std(r)
            except: 
                re2_m = 99
                re2_s = 99
            re2 = "{0:4.2f} ({1:4.2f})".format(re2_m, re2_s)
            
            # VAE 3: graphical
            r = []
            p = path + ex[1]['graphical']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=50).cpu())
                re3_m = np.mean(r)
                re3_s = np.std(r)
            except: 
                re3_m = 99
                re3_s = 99
            re3 = "{0:4.2f} ({1:4.2f})".format(re3_m, re3_s)

            # VAE 4: random
            r = []
            p = path + ex[1]['random']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=50).cpu())
                re4_m = np.mean(r)
                re4_s = np.std(r)
            except: 
                re4_m = 99
                re4_s = 99
            re4 = "{0:4.2f} ({1:4.2f})".format(re4_m, re4_s)
            
            print("| {0:<20} | {1:<13} | {2:<13} | {3:<13} | {4:<13} |".format(type(bn).__name__, re1, re2, re3, re4))
            print(hline)

def print_test_log_lik_real(experiments, device):
    hline = "+"+"-"*22+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"

    print(hline)

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            torch.manual_seed(0)
            np.random.seed(0)
            bn = ex[0]
            dataloader = ex[1]
            num_latent = bn.get_num_latent()
            sample = next(iter(dataloader)).to(device).double()
            x = sample[:,num_latent:]
            
            # VAE 1: vanilla
            r = []
            p = path + ex[2]['vanilla']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=sample.shape[0]).cpu())
                re1_m = np.mean(r)
                re1_s = np.std(r)
            except: 
                re1_m = 99
                re1_s = 99
            re1 = "{0:4.2f} ({1:4.2f})".format(re1_m, re1_s)
            
            # VAE 2: fully-connected
            r = []
            p = path + ex[2]['fc']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=sample.shape[0]).cpu())
                re2_m = np.mean(r)
                re2_s = np.std(r)
            except: 
                re2_m = 99
                re2_s = 99
            re2 = "{0:4.2f} ({1:4.2f})".format(re2_m, re2_s)
            
            # VAE 3: graphical
            r = []
            p = path + ex[2]['graphical']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=sample.shape[0]).cpu())
                re3_m = np.mean(r)
                re3_s = np.std(r)
            except: 
                re3_m = 99
                re3_s = 99
            re3 = "{0:4.2f} ({1:4.2f})".format(re3_m, re3_s)

            # VAE 4: random
            r = []
            p = path + ex[2]['random']
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    r.append(log_lik(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)), x, batch_size=sample.shape[0]).cpu())
                re4_m = np.mean(r)
                re4_s = np.std(r)
            except: 
                re4_m = 99
                re4_s = 99
            re4 = "{0:4.2f} ({1:4.2f})".format(re4_m, re4_s)
            
            name = '{} (Real)'.format(type(bn).__name__)
            print("| {0:<20} | {1:<13} | {2:<13} | {3:<13} | {4:<13} |".format(name, re1, re2, re3, re4))
            print(hline)


def print_number_collapsed_units(experiments, device):
    # experiments = [
    #   (bn1,   
    #    {'vanilla': path,
    #     'fc':path,
    #     'graphical':path
    #    }),
    #   (bn2: ...)
    # ]
    hline = "+"+"-"*22+"+"+"-"*15+"+"+"-"*15+"+"+"-"*15+"+"

    print("+"+'-'*70+"+")
    print("|"+" "*23+"NUM COLLAPSED UNITS"+" "*24+"|")
    print("+"+'-'*70+"+")
    print("| Belief Network       | Vanilla       | FC            | True          |")
    print("+"+"="*22+"+"+"="*15+"+"+"="*15+"+"+"="*15+"+")

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            num_latent = bn.get_num_latent()

            # VAE 1: vanilla
            ia = []
            p = path + ex[1]['vanilla']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    model = torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)).double().to(device)
                    x,_ = model.sample(100)
                    A = get_units_variances(model, x, num_latent).cpu()
                    ia.append(np.count_nonzero(A <= 0.01))
                ia1 = "{0:4.2f} ({1:4.2f})".format(np.mean(ia), np.std(ia))
            except: 
                ia1 = "99 (99)".format(np.mean(ia), np.std(ia))
            
            # VAE 2: fully-connected
            ia = []
            p = path + ex[1]['fc']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    model = torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)).double().to(device)
                    x,_ = model.sample(100)
                    A = get_units_variances(model, x, num_latent).cpu()
                    ia.append(np.count_nonzero(A <= 0.01))
                ia2 = "{0:4.2f} ({1:4.2f})".format(np.mean(ia), np.std(ia))
            except: 
                ia2 = "99 (99)".format(np.mean(ia), np.std(ia))
            
            # VAE 3: graphical
            ia = []
            p = path + ex[1]['graphical']
            torch.manual_seed(0)
            np.random.seed(0)
            try:
                num_runs = len(os.listdir(p)) - 1
                for run in range(1,num_runs+1):
                    model = torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)).double().to(device)
                    x,_ = model.sample(100)
                    A = get_units_variances(model, x, num_latent).cpu()
                    ia.append(np.count_nonzero(A <= 0.01))
                ia3 = "{0:4.2f} ({1:4.2f})".format(np.mean(ia), np.std(ia))
            except: 
                ia3 = "99 (99)".format(np.mean(ia), np.std(ia))
            
            print("| {0:<20} | {1:<13} | {2:<13} | {3:<13} |".format(type(bn).__name__, ia1, ia2, ia3))
            print(hline)


def print_sparsity(bns):
    hline = "+"+'-'*22+"+"+'-'*10+"+"
    print(hline)
    print("| Belief Network       | SPARSITY |")
    print("+"+'='*22+"+"+'='*10+"+")
    for bn in bns:
        s = "{:.3f}%".format(sparsity(bn))
        print("| {0:<20} | {1:<8} |".format(type(bn).__name__, s))
        print(hline)


def plot_training_size_results(x, bn, experiments, jitter, device, save_to=None):
    # experiments = [
    #   (2, {
    #       'vanilla': path,
    #       'fc': path,
    #       'graphical': path,
    #   })
    # ]

    plt.style.use('seaborn')
    plt.rc('axes', labelsize=20)
    plt.rc('legend', fontsize=20)
    plt.rcParams.update({'font.size': 20})

    path = './experiment_logs/'
    fig, ax = plt.subplots(1,1, figsize=(10,6))
    num_latent = bn.get_num_latent()
    num_vertices = bn.get_num_vertices()
    
    
    x = x[:,num_latent:]

    torch.manual_seed(0)
    np.random.seed(0)


    model_names = ['vanilla', 'fc', 'graphical']
    labels = ['VAE', r'SIReN-VAE$_{FC}$', r'SIReN-VAE$_{True}$']
    
    for model_name, label in zip(model_names, labels):
        means = []
        stds = []
        factors = []
        for ex in experiments:
            n = ex[0]
            factors.append(n)
            size = n*num_vertices
            size_losses = []
            path = './experiment_logs/'+ex[1][model_name]
            for run in os.listdir(path):
                if not run.isdigit(): continue
                run_path = path + '/{}/'.format(run)
                with torch.no_grad():
                    model = torch.load(run_path+'model.pt', map_location=torch.device(device)).double().to(device)
                    # z, log_q_z_given_x, log_p_x_and_z = model(x)
                    size_losses.append(model.loss(x).item())
            means.append(np.mean(size_losses))
            stds.append(np.std(size_losses))

        # losses = np.array(losses)
        means = np.array(means)
        stds = np.array(stds)

        sizes = [n*num_vertices for n in factors]
        jitter = jitter*np.random.rand(len(sizes))-jitter/2
        ax.errorbar(sizes+jitter, means, yerr=stds, label=label, fmt=':', capsize=3, capthick=1) 
        ax.fill_between(sizes+jitter, means-stds, means+stds, alpha=0.25)
    
    ax.set_xlabel('Training set size')
    ax.set_ylabel(r'$-$ELBO')
    # ax.set_title(type(bn).__name__)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels)#, prop={'size': 16})



    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(20)

    if save_to is not None:
        plt.savefig('./experiment_logs/figures/data-sprase_setting/'+save_to,
                    dpi=600, bbox_inches='tight', facecolor='white') 

    plt.show()
    
    

def get_units_variances(model, x, num_latent):
    model = set_model(model, True, x.device)
    with torch.no_grad():
        means = torch.zeros((x.shape[0],num_latent))
        
        # sample z
        for i in range(x.shape[0]):
            xi = x[i].expand(500,-1)
            z, _ = model.inference_network(xi)
            
            means[i] = torch.mean(z,dim=0)
        
        vars_of_means = torch.var(means, dim=0)
    
    return vars_of_means   


def plot_unit_variances(variances):
    fig, ax = plt.subplots(1,1,figsize=(3,2))
    ax.hist(np.log(variances.cpu().numpy()), bins=20)
    ax.axvline(np.log(0.01), color='r', linestyle='dashed')
    ax.set_xlabel('Log variance')
    ax.set_ylabel('Number of units')
    plt.show()


def graph_distance_matrix(bn):
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()
    # fig, ax = plt.subplots(1,2, figsize=(10,5), sharey=True)

    factor = 6
    fig = plt.figure(figsize=((num_obs/num_latent)*factor, factor))
    spec = gridspec.GridSpec(ncols=2, nrows=1, 
                width_ratios=[1, num_obs/num_latent], wspace=0.1,
                hspace=0.05)#, height_ratios=[1, 1])

    d = np.zeros((num_latent+num_obs, num_latent+num_obs))
    for i in range(num_latent):
        for j in range(i+1, num_latent+num_obs):
            dist = graph_distance(bn.forward_graph, i, j)
            d[i,j] = dist
            d[j,i] = dist

    d = np.clip(d, d.min(), np.unique(d)[-2] + 1)

    d = -d + np.max(d)

    ax0 = fig.add_subplot(spec[0])
    ax0.imshow(np.triu(d[:num_latent,:num_latent] - np.diag(np.diag(d[:num_latent,:num_latent]))), cmap='gist_yarg')
    ax0.set_xticks([i for i in range(0,num_latent,2)])
    ax0.set_yticks([i for i in range(0,num_latent,2)])
    for tick in ax0.get_xticklabels():
        tick.set_rotation(90)

    ax1 = fig.add_subplot(spec[1])
    ax1.imshow(d[:num_latent,num_latent:], cmap='gist_yarg')
    ax1.set_xticks([i for i in range(0,num_obs,2)])
    for tick in ax1.get_xticklabels():
        tick.set_rotation(90)
    ax1.set_yticks([i for i in range(0,num_latent,2)])
    plt.show()


def graph_distance(g, i, j):
    def _d(visited, g, i, j):
        min_dist = len(g.get_vertices())
        visited.append(i)
        queue.append((i,0))

        while queue:
            v, d = queue.pop()

            for u in g.get_out_neighbours(v):
                if u not in visited:
                    if u == j:
                        min_dist = d+1
                    else:
                        if d+1 < min_dist:
                            queue.append((u,d+1))
                    visited.append(u)

        return min_dist if min_dist != len(g.get_vertices()) else -1

    visited = []
    queue = []
    return _d(visited, g, i, j)


def double_model(model):
    # inference network
    for step in model.inference_network.nf.steps:
        for layer in step.g.layers:
            layer._weight = layer._weight.double()
            layer._bias = torch.nn.Parameter(layer._bias.double())
            layer.mask = layer.mask.double()

    # generative network
    for step in model.generative_network.nf.steps:
        for layer in step.g.layers:
            layer._weight = layer._weight.double()
            layer._bias = torch.nn.Parameter(layer._bias.double())
            layer.mask = layer.mask.double()
    model.generative_network.decoder_net = model.generative_network.decoder_net.double()

    return model



def print_inactive_units(experiments, headings, device):
    # experiments = [
    #   (bn1,  [path1, path2, ...]),
    #   (bn2:  [path1, path2, ...])
    # ]

    num_columns = len(headings)
    hline = "+"+"-"*22+"+" + ("-"*7+"+"+"-"*16+"+")*num_columns

    print("+"+'-'*22+"+" + ('-'*24+'+')*num_columns)
    column_headings1 = "|                      |"
    for heading in headings:
        column_headings1 += ' {:<15} |'.format(heading)
    print(column_headings1)

    print("+    Belief Network    +" + \
                        "------------------------+"*num_columns)
    print("|                      |" + \
                        " #IA   | p(x)           |"*num_columns)
    
    print("+"+"="*22+"+" + ("="*24+"+")*num_columns)

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            num_latent = bn.get_num_latent()

            torch.manual_seed(0)
            np.random.seed(0)
            sample = sample_batch(bn.sample, 5000).double().to(device)[:,num_latent:]

            inactive_units = []
            elbos = []
            for i in range(num_columns):
                p = path + ex[1][i]
                try:
                    ia = []
                    eb = []
                    num_runs = len(os.listdir(p)) - 1
                    for run in range(1,num_runs+1):
                        torch.manual_seed(0)
                        np.random.seed(0)
                        model = double_model(torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)).to(device))
                    
                        x,_ = model.sample(100)
                        A = get_units_variances(model, x.double(), num_latent).cpu()
                        ia.append(np.count_nonzero(A <= 0.01))
                        eb.append(log_lik(model, sample, batch_size=50).cpu())
                    inactive_units.append("{:.2f}".format(np.mean(ia)))
                    elbos.append("{:.2f} ({:.2f})".format(np.mean(eb), np.std(eb)))
                except: 
                    inactive_units.append("-99")
                    elbos.append("-99")
            
            output = "| {0:<20} |".format(type(bn).__name__)
            for i in range(num_columns):
                output += " {0:<5} | {1:<7} |".format(inactive_units[i], 
                                elbos[i])
            print(output)
            print(hline)


def print_inactive_units_real(experiments, headings, device):
    # experiments = [
    #   (bn1, dataloader1, [path1, path2, ...]),
    #   (bn2, dataloader2, [path1, path2, ...])
    # ]

    num_columns = len(headings)
    hline = "+"+"-"*22+"+" + ("-"*7+"+"+"-"*9+"+")*num_columns

    print("+"+'-'*22+"+" + ('-'*17+'+')*num_columns)
    column_headings1 = "|                      |"
    for heading in headings:
        column_headings1 += ' {:<15} |'.format(heading)
    print(column_headings1)

    print("+    Belief Network    +" + \
                        "-----------------+"*num_columns)
    print("|                      |" + \
                        " #IA   | p(x)    |"*num_columns)
    
    print("+"+"="*22+"+" + ("="*17+"+")*num_columns)

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            num_latent = bn.get_num_latent()
            dataloader = ex[1]
            sample = next(iter(dataloader)).to(device).double()[:,num_latent:]

            inactive_units = []
            elbos = []
            for i in range(num_columns):
                torch.manual_seed(0)
                np.random.seed(0)
                p = path + ex[2][i]
                try:
                    ia = []
                    eb = []
                    num_runs = len(os.listdir(p)) - 1
                    for run in range(1,num_runs+1):
                        model = torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)).double().to(device)
                        x,_ = model.sample(100)
                        A = get_units_variances(model, x, num_latent).cpu()
                        ia.append(np.count_nonzero(A <= 0.01))
                        eb.append(log_lik(model, sample, batch_size=sample.shape[0]).cpu())
                    inactive_units.append("{:.2f}".format(np.mean(ia)))
                    elbos.append("{:.2f}".format(np.mean(eb)))
                except: 
                    inactive_units.append("-99")
                    elbos.append("-99")
            
            output = "| {0:<20} |".format("{} (Real)".format(type(bn).__name__))
            for i in range(num_columns):
                output += " {0:<5} | {1:<7} |".format(inactive_units[i], 
                                elbos[i])
            print(output)
            print(hline)


def get_epoch_vs_ia_units(path, m, num_latent, device, factor):
    with open(path+'metrics.json') as f:
        metrics = json.load(f)

    epochs = [0]
    ia = [0]
    if 'inactive_units' in metrics:
        inactive_units = metrics['inactive_units']
        epochs.extend([int(e) for e in inactive_units.keys()])
        ia.extend([int(a) for a in inactive_units.values()])
        epochs, ia = zip(*sorted(zip(epochs, ia)))
        epochs = list(epochs)
        ia = list(ia)
        begin = epochs[-1]//10 + 1 
    else:
        metrics['inactive_units'] = {}
        begin = 1

    for i in range(begin,m+1):
        torch.manual_seed(0)
        torch.cuda.manual_seed(0)
        epoch = i*factor
        model = torch.load(path + 'model_{}.pt'.format(epoch), map_location=torch.device(device)).to(device)
        x,_ = model.sample(100)
        A = get_units_variances(model, x.to(device), num_latent)
        cnt = np.count_nonzero(A <= 0.01)
        ia.append(cnt)
        epochs.append(epoch)
        metrics['inactive_units'][epoch] = cnt

    with open(path+'metrics.json', 'w') as f:
        json.dump(metrics, f)

    return epochs, ia


def load_model(path, device, double=True):
    return set_model(torch.load(path, map_location=torch.device(device)), double, device)



def set_model(model, double=True, device='cpu'):
    if double:
        model = model.double().to(device)
    else:
        model = model.float().to(device)
    # if isinstance(model.bn, Tree):
    #     model.bn = Tree(device, double)

    if isinstance(model.inference_network, VanillaInferNetwork):
        model.inference_network.device = device
        if double:
            model.inference_network.encoder_net.net = model.inference_network.encoder_net.net.double().to(device)
        else:
            model.inference_network.encoder_net.net = model.inference_network.encoder_net.net.float().to(device)
    else:
        model.inference_network.nf.device = device
        if double:
            if not hasattr(model.inference_network.nf, 'scale') or model.inference_network.nf.scale is None:
                model.inference_network.nf.scale = torch.tensor([1.0]).double().to(device)
            else:
                model.inference_network.nf.scale = model.inference_network.nf.scale.double().to(device)
            if not hasattr(model, 'shift') or model.inference_network.nf.shift is None:
                model.inference_network.nf.shift = torch.tensor([0.0]).double().to(device)
            else:
                model.inference_network.nf.shift = model.inference_network.nf.shift.double().to(device)
        else:
            if not hasattr(model.inference_network.nf, 'scale') or model.inference_network.nf.scale is None:
                model.inference_network.nf.scale = torch.tensor([1.0]).float().to(device)
            else:
                model.inference_network.nf.scale = model.inference_network.nf.scale.float().to(device)
            if not hasattr(model, 'shift') or model.inference_network.nf.shift is None:
                model.inference_network.nf.shift = torch.tensor([0.0]).float().to(device)
            else:
                model.inference_network.nf.shift = model.inference_network.nf.shift.float().to(device)
        for block in model.inference_network.nf.steps:
            for layer in block.g.layers:
                if double:
                    layer._weight = layer._weight.double().to(device)
                    layer._bias = torch.nn.Parameter(layer._bias.double().to(device))
                    layer.mask = layer.mask.double().to(device)
                else:
                    layer._weight = layer._weight.float().to(device)
                    layer._bias = torch.nn.Parameter(layer._bias.float().to(device))
                    layer.mask = layer.mask.float().to(device)
        
    if isinstance(model.generative_network, VanillaGenNetwork):
        model.generative_network.device = device
        if double:
            model.generative_network.decoder_net.net = model.generative_network.decoder_net.net.double().to(device)
        else:
            model.generative_network.decoder_net.net = model.generative_network.decoder_net.net.float().to(device)
    else:
        model.generative_network.device = device
        if double:
            if not hasattr(model.generative_network.nf, 'scale') or model.generative_network.nf.scale is None:
                model.generative_network.nf.scale = torch.tensor([1.0]).double().to(device)
            else:
                model.generative_network.nf.scale = model.generative_network.nf.scale.double().to(device)
            if not hasattr(model, 'shift') or model.generative_network.nf.shift is None:
                model.generative_network.nf.shift = torch.tensor([0.0]).double().to(device)
            else:
                model.generative_network.nf.shift = model.generative_network.nf.shift.double().to(device)
        else:
            if not hasattr(model.generative_network.nf, 'scale') or model.generative_network.nf.scale is None:
                model.generative_network.nf.scale = torch.tensor([1.0]).float().to(device)
            else:
                model.generative_network.nf.scale = model.generative_network.nf.scale.float().to(device)
            if not hasattr(model, 'shift') or model.generative_network.nf.shift is None:
                model.generative_network.nf.shift = torch.tensor([0.0]).float().to(device)
            else:
                model.generative_network.nf.shift = model.generative_network.nf.shift.float().to(device)
        for block in model.generative_network.nf.steps:
            for layer in block.g.layers:
                if double:
                    layer._weight = layer._weight.double().to(device)
                    layer._bias = torch.nn.Parameter(layer._bias.double().to(device))
                    layer.mask = layer.mask.double().to(device)
                else:
                    layer._weight = layer._weight.float().to(device)
                    layer._bias = torch.nn.Parameter(layer._bias.float().to(device))
                    layer.mask = layer.mask.float().to(device)

    return model


# Universal metric printing function
def print_metric(experiments, headings, metric, device):
    # experiments = [
    #   (bn1, dataloader, [path1, path2, ...]),
    #   (bn2, dataloader, [path1, path2, ...])
    # ]

    num_columns = len(headings)
    hline = "+"+"-"*22+"+" + ("-"*25+"+")*num_columns

    if metric == 'recon_err': h = 'RECONSTRUCTION ERROR'
    elif metric == 'nll': h = 'NEGATIVE LOG LIKELIHOOD log p(x)'
    elif metric == 'inactive_units': h = 'NUM INACTIVE UNITS'
    
    topline = '+'+('-')*(len(hline)-2)+'+'
    h = h.ljust(len(hline)-4)
    print(topline)
    print('| {} |'.format(h))
    print(hline)


    column_headings1 = "|    Belief Network    |"
    for heading in headings:
        column_headings1 += ' {:<23} |'.format(heading)
    print(column_headings1)
    print("+"+"="*22+"+" + ("="*25+"+")*num_columns)

    path = './experiment_logs/'
    for ex in experiments:
        with torch.no_grad():
            bn = ex[0]
            num_latent = bn.get_num_latent()
            dataloader = ex[1]

            metrics = []
            if metric == 'nll' or metric == 'recon_err':
                sample = next(iter(dataloader)).to(device)
                x = sample[:,num_latent:]

            for i in range(num_columns):
                torch.manual_seed(0)
                np.random.seed(0)
                p = path + ex[2][i]
                # try:
                m = [] 
                for run in os.listdir(p):
                    if not run.isdigit(): continue
                    model = torch.load(p+'/{}/model.pt'.format(run), map_location=torch.device(device)).double().to(device)
                    model = set_model(model, True, device)

                    if metric == 'recon_err':
                        m.append(re(model, x).cpu())

                    elif metric == 'nll':
                        m.append(-log_lik(model, x, batch_size=100).cpu())

                    elif metric == 'inactive_units':
                        torch.manual_seed(0)
                        np.random.seed(0)
                        x,_ = model.sample(100)
                        A = get_units_variances(model, x.double(), num_latent).cpu()
                        m.append(np.count_nonzero(A <= 0.01))

                metrics.append("{:.2f} ({:.2f})".format(np.mean(m), np.std(m)))

                # except: 
                #     metrics.append("-99")

                output = "| {0:<20} |".format("{}".format(type(bn).__name__))

            for i in range(num_columns):
                output += " {:<23} |".format(metrics[i])
            print(output)
            print(hline)


def plot_epoch_vs_ia_units(experiments, num_latent, ylim, device, N=0, 
        factor=10, separate_elbo=False, save_to=None):
    # experiments = [{
    #    'path': path,
    #    'm': m,
    #    'legend': legend
    # }]
    plt.rcParams.update({'font.size': 16})

    num_ex = len(experiments)
    if separate_elbo:
        fig, ax = plt.subplots(1,2,figsize=(20,5))
        ax1 = ax[0]
        ax2 = ax[1]
        ax2.set_xlabel('Epoch')
        plt.subplots_adjust(wspace=0.1, hspace=0.05)
    else:
        fig, ax = plt.subplots(1,1,figsize=(10,5))
        ax1 = ax
        ax2 = ax.twinx()
    # colours = ['red','blue', 'green', 'darkorange', 'teal'][:num_ex]
    colours = global_colours[:num_ex]

    linestyles = []
    labels = []
    for i, ex in enumerate(experiments):
        m = ex['m']
        path = ex['path']
        iters = m*factor

        with open(path+'metrics.json') as f:
            metrics = json.load(f)
    
        ax2.plot(range(iters), metrics['val']['values'][:iters], c=colours[i])
        
        epochs, ia = get_epoch_vs_ia_units(path, m, num_latent, device, factor)
        ax1.plot(epochs[:(m+1)], ia[:(m+1)], colours[i], marker='o')

        linestyles.append('-')
        labels.append(ex['legend'])

    ax2.set_ylabel(r'VaLidation $-$ELBO')
    ax2.set_ylim(ylim)

    ax1.set_xlabel('Epoch')
    ax1.set_ylabel('Number of inactive units')

    if N > 0:
        num_ex += 1
        ax1.axvline(N, color='grey', linestyle='dashed', alpha=0.5)
        ax2.axvline(N, color='grey', linestyle='dashed', alpha=0.5)
        linestyles.append('--')
        colours.append('grey')
        labels.append('N (WU)')

    lines = [Line2D([0], [0], color=colours[i], linewidth=3, 
                linestyle=linestyles[i]) for i in range(num_ex)]
    plt.legend(lines, labels)

    if save_to is not None:
        plt.savefig('./experiment_logs/figures/posterior_collapse/'+save_to,
                    dpi=600, bbox_inches='tight', facecolor='white') 
    plt.show()


def plot_MI_true_z_vs_inferred_z(path, prefix, step_size=1, title=None):
    mi = np.load(path+'/MI_true-z-vs-inferred-z.npy')

    fig, ax = plt.subplots(1,1, figsize=(5,5))
    plt.rcParams.update({'font.size': 16})

    ax.imshow(mi, cmap='gist_yarg')
    ax.set_ylabel(r'True $z^*\sim p(z)$')
    ax.set_xlabel(r'Inferred $z\sim q_\phi(z|x)$')

    z = mi.shape[0]
    ax.set_xticks(range(0,z,step_size))
    ax.set_yticks(range(0,z,step_size))

    if title is not None:
        ax.set_title(title)

    plt.savefig(path + '/MI_true-z-vs-inferred-z.png', dpi=600, bbox_inches='tight')
    plt.savefig('./experiment_logs/figures/siren-vae_latent_space/{}_MI_true-z-vs-inferred-z.png'.format(prefix), dpi=600, bbox_inches='tight')
    plt.show()


def plot_MI_model(path, prefix, hatch='//', step_size=1):
    mi1 = np.load(path+'/MI-model-z-vs-model-x__z.npy')
    mi2 = np.load(path+'/MI-model-z-vs-model-x__x.npy')

    z = mi1.shape[0]
    x = mi2.shape[1]
    fig, ax = plt.subplots(1,2, figsize=(mi2.shape[1]*5/mi1.shape[0],5),#(mi2.shape[1],mi1.shape[0]),
     gridspec_kw={'width_ratios': [5, 5*x/z]}, sharey=True)
    plt.rcParams.update({'font.size': 16})

    # between model z and model z
    ax[0].imshow(mi1, cmap='gist_yarg')
    ax[0].set_ylabel(r'Model $z\sim p_\theta(z)$')
    ax[0].set_xlabel(r'Model $z\sim p_\theta(z)$')

    ax[0].set_xticks(range(0,z,step_size))
    ax[0].set_yticks(range(0,z,step_size))

    for i in range(z):
        for j in range(i,z):
            ax[0].add_patch(mpl.patches.Rectangle((i-.5, j-.5), 1, 1, hatch=hatch, fill=False, snap=False))

    # between model z and model x
    ax[1].imshow(mi2, cmap='gist_yarg')
    # ax[1].set_ylabel(r'Model $z\sim p(z)$')
    ax[1].set_xlabel(r'Model $x\sim p_\theta(x|z)$')
    ax[1].set_xticks(range(0,x,step_size))
    plt.subplots_adjust(wspace=0.02, hspace=0.01)

    plt.savefig(path + '/MI-model-z-vs-model-x.png', dpi=600, bbox_inches='tight')
    plt.savefig('./experiment_logs/figures/siren-vae_latent_space/{}_MI-model-z-vs-model-x.png'.format(prefix), dpi=600, bbox_inches='tight')
    plt.show()


def plot_MI_true(path, prefix, hatch='//', step_size=1):
    mi1 = np.load(path+'/MI-true-z-vs-true-x__z.npy')
    mi2 = np.load(path+'/MI-true-z-vs-true-x__x.npy')

    z = mi1.shape[0]
    x = mi2.shape[1]
    fig, ax = plt.subplots(1,2, figsize=(mi2.shape[1]*5/mi1.shape[0],5),#(mi2.shape[1],mi1.shape[0]), 
    gridspec_kw={'width_ratios': [5, 5*x/z]}, sharey=True)
    plt.rcParams.update({'font.size': 16})

    # between true z and true z
    ax[0].imshow(mi1, cmap='gist_yarg')
    ax[0].set_ylabel(r'True $z^*\sim p(z)$')
    ax[0].set_xlabel(r'True $z^*\sim p(z)$')
    ax[0].set_xticks(range(z))
    ax[0].set_xticklabels(['' for _ in range(z)])
    ax[0].set_yticks(range(0,z,step_size))

    for i in range(z):
        for j in range(i,z):
            ax[0].add_patch(mpl.patches.Rectangle((i-.5, j-.5), 1, 1, hatch=hatch, fill=False, snap=False))

    # between true z and true x
    ax[1].imshow(mi2, cmap='gist_yarg')
    # ax[1].set_ylabel(r'True $z\sim p(z)$')
    ax[1].set_xlabel(r'True $x^*\sim p(x|z)$')
    plt.subplots_adjust(wspace=0.02, hspace=0.01)
    ax[1].set_xticks(range(x))
    ax[1].set_xticklabels(['' for _ in range(x)])

    plt.savefig(path + '/MI-true-z-vs-true-x.png', dpi=600, bbox_inches='tight')
    plt.savefig('./experiment_logs/figures/siren-vae_latent_space/{}_MI-true-z-vs-true-x.png'.format(prefix), dpi=600, bbox_inches='tight')
    plt.show()