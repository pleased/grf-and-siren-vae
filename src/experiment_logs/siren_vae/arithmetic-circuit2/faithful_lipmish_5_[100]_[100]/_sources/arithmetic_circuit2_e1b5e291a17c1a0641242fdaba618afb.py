import torch
import torch.nn as nn
import sys
import numpy as np

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal, Laplace

from graph.belief_network import BeliefNetwork


class ArithmeticCircuit2(BeliefNetwork):

    def __init__(self):
        super(ArithmeticCircuit2, self).__init__()
        
        # Node distributions
        self.z0 = Laplace(5,1)
        self.z1 = Laplace(-2,1)
        self.z2 = self.z2tmp
        self.z3 = Normal(7,2)
        self.z4 = self.z4tmp

        self.x0 = self.x0tmp
        self.x1 = self.x1tmp
        self.x2 = self.x2tmp
        self.x3 = self.x3tmp
        self.x4 = self.x4tmp
        self.x5 = self.x5tmp
        self.x6 = self.x6tmp
        self.x7 = self.x7tmp
        self.x8 = self.x8tmp
        self.x9 = self.x9tmp

        # graph
        self.forward_graph = ForwardGraph().initialize(*ArithmeticCircuit2._construct_graph())
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 6
        self.num_obs = 2


    def z2tmp(self, z0, z1): 
        return Normal((z0*z1)/7.9 - 7, 0.1)

    def z4tmp(self, z2, z3): 
        return Normal(torch.tanh(z2 + z3), 0.1)


    def x0tmp(self, z0, z1):
        return Normal(torch.tanh(z0 + z1 - 2.8), 0.1)

    def x1tmp(self, z2):
        return Normal(torch.tanh(z2), 1.1)

    def x2tmp(self, z2, z3):
        return Normal(torch.tanh(z2+z3), 0.1)
    
    def x3tmp(self, z2): 
        return Normal(z2+8, 0.1)

    def x4tmp(self, z3):
        return Normal(torch.sigmoid(z3-6.97), 1.1)

    def x5tmp(self, z2, z4):
        return Normal((z2*z4)/6.1, 0.1)

    def x6tmp(self, z4):
        return Normal(z4, 1.1)

    def x7tmp(self, z4): 
        return Normal(z4, 0.1)

    def x8tmp(self, z4):
        return Normal(torch.tanh(z4), 2.1)

    def x9tmp(self, z4):
        return Normal(torch.sin(z4), 1.1)


    def sample(self, batch_size, train=True):
        z0 = self.z0.sample((batch_size,1))
        z1 = self.z1.sample((batch_size,1))
        z2 = self.z2(z0, z1).sample()
        z3 = self.z3.sample((batch_size,1))
        z4 = self.z4(z2, z3).sample()
    
        x0 = self.x0(z0, z1).sample()
        x1 = self.x1(z2).sample()
        x2 = self.x2(z2, z3).sample()
        x3 = self.x3(z2).sample()
        x4 = self.x4(z3).sample()
        x5 = self.x5(z2, z4).sample()
        x6 = self.x6(z4).sample()
        x7 = self.x7(z4).sample()
        x8 = self.x8(z4).sample()
        x9 = self.x9(z4).sample()

        sample = torch.cat([z0,z1,z2,z3,z4,x0,x1,x2,x3,x4,x5,x6,x7,x8,x9],
                         dim=1)
        return sample


    def log_likelihood(self, x, z):
        log_p_x =  self.x0(z[:,0], z[:,1]).log_prob(x[:,0])
        log_p_x += self.x1(z[:,2]).log_prob(x[:,1])
        log_p_x += self.x2(z[:,2], z[:,3]).log_prob(x[:,2])
        log_p_x += self.x3(z[:,2]).log_prob(x[:,3])
        log_p_x += self.x4(z[:,3]).log_prob(x[:,4])
        log_p_x += self.x5(z[:,2], z[:,4]).log_prob(x[:,5])
        log_p_x += self.x6(z[:,4]).log_prob(x[:,6])
        log_p_x += self.x7(z[:,4]).log_prob(x[:,7])
        log_p_x += self.x8(z[:,4]).log_prob(x[:,8])
        log_p_x += self.x9(z[:,4]).log_prob(x[:,9])
        return log_p_x


    def log_prior(self, z):
        log_p_z = self.z0.log_prob(z[:,0]) 
        log_p_z += self.z1.log_prob(z[:,1])    
        log_p_z += self.z2(z[:,0], z[:,1]).log_prob(z[:,2])     
        log_p_z += self.z3.log_prob(z[:,3]) 
        log_p_z += self.z4(z[:,2], z[:,3]).log_prob(z[:,4])
      
        return log_p_z 


    def log_joint(self, x, z):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z)
        return log_lik + log_prior


    def get_num_latent(self):
        return 5


    def get_num_obs(self):
        return 10


    def get_num_vertices(self):
        return 15


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['z0', 'z1', 'z2', 'z3', 'z4', 
                    'x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9']
        edges = [('z0','z2'), ('z0','x0'), 
                 ('z1','z2'), ('z1', 'x0'),
                 ('z2','z4'), ('z2','x1'), ('z2','x2'), ('z2', 'x3'), 
                 ('z2', 'x5'),   
                 ('z3','z4'), ('z3','x2'), ('z3', 'x4'), 
                 ('z4', 'x5'), ('z4', 'x6'), ('z4', 'x7'), ('z4', 'x8'), 
                 ('z4', 'x9'),
                 ('z1', 'x1'), ('z0', 'x1'),
                 ('z1', 'x3'), ('z0', 'x3'),
                 ('z1', 'x2'), ('z0', 'x2'),
                 ('z1', 'x5'), ('z0', 'x5'),
                 ('z3', 'x5'), ('z3', 'x5'),
                 ('z2', 'x6'), ('z3', 'x6'),
                 ('z2', 'x7'), ('z3', 'x7'),
                 ('z2', 'x8'), ('z3', 'x8'),
                 ('z2', 'x9'), ('z3', 'x9')]
                                  

                #  ('z0', 'z1'), ('z2', 'z3')]
                #  ('z1', 'x2'), ('z0', 'x2'),
                #  ('z1', 'x3'), ('z0', 'x3'),
                #  ('z1', 'x1'), ('z0', 'x1')]
        observed = {'x0', 'x1'}
        return vertices, edges, observed