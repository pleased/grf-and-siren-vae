import torch
import numpy as np
import sys
if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal

from graph.belief_network import BeliefNetwork


class _WrapperGraph():

    def __init__(self, bn, augment_sizes, inverse=False):
        super(_WrapperGraph, self).__init__()
        self.is_inverse = inverse
        self.bn = bn
        self.num_latent = bn.get_num_latent()
        self.num_augment = sum(augment_sizes)
        self.augment_sizes = augment_sizes
        self.n = self.bn.forward_graph.n + self.num_augment
        

    def num_vertices(self):
        return self.n


    def topological_order(self):
        if self.is_inverse:
            original_order = self.bn.inv_topological_order()
        else:
            original_order = self.bn.topological_order()        
        
        order = []
        for i in original_order:
            if i < self.num_latent:
                order.append(i)
                # Add all of i's nuisance variables
                order.extend(self.get_nuisance_idxs(i))
            else:
                # shift observed variable's index
                order.append(i+self.num_augment)
        
        return order

    
    def get_in_neighbours(self, i):
        if self.is_inverse:
            bn = self.bn.inverse_graph
        else:
            bn = self.bn.forward_graph
        
        in_neighbours = []

        # CASE 1: i is an original latent variable
        #   - edge from incoming observed variable
        #   - edge from incoming original latent
        #   - edge from all nuisance variables of incoming original latent
        if i < self.num_latent:
            original_in_neighbours = bn.get_in_neighbours(i)

            in_neighbours.extend(self.get_nuisance_idxs(i)) ##
            
            for j in original_in_neighbours:
                # CASE 1: neighbour is an original latent
                if j < self.num_latent:
                    in_neighbours.append(j)
                    # Add all of j's nuisance variables
                    in_neighbours.extend(self.get_nuisance_idxs(j))

                # CASE 2: neighbours is an observed variable
                else:
                    # shift observed variable's index
                    in_neighbours.append(j+self.num_augment)

        # CASE 2: i is a nuisance variable
        #   - edge from i's original latent
        #   - edge from all i's nuisance variables with lower index
        #   - edge from original latent's incoming observed variable
        #   - edge from original latent's incoming original latent
        #   - edge from all nuisance variables of incoming original latent
        elif i >= self.num_latent and i < (self.num_latent+self.num_augment):
            original_latent = self.get_original_latent_idx(i)
            original_in_neighbours = bn.get_in_neighbours(original_latent)
    
            in_neighbours.append(original_latent)
            n = self.get_nuisance_idxs(original_latent)
            #in_neighbours.extend(n[n<i])
            in_neighbours.extend(n)

            for j in original_in_neighbours:
                # CASE 1: neighbour is an original latent
                if j < self.num_latent:
                    in_neighbours.append(j)
                    # Add all of j's nuisance variables
                    in_neighbours.extend(self.get_nuisance_idxs(j))
                
                # case 2: neighbours is an observed variable
                else:
                    # shift observed variable's index
                    in_neighbours.append(j+self.num_augment)

        # CASE 3: i is an observed variable
        #   - edge from incoming observed variable
        #   - edge from incoming original latent
        #   - edge from all nuisance variables of incoming original latent
        else:
            # shift observed variable's index
            original_in_neighbours = bn.get_in_neighbours(i-self.num_augment)

            for j in original_in_neighbours:
                # CASE 1: neighbour is an original latent
                if j < self.num_latent:
                    in_neighbours.append(j)
                    # Add all of j's nuisance variables
                    in_neighbours.extend(self.get_nuisance_idxs(j))

                # CASE 2: neighbours is an observed variable
                else:
                    # shift observed variable's index
                    in_neighbours.append(j+self.num_augment)
        
        return in_neighbours
        

    def get_out_neighbours(self, i):
        if self.is_inverse:
            bn = self.bn.inverse_graph
        else:
            bn = self.bn.forward_graph
        
        out_neighbours = []

        # CASE 1: i is an original latent variable
        #   - edge to all nuisance variables of original latent
        #   - edge to outgoing observed variable
        #   - edge to outgoing original latent
        #   - edge to all nuisance variables of outgoing original latent
        if i < self.num_latent:
            original_out_neighbours = bn.get_out_neighbours(i)
            out_neighbours.extend(self.get_nuisance_idxs(i))
            
            for j in original_out_neighbours:
                # CASE 1: neighbour is an original latent
                if j < self.num_latent:
                    out_neighbours.append(j)
                    # Add all of j's nuisance variables
                    out_neighbours.extend(self.get_nuisance_idxs(j))
                
                # CASE 2: neighbour is an observed variable
                else:
                    # shift observed variable's index
                    out_neighbours.append(j+self.num_augment)

        # CASE 2: i is a nuisance variable
        #   - edge to all i's nuisance variables with higher index
        #   - edge to original latent's outgoing observed variable
        #   - edge to original latent's outgoing original latent
        #   - edge to all nuisance variables of outgoing original latent
        elif i >= self.num_latent and i < (self.num_latent+self.num_augment):
            original_latent = self.get_original_latent_idx(i)
            original_out_neighbours = bn.get_out_neighbours(original_latent)
    
            n = self.get_nuisance_idxs(original_latent)
            #out_neighbours.extend(n[n>i])
            out_neighbours.extend(n)
            out_neighbours.append(original_latent) ##

            for j in original_out_neighbours:
                # CASE 1: neighbour is an original latent
                if j < self.num_latent:
                    out_neighbours.append(j)
                    # Add all of j's nuisance variables
                    out_neighbours.extend(self.get_nuisance_idxs(j))
                
                # case 2: neighbours is an observed variable
                else:
                    # shift observed variable's index
                    out_neighbours.append(j+self.num_augment)

        # CASE 3: i is an observed variable
        #   - edge to outgoing observed variable
        #   - edge to outgoing original latent
        #   - edge to all nuisance variables of outgoing original latent
        else:
            # shift observed variable's index
            original_out_neighbours = bn.get_out_neighbours(i-self.num_augment)

            for j in original_out_neighbours:
                # CASE 1: neighbour is an original latent
                if j < self.num_latent:
                    out_neighbours.append(j)
                    # Add all of j's nuisance variables
                    out_neighbours.extend(self.get_nuisance_idxs(j))

                # CASE 2: neighbours is an observed variable
                else:
                    # shift observed variable's index
                    out_neighbours.append(j+self.num_augment)

        return out_neighbours


    def get_nuisance_idxs(self, i):
        nuisance_idxs = np.arange(self.num_latent, self.num_latent+self.num_augment)
        from_idx = sum(self.augment_sizes[:i])
        to_idx = from_idx + self.augment_sizes[i]
        return nuisance_idxs[from_idx:to_idx]

    
    def get_original_latent_idx(self, nuisance_idx):
        latent_idx = -1
        for i in range(self.num_latent):
            x = self.num_latent + sum(self.augment_sizes[:(i+1)])
            if nuisance_idx < x:
                latent_idx = i
                break
        return latent_idx

    
    def get_vertices(self):
        return np.arange(self.n)



class AugmentedBN(BeliefNetwork):

    """Augments the given belief network with a specified
    number of nuisance variables.
    """

    def __init__(self, bn, augment_sizes):
        super(AugmentedBN, self).__init__()
        self.bn = bn
        self.bn_num_latent = bn.get_num_latent()
        self.num_augment = sum(augment_sizes)
        self.num_latent = self.bn_num_latent + self.num_augment
        self.num_obs = bn.get_num_obs()

        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph(bn, augment_sizes)
            self.inverse_graph = _WrapperGraph(bn, augment_sizes, inverse=True)
        else:
            #self.forward_graph = ForwardGraph().initialize(*AugmentedBN._construct_graph(bn.inverse_graph, augment_sizes))
            #self.inverse_graph = invert.properly(self.forward_graph)
            self.inverse_graph = ForwardGraph().initialize(*AugmentedBN._augment_graph(bn, augment_sizes))


    def sample(self, batch_size, train=True):
        sample = self.bn.sample(batch_size, train)
        z = sample[:,:self.bn_num_latent]
        x = sample[:,self.bn_num_latent:]
        aug = Normal(0.0,1.0).sample((batch_size, self.num_augment))

        sample = torch.cat((z,aug,x), dim=1)
        return sample

    
    def log_likelihood(self, x, z):
        return self.bn.log_likelihood(x, z[:,:self.bn_num_latent])


    def log_prior(self, z):
        assert z[:,self.bn_num_latent:].shape[1] == self.num_augment
        log_p_z = self.bn.log_prior(z[:,:self.bn_num_latent])
        log_p_z_aug = Normal(0.0, 1.0).log_prob(z[:,self.bn_num_latent:]).sum(dim=1)
        return log_p_z + log_p_z_aug


    def log_joint(self, x, z):
        # p(x,z) = p(x|z)p(z)
        log_lik = self.log_likelihood(x, z)
        log_prior = self.log_prior(z)
        return log_lik + log_prior


    def log_aug_prior(self, z_aug):
        return Normal(0.0, 1.0).log_prob(z_aug).sum(dim=1)

    
    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph) # TODO Fix


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph)


    @staticmethod
    def _augment_graph(bn, augment_sizes):
        num_latent = bn.get_num_latent()
        num_obs = bn.get_num_obs()
        num_augment = sum(augment_sizes)
        
        # Vertices
        vertices = []
        # Add original latent vars
        for i in range(num_latent):
            vertices.append('x{}'.format(i))
        # Add augmented latent vars
        for i in range(num_latent):
            for j in range(augment_sizes[i]):
                vertices.append('x{}_aug{}'.format(i,j))
        for i in range(num_latent,num_latent+num_obs):
            vertices.append('x{}'.format(i))
        
        # Edges (from, to)
        edges = []
        # Original edges
        for i_from in range(num_latent+num_obs):
            for i_to in bn.inverse_graph.get_out_neighbours(i_from):
                edges.append(('x{}'.format(i_from),
                              'x{}'.format(i_to)))

        # Edges within augmentation clique - fully connected
        for i in range(num_latent):
            aug_size = augment_sizes[i]
            for i_aug in range(aug_size):
                edges.append((
                    'x{}'.format(i),
                    'x{}_aug{}'.format(i,i_aug)))
                edges.append((
                    'x{}_aug{}'.format(i,i_aug),
                    'x{}'.format(i)))
            for i_aug_from in range(aug_size):
                for i_aug_to in range(i_aug_from+1, aug_size):
                    edges.append((
                        'x{}_aug{}'.format(i, i_aug_from),
                        'x{}_aug{}'.format(i, i_aug_to)))
                    edges.append((
                        'x{}_aug{}'.format(i, i_aug_to),
                        'x{}_aug{}'.format(i, i_aug_from)))

        # Edges between cliques - All fine-level nodes in a
        # clique point to all subnodes of any adjacent clique.
        for i_from in range(num_latent+num_obs):
            for i_to in bn.inverse_graph.get_out_neighbours(i_from):
                
                if i_from < num_latent and i_to < num_latent:
                    for i_aug_to in range(augment_sizes[i_to]):
                        edges.append((
                            'x{}'.format(i_from),
                            'x{}_aug{}'.format(i_to, i_aug_to)))
                    
                        for i_aug_from in range(augment_sizes[i_from]):
                            edges.append((
                                'x{}_aug{}'.format(i_from, i_aug_from),
                                'x{}_aug{}'.format(i_to, i_aug_to)))
                            edges.append((
                                'x{}_aug{}'.format(i_from, i_aug_from),
                                'x{}'.format(i_to)))

                if i_from < num_latent and i_to >= num_latent:
                    for i_aug_from in range(augment_sizes[i_from]):
                        edges.append((
                            'x{}_aug{}'.format(i_from, i_aug_from),
                            'x{}'.format(i_to)))

                if i_from >= num_latent and i_to < num_latent:
                    for i_aug_to in range(augment_sizes[i_to]):
                        edges.append((
                            'x{}'.format(i_from),
                            'x{}_aug{}'.format(i_to, i_aug_to)))

        # Observed
        observed = set()
        for i in range(num_obs):
            observed.add('x{}'.format(i+num_latent))  

        # print('vertices', vertices)
        # print('edges', edges)
        # print('observed', observed)
        return vertices, edges, observed