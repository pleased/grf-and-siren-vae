import torch
import sys
import numpy as np
import pandas as pd

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from graph.belief_network import BeliefNetwork

class Sangiovese(BeliefNetwork):

    def __init__(self, structure='faithful'):
        super(Sangiovese, self).__init__()

        # graph
        graph = Sangiovese._construct_graph(structure)
        self.edges = graph[1]
        self.forward_graph = ForwardGraph().initialize(*graph)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 4
        self.num_obs = 8

        train, test = self.load_data()
        np.random.shuffle(train)
        np.random.shuffle(test)
        self.train_idx = 0
        self.test_idx = 0
        self.train = train.astype(np.float32)
        self.test = test.astype(np.float32)
        self.train_N = self.train.shape[0]
        self.test_N = self.test.shape[0]


    def load_data(self):
        dir_f = './datasets/sangiovese/'

        train = pd.read_csv(dir_f + "sangio_train.csv").values
        mu, sigma = train.mean(0), train.std(0)
        test =  pd.read_csv(dir_f + "sangio_test.csv").values
 
        return (train - mu)/sigma, (test - mu)/sigma

    
    def sample(self, batch_size, train=True):
        if train:
            return self._sample_train(batch_size)
        else:
            return self._sample_test(batch_size)


    def _sample_train(self, batch_size):
        sample = self.train.take(
            range(self.train_idx,self.train_idx + batch_size),
            axis=0, mode='wrap')
        self.train_idx = (self.train_idx + batch_size)%self.train_N
        return torch.tensor(sample)


    def _sample_test(self, batch_size):
        sample = self.test.take(
            range(self.test_idx, self.test_idx + batch_size),
            axis=0, mode='wrap')
        self.test_idx = (self.test_idx + batch_size)%self.test_N
        return torch.tensor(sample)


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for EightPairs BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for EightPairs BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for EightPairs BN")


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph(structure):
        vertices = ['SproutN', 'SPAD06', 'NDVI06', 'BunchN', 'NDVI08', 'SPAD08',
                    'WoodW', 'Anthoc', 'Potass', 'Polyph', 'Acid', 'pH']

        observed = {'NDVI08', 'SPAD08', 'WoodW', 'Anthoc', 'Potass', 'Polyph', 
                    'Acid', 'pH'}

        edges = [
            ('SproutN','SPAD06'), ('SproutN','NDVI06'), ('SproutN','BunchN'),
            ('SproutN','NDVI08'), ('SproutN','WoodW'), ('SproutN','Acid'),
            ('SproutN','pH'),
            ('SPAD06','NDVI06'),('SPAD06','SPAD08'),('SPAD06','WoodW'),
            ('SPAD06','Potass'),('SPAD06','Acid'),('SPAD06','pH'),
            ('NDVI06','NDVI08'),('NDVI06','SPAD08'),('NDVI06','Polyph'),
            ('NDVI06','Acid'),
            ('BunchN','WoodW'),('BunchN','Anthoc'),('BunchN','Potass'),('BunchN','Polyph'),('BunchN','Acid')]  
            
       
        return vertices, edges, observed