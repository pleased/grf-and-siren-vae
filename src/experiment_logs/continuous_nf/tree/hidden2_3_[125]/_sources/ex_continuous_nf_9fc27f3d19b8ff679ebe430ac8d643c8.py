"""
Approximate the posterior using the structured conditional continuous 
normalizing flow (SCCNF).
"""

import torch
import numpy as np
import matplotlib.pyplot as plt
import json

from sacred import Experiment
from sacred.observers import FileStorageObserver
from functools import partial
from itertools import chain
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from ex_binary_tree_config import bt_ingredient, create_tree
from ex_utils import batch_iterator, count_parameters, sample_batch

from modules.factory import build_continuous_nf
from graph.belief_network import *
from data.load_data import load_protein, load_mehra, load_dataset
 

ex = Experiment('continuous_nf', ingredients=[bt_ingredient])
device = "cpu" #if not(torch.cuda.is_available()) else "cuda:0"


def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network [arithmetic, arithmetic-mul, binary-tree]
    bn = 'arithmetic-circuit'

    augment = True
    augment_sizes = [3]*15

    num_flow_steps = 10
    integration_times = [0.0, 1.0]
    hidden_dim = 100
    dag_net = 'hidden2'#sccnf' # 'hidden', 

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    num_epochs = 100
    
    lr = 1e-2
    seed = 0
    patience = 20

    run = 1
    double = False

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False

    # Add observer
    ex_name = 'continuous_nf'
    sub_folder = '{}_{}_[{}]'.format(dag_net, num_flow_steps, hidden_dim)
    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _run):
    # Training info
    lr = _config['lr']
    integration_times = torch.tensor(_config['integration_times']).to(device)
    batch_size = _config['batch_size']
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    num_epochs = _config['num_epochs']
    hidden_dim = _config['hidden_dim']
    dag_net = _config['dag_net']
    num_flow_steps = _config['num_flow_steps']
    integration_times = torch.tensor(_config['integration_times']).to(device)
    checkpoint_n = _config['checkpoint_n']

    # BN initialization
    if _config['bn'] == 'arithmetic-circuit':
        bn = ArithmeticCircuit()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'tree':
        bn = Tree(device)
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    elif _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
        trainloader, valloader, testloader = load_dataset(_config['bn'], batch_size, _config['double'])
    else:
        raise Exception("Unknown belief network: {}".format(_config['bn']))

    num_latent_original = bn.get_num_latent()
    if _config['augment'] and dag_net == 'sccnf':
        augment_sizes = _config['augment_sizes']
        bn = AugmentedBN(bn, augment_sizes)
 
    n = bn.get_num_vertices()
    num_latent = bn.get_num_latent()

    # Joint space normalization - Before training apply change of variable
    # on p(x,z) to normalize moments of its marginnals to be the same as
    # those of q0 (0 mean, 1 std). Moments estimated by 10 000 draws from
    # the joint. This avoids flows that can be scaled arbitrarily and 
    # could render its training unstable
    min_std = 1e-5
    batch = torch.cat([batch for batch in trainloader], dim=0)
    z = batch[:,:num_latent]
    x = batch[:,num_latent:]

    z_std = torch.std(z, dim=0).to(device)
    z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
    z_shift = torch.mean(z, dim=0).to(device)

    x_std = torch.std(x, dim=0).to(device)
    x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
    x_shift = torch.mean(x, dim=0).to(device)
    del batch

    # Initialize the CNF and optimizer TODO:
    model = build_continuous_nf(num_flow_steps=num_flow_steps, bn=bn, 
        x_shift=x_shift, x_scale=x_scale, z_shift=z_shift, z_scale=z_scale, device=device, dag_net=dag_net,
        hidden_dim=hidden_dim).to(device)

    optimizer = torch.optim.Adam(chain(model.parameters()), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True, min_lr=1e-6, patience=_config['patience'])

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)

    # print('Model parameters: ')
    # for name, p in model.named_parameters():
    #     print(name,', Shape: ',p.data.shape)

    # Log model capacity
    ex.info['num model params'] = model.count_parameters()

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_shifted_kl = 0.0
        train_true_kl = 0.0

        model.train()
        for train_batch in trainloader:
            x = train_batch[:,num_latent:]
            z = train_batch[:,:num_latent]

            # Reverse kl-divergence:
            # For z ~ q(z|x): log q(z|x) - log p(x,z)
            z0, zT, delta_log_q = model(x, integration_times)
            reverse_kl = model.shifted_reverse_kl(x,zT,z0, delta_log_q)

            loss = reverse_kl

            l = loss.detach().item()
            train_shifted_kl += l    

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        
        # Log training metrics - training loss, kl-divergence
        _run.log_scalar("train", value=train_shifted_kl/num_train_batches)
        if device == 'cuda:0':
            _run.log_scalar("memory.usage", value=torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000)

        # Log val metrics - val loss, kl-diveregnce
        model.eval()
        with torch.no_grad():
            val_shifted_kl = 0.0

            for val_batch in valloader:
                x = val_batch[:,num_latent:]
                z = val_batch[:,:num_latent]

                z0, zT, delta_log_q = model(x, integration_times)

                val_shifted_kl += model.shifted_reverse_kl(x, zT, z0, delta_log_q).item()

            _run.log_scalar("val", val_shifted_kl/num_val_batches)

        # Decay learning rate every 100 epochs
        lr_scheduler.step(train_shifted_kl)
        
        # Calculate log-likelihood of samples from the inference
        # network evaluated on the analytical posterior
        epochs.set_description('Train: {:.3f}, Val: {:.3f}'.format(
            train_shifted_kl/num_train_batches, val_shifted_kl/num_val_batches),
            refresh=False)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1

    # Peak memory usage
    if device == 'cuda:0':
        ex.info['memory usage (MB)'] = torch.cuda.memory_stats(device)['allocated_bytes.all.peak']/1000000 

    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))

    model.eval()
    with torch.no_grad():
        test_shifted_kl = 0

        num_test_batches = 0
        for test_batch in testloader:
            x = test_batch[:,num_latent:]
            z = test_batch[:,:num_latent]

            z0, zT, delta_log_q = model(x, integration_times)

            test_shifted_kl += model.shifted_reverse_kl(x, zT, z0, delta_log_q).item()
            num_test_batches += 1
        # log test metrics 
        _run.log_scalar("test", value=test_shifted_kl/num_test_batches) 


    # Samples from joint vs inference network
    # with torch.no_grad():
    #     batch = sample_batch(graph.sample, 100000)
    #     sample = (batch).double().to(device)
    #     x = sample[:,num_latent:]
    #     z0, zT, delta_log_q = model(x, integration_times)

    #     inference_network_samples = zT[:,:num_latent_original]
    #     if bn == 'binary-tree':
    #         bn_samples = graph.sample_posterior(x)
    #         label = ['True','Inference network']
    #     else:
    #         bn_samples = sample[:,:num_latent]
    #         label = ['Joint','Inference network']
                     
    #     for i in range(num_latent_original):
    #         z_joint = bn_samples[:,i].cpu().tolist()
    #         z_infnet = inference_network_samples[:,i].cpu().tolist()
    #         plt.hist(x=[z_joint, z_infnet], bins=50, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red'], edgecolor='none',
    #                     label=label)
    #         plt.xlabel("z_"+str(i))
    #         plt.ylabel("Density")
    #         plt.legend()
    #         plt.savefig(path+'/{}/z_{}'.format(_run._id,i))
    #         plt.clf()