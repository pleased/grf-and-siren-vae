from tabnanny import verbose
import torch
import torch.nn as nn
import types

from sacred import Experiment
from sacred.observers import FileStorageObserver
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader, random_split
from tqdm import trange
from torch.distributions import Bernoulli, Normal

from modules import factory, MLP
from data.load_data import load_onehot, load_mnist, load_binary_mnist
from graph.belief_network import VanillaBN, FCBN
from vae.vanilla.gen_network import VanillaGenNetwork
from vae.graph.infer_network import GraphInferNetwork

ex = Experiment('SIReN-VAE Ablation Study')


@ex.config
def cfg():
    # Dataset [onehot, mnist]
    dataset = 'onehot'
    num_latent = 2

    # Model specification
    add_grf_posterior = False
    add_grf_prior = False
    indep_bn = True
    fc_bn = False

    # Flow specification
    num_blocks = 8
    flow_hidden_dims = [200]
    vae_hidden_dims = [50]

    # Training specifications
    batch_size = 100
    num_train_samples = 5000
    num_val_samples = 1000
    num_epochs = 100
    lr = 1e-2
    seed = 0
    patience = 10

    # Add observer
    ex_name = 'SIReN-VAE_ablation_study'
    if not add_grf_posterior:
        sub_folder = '{}/VAE'.format(dataset)
    elif add_grf_posterior and not add_grf_prior:
        sub_folder = '{}/VAE_GRF-post'.format(dataset)
    elif add_grf_posterior and add_grf_prior:
        sub_folder = '{}/VAE_GRF-post_GRF-prior'.format(dataset)
    path = './experiment_logs/{}/{}'.format(ex_name,sub_folder)
    ex.observers.append(FileStorageObserver(path))



class MNISTConvEncoder(nn.Module):
    def __init__(self, D, K):
        super(MNISTConvEncoder, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5, 
                      stride=2, padding=2),                              
            nn.ReLU())
        self.conv2 = nn.Sequential(         
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, 
                      stride=2, padding=2),     
            nn.ReLU())        
        self.fully_connected = nn.Linear(32*7*7, K)
    
    def forward(self, x):
        x = x.view(x.shape[0],1,28,28)
        x = self.conv2(self.conv1(x))
        x = x.view(x.size(0), -1) 
        return self.fully_connected(x)



class MNISTConvDecoder(nn.Module):
    def __init__(self, D, K):
        super(MNISTConvDecoder, self).__init__()
        self.D = D
        self.fully_connected = nn.Linear(K, 32*7*7)
        self.conv2 = nn.Sequential(
            nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=5, 
                      stride=2, padding=2, output_padding=1),
            nn.ReLU())
        self.conv1 = nn.Sequential(
            nn.ConvTranspose2d(in_channels=16, out_channels=1, kernel_size=5, 
                      stride=2, padding=2, output_padding=1), 
            nn.ReLU())
        
    def forward(self, z):
        z = self.fully_connected(z)
        z = z.view(z.size(0), 32, 7, 7)
        z = self.conv1(self.conv2(z))
        return z.view(z.size(0), self.D)


class XencodingInferNetwork(nn.Module):
    
    def __init__(self, infer_network, x_encoder):
        super(XencodingInferNetwork, self).__init__()
        self.infer_network = infer_network
        self.x_encoder = x_encoder

    def forward(self, x):
        x = self.x_encoder(x)
        return self.infer_network(x)


def sample_bernoulli(self, batch_size):
    # Generate sample of latent variables
    z = Normal(
        loc=torch.zeros(self.latent_dim), 
        scale=torch.ones(self.latent_dim)
        ).sample((batch_size,)).to(self.device)

    if hasattr(self, 'nf'):
        z, _ = self.nf.inverse(z)

    # Calculate mean and standard deviation 
    params = self.decoder_net(z)
    return torch.sigmoid(params), z

def forward_bernoulli(self, x, z):
    # Compute the density of z
    if hasattr(self, 'prior'):
        log_p_z = self.prior(z)
    else:
        _, log_p_z = self.nf(z)
    # Calculate mean and standard deviation 
    params = self.decoder_net(z)
    p = Bernoulli(probs=torch.sigmoid(params))
    # Compute the obs likelihood
    log_p_x_given_z = p.log_prob(x).sum(dim=1)
    return log_p_x_given_z, log_p_z

def build_vanilla_vae_onehot(bn, cfg, device):
    h = cfg['vae_hidden_dims']
    model = factory.build_vanilla_vae(bn, h, h, device, condition_sigma=False)
    model.generative_network.forward = types.MethodType(forward_bernoulli, model.generative_network)  
    model.generative_network.sample = types.MethodType(sample_bernoulli, model.generative_network)    
    return model

def build_vanilla_vae_mnist(bn, cfg, device):
    latent_dim = bn.get_num_latent()
    gen_h = cfg['vae_hidden_dims']
    infer_h = gen_h[:]
    infer_h.reverse()
    model = factory.build_vanilla_vae(bn, infer_h, gen_h, device, condition_sigma=False, activation_function=nn.ELU(), likelihood="bernoulli")
    model.inference_network.encoder_net = MNISTConvEncoder(784, latent_dim)
    model.generative_network.decoder_net = MNISTConvDecoder(784, latent_dim)
    
    model.generative_network.forward = types.MethodType(forward_bernoulli, model.generative_network)  
    model.generative_network.sample = types.MethodType(sample_bernoulli, model.generative_network)   
    
    return model

# def build_conv_vanilla_vae(bn, cfg, device):
#     h = cfg['vae_hidden_dims']
#     latent_dim = bn.get_num_latent()
#     obs_dim = bn.get_num_obs()
#     model = factory.build_vanilla_vae(bn, h, h, device, condition_sigma=False)
#     model.inference_network.encoder_net = MNISTConvEncoder(obs_dim, latent_dim)
#     model.generative_network.decoder_net = MNISTConvDecoder(obs_dim, latent_dim)
#     return model


def build_vae_post_onehot(bn, cfg, device):
    latent_dim = bn.get_num_latent()
    obs_dim = bn.get_num_obs()
    model = build_vae_post_prior_onehot(bn, cfg, device)
    model.generative_network = VanillaGenNetwork(latent_dim, obs_dim,
            cfg['vae_hidden_dims'], device, condition_sigma=False, likelihood="bernoulli")
    model.generative_network.forward = types.MethodType(forward_bernoulli, model.generative_network)  
    model.generative_network.sample = types.MethodType(sample_bernoulli, model.generative_network)   
    return model


def build_vae_post_mnist(bn, cfg, device):
    latent_dim = bn.get_num_latent()
    model = build_vae_post_prior_mnist(bn, cfg, device)
    # Replace gen network
    model.generative_network = VanillaGenNetwork(latent_dim, 784,
            cfg['vae_hidden_dims'], device, activation_function=nn.ELU(),
            condition_sigma=False, likelihood="bernoulli")
    model.generative_network.decoder_net = MNISTConvDecoder(784, latent_dim)
    model.generative_network.forward = types.MethodType(forward_bernoulli, model.generative_network)  
    model.generative_network.sample = types.MethodType(sample_bernoulli, model.generative_network)   
    return model


# def build_conv_vae_post(bn, cfg, device, obs_dim):
#     latent_dim = bn.get_num_latent()
#     model = build_vae_post_prior(bn, cfg, device)

#     # Fix gen network
#     model.generative_network = VanillaGenNetwork(latent_dim, obs_dim,
#             cfg['vae_hidden_dims'], device, condition_sigma=False)
#     model.generative_network.decoder_net = MNISTConvDecoder(obs_dim, latent_dim)

#     # Fix infer network
#     infer_network = model.inference_network
#     model.inference_network = ConvInferNetwork(infer_network, 
#                                 MNISTConvEncoder(obs_dim, obs_dim))

#     return model



def build_vae_post_prior_onehot(bn, cfg, device):
    infer_args = {
        'type': 'resflow', 
        'num_flow_steps': cfg['num_blocks'], 
        'hidden_dims': cfg['flow_hidden_dims'],
        'coeff': 0.99, 'n_power_iterations': 5,
        'activation_function': 'lipmish'
    }
    gen_args = {
        'type': 'resflow',
        'num_flow_steps': cfg['num_blocks'],
        'flow_hidden_dims': cfg['flow_hidden_dims'],
        'decoder_hidden_dims': cfg['vae_hidden_dims'],
        'coeff': 0.99, 'n_power_iterations': 5,
        'activation_function': 'lipmish'
    }
    model = factory.build_graph_vae(bn, infer_args, gen_args, device, 
                                    condition_sigma=False, x_as_unit=True, z_as_unit=True)

    model.generative_network.forward = types.MethodType(forward_bernoulli, model.generative_network)  
    model.generative_network.sample = types.MethodType(sample_bernoulli, model.generative_network)   

    return model

def build_vae_post_prior_mnist(bn, cfg, device):
    latent_dim = bn.get_num_latent()
    infer_args = {
        'type': 'resflow', 
        'num_flow_steps': cfg['num_blocks'], 
        'hidden_dims': cfg['flow_hidden_dims'],
        'coeff': 0.99, 'n_power_iterations': 5,
        'activation_function': 'lipmish'
    }
    gen_args = {
        'type': 'resflow',
        'num_flow_steps': cfg['num_blocks'],
        'flow_hidden_dims': cfg['flow_hidden_dims'],
        'decoder_hidden_dims': cfg['vae_hidden_dims'],
        'coeff': 0.99, 'n_power_iterations': 5,
        'activation_function': 'lipmish'
    }
    model = factory.build_graph_vae(bn, infer_args, gen_args, device, 
                    condition_sigma=False, x_as_unit=True, 
                    z_as_unit=True, gen_decoder_act_func=nn.ELU())
    # Fix gen network
    model.generative_network.decoder_net = MNISTConvDecoder(784, latent_dim)
    # Fix infer network
    x_encoder = MNISTConvEncoder(784, 784)
    infer_network = model.inference_network
    model.inference_network = XencodingInferNetwork(infer_network, 
                                x_encoder)

    model.generative_network.forward = types.MethodType(forward_bernoulli, model.generative_network)  
    model.generative_network.sample = types.MethodType(sample_bernoulli, model.generative_network)   
    
    return model
    



# def build_conv_vae_post_prior(bn, cfg, device, obs_dim):
#     latent_dim = bn.get_num_latent()
#     model = build_vae_post_prior(bn, cfg, device, False)

#     # Fix gen network
#     model.generative_network.decoder_net = MNISTConvDecoder(obs_dim, latent_dim)

#     # Fix infer network
#     # infer_network = model.inference_network
#     # model.inference_network = ConvInferNetwork(infer_network, 
#     #                             MNISTConvEncoder(obs_dim, obs_dim))
#     return model


@ex.automain
def run(_config, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr']

    # Get data and iterators
    num_train_samples = _config['num_train_samples']
    num_val_samples = _config['num_val_samples']
    if _config['dataset'] == 'onehot':
        num_obs = 5
        X = load_onehot(num_train_samples+num_val_samples).to(device)

    elif _config['dataset'] == 'mnist':
        num_obs = 784
        # 60 000 train, 10 000 test
        X, _ = load_binary_mnist()

    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    X_train, X_val = random_split(X, [num_train_samples, num_val_samples],
                generator=torch.Generator().manual_seed(_config['seed']))
    if  _config['dataset'] == 'mnist':
        #### Remove later
        X_train = torch.utils.data.Subset(X_train, range(10000))
        num_train_samples = 10000
        num_train_batches = num_train_samples//batch_size
        X_val = torch.utils.data.Subset(X_val, range(1000))
        num_val_samples = 1000
        num_val_batches = num_val_samples//batch_size
        #################
    train_batcher = DataLoader(X_train, batch_size=batch_size) 
    val_batcher = DataLoader(X_val, batch_size=batch_size) 
    mnist = True if _config['dataset'] == 'mnist' else False
    onehot = True if _config['dataset'] == 'onehot' else False

    # Build model
    post = _config['add_grf_posterior']
    prior = _config['add_grf_prior']
    indep = _config['indep_bn']
    fc = _config['fc_bn']
    num_latent = _config['num_latent']
    if indep:
        bn = VanillaBN(num_latent=num_latent, num_obs=num_obs)
    elif fc:
        bn = FCBN(num_latent=num_latent, num_obs=num_obs)

    # -> Vanilla VAE
    if not post and not prior:
        # if mnist: model = build_conv_vanilla_vae(bn, _config, device).to(device)
        # else: model = build_vanilla_vae(bn, _config, device).to(device)
        if onehot: model = build_vanilla_vae_onehot(bn, _config, device).to(device)
        elif mnist: model = build_vanilla_vae_mnist(bn, _config, device).to(device)
    
    # -> VAE + GRF Posterior
    elif post and not prior:
        # if mnist: 
        #     if indep:
        #         bn = VanillaBN(num_latent=num_latent, num_obs=num_obs)
        #     elif fc:
        #         bn = FCBN(num_latent=num_latent, num_obs=num_obs)

        #     model = build_conv_vae_post(bn, _config, device, num_obs).to(device)
        # else: model = build_vae_post(bn, _config, device).to(device)
        if onehot: model = build_vae_post_onehot(bn, _config, device).to(device)
        elif mnist: model = build_vae_post_mnist(bn, _config, device).to(device)

    # -> VAE + GRF Posterior + GRF Prior
    elif post and prior:
        # if mnist: 
        #     if indep:
        #         bn = VanillaBN(num_latent=num_latent, num_obs=num_obs)
        #     elif fc:
        #         bn = FCBN(num_latent=num_latent, num_obs=num_obs)

        #     model = build_conv_vae_post_prior(bn, _config, device, num_obs).to(device)
        # else: model = build_vae_post_prior(bn, _config, device).to(device)
        if onehot: model = build_vae_post_prior_onehot(bn, _config, device).to(device)
        elif mnist: model = build_vae_post_prior_mnist(bn, _config, device).to(device)

    else:
        raise Exception("Model specification is not compatible.") 


    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True,        
                    min_lr=1e-6, patience=_config['patience'])

    # Train
    epochs = trange(_config['num_epochs'], mininterval=1) 
    for epoch in epochs:
        train_elbo = 0.0

        for x in train_batcher:
            if mnist: x = x[0]
            loss = model.loss(x.to(device))
            # z, log_q_z_given_x, log_p_x_and_z = model(x.to(device))
            # loss = model.loss(log_q_z_given_x, log_p_x_and_z)
            train_elbo += loss.detach().item()

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # Log training metrics
        _run.log_scalar("train.elbo", value = train_elbo/num_train_batches)

        # Log val metrics
        with torch.no_grad():
            val_elbo = 0.0
            for x in val_batcher:
                if mnist: x = x[0]
                loss = model.loss(x.to(device))
                # _, log_q_z_given_x, log_p_x_and_z = model(x.to(device))
                # val_elbo += model.loss(log_q_z_given_x, log_p_x_and_z).item()
                val_elbo += loss.item()
            _run.log_scalar("val.elbo", val_elbo/num_val_batches)

        
        lr_scheduler.step(val_elbo/num_val_batches)
        epochs.set_description('Train ELBO: {:.3f}, Test ELBO: {:.3f}'.format(
                    train_elbo/num_train_batches, val_elbo/num_val_batches), refresh=False)

        if epoch%50 == 0:
            # Save the model
            path = _config['path']
            torch.save(model, path+'/{}/model_{}.pt'.format(_run._id, epoch))


    
    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))





    