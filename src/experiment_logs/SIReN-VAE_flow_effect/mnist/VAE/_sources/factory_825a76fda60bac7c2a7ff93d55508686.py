import torch
import torch.nn as nn
import numpy as np

from math import pi

from normalizing_flow.discrete.ignf import NormalizingFlowStep
from normalizing_flow.discrete.ignf.conditioners import DAGConditioner, DAGMaskedConditioner, AutoregressiveConditioner, CouplingConditioner
from normalizing_flow.discrete.ignf.normalizers import AffineNormalizer, MonotonicNormalizer
from normalizing_flow.discrete.ignf.discrete_nf import DiscreteNF

from normalizing_flow.discrete.resflow.residual_blocks import GraphicalResidualBlock, GraphicalLipschitzResidualBlock
from normalizing_flow.discrete.resflow.graphical_residual_flow import GraphicalResidualFlow

from normalizing_flow.continuous.sccnf.continuous_nf import ContinuousNF
from normalizing_flow.continuous.sccnf.ode_func import ODEfunc
from normalizing_flow.continuous.sccnf.ode_nets import DAGNet, DAGNetHidden, DAGNetHidden2

from vae import VAE
from vae.graph.gen_network import GraphGenNetwork
from vae.graph.infer_network import GraphInferNetwork
from vae.graph.nf_discrete_time import NFDiscreteTime
from vae.graph.gf_discrete_time import GFDiscreteTime
from vae.vanilla.gen_network import VanillaGenNetwork
from vae.vanilla.infer_network import VanillaInferNetwork

from modules import FactorizedInverseBinaryTree, NormalLogDensity, StandardNormalLogDensity

from graph.utils import adjacency_matrix, graph_masks, autoregressive_masks


def build_factorized_inverse_binary_tree(hidden_dims, bn):
    return FactorizedInverseBinaryTree(hidden_dims, bn, NormalLogDensity())


def build_discrete_nf(num_flow_steps, bn, hidden_dims, conditioner_type,
        normalizer_type, device, mono_args=None):
    num_latent = bn.get_num_latent()
    num_obs = bn.get_num_obs()
    flow_steps = []

    # Normalizer
    normalizer = None
    normalizer_args = {}
    norm_num_params = 0
    if normalizer_type == 'affine':
        normalizer = AffineNormalizer
        norm_num_params = AffineNormalizer.num_params()
    elif normalizer_type == 'monotonic':
        normalizer = MonotonicNormalizer
        normalizer_args['int_net_hidden'] = mono_args['int_net_hidden']
        normalizer_args['int_net_final_activation'] = mono_args['int_net_final_activation']
        normalizer_args['cond_size'] = mono_args['cond_size']
        norm_num_params = mono_args['cond_size']
        normalizer_args['num_steps']= mono_args['mono_num_steps']
        normalizer_args['solver'] = mono_args['mono_solver']
    else:
        raise Exception("Normalizer type unknown: {}".format(normalizer_type))

    # Conditioner
    conditioner = None
    conditioner_args = {
        'in_size': num_latent,
        'hidden': hidden_dims,
        'out_size': norm_num_params,
        "cond_in": num_obs
    }
    if conditioner_type == 'dag':
        conditioner = DAGConditioner
        conditioner_args['A'] = torch.tensor(adjacency_matrix(bn.inverse_graph), dtype=torch.float32).to(device)

    elif conditioner_type == 'dag-masked':
        conditioner = DAGMaskedConditioner
        masks = graph_masks(
            bn.inverse_graph, 
            input_vars=[*range(num_latent+num_obs)], 
            output_vars=[*range(num_latent)]*norm_num_params, 
            hidden_dims=hidden_dims
            )
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        conditioner_args['masks'] = masks       

    elif conditioner_type == 'autoregressive':
        conditioner = AutoregressiveConditioner
        masks = autoregressive_masks(
            in_dim=num_latent+num_obs, 
            out_dim=num_latent, 
            num_out_chunks=norm_num_params,
            hidden_dims=hidden_dims
            )
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        torch.set_printoptions(profile="full")
       
        conditioner_args['masks'] = masks
        reverse = False

    elif conditioner_type == 'coupling':
        conditioner = CouplingConditioner
        reverse = False

    else:
        raise Exception("Conditioner type unknown: {}".format(conditioner_type))
    
    # Create steps of flow
    for step in range(num_flow_steps):
        if conditioner_type == 'autoregressive' or conditioner_type == 'coupling':
            conditioner_args['reverse'] = reverse
            reverse = not reverse

        c = conditioner(**conditioner_args)
        n = normalizer(**normalizer_args)
        flow_step = NormalizingFlowStep(c, n)
        flow_steps.append(flow_step)

    return DiscreteNF(flow_steps, bn, StandardNormalLogDensity(), device)


def build_discrete_nf_density_estimation(num_flow_steps, bn, hidden_dims, 
        conditioner_type, normalizer_type, device, mono_args=None):
    D = bn.get_num_vertices()
    flow_steps = []

    # Normalizer
    normalizer = None
    normalizer_args = {}
    norm_num_params = 0
    if normalizer_type == 'affine':
        normalizer = AffineNormalizer
        norm_num_params = AffineNormalizer.num_params()
    elif normalizer_type == 'monotonic':
        normalizer = MonotonicNormalizer
        normalizer_args['int_net_hidden'] = mono_args['int_net_hidden']
        normalizer_args['int_net_final_activation'] = mono_args['int_net_final_activation']
        normalizer_args['cond_size'] = mono_args['cond_size']
        norm_num_params = mono_args['cond_size']
        normalizer_args['num_steps']= mono_args['mono_num_steps']
        normalizer_args['solver'] = mono_args['mono_solver']
    else:
        raise Exception("Normalizer type unknown: {}".format(normalizer_type))

    # Conditioner
    conditioner = None
    conditioner_args = {
        'in_size': D,
        'hidden': hidden_dims,
        'out_size': norm_num_params,
        "cond_in": 0
    }
    if conditioner_type == 'dag':
        conditioner = DAGConditioner
        conditioner_args['A'] = torch.tensor(adjacency_matrix(bn.forward_graph), dtype=torch.float32).to(device)

    elif conditioner_type == 'dag-masked':
        conditioner = DAGMaskedConditioner
        masks = graph_masks(
            bn.forward_graph, 
            input_vars=[*range(D)], 
            output_vars=[*range(D)]*norm_num_params, 
            hidden_dims=hidden_dims
            )
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        conditioner_args['masks'] = masks       

    elif conditioner_type == 'autoregressive':
        conditioner = AutoregressiveConditioner
        masks = autoregressive_masks(
            in_dim=D, 
            out_dim=D, 
            num_out_chunks=norm_num_params,
            hidden_dims=hidden_dims
            )
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        torch.set_printoptions(profile="full")
       
        conditioner_args['masks'] = masks
        reverse = False

    elif conditioner_type == 'coupling':
        conditioner = CouplingConditioner
        reverse = False

    else:
        raise Exception("Conditioner type unknown: {}".format(conditioner_type))
    
    # Create steps of flow
    for step in range(num_flow_steps):
        if conditioner_type == 'autoregressive' or conditioner_type == 'coupling':
            conditioner_args['reverse'] = reverse
            reverse = not reverse

        c = conditioner(**conditioner_args)
        n = normalizer(**normalizer_args)
        flow_step = NormalizingFlowStep(c, n)
        flow_steps.append(flow_step)

    return DiscreteNF(flow_steps, bn, StandardNormalLogDensity(), device, generative=False)


def build_residual_flow(res_type, num_residual_blocks, graph, hidden_dims, 
        activation_function, coeff, n_power_iterations, device, grad_in_forward, scale):
    residual_blocks = []
    latent_dim = graph.get_num_latent()
    cond_dim = graph.get_num_obs()

    masks = graph_masks(
        graph.inverse_graph, 
        input_vars=[*range(latent_dim+cond_dim)], 
        output_vars=[*range(latent_dim)], 
        hidden_dims=hidden_dims,
        self_dependent=True)
    masks = [torch.from_numpy(mask).to(device) for mask in masks]
    args = {
        'in_dim': latent_dim, 
        'hidden_dims': hidden_dims, 
        'cond_dim': cond_dim,
        'masks': masks,
        'activation_function': activation_function
    }
    if res_type == 'graphical':
        residual_block = GraphicalResidualBlock
    elif res_type == 'graphical-lipschitz':
        residual_block = GraphicalLipschitzResidualBlock
        args['coeff'] = coeff
        args['n_power_iterations'] = n_power_iterations
    else:
        raise Exception("Unknown residual block type: {}".format(res_type))

    # Create blocks of flow
    for block in range(num_residual_blocks):
        block = residual_block(**args)
        residual_blocks.append(block)

    return GraphicalResidualFlow(residual_blocks, graph, StandardNormalLogDensity(), device,  scale,
        grad_in_forward=grad_in_forward)


def build_residual_flow_density_estimation(res_type, num_residual_blocks, 
        graph, hidden_dims, activation_function, coeff, n_power_iterations,
        device, grad_in_forward, scale=None):
    residual_blocks = []
    D = graph.get_num_vertices()

    masks = graph_masks(
        graph.forward_graph, 
        input_vars=[*range(D)], 
        output_vars=[*range(D)], 
        hidden_dims=hidden_dims,
        self_dependent=True)
    masks = [torch.from_numpy(mask).to(device) for mask in masks]
    args = {
        'in_dim': D, 
        'hidden_dims': hidden_dims, 
        'cond_dim': 0,
        'masks': masks,
        'activation_function': activation_function
    }
    if res_type == 'graphical':
        residual_block = GraphicalResidualBlock
    elif res_type == 'graphical-lipschitz':
        residual_block = GraphicalLipschitzResidualBlock
        args['coeff'] = coeff
        args['n_power_iterations'] = n_power_iterations
    else:
        raise Exception("Unknown residual block type: {}".format(res_type))

    # Create blocks of flow
    for block in range(num_residual_blocks):
        block = residual_block(**args)
        residual_blocks.append(block)

    return GraphicalResidualFlow(residual_blocks, graph, 
        StandardNormalLogDensity(), device, scale, generative=False,
        grad_in_forward=grad_in_forward)


def build_continuous_nf(num_flow_steps, bn, x_shift, x_scale, 
        z_shift, z_scale, device, dag_net='sccnf', hidden_dim=100):
    latent_dim = bn.get_num_latent()
    cond_dim = bn.get_num_obs()
    
    if dag_net == 'sccnf':
        adjacency = torch.from_numpy(adjacency_matrix(bn.inverse_graph))
        diffeq = DAGNet(data_dim=latent_dim, cond_dim=cond_dim,       
                        adjacency=adjacency, device=device, 
                        num_flow_steps=num_flow_steps).to(device)
    elif dag_net == 'hidden':
        masks = graph_masks(
                    bn.inverse_graph, 
                    input_vars=[*range(latent_dim+cond_dim)], 
                    output_vars=[*range(latent_dim)], 
                    hidden_dims=[hidden_dim])
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        diffeq = DAGNetHidden(data_dim=latent_dim, cond_dim=cond_dim,
                        hidden_dim=hidden_dim, num_flow_steps=num_flow_steps,
                        masks=masks, device=device).to(device)
    elif dag_net == 'hidden2':
        masks = graph_masks(
            bn.inverse_graph,
            input_vars=[*range(latent_dim+cond_dim)],
            output_vars=[*range(latent_dim)],
            hidden_dims=[hidden_dim]*(num_flow_steps-1),
            self_dependent=True
        )
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        diffeq = DAGNetHidden2(data_dim=latent_dim, cond_dim=cond_dim,
                        hidden_dim=hidden_dim, num_flow_steps=num_flow_steps,
                        masks=masks, device=device).to(device)

    odefunc = ODEfunc(diffeq) 
    cnf = ContinuousNF(odefunc=odefunc, bn=bn, cond_shift=x_shift, 
        cond_scale=x_scale, z_shift=z_shift, z_scale=z_scale)

    return cnf


def build_continuous_nf_density_estimation(num_flow_steps, bn, z_shift,
        z_scale, device, dag_net='sccnf', hidden_dim=100):
    D = bn.get_num_vertices()

    if dag_net == 'sccnf':
        adjacency = torch.from_numpy(adjacency_matrix(bn.forward_graph))
        diffeq = DAGNet(data_dim=D, cond_dim=0,       
                        adjacency=adjacency, device=device, 
                        num_flow_steps=num_flow_steps).to(device)
    elif dag_net == 'hidden':
        masks = graph_masks(
                    bn.forward_graph, 
                    input_vars=[*range(D)], 
                    output_vars=[*range(D)], 
                    hidden_dims=[hidden_dim])
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        diffeq = DAGNetHidden(data_dim=D, cond_dim=0,
                    hidden_dim=hidden_dim, 
                    num_flow_steps=num_flow_steps, masks=masks,
                    device=device).to(device)
    elif dag_net == 'hidden2':
        masks = graph_masks(
            bn.forward_graph,
            input_vars=[*range(D)],
            output_vars=[*range(D)],
            hidden_dims=[hidden_dim]*(num_flow_steps-1),
            self_dependent=True
        )
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        diffeq = DAGNetHidden2(data_dim=D, cond_dim=0,
                        hidden_dim=hidden_dim, num_flow_steps=num_flow_steps,
                        masks=masks, device=device).to(device)

    odefunc = ODEfunc(diffeq) 
    cnf = ContinuousNF(odefunc=odefunc, bn=bn, cond_shift=None, 
        cond_scale=None, z_shift=z_shift, z_scale=z_scale,
        generative=False)

    return cnf



def build_graph_vae(graph, infer_args, gen_args, device, condition_sigma=True):
    ######################################
    #  Initialize the inference network  #
    ######################################
    # Args:
    #   - type (ignf, resflow)
    #   - num_flow_steps
    #   - hidden_dims
    num_infer_flow_steps = infer_args['num_flow_steps']
    infer_nf_type = infer_args['type']
    hidden_dims = infer_args['hidden_dims']

    # >>> Inverse Graphical Normalizing Flow <<< 
    # Args:
    #   - normalizer_type (affine, monotonic)
    #   - conditioner_type (dag-masked, coupling, autoregressive)
    if infer_nf_type == 'ignf':
        # nf args
        normalizer_type = infer_args['normalizer_type']
        conditioner_type = infer_args['conditioner_type']
        num_latent = graph.get_num_latent()
        num_obs = graph.get_num_obs()

        # Normalizer
        normalizer = None
        normalizer_args = {}
        norm_num_params = 0

        if normalizer_type == 'affine':
            normalizer = AffineNormalizer
            norm_num_params = AffineNormalizer.num_params()
        elif normalizer_type == 'monotonic':
            # normalizer = MonotonicNormalizer
            # norm_num_parameters = MonotonicNormalizer.num_params()
            # normalizer_args['int_net_hidden'] = [150]
            # normalizer_args['cond_size'] = 10
            # normalizer_args['num_steps'] = 15
            # normalizer_args['solver'] = 'CC'
            # norm_num_params = 10
            raise Exception("Monotonic normalizer not yet available.")
        else:
            raise Exception("Normalizer type unknown: {}".format(normalizer_type))

        # Conditioner
        conditioner = None
        conditioner_args = {
            'in_size': num_latent,
            'hidden': hidden_dims,
            'out_size': norm_num_params,
            "cond_in": num_obs
        }
        if conditioner_type == 'dag-masked':
            conditioner = DAGMaskedConditioner
            masks = graph_masks(
                graph.inverse_graph, 
                input_vars=[*range(num_latent+num_obs)], 
                output_vars=[*range(num_latent)]*norm_num_params, 
                hidden_dims=hidden_dims
                )
            masks = [torch.from_numpy(mask).to(device) for mask in masks]
            conditioner_args['masks'] = masks       

        elif conditioner_type == 'autoregressive':
            conditioner = AutoregressiveConditioner
            masks = autoregressive_masks(
                in_dim=num_latent+num_obs, 
                out_dim=num_latent, 
                num_out_chunks=norm_num_params,
                hidden_dims=hidden_dims
                )
            masks = [torch.from_numpy(mask).to(device) for mask in masks]
            torch.set_printoptions(profile="full")
        
            conditioner_args['masks'] = masks
            reverse = False

        elif conditioner_type == 'coupling':
            conditioner = CouplingConditioner
            reverse = False

        else:
            raise Exception("Conditioner type unknown: {}".format(conditioner_type))
        
        # Create steps of flow
        steps = []
        for _ in range(num_infer_flow_steps):
            if conditioner_type == 'autoregressive' or conditioner_type == 'coupling':
                conditioner_args['reverse'] = reverse
                reverse = not reverse

            c = conditioner(**conditioner_args)
            n = normalizer(**normalizer_args)
            flow_step = NormalizingFlowStep(c, n)
            steps.append(flow_step)

    # >>> Graphical Residual Flow <<<
    # Args:
    #   - activation_function 
    #   - coeff
    #   - n_power_iterations
    elif infer_nf_type == 'resflow':
        latent_dim = graph.get_num_latent()
        cond_dim = graph.get_num_obs()
        activation_function = infer_args['activation_function']

        masks = graph_masks(
            graph.inverse_graph, 
            input_vars=[*range(latent_dim+cond_dim)], 
            output_vars=[*range(latent_dim)], 
            hidden_dims=hidden_dims,
            self_dependent=True)
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        args = {
            'in_dim': latent_dim, 
            'hidden_dims': hidden_dims, 
            'cond_dim': cond_dim,
            'masks': masks,
            'activation_function': activation_function
        }
        residual_block = GraphicalLipschitzResidualBlock
        args['coeff'] = infer_args['coeff']
        args['n_power_iterations'] = infer_args['n_power_iterations']

        # Create blocks of flow
        steps = []
        for _ in range(num_infer_flow_steps):
            step = residual_block(**args)
            steps.append(step)
    else:
         raise Exception("Normalizing flow type unknown: {}".format(infer_nf_type))

    nf = GFDiscreteTime(steps, graph, device)
    inference_network = GraphInferNetwork(nf)

    #######################################
    #  Initialize the generative network  #
    #######################################
    # Args:
    #   - type (ignf, resflow
    #   - num_flow_steps
    #   - flow_hidden_dims
    #   - decoder_hidden_dims
    num_gen_flow_steps = gen_args['num_flow_steps']
    gen_nf_type = gen_args['type']
    flow_hidden_dims = gen_args['flow_hidden_dims']
    decoder_hidden_dims = gen_args['decoder_hidden_dims']

    # >>> Inverse Graphical Normalizing Flow <<< 
    # Args:
    #   - normalizer_type (affine, monotonic)
    #   - conditioner_type (dag-masked, coupling, autoregressive)
    if gen_nf_type == 'ignf':
        # nf args
        normalizer_type = gen_args['normalizer_type']
        conditioner_type = gen_args['conditioner_type']
        num_latent = graph.get_num_latent()
        num_obs = graph.get_num_obs()

        # Normalizer
        normalizer = None
        normalizer_args = {}
        norm_num_params = 0

        if normalizer_type == 'affine':
            normalizer = AffineNormalizer
            norm_num_params = AffineNormalizer.num_params()
        elif normalizer_type == 'monotonic':
            # normalizer = MonotonicNormalizer
            # norm_num_parameters = MonotonicNormalizer.num_params()
            # normalizer_args['int_net_hidden'] = [150]
            # normalizer_args['cond_size'] = 10
            # normalizer_args['num_steps'] = 15
            # normalizer_args['solver'] = 'CC'
            # norm_num_params = 10
            raise Exception("Monotonic normalizer not yet available.")
        else:
            raise Exception("Normalizer type unknown: {}".format(normalizer_type))

        # Conditioner
        conditioner = None
        conditioner_args = {
            'in_size': num_latent,
            'hidden': flow_hidden_dims,
            'out_size': norm_num_params
        }
        if conditioner_type == 'dag-masked':
            conditioner = DAGMaskedConditioner
            masks = graph_masks(
                graph.forward_graph, 
                input_vars=[*range(num_latent)], 
                output_vars=[*range(num_latent)]*norm_num_params, 
                hidden_dims=flow_hidden_dims
                )
            masks = [torch.from_numpy(mask).to(device) for mask in masks]
            conditioner_args['masks'] = masks       

        elif conditioner_type == 'autoregressive':
            conditioner = AutoregressiveConditioner
            masks = autoregressive_masks(
                in_dim=num_latent, 
                out_dim=num_latent, 
                num_out_chunks=norm_num_params,
                hidden_dims=flow_hidden_dims
                )
            masks = [torch.from_numpy(mask).to(device) for mask in masks]
            torch.set_printoptions(profile="full")
        
            conditioner_args['masks'] = masks
            reverse = False

        elif conditioner_type == 'coupling':
            conditioner = CouplingConditioner
            reverse = False

        else:
            raise Exception("Conditioner type unknown: {}".format(conditioner_type))
        
        # Create steps of flow
        steps = []
        for _ in range(num_gen_flow_steps):
            if conditioner_type == 'autoregressive' or conditioner_type == 'coupling':
                conditioner_args['reverse'] = reverse
                reverse = not reverse

            c = conditioner(**conditioner_args)
            n = normalizer(**normalizer_args)
            flow_step = NormalizingFlowStep(c, n)
            steps.append(flow_step)

    # >>> Graphical Residual Flow <<<
    # Args:
    #   - activation_function 
    #   - coeff
    #   - n_power_iterations
    elif gen_nf_type == 'resflow':
        latent_dim = graph.get_num_latent()
        cond_dim = graph.get_num_obs()
        activation_function = gen_args['activation_function']

        masks = graph_masks(
            graph.forward_graph, 
            input_vars=[*range(latent_dim)], 
            output_vars=[*range(latent_dim)], 
            hidden_dims=flow_hidden_dims,
            self_dependent=True)
        masks = [torch.from_numpy(mask).to(device) for mask in masks]
        args = {
            'in_dim': latent_dim, 
            'hidden_dims': flow_hidden_dims,
            'masks': masks,
            'activation_function': activation_function
        }
        residual_block = GraphicalLipschitzResidualBlock
        args['coeff'] = gen_args['coeff']
        args['n_power_iterations'] = gen_args['n_power_iterations']

        # Create blocks of flow
        steps = []
        for _ in range(num_gen_flow_steps):
            step = residual_block(**args)
            steps.append(step)
    else:
         raise Exception("Normalizing flow type unknown: {}".format(gen_nf_type))

    nf = NFDiscreteTime(steps, graph, device)
    generative_network = GraphGenNetwork(graph, nf, decoder_hidden_dims, device,
                condition_sigma=condition_sigma)

    # Ininitial the final model
    return VAE(inference_network, generative_network)


def build_vanilla_vae(graph, infer_hidden_dims, gen_hidden_dims, device, 
                        condition_sigma=True):
    latent_dim = graph.get_num_latent()
    obs_dim = graph.get_num_obs()
    
    inference_network = VanillaInferNetwork(latent_dim, obs_dim, 
        infer_hidden_dims, device, condition_sigma)
    generative_network = VanillaGenNetwork(latent_dim, obs_dim,
        gen_hidden_dims, device, condition_sigma)

    return VAE(inference_network, generative_network)


