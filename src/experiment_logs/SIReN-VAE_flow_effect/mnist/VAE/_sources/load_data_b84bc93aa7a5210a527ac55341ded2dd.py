import torch
import torchvision
import torchvision.transforms as transforms


def load_onehot(num_samples):
    torch.manual_seed(0)
    p = torch.tensor([1/3]*3)
    sample = torch.stack(
         [torch.zeros_like(p).scatter_(0, torch.multinomial(p,1), 1.) 
        for _ in range(num_samples)])
    return sample


def load_mnist():
    print('Loading MNIST:')
    flatten = transforms.Compose([transforms.ToTensor(),
                transforms.Lambda(lambda img: img.view(-1))])
    mnist_train_dataset = torchvision.datasets.MNIST(root='./data/datasets/',
                train=True, download=True, transform=flatten)
    mnist_test_dataset = torchvision.datasets.MNIST(root='./data/datasets/', 
                train=False, download=True, transform=flatten)
    print('Data dimension: ', len(mnist_train_dataset[0][0]))
    print('Train dataset size: ', len(mnist_train_dataset))
    print('Test dataset size: ', len(mnist_test_dataset))
    return mnist_train_dataset, mnist_test_dataset