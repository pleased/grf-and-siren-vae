import torch
import torchvision
import torchvision.transforms as transforms
import numpy as np


def load_onehot(num_samples):
    torch.manual_seed(0)
    p = torch.tensor([1/3]*3)
    sample = torch.stack(
         [torch.zeros_like(p).scatter_(0, torch.multinomial(p,1), 1.) 
        for _ in range(num_samples)])
    return sample


def load_mnist():
    print('Loading MNIST:')
    flatten = transforms.Compose([transforms.ToTensor(),
                transforms.Lambda(lambda img: img.view(-1))])
    train_data = torchvision.datasets.MNIST(root='./data/datasets/',
                train=True, download=True, transform=flatten)
    test_data = torchvision.datasets.MNIST(root='./data/datasets/', 
                train=False, download=True, transform=flatten)
    print('Data dimension: ', len(train_data[0][0]))
    print('Train dataset size: ', len(train_data))
    print('Test dataset size: ', len(test_data))
    return train_data, test_data


def load_binary_mnist():
    print('Loading binary MNIST:')
    transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,)),
                       lambda x: x>0,
                       lambda x: x.float(),
            ])
    train_data = torchvision.datasets.MNIST('./pytorch/data/', 
                train=True, download=True, transform=transform)
    test_data = torchvision.datasets.MNIST('./pytorch/data/', 
                train=False, download=True, transform=transform)
    print('Data dimension: ', len(train_data[0][0]))
    print('Train dataset size: ', len(train_data))
    print('Test dataset size: ', len(test_data))
    return train_data, test_data


def load_2_spirals(num_samples):
    rng = np.random.RandomState()
    n = np.sqrt(rng.rand(num_samples//2, 1))*540*(2*np.pi)/360
    d1x = -np.cos(n)*n + rng.rand(num_samples//2, 1)*0.5
    d1y = np.sin(n)*n + rng.rand(num_samples//2, 1)*0.5
    spirals = np.vstack((np.hstack((d1x, d1y)), np.hstack((-d1x, -d1y))))/3
    spirals += rng.randn(*spirals.shape)*0.1

    return torch.tensor(spirals, dtype=torch.float32) 


def load_pinwheel(num_samples):
    rng = np.random.RandomState()
    radial_std = 0.3
    tangential_std = 0.1
    num_classes = 5
    num_per_class = num_samples//5
    rate = 0.25
    rads = np.linspace(0, 2*np.pi, num_classes, endpoint=False)

    features = rng.randn(num_classes*num_per_class, 2) \
            * np.array([radial_std, tangential_std])
    features[:, 0] += 1.
    labels = np.repeat(np.arange(num_classes), num_per_class)

    angles = rads[labels] + rate*np.exp(features[:, 0])
    rotations = np.stack([np.cos(angles), -np.sin(angles), np.sin(angles), np.cos(angles)])
    rotations = np.reshape(rotations.T, (-1, 2, 2))

    wheels = 2*rng.permutation(np.einsum("ti,tij->tj", features, rotations))

    return torch.tensor(wheels, dtype=torch.float32)