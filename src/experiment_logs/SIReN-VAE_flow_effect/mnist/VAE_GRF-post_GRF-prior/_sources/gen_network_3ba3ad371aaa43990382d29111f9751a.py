import torch
import torch.nn as nn

from torch.nn.functional import relu, tanh
from torch.distributions import Normal, Bernoulli

from modules import MLP, StandardNormalLogDensity

class VanillaGenNetwork(nn.Module):

    def __init__(self, latent_dim, obs_dim, hidden_dims, device,
                    activation_function = nn.ReLU(), 
                    batch_norm=False, condition_sigma=True,
                    likelihood='gaussian'):
        super(VanillaGenNetwork, self).__init__()
        self.device = device
        self.latent_dim = latent_dim
        self.obs_dim = obs_dim
        self.prior = StandardNormalLogDensity()
        self.likelihood = likelihood

        if condition_sigma:
            if likelihood != 'gaussian':
                raise Exception('Model mis-specification.')
            self.decoder_net = MLP(in_size=latent_dim, hidden=hidden_dims,
                out_size=(2*obs_dim), act_f=activation_function, 
                batch_norm=batch_norm)
        else:
            self.decoder_net = MLP(in_size=latent_dim, hidden=hidden_dims,
                out_size=(obs_dim), act_f=activation_function,
                batch_norm=batch_norm)
            self.log_sigma = torch.nn.Parameter(torch.zeros(1))


    def forward(self, x, z):
        '''Forward pass through the generative network. 


        Parameters
        ----------
        x : torch.tensor
            The observed variables of size [batch size, x_dim].
        z : torch.tensor
            A sample of size [batch size, z_dim] of the latent 
            variables from the inference network.

        Returns
        -------
        p_x_and_z : torch.tensor
            The joint density of x and z under the given model.
        '''
        # Compute the density of z
        log_p_z = self.prior(z)

        # Calculate mean and standard deviation 
        params = self.decoder_net(z)

        if self.likelihood == 'gaussian':
            if params.shape[1] == 2*self.obs_dim:
                mu = params[:,:self.obs_dim]
                sigma = torch.exp(params[:,self.obs_dim:])#.clamp(-5,5))
            else:
                mu = params
                sigma = torch.exp(self.log_sigma)

            p = Normal(loc=mu, scale=sigma)
        
        elif self.likelihood == 'bernoulli':
            p = Bernoulli(probs=torch.sigmoid(params))

        else:
            raise Exception('Likelihood specification not implemented.')

         # Compute the obs likelihood
        log_p_x_given_z = p.log_prob(x).sum(dim=1)
        return log_p_x_given_z + log_p_z


    def sample(self, batch_size, use_mu=False):
        # Generate sample of latent variables
        z = Normal(
            loc=torch.zeros(self.latent_dim, dtype=torch.float32), 
            scale=torch.ones(self.latent_dim, dtype=torch.float32)
            ).sample((batch_size,)).to(self.device)

        # Calculate mean and standard deviation 
        params = self.decoder_net(z)

        if self.likelihood == 'gaussian':
            if params.shape[1] == 2*self.obs_dim:
                mu = params[:,:self.obs_dim]
                sigma = torch.exp(params[:,self.obs_dim:])#.clamp(-5,5))
            else:
                mu = params
                sigma = torch.exp(self.log_sigma)

            p = Normal(loc=mu, scale=sigma)

        elif self.likelihood == 'bernoulli':
            mu = params
            p = Bernoulli(probs=torch.sigmoid(params))

        # Generate sample of observed variables:
        x = p.sample().to(self.device)

        return x, z


    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)
