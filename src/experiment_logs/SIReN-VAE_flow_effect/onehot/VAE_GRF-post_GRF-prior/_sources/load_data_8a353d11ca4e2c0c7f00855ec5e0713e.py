import torch
import torchvision
import torchvision.transforms as transforms
import numpy as np
import pandas as pd
import random

from math import ceil
from sklearn.neighbors import NearestNeighbors


def load_dataset(bn, batch_size=100, double=False, device='cpu'):
    if bn == 'arithmetic-circuit':
        return load_arithmetic_circuit(batch_size, double, device)
    elif bn == 'tree':
        return load_tree(batch_size, double, device)
    elif bn == 'ecoli70-alt':
        return load_ecoli_synth(batch_size, double, device)
    elif bn == 'arth150-synth':
        return load_arth_synth(batch_size, double, device)
    elif bn == 'arithmetic-circuit2':
        return load_arithmetic_circuit2(batch_size, double, device)


def load_arithmetic_circuit(batch_size, double, device):
    # Load data
    train = torch.load('./data/datasets/arithmetic-circuit/train.pt').to(device)
    val = torch.load('./data/datasets/arithmetic-circuit/val.pt').to(device)
    test = torch.load('./data/datasets/arithmetic-circuit/test.pt').to(device)
    if double:
        train = train.double()
        val = val.double()
        test = test.double()

    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=batch_size)
  
    return trainloader, valloader, testloader


def load_tree(batch_size, double, device):
    # Load data
    train = torch.load('./data/datasets/tree/train.pt', map_location=torch.device(device)).to(device)
    val = torch.load('./data/datasets/tree/val.pt', map_location=torch.device(device)).to(device)
    test = torch.load('./data/datasets/tree/test.pt', map_location=torch.device(device)).to(device)

    if double:
        train = train.double()
        val = val.double()
        test = test.double()


    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=batch_size)
  
    return trainloader, valloader, testloader


def load_ecoli_synth(batch_size, double, device):
    # Load data
    train = torch.load('./data/datasets/ecoli-synth/train.pt', map_location=torch.device(device)).to(device)
    val = torch.load('./data/datasets/ecoli-synth/val.pt', map_location=torch.device(device)).to(device)
    test = torch.load('./data/datasets/ecoli-synth/test.pt', map_location=torch.device(device)).to(device)
    if double:
        train = train.double()
        val = val.double()
        test = test.double()


    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=batch_size)
  
    return trainloader, valloader, testloader


def load_arth_synth(batch_size, double, device):
    # Load data
    train = torch.load('./data/datasets/arth150-synth/train.pt', map_location=torch.device(device)).to(device)
    val = torch.load('./data/datasets/arth150-synth/val.pt', map_location=torch.device(device)).to(device)
    test = torch.load('./data/datasets/arth150-synth/test.pt', map_location=torch.device(device)).to(device)
    if double:
        train = train.double()
        val = val.double()
        test = test.double()


    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=batch_size)
  
    return trainloader, valloader, testloader


def load_arithmetic_circuit2(batch_size, double, device):
    # Load data
    train = torch.load('./data/datasets/arithmetic-circuit2/train.pt', map_location=torch.device(device)).to(device)
    val = torch.load('./data/datasets/arithmetic-circuit2/val.pt', map_location=torch.device(device)).to(device)
    test = torch.load('./data/datasets/arithmetic-circuit2/test.pt', map_location=torch.device(device)).to(device)
    if double:
        train = train.double()
        val = val.double()
        test = test.double()

    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=batch_size)
  
    return trainloader, valloader, testloader


def load_protein(batch_size=100, device='cpu', double=False, seed=0):
    train = torch.tensor(torch.load('./data/datasets/human_protein/X_train.pkt', map_location=torch.device(device)), dtype=torch.float32).to(device)
    test = torch.tensor(torch.load('./data/datasets/human_protein/X_valid.pkt', map_location=torch.device(device)), dtype=torch.float32).to(device)
    if double:
        train = train.double()
        test = test.double()

    mu, sigma = train.mean(0), train.std(0)
    train = (train - mu)/sigma
    test = (test - mu)/sigma

    torch.manual_seed(seed)

    train, val = torch.utils.data.random_split(train, [9000, 1000])

    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=1672, shuffle=True)

    return trainloader, valloader, testloader



def load_mehra(device='cpu', double=False):
    train = torch.load('./data/datasets/MEHRA/train.pt', map_location=torch.device(device)).to(device)
    test = torch.load('./data/datasets/MEHRA/test.pt', map_location=torch.device(device)).to(device)
    if double:
        train = train.double()
        test = test.double()

    train, val = torch.utils.data.random_split(train, [4000, 1000])
    train = train.dataset[train.indices]
    val = val.dataset[val.indices]

    # Normalize
    mu, sigma = train.mean(0), train.std(0)
    train = (train - mu)/sigma
    val = (val - mu)/sigma
    test = (test - mu)/sigma

    trainloader = torch.utils.data.DataLoader(train, batch_size=100, shuffle=True)
    valloader = torch.utils.data.DataLoader(val, batch_size=100, shuffle=True)
    testloader = torch.utils.data.DataLoader(test, batch_size=1885, shuffle=True)
    
    return trainloader, valloader, testloader



def load_onehot(num_samples):
    torch.manual_seed(0)
    p = torch.tensor([1/5]*5)
    sample = torch.stack(
         [torch.zeros_like(p).scatter_(0, torch.multinomial(p,1), 1.) 
        for _ in range(num_samples)])
    return sample



def load_binary_mnist():
    print('Loading binary MNIST:')
    transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.1307,), (0.3081,)),
                lambda x: x>0,
                lambda x: x.float(),
                transforms.Lambda(lambda img: img.view(-1))
            ])
    train_data = torchvision.datasets.MNIST('./pytorch/data/', 
                train=True, download=True, transform=transform)
    test_data = torchvision.datasets.MNIST('./pytorch/data/', 
                train=False, download=True, transform=transform)
    print('Data dimension: ', len(train_data[0][0]))
    print('Train dataset size: ', len(train_data))
    print('Test dataset size: ', len(test_data))
    return train_data, test_data
