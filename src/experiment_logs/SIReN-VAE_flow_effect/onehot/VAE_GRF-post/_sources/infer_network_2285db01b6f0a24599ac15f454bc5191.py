import torch
import torch.nn as nn

class GraphInferNetwork(nn.Module):

    def __init__(self, nf):
        super(GraphInferNetwork, self).__init__()
        self.nf = nf


    def forward(self, x, eps=None, sum_factors=True):
        '''Forward pass through the inference network.

        Parameters
        ----------
        x : torch.tensor
            The observed variables of size [batch size, x_dim] for
            which to infer the latent representation z.

        Returns
        -------
        z : torch.tensor
            A posterior sample of size [batch size, z_dim] from the 
            approximate posterior q(z|x).
        log_q_z_given_x : torch.tensor
            The density of the sample z.
        '''
        z, log_q_z_given_x = self.nf(x, eps, sum_factors=sum_factors)
        return z, log_q_z_given_x

    
    def count_parameters(self):
        total = sum(p.numel() for p in  self.parameters() if p.requires_grad)
        masked = 0
        for s in self.nf.steps:
            for m in s.g.masks:
                masked += torch.numel(m) - torch.count_nonzero(m)
        return (total - masked).item()