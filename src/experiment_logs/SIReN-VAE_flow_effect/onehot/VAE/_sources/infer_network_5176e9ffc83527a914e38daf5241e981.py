import torch
import torch.nn as nn

from torch.distributions import Normal

from modules import MLP, StandardNormalLogDensity

class VanillaInferNetwork(nn.Module):

    def __init__(self, latent_dim, obs_dim, hidden_dims, device,
                    activation_function = nn.ReLU(), 
                    batch_norm=False, condition_sigma=True):
        super(VanillaInferNetwork, self).__init__()
        self.device = device
        self.latent_dim = latent_dim
        self.posterior = StandardNormalLogDensity()

        if condition_sigma:
            self.encoder_net = MLP(in_size=obs_dim, hidden=hidden_dims, 
                out_size=(2*latent_dim), act_f=activation_function,
                batch_norm=batch_norm)
        else:
            self.encoder_net = MLP(in_size=obs_dim, hidden=hidden_dims,
                out_size=(latent_dim), act_f=activation_function,
                batch_norm=batch_norm)
            self.log_sigma = torch.nn.Parameter(torch.zeros(1))


    def forward(self, x, sum_factors=True):
        # Calculate mean and standard deviation 
        params = self.encoder_net(x)
        if params.shape[1] == 2*self.latent_dim:
            mu = params[:,:self.latent_dim]
            sigma = torch.exp(params[:,self.latent_dim:])#.clamp(-5,5))
        else:
            mu = params
            sigma = torch.exp(self.log_sigma)
        
        # Sample z (via Reparameterization trick)
        z = Normal(
            loc=torch.zeros(self.latent_dim, dtype=torch.float32), 
            scale=torch.ones(self.latent_dim, dtype=torch.float32)
            ).sample((x.shape[0],)).to(self.device)
        z = z*sigma + mu
        
        # Calculate posterior density
        q_z = Normal(loc=mu, scale=sigma)
        log_q_z_given_x = q_z.log_prob(z)
        if sum_factors: log_q_z_given_x = log_q_z_given_x.sum(dim=1) 

        return z, log_q_z_given_x


    def encode(self, x):
        params = self.encoder_net(x)
        if params.shape[1] == 2*self.latent_dim:
            mu = params[:,:self.latent_dim]
            sigma = torch.exp(params[:,self.latent_dim:])#.clamp(-5,5))
        else:
            mu = params
            sigma = torch.exp(self.log_sigma)

        return mu, sigma


    def count_parameters(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)