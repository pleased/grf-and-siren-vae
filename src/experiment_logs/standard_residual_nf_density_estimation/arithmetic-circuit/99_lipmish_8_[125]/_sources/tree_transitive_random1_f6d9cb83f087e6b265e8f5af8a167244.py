import torch
import numpy as np

from torch.distributions import Normal, MultivariateNormal
from math import log

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph


from graph.belief_network import BeliefNetwork

class TreeRandom1(BeliefNetwork):

    def __init__(self, device, double=False):
        super(TreeRandom1, self).__init__(device, double)

        # graph
        self.forward_graph = ForwardGraph().initialize(*TreeRandom1._construct_graph())
        self.inverse_graph = ForwardGraph().initialize(*TreeRandom1._construct_inverse_graph())

        self.num_latent = 6
        self.num_obs = 1


    @staticmethod
    def _construct_graph():
        vertices = ['z0','z1', 'z2','z3', 'z4','z5', 'x0']
        edges = [('z0','z1') ,('z2','z3'), ('z0','x0'), ('z1','z4'),
                 ('z3','x0'), ('z3','z5'), ('z4','x0'), 
                 ('z5','x0')]  
        observed = {'x0'}
        return vertices, edges, observed


    @staticmethod
    def _construct_inverse_graph():
        vertices = ['z0','z1', 'z2','z3', 'z4','z5', 'x0']
        edges = [('x0','z0'),('x0','z2'),('x0','z5'),('z5','z4'),('z5','z0'),('z5','z3'),('z4','z1'),('z3','z2'),('z1','z0')]  
        observed = {'x0'}
        return vertices, edges, observed