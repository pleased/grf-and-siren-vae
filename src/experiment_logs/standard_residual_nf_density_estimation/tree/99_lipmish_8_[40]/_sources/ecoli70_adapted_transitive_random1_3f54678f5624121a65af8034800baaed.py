import torch
import json


import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from torch.distributions import Normal

from graph.belief_network import EColi70Adapted


class EColi70AdaptedRandom1(EColi70Adapted):

    def __init__(self):
        super(EColi70AdaptedRandom1, self).__init__()

        # graph
        with open('./graph/belief_network/bnlearn/ecoli70_adapted_transitive_random.json') as f:
            bn = json.load(f)
        self.vertices = bn['vertices']
        self.edges = bn['edges']
        self.observed = set(bn['observed'])
        self.intercepts = bn['intercepts']
        self.coefficients = bn['coefficients']
        self.stds = bn['stds']
        self.forward_graph = ForwardGraph().initialize(self.vertices, 
                self.edges, self.observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = len(self.vertices) - len(self.observed)
        self.num_obs = len(self.observed)