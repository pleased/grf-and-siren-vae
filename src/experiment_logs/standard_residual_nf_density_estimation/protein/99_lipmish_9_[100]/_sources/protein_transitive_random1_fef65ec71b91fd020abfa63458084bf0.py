import torch

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import Protein

class ProteinRandom1(Protein):

    def __init__(self):
        super(ProteinRandom1, self).__init__()

        # graph
        graph = ProteinRandom1._construct_graph()
        self.edges = graph[1]
        self.forward_graph = ForwardGraph().initialize(*graph)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 11


    @staticmethod
    def _construct_graph():
        vertices = ['raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk']
        observed = {'raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk'}

        edges = [('plcg','jnk'),('plcg','p38'),('plcg','pip3'),('plcg','akt'),
                 ('pip3','pip2'),('pip3','pkc'),('pip3','raf'),
                 ('pip2','pkc'),('pip2','p38'),('pip2','erk'),('pip2','mek'),
                 ('pkc','pka'),('pkc','akt'),
                 ('pka','jnk'),('pka','raf'),('pka','p38'),
                 ('raf','erk'),('raf','mek'),
                 ('mek','erk'),('erk','akt')]
                 
        return vertices, edges, observed