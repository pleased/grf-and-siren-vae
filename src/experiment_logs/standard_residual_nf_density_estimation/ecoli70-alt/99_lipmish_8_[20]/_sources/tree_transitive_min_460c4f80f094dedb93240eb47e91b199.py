import torch
import numpy as np

from torch.distributions import Normal, MultivariateNormal
from math import log

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import Tree

class TreeTransitiveMin(Tree):

    def __init__(self, device, double=False):
        super(TreeTransitiveMin, self).__init__()

        # graph
        self.forward_graph = ForwardGraph().initialize(*TreeTransitiveMin._construct_graph())
        self.inverse_graph = invert.properly(self.forward_graph)


    @staticmethod
    def _construct_graph():
        vertices = ['z0','z1', 'z2','z3', 'z4','z5', 'x0']
        edges = [('z0','z1') ,('z2','z3'), ('z1','z4'),
                 ('z3','z5'), ('z4','x0'), 
                 ('z5','x0')]  
        observed = {'x0'}
        return vertices, edges, observed