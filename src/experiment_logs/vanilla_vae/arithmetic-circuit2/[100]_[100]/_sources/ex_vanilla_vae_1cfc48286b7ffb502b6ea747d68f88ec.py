import torch
import numpy as np
import matplotlib.pyplot as plt

from sacred import Experiment
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_vanilla_vae
from ex_utils import batch_iterator, sample_batch, kolmogorov_smirnov
from graph.belief_network import GaussianBinaryTree, GaussianTernaryTree, \
    ArithmeticCircuit, LargerBN1, LargerBN2, ArithmeticCircuit2

ex = Experiment('vanilla_vae')
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"

@ex.config
def cfg():
    # Belief network [arithmetic-circuit2]
    bn = 'arithmetic-circuit2'

    encoder_hidden_dims = [100]
    decoder_hidden_dims = [100]

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    num_epochs = 100

    lr = 1e-2
    patience = 20
    seed = 0

    plotting_range = None
    normalize = False

    # Add observer
    ex_name = 'vanilla_vae'
    sub_folder = '{}_{}'.format(encoder_hidden_dims, decoder_hidden_dims)
    path = './experiment_logs/{}/{}/{}'.format(bn, ex_name, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _rnd, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr']
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']

    # BN initialization
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    else:
        raise Exception("Unknown belief network: {}".format(bn))
    num_latent = bn.get_num_latent()
    n = bn.get_num_vertices()

    # Normalization
    if _config['normalize']:
        min_std = 1e-5
        batch = sample_batch(bn.sample, 10000)
        x = batch[:,num_latent:]
        z = batch[:,:num_latent]
        x_std = torch.std(x, dim=0).to(device)
        x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
        x_shift = torch.mean(x, dim=0).to(device)
        z_std = torch.std(z, dim=0).to(device)
        z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
        z_shift = torch.mean(z, dim=0).to(device)
    else:
        x_scale = 1.0
        z_scale = 1.0
        x_shift = 0.0
        z_shift = 0.0

    # Draw train and test sets and return minibatch iterators
    train = torch.zeros((_config['num_train_samples'], n), dtype=torch.float64).to(device)
    test = torch.zeros((_config['num_val_samples'], n), dtype=torch.float64).to(device)
    get_data = partial(bn.sample)
    iterators = partial(batch_iterator, get_data, train, test, batch_size)
    # Initialize model and optimizer
    model = build_vanilla_vae(bn, _config['encoder_hidden_dims'],
        _config['decoder_hidden_dims'], device).double().to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_schedular = ReduceLROnPlateau(optimizer, 'min', verbose=True,
                     min_lr=1e-6, patience=_config['patience'])

    # Train
    epochs = trange(_config['num_epochs'], mininterval=1)
    for epoch in epochs:
        train_batcher, test_batcher = iterators()
        train_elbo = 0.0

        model.train()
        for _ in range(num_train_batches):
            train_batch = train_batcher()
            x = train_batch[:,num_latent:]
            x = (x - x_shift)/x_scale
            z, log_q_z_given_x, log_p_x_and_z = model(x)
            loss = model.loss(log_q_z_given_x, log_p_x_and_z)

            train_elbo += loss.detach().item()
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        # log training metrics - kl-divergence
        _run.log_scalar("train", value=train_elbo/num_train_batches) 

        # Log test metrics - kl-divergence
        model.eval()
        with torch.no_grad():
            val_elbo = 0.0
            for idx in range(num_val_batches):
                val_batch = test_batcher()
                x = val_batch[:,num_latent:]
                x = (x - x_shift)/x_scale
                _, log_q_z_given_x, log_p_x_and_z = model(x)
                val_elbo += model.loss(log_q_z_given_x, log_p_x_and_z).item()
            _run.log_scalar("val", val_elbo/num_val_batches)


        # Print progress
        epochs.set_description('Train -ELBO: {:.3f}, Val -ELBO: {:.3f}'.format(
            train_elbo/num_train_batches, val_elbo/num_val_batches),
            refresh=False)
        # Decay learning rate every 100 epochs
        lr_schedular.step(train_elbo/num_train_batches)

    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))
    
    # # Some plots of the posterior and likelihood distributions
    # with torch.no_grad():
    #     num_obs = graph.get_num_obs()
    #     sample = sample_batch(graph.sample, 50000).double().to(device)
        
    #     # True samples from the forward model
    #     true_x = sample[:,num_latent:]
    #     true_x = (true_x - x_shift)/x_scale
    #     true_z = sample[:,:num_latent]
    #     true_z = (true_z - z_shift)/z_scale

    #     # Samples from the learned model
    #     model_x, model_prior_z = model.generative_network.sample(50000)

    #     # Samples from the inference network
    #     model_post_z, log_q_z_given_x, log_p_x_and_z = model(model_x)
    #     print('log p(x) = ', torch.mean(log_p_x_and_z - log_q_z_given_x).item())
    #     #model_x = model_x*x_scale + x_shift

    #     plotting_range = _config['plotting_range']
    #     # Plot samples from prior vs posterior
    #     for i in range(num_latent):
    #         z_joint = model_prior_z[:,i].cpu().tolist()
    #         z_infer = model_post_z[:,i].cpu().tolist()
    #         z_true = true_z[:,i].cpu().tolist()
    #         plt.hist(x=[z_joint, z_infer, z_true], bins=50, range=plotting_range, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red', 'gold'], edgecolor='none',
    #                     label=['Generative network', 'Inference Network', 'True'])
    #         plt.xlabel("z_"+str(i))
    #         plt.ylabel("Density")
    #         plt.legend()
    #         plt.savefig(path+'/{}/z_{}'.format(_run._id,i))
    #         #plt.show()
    #         plt.clf()
        
    #     for i in range(num_obs):
    #         xtrue = true_x[:,i].cpu().tolist()
    #         xmodel = model_x[:,i].cpu().tolist()
    #         ks, _ = kolmogorov_smirnov(xmodel, xtrue)
    #         fig =  plt.figure()
    #         ax = fig.add_subplot(111)
    #         ax.hist(x=[xtrue, xmodel], bins=50, range=plotting_range, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red'], edgecolor='none',
    #                     label=['True','Generative network'])
    #         ax.set_xlabel("x_"+str(i))
    #         ax.set_ylabel("Likelihood density")
    #         ax.legend()
    #         ax.text(0.02,0.035,"ks={:.3f}".format(ks), transform=ax.transAxes, bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))
    #         plt.savefig(path+'/{}/x_{}'.format(_run._id,i))
    #         #plt.show()
    #         plt.clf()