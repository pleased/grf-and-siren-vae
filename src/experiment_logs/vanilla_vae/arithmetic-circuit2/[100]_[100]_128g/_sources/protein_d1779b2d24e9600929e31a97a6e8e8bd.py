import torch
import sys
import numpy as np
import pandas as pd
import os
import random

if sys.platform != 'win32':
    import graph_tool.all as gt
    from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import BeliefNetwork

class Protein(BeliefNetwork):

    def __init__(self, structure='faithful'):
        super(Protein, self).__init__()

        # graph
        if sys.platform == 'win32':
            self.forward_graph = _WrapperGraph()
            self.inverse_graph = _WrapperGraph(inverse=True)
        else:
            graph = Protein._construct_graph(structure)
            self.edges = graph[1]
            self.forward_graph = ForwardGraph().initialize(*graph)
            self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 11

        self.train, self.test = self.load_data()
        # self.train = train.to_numpy()
        np.random.shuffle(self.train)
        # self.test = test.to_numpy()
        np.random.shuffle(self.test)
        self.train_idx = 0
        self.test_idx = 0
        self.train_N = self.train.shape[0]
        self.test_N = self.test.shape[0]

    def load_data(self):
        dir_f = './datasets/human_protein/'
        # train_path = os.path.join(dir, 'train.csv')
        # test_path = os.path.join(dir, 'test.csv')
        # train = pd.read_csv(train_path)
        # test = pd.read_csv(test_path)

        train = torch.load(dir_f + "X_train.pkt")
        mu, sigma = train.mean(0), train.std(0)
        test = torch.load(dir_f + "X_valid.pkt")
 
        return (train - mu)/sigma, (test - mu)/sigma

    
    def sample(self, batch_size, train=True):
        if train:
            return self._sample_train(batch_size)
        else:
            return self._sample_test(batch_size)


    def _sample_train(self, batch_size):
        sample = self.train.take(
            range(self.train_idx,self.train_idx + batch_size),
            axis=0, mode='wrap')
        self.train_idx = (self.train_idx + batch_size)%self.train_N
        return torch.tensor(sample)


    def _sample_test(self, batch_size):
        sample = self.test.take(
            range(self.test_idx, self.test_idx + batch_size),
            axis=0, mode='wrap')
        self.test_idx = (self.test_idx + batch_size)%self.test_N
        return torch.tensor(sample)


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for EightPairs BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for EightPairs BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for EightPairs BN")


    def get_num_latent(self):
        return 0


    def get_num_obs(self):
        return 11


    def get_num_vertices(self):
        return 11


    def topological_order(self):
        if sys.platform == 'win32':
            return self.forward_graph.topological_order()
        else:
            return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        if sys.platform == 'win32':
            return self.inverse_graph.topological_order()
        else:
            return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph(structure):
        vertices = ['raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk']
        observed = {'raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk'}

        if structure == 'faithful':
            edges = [('raf','mek'),('mek','erk'),('plcg','pip2'),
                    ('plcg','pip3'),('plcg','pkc'),('pip2','pkc'), ('pip3','pip2'),('pip3','akt'),('erk','akt'),
                    ('pka','raf'),('pka','mek'),('pka','erk'),
                    ('pka','akt'),('pka','p38'),('pka','jnk'),
                    ('pkc','raf'),('pkc','mek'),('pkc','pka'),
                    ('pkc','p38'),('pkc','jnk')]  
            
        elif structure == 'fully-connected':
            D = 11
            t_order = list(range(D))
            np.random.shuffle(t_order)

            edges = []
            for i in range(D-1):
                for j in range(i+1,D):
                    edges.append((vertices[t_order[i]],vertices[t_order[j]]))

        elif structure == 'random':
            D = 11
            t_order = list(range(D))
            np.random.shuffle(t_order)

            all_edges = []
            for i in range(D-1):
                for j in range(i+1,D):
                    all_edges.append((vertices[t_order[i]],vertices[t_order[j]]))
            edges = random.sample(all_edges, 20)
            
        return vertices, edges, observed