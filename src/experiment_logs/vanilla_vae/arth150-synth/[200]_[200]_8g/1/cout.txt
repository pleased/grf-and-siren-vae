INFO - vanilla_vae - Running command 'run'
INFO - vanilla_vae - Started run with ID "4"
[LOADING FROM CHECKPOINT] Start epoch: 600
  0%|          | 0/200 [00:00<?, ?it/s]Train -ELBO: 62.504, Val -ELBO: 61.937:   8%|▊         | 17/200 [00:01<00:10, 16.81it/s]Epoch   640: reducing learning rate of group 0 to 1.0000e-04.
Train -ELBO: 61.671, Val -ELBO: 61.389:  26%|██▌       | 52/200 [00:02<00:05, 27.38it/s]Epoch   671: reducing learning rate of group 0 to 1.0000e-05.
Epoch   692: reducing learning rate of group 0 to 1.0000e-06.
Train -ELBO: 61.838, Val -ELBO: 61.302:  46%|████▌     | 92/200 [00:03<00:03, 33.10it/s]Train -ELBO: 61.372, Val -ELBO: 61.327:  65%|██████▌   | 130/200 [00:04<00:01, 35.03it/s]Train -ELBO: 61.385, Val -ELBO: 61.276:  84%|████████▎ | 167/200 [00:05<00:00, 35.58it/s]Train -ELBO: 61.305, Val -ELBO: 61.377: 100%|██████████| 200/200 [00:05<00:00, 33.70it/s]
INFO - vanilla_vae - Completed after 0:00:08
