INFO - vanilla_vae - Running command 'run'
INFO - vanilla_vae - Started run with ID "3"
[LOADING DATA] Mehra
  0%|          | 0/200 [00:00<?, ?it/s]Train -ELBO: 8.697, Val -ELBO: 8.725:   8%|▊         | 15/200 [00:01<00:12, 14.48it/s]Train -ELBO: 7.736, Val -ELBO: 7.881:  20%|█▉        | 39/200 [00:02<00:08, 19.84it/s]Train -ELBO: 7.609, Val -ELBO: 7.825:  32%|███▏      | 64/200 [00:03<00:06, 21.90it/s]Train -ELBO: 7.565, Val -ELBO: 7.757:  43%|████▎     | 86/200 [00:04<00:05, 21.00it/s]Train -ELBO: 7.477, Val -ELBO: 7.772:  55%|█████▌    | 110/200 [00:05<00:04, 21.66it/s]Train -ELBO: 7.446, Val -ELBO: 7.746:  66%|██████▌   | 132/200 [00:06<00:03, 21.30it/s]Epoch   154: reducing learning rate of group 0 to 1.0000e-04.
Train -ELBO: 7.341, Val -ELBO: 7.727:  80%|███████▉  | 159/200 [00:07<00:01, 22.85it/s]Epoch   177: reducing learning rate of group 0 to 1.0000e-05.
Train -ELBO: 7.332, Val -ELBO: 7.632:  93%|█████████▎| 186/200 [00:08<00:00, 23.91it/s]Epoch   192: reducing learning rate of group 0 to 1.0000e-06.
Train -ELBO: 7.341, Val -ELBO: 7.645: 100%|██████████| 200/200 [00:08<00:00, 22.44it/s]
INFO - vanilla_vae - Completed after 0:00:10
