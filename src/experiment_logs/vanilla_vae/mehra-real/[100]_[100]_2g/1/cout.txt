INFO - vanilla_vae - Running command 'run'
INFO - vanilla_vae - Started run with ID "2"
[LOADING DATA] Mehra
  0%|          | 0/400 [00:00<?, ?it/s]Train -ELBO: 9.501, Val -ELBO: 10.361:   7%|▋         | 28/400 [00:01<00:13, 27.78it/s]Epoch    30: reducing learning rate of group 0 to 1.0000e-04.
Epoch    48: reducing learning rate of group 0 to 1.0000e-05.
Epoch    59: reducing learning rate of group 0 to 1.0000e-06.
Train -ELBO: 9.410, Val -ELBO: 10.295:  22%|██▏       | 87/400 [00:02<00:06, 45.69it/s]Train -ELBO: 9.415, Val -ELBO: 10.319:  34%|███▍      | 138/400 [00:03<00:05, 48.10it/s]Train -ELBO: 9.262, Val -ELBO: 10.326:  48%|████▊     | 194/400 [00:04<00:04, 50.99it/s]Train -ELBO: 9.316, Val -ELBO: 10.290:  66%|██████▌   | 262/400 [00:05<00:02, 56.92it/s]Train -ELBO: 9.411, Val -ELBO: 10.330:  82%|████████▏ | 329/400 [00:06<00:01, 60.00it/s]Train -ELBO: 9.318, Val -ELBO: 10.326:  99%|█████████▉| 395/400 [00:07<00:00, 61.90it/s]Train -ELBO: 9.413, Val -ELBO: 10.323: 100%|██████████| 400/400 [00:07<00:00, 55.99it/s]
INFO - vanilla_vae - Completed after 0:00:09
