import torch
import numpy as np
import matplotlib.pyplot as plt
import json

from sacred import Experiment
from sacred.observers import FileStorageObserver
from functools import partial
from torch.optim.lr_scheduler import ReduceLROnPlateau
from tqdm import trange

from modules.factory import build_vanilla_vae, build_iw_vanilla_vae, \
                            build_dreg_iw_vanilla_vae
from ex_utils import batch_iterator, sample_batch, kolmogorov_smirnov
from graph.belief_network import *
from data.load_data import *

ex = Experiment('vanilla_vae')
device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"

@ex.command(unobserved=True)
def plot_z_distributions(_config):
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    # Load model
    path = _config['path']
    model = torch.load(path + '/{}/model.pt'.format(_config['run'])).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 20000).double().to(device)
        true_x = X[:,num_latent:]
        true_z = X[:,:num_latent]

        model_x, model_prior_z = model.generative_network.sample(20000)
        model_post_z, _, _ = model(true_x)

        for i in range(num_latent):
            x = [
                model_prior_z[:,i].cpu().tolist(),
                model_post_z[:,i].cpu().tolist(),
                true_z[:,i].cpu().tolist()
            ]
            plt.hist(x=x, bins=50, alpha=0.5, histtype='stepfilled',
                        density=True, color=['steelblue', 'red', 'gold'], edgecolor='none', 
                        label=[r'$p_\theta(z)$', r'$q(z|x)$', r'$p(z)$']) 
            plt.xlabel(r'$z_{}$'.format(i))
            plt.legend()
            plt.savefig(path + '/{}/z{}.png'.format(_config['run'], i), dpi=600, bbox_inches='tight')
            plt.clf()


@ex.command(unobserved=True)
def plot_x_distributions(_config):
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    if _config['normalize']:
        min_std = 1e-5
        batch = sample_batch(bn.sample, 10000)
        x = batch[:,num_latent:]
        z = batch[:,:num_latent]
        x_std = torch.std(x, dim=0).to(device)
        x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
        x_shift = torch.mean(x, dim=0).to(device)
        z_std = torch.std(z, dim=0).to(device)
        z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
        z_shift = torch.mean(z, dim=0).to(device)
    else:
        x_scale = 1.0
        z_scale = 1.0
        x_shift = 0.0
        z_shift = 0.0


    # Load model
    path = _config['path']
    model = torch.load(path + '/{}/model.pt'.format(_config['run'])).double().to(device)

    with torch.no_grad():
        X = sample_batch(bn.sample, 20000).double().to(device)
        true_x = X[:,num_latent:]
        true_x = (true_x - x_shift)/x_scale
        model_x, _ = model.generative_network.sample(20000)

        for i in range(num_obs):
            x = [
                model_x[:,i].cpu().tolist(),
                true_x[:,i].cpu().tolist()
            ]
            ks, _ = kolmogorov_smirnov(x[0], x[1])
            plt.hist(x=x, bins=50, alpha=0.5, histtype='stepfilled',
                        density=True, color=['steelblue', 'red'], edgecolor='none', 
                        label=[r'$p_\theta(x|z)$', r'$p(x,z)$']) 
            plt.xlabel(r'$x_{}$'.format(i))
            # plt.text(0.0,0.,"ks={:.3f}".format(ks), 
            #         # transform=ax[i,j].transAxes, 
            #         bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))

            plt.legend()
            plt.subplots_adjust(wspace=0.15, hspace=0.15)
            plt.savefig(path + '/{}/x{}.png'.format(_config['run'], i), dpi=600, bbox_inches='tight')
            plt.clf()


@ex.command(unobserved=True)
def samples(_config):
    # BN + true samples
    # Initialize BN
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    num_latent, num_obs = bn.get_num_latent(), bn.get_num_obs()

    true_z = torch.zeros((10000, num_latent))
    true_x = torch.zeros((10000, num_obs))
    model_z = torch.zeros((10000, num_latent))
    model_x = torch.zeros((10000, num_obs))
    inferred_z = torch.zeros((10000, num_latent))

    # Model + infer
    model = torch.load(_config['path']+'/{}/model.pt'.format(_config['run'])).double().to(device)

    for i in range(5):
        t = sample_batch(bn.sample, 2000)
        true_z[i*2000:i*2000+2000,:] = t[:,:num_latent]
        true_x[i*2000:i*2000+2000,:] = t[:,num_latent:]
        
        iz, _ = model.inference_network(t[:,num_latent:].double().to(device))
        inferred_z[i*2000:i*2000+2000,:] = iz.cpu()

        mx, mz = model.sample(2000)
        model_z[i*2000:i*2000+2000,:] = mz
        model_x[i*2000:i*2000+2000,:] = mx

    samples = {
        'true_x': true_x,
        'model_x': model_x,
        'true_z': true_z,
        'model_z': model_z,
        'inferred_z': inferred_z
    }
    torch.save(samples, _config['path']+'/{}/samples.pt'.format(_config['run']))


def save_ckp(state, checkpoint_dir):
    f_path = checkpoint_dir + 'checkpoint.pt'
    torch.save(state, f_path)


def load_ckp(checkpoint_path, model, optimizer, scheduler):
    checkpoint = torch.load(checkpoint_path+'checkpoint.pt')
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    scheduler.load_state_dict(checkpoint['scheduler'])
    return model, optimizer, scheduler, checkpoint['epoch']
    # return model, optimizer, checkpoint['epoch']


@ex.config
def cfg():
    # Belief network [arithmetic-circuit2, ecoli70-synth, ecoli70-alt,
    #   ecoli70-complex, arth150-synth]
    bn = 'arithmetic-circuit2'

    encoder_hidden_dims = [100]
    decoder_hidden_dims = [100]
    condition_sigma = True

    batch_size = 100
    num_train_samples = 10000
    num_val_samples = 5000
    num_train_batches = num_train_samples//batch_size
    num_val_batches = num_val_samples//batch_size
    num_epochs = 100

    lr = 1e-2
    patience = 20
    seed = 0

    plotting_range = None
    normalize = False

    iw = False
    k = 1
    dreg = False
    wu = False      # warm-up
    wu_N = 1    

    # For plotting 
    run = 1

    # Create checkpoint every n epochs
    checkpoint_n = 10
    load_from_checkpoint = False
    save_checkpoints = False

    # Add observer
    ex_name = 'vanilla_vae'
    sub_folder = '{}_{}'.format(encoder_hidden_dims, decoder_hidden_dims)
    if iw and not dreg:
        ex_name = 'iw_' + ex_name
        sub_folder = '{}_{}_{}k'.format(encoder_hidden_dims, 
                                        decoder_hidden_dims, k)   
    elif dreg:
        ex_name = 'iw_' + ex_name + '_dreg'
        sub_folder = '{}_{}_{}k'.format(encoder_hidden_dims, 
                                        decoder_hidden_dims, k)   
    
    if wu:
        ex_name += '_wu'
        sub_folder += '_{}N'.format(wu_N)

    path = './experiment_logs/{}/{}/{}'.format(ex_name, bn, sub_folder)
    ex.observers.append(FileStorageObserver(path))


@ex.automain
def run(_config, _rnd, _run):
    # Training info
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = _config['batch_size']
    lr = _config['lr']
    num_train_batches = _config['num_train_batches']
    num_val_batches = _config['num_val_batches']
    checkpoint_n = _config['checkpoint_n']

    # BN initialization
    real = False
    if  _config['bn'] == 'arithmetic-circuit2':
        bn = ArithmeticCircuit2()
    elif  _config['bn'] == 'ecoli70-synth':
        bn = EColi70()
    elif  _config['bn'] == 'ecoli70-alt':
        bn = EColi70Adapted()
    elif  _config['bn'] == 'ecoli70-complex':
        bn = EColi70Complex()
    elif  _config['bn'] == 'ecoli70-real':
        bn = EColi70()
        trainloader, valloader = load_ecoli70()
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    elif  _config['bn'] == 'arth150-synth':
        bn = Arth150()
    elif  _config['bn'] == 'arth150-real':
        bn = Arth150()
        print('[LOADING DATA] Arth150')
        trainloader, valloader, _ = load_arth150()
        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    # Mehra
    elif _config['bn'] == 'mehra-real':
        bn = Mehra()
        print('[LOADING DATA] Mehra')
        trainloader, valloader, _ = load_mehra()

        if _config['num_train_samples'] < len(trainloader.dataset):
            X = trainloader.dataset[:_config['num_train_samples']]
            trainloader = torch.utils.data.DataLoader(X, batch_size=_config['batch_size'])

        num_train_batches = len(trainloader.dataset)//trainloader.batch_size
        num_val_batches = len(valloader.dataset)//valloader.batch_size
        real = True
    else:
        raise Exception("Unknown belief network: {}".format(bn))
    num_latent = bn.get_num_latent()
    n = bn.get_num_vertices()

    # Normalization
    if _config['normalize']:
        min_std = 1e-5
        batch = sample_batch(bn.sample, 10000)
        x = batch[:,num_latent:]
        z = batch[:,:num_latent]
        x_std = torch.std(x, dim=0).to(device)
        x_scale = torch.maximum(x_std, torch.ones_like(x_std)*min_std)
        x_shift = torch.mean(x, dim=0).to(device)
        z_std = torch.std(z, dim=0).to(device)
        z_scale = torch.maximum(z_std, torch.ones_like(z_std)*min_std)
        z_shift = torch.mean(z, dim=0).to(device)
    else:
        x_scale = 1.0
        z_scale = 1.0
        x_shift = 0.0
        z_shift = 0.0

    # Draw train and test sets and return minibatch iterators
    # train = torch.zeros((_config['num_train_samples'], n), dtype=torch.float64).to(device)
    # test = torch.zeros((_config['num_val_samples'], n), dtype=torch.float64).to(device)
    # get_data = partial(bn.sample)
    # iterators = partial(batch_iterator, get_data, train, test, batch_size)
    if not real:
        train = bn.sample(_config['num_train_samples'])
        val = bn.sample(_config['num_val_samples'])
        trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        valloader = torch.utils.data.DataLoader(val, batch_size=batch_size, shuffle=True)

    # Initialize model and optimizer
    if _config['iw'] and not _config['dreg']:
        k = _config['k']
        model = build_iw_vanilla_vae(bn, k, _config['encoder_hidden_dims'],
            _config['decoder_hidden_dims'], condition_sigma=_config['condition_sigma'], device=device).double().to(device)
    elif _config['dreg']:
        k = _config['k']
        model = build_dreg_iw_vanilla_vae(bn, k, _config['encoder_hidden_dims'],
            _config['decoder_hidden_dims'], condition_sigma=_config['condition_sigma'], device=device).double().to(device)
    else:
        model = build_vanilla_vae(bn, _config['encoder_hidden_dims'],
            _config['decoder_hidden_dims'], condition_sigma=_config['condition_sigma'], device=device).double().to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer, 'min', verbose=True,
                     min_lr=1e-6, patience=_config['patience'])

     # Warm-up
    wu = _config['wu']
    if wu: wu_N = _config['wu_N']

    def beta(epoch):
        if wu:
            return min((1.0/wu_N)*(epoch+1),1.0)
        else:
            return 1.0

    # Load from checkpoint if required
    start_epoch = 0
    if _config['load_from_checkpoint']:
        ckp_path = _config['path']+'/{}/'.format(_config['run'])
        model, optimizer, lr_scheduler, start_epoch = load_ckp(ckp_path, model, optimizer, lr_scheduler)
        print('[LOADING FROM CHECKPOINT] Start epoch: {}'.format(start_epoch))
        # model, optimizer, start_epoch = load_ckp(ckp_path, model, optimizer)
        with open(ckp_path+'metrics.json') as f:
            metrics = json.load(f)
        metrics['val']['steps'] = metrics['val']['steps'][:start_epoch]
        metrics['val']['timestamps'] = metrics['val']['timestamps'][:start_epoch]
        metrics['val']['values'] = metrics['val']['values'][:start_epoch]
        metrics['train']['steps'] = metrics['train']['steps'][:start_epoch]
        metrics['train']['timestamps'] = metrics['train']['timestamps'][:start_epoch]
        metrics['train']['values'] = metrics['train']['values'][:start_epoch]
        with open(_config['path']+'/{}/metrics.json'.format(_run._id), 'w') as f:
            json.dump(metrics, f)

    # Train
    epochs = trange(start_epoch, _config['num_epochs'], mininterval=1)
    t = start_epoch + 1
    for epoch in epochs:
        train_elbo = 0.0

        model.train()
        for train_batch in trainloader:
            x = train_batch[:,num_latent:].double().to(device)
            x = (x - x_shift)/x_scale

            if not _config['dreg']:
                loss = model.loss(x, beta=beta(epoch))

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            else:
                optimizer.zero_grad()
                log_p_loss, log_q_loss = model.loss(x, beta=beta(epoch))
                # Gradient of generative network
                grad_dec = torch.autograd.grad(log_p_loss, 
                    model.generative_network.parameters(), retain_graph=True) 
                for i,p in enumerate(model.generative_network.parameters()):
                    p.grad = grad_dec[i]#.clone()
                # Gradient of inference network
                grad_enc = torch.autograd.grad(log_q_loss, model.
                    inference_network.parameters())
                for i,p in enumerate(model.inference_network.parameters()):
                    p.grad = grad_enc[i]#.clone()
                
                loss = log_p_loss
                optimizer.step()


            train_elbo += loss.detach().item()
            

        # log training metrics - kl-divergence
        _run.log_scalar("train", value=train_elbo/num_train_batches) 

        # Log test metrics - kl-divergence
        model.eval()
        with torch.no_grad():
            val_elbo = 0.0
            for val_batch in valloader:
                x = val_batch[:,num_latent:].double().to(device)
                x = (x - x_shift)/x_scale
                val_elbo += model.loss(x).item()
            _run.log_scalar("val", val_elbo/num_val_batches)


        # Print progress
        epochs.set_description('Train -ELBO: {:.3f}, Val -ELBO: {:.3f}'.format(
            train_elbo/num_train_batches, val_elbo/num_val_batches),
            refresh=False)
        # Decay learning rate every 100 epochs
        if wu:
            if epoch >= wu_N:
                lr_scheduler.step(train_elbo/num_train_batches)
        else:
            lr_scheduler.step(train_elbo/num_train_batches)

        # create checkpoint
        if t % checkpoint_n == 0: 
            path = _config['path']
            checkpoint = {
                'epoch': t,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'scheduler': lr_scheduler.state_dict()
            }
            save_ckp(checkpoint, path+'/{}/'.format(_run._id))
            if _config['save_checkpoints']:
                torch.save(model, path+'/{}/model_{}.pt'.format(_run._id, t))
            else:
                torch.save(model, path+'/{}/model.pt'.format(_run._id))
        t += 1

    # Save the model
    path = _config['path']
    torch.save(model, path+'/{}/model.pt'.format(_run._id))
    
    # # Some plots of the posterior and likelihood distributions
    # with torch.no_grad():
    #     num_obs = graph.get_num_obs()
    #     sample = sample_batch(graph.sample, 50000).double().to(device)
        
    #     # True samples from the forward model
    #     true_x = sample[:,num_latent:]
    #     true_x = (true_x - x_shift)/x_scale
    #     true_z = sample[:,:num_latent]
    #     true_z = (true_z - z_shift)/z_scale

    #     # Samples from the learned model
    #     model_x, model_prior_z = model.generative_network.sample(50000)

    #     # Samples from the inference network
    #     model_post_z, log_q_z_given_x, log_p_x_and_z = model(model_x)
    #     print('log p(x) = ', torch.mean(log_p_x_and_z - log_q_z_given_x).item())
    #     #model_x = model_x*x_scale + x_shift

    #     plotting_range = _config['plotting_range']
    #     # Plot samples from prior vs posterior
    #     for i in range(num_latent):
    #         z_joint = model_prior_z[:,i].cpu().tolist()
    #         z_infer = model_post_z[:,i].cpu().tolist()
    #         z_true = true_z[:,i].cpu().tolist()
    #         plt.hist(x=[z_joint, z_infer, z_true], bins=50, range=plotting_range, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red', 'gold'], edgecolor='none',
    #                     label=['Generative network', 'Inference Network', 'True'])
    #         plt.xlabel("z_"+str(i))
    #         plt.ylabel("Density")
    #         plt.legend()
    #         plt.savefig(path+'/{}/z_{}'.format(_run._id,i))
    #         #plt.show()
    #         plt.clf()
        
    #     for i in range(num_obs):
    #         xtrue = true_x[:,i].cpu().tolist()
    #         xmodel = model_x[:,i].cpu().tolist()
    #         ks, _ = kolmogorov_smirnov(xmodel, xtrue)
    #         fig =  plt.figure()
    #         ax = fig.add_subplot(111)
    #         ax.hist(x=[xtrue, xmodel], bins=50, range=plotting_range, alpha=0.5, 
    #                     histtype='stepfilled', density=True,
    #                     color=['steelblue', 'red'], edgecolor='none',
    #                     label=['True','Generative network'])
    #         ax.set_xlabel("x_"+str(i))
    #         ax.set_ylabel("Likelihood density")
    #         ax.legend()
    #         ax.text(0.02,0.035,"ks={:.3f}".format(ks), transform=ax.transAxes, bbox=dict(facecolor='white', alpha=0.7, edgecolor='grey'))
    #         plt.savefig(path+'/{}/x_{}'.format(_run._id,i))
    #         #plt.show()
    #         plt.clf()