import torch

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert


from graph.belief_network import BeliefNetwork

class Protein(BeliefNetwork):

    def __init__(self):
        super(Protein, self).__init__()

        # graph
        graph = Protein._construct_graph()
        self.edges = graph[1]
        self.forward_graph = ForwardGraph().initialize(*graph)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 0
        self.num_obs = 11


    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for Protein BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for Protein BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for Protein BN")


    def get_num_latent(self):
        return 0


    def get_num_obs(self):
        return 11


    def get_num_vertices(self):
        return 11

    def get_num_edges(self):
        return 20


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk']
        observed = {'raf', 'mek', 'plcg', 'pip2', 'pip3', 
                        'erk', 'akt', 'pka', 'pkc', 'p38', 'jnk'}

        edges = [('raf','mek'),('mek','erk'),('plcg','pip2'),
                    ('plcg','pip3'),('plcg','pkc'),('pip2','pkc'), ('pip3','pip2'),('pip3','akt'),('erk','akt'),
                    ('pka','raf'),('pka','mek'),('pka','erk'),
                    ('pka','akt'),('pka','p38'),('pka','jnk'),
                    ('pkc','raf'),('pkc','mek'),('pkc','pka'),
                    ('pkc','p38'),('pkc','jnk')]  
            
            
        return vertices, edges, observed