import numpy as np
import os
import pandas as pd
import time

import subprocess

from sklearn.model_selection import train_test_split
from scipy.stats import kstest

def reshape_batch(x, batch_size=250):
    """Reshape input as [batch_size, .]"""
    return x.view(batch_size, -1)


def batch_iterator(sample, train, test, batch_size):
    """Create iterator for iterating over train and test batches.

    Parameters
    ----------
    get_data : function
        Function that can be called to get batch_size amount of data with 
        get_data(batch_size=batch_size)
    train : np.ndarray
        Object of size [num train samples, dim of datapoint] to hold train data
    test : np.ndarray
        Object of size [num test samples, dim of datapoint] to hold test data
    batch_size : int

    Returns
    -------
    batch_iterators : list
    """
    # Adapted from: 'Faithful Inversion of Generative Models for Effective Amortized Inference' by Webb et al

    num_train_batches = train.shape[0]//batch_size
    for i in range(num_train_batches):
        batch = sample(batch_size=batch_size, train=True)
        if len(batch.shape) == 1:
            batch = np.reshape(batch, (-1, 1))
        train[(i*batch_size):((i+1)*batch_size),:] = batch

    num_test_batches = test.shape[0]//batch_size
    for i in range(num_test_batches):
        batch = sample(batch_size=batch_size, train=False)
        if len(batch.shape) == 1:
            batch = np.reshape(batch, (-1, 1))
        test[(i*batch_size):((i+1)*batch_size),:] = batch

    batch_iterators = [batch_iterator_factory(a, batch_size) for a in [train, test]] 

    return batch_iterators


def batch_iterator_factory(x, batch_size):
    batch_idx = 0
    num_batches = x.shape[0]//batch_size

    def next_batch():
        nonlocal batch_idx, num_batches
        minibatch = x[batch_idx*batch_size:(batch_idx+1)*batch_size,:]
        batch_idx += 1
        if batch_idx == num_batches:
            batch_idx = 0
        return minibatch

    return next_batch


def sample_batch(sampler, batch_size):
    return sampler(batch_size)


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def kolmogorov_smirnov(z_infer, z_true):
    ks = kstest(z_infer, z_true)
    return ks


def create_protein_dataset_files():
    dir = './datasets/human_protein/'
    files = ['cd3cd28.csv',
                 'cd3cd28icam2.csv',
                 'b2camp.csv',
                 'cd3cd28+aktinhib.csv',
                 'cd3cd28+u0126.csv',
                 'pma.csv',
                 'cd3cd28+g0076.csv',
                 'cd3cd28+psitect.csv',
                 'cd3cd28+ly.csv']
    dfs = []
    for file in files:
        path = os.path.join(dir, file)
        print(path)
        df = pd.read_csv(path)
        dfs.append(df)

    df = pd.concat(dfs, axis=0)

    x_train, x_test = train_test_split(df, test_size=0.33,
              random_state=42)

    mu = x_train.mean(axis=0)
    std = x_train.std(axis=0)
    x_train = (x_train - mu)/std
    x_test = (x_test - mu)/std

    x_train.to_csv(os.path.join(dir, 'train.csv'), index=False)
    x_test.to_csv(os.path.join(dir, 'test.csv'), index=False)


def get_gpu_memory_map():
    """Get the current gpu usage.

    Returns
    -------
    usage: dict
    Keys are device ids as integers.
    Values are memory usage as integers in MB.
    """
    result = subprocess.check_output(
        ['nvidia-smi', '--query-gpu=memory.used',
        '--format=csv,nounits,noheader'
        ], encoding='utf-8')
    # Convert lines into a dictionary
    gpu_memory = [int(x) for x in result.strip().split('\n')]
    gpu_memory_map = dict(zip(range(len(gpu_memory)), gpu_memory))
    return gpu_memory_map


class Timer():

    def start(self):
        self.start_time = time.perf_counter()

    def stop(self):
        elapsed_time = time.perf_counter() - self.start_time
        return elapsed_time*1000