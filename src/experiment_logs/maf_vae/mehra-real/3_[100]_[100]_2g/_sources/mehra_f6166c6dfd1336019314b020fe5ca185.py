import torch

import graph_tool.all as gt
from graph.forward_graph import ForwardGraph

import graph.invert as invert

from graph.belief_network import BeliefNetwork

class Mehra(BeliefNetwork):

    def __init__(self):
        super(Mehra, self).__init__()

        # graph
        vertices, edges, observed = Mehra._construct_graph()
        self.edges = edges
        self.vertices = vertices
        self.observed = observed
        self.forward_graph = ForwardGraph().initialize(vertices, edges, observed)
        self.inverse_graph = invert.properly(self.forward_graph)

        self.num_latent = 3
        self.num_obs = 7



    def log_likelihood(self, x, z):
        raise Exception("Log-likelihood not available for MEHRA BN")


    def log_prior(self, z):
        raise Exception("Log-priors not available for MEHRA BN")


    def log_joint(self, x, z):
        raise Exception("Log-joint not available for MEHRA BN")


    def get_num_latent(self):
        return self.num_latent


    def get_num_obs(self):
        return self.num_obs


    def get_num_vertices(self):
        return self.num_latent + self.num_obs

    
    def get_num_edges(self):
        return 10


    def topological_order(self):
        return gt.topological_sort(self.forward_graph)


    def inv_topological_order(self):
        return gt.topological_sort(self.inverse_graph) 


    @staticmethod
    def _construct_graph():
        vertices = ['wd', 'co', 'pm10', 't2m', 'ws', 'blh', 'ssr', 'no2', 'so2',
                    'pm2.5']

        observed = {'t2m', 'ws', 'blh', 'ssr', 'no2', 'so2', 'pm2.5'}

        edges = [('wd','t2m'),('wd','ws'),('wd','blh'),('wd','ssr'),
                 ('co','no2'),('co','so2'),('co','ssr'),
                 ('pm10','no2'),('pm10','ssr'),('pm10','pm2.5')]  
            
       
        return vertices, edges, observed