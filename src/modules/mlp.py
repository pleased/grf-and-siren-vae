# From: https://github.com/AWehenkel/Graphical-Normalizing-Flows 

import torch.nn as nn


class MLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, act_f=nn.ReLU(),      
                    batch_norm=False):
        super(MLP, self).__init__()
        self.in_size = in_size
        self.hidden = hidden
        self.out_size = out_size
        self.act_func = act_f
        
        layers_dim = [in_size] + hidden + [out_size]
        layers = []
        
        for in_dim, out_dim in zip(layers_dim[:-1], layers_dim[1:]):
            layers += [nn.Linear(in_dim, out_dim)]
            if batch_norm: layers += [nn.BatchNorm1d(out_dim)]
            layers += [self.act_func]

        layers.pop()
        if batch_norm: layers.pop()
        self.net = nn.Sequential(*layers)

    
    def forward(self, x, context=None):
        return self.net(x)