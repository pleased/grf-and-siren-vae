import torch
import torch.nn as nn
import torch.distributions as dist

from modules.gaussian import GaussianModel
from functools import reduce
from math import sqrt

class FactorizedInverseBinaryTree(nn.Module):

    """Implementation of the inference network for the binary tree linear
    Gaussian model, as specified in the paper 'Faithful inversion of 
    graphical models for effective amortized inference.'"""
    
    def __init__(self, hidden_dims, bn, normal_log_density):
        super(FactorizedInverseBinaryTree, self).__init__()
        self.hidden_dims = hidden_dims
        self.g_inverse = bn.inverse_graph
        self.normal_log_density = normal_log_density

        # Initialize gaussian model for each conditional
        n = len(self.g_inverse.get_vertices())
        self.num_latent = (n+1)//2 - 1
        gauss_nets = [None]*self.num_latent
        self.topological_order = list(filter(lambda x: x < self.num_latent, 
            list(bn.inv_topological_order())))

        for i in self.topological_order:
            num_parents = len(self.g_inverse.get_in_neighbours(i).tolist())
            gauss_nets[i] = GaussianModel(
                in_size=num_parents, 
                hidden_dims=hidden_dims,
                out_size=2)
        
        self.gaussian_nets = nn.ModuleList(gauss_nets)

    
    def forward(self, x):
        """
        Parameters
        ----------
        inverse_g : graph_tool.all.Graph
            The inverted binary tree graph 
        x : torch.tensor
            [batch_size, n] sample from forward graph 
        
        Returns
        -------
        log_likelihood : float
        """
        means = [None for i in range(self.num_latent)]
        stds = [None for i in range(self.num_latent)]

        for i in self.topological_order:
            parents = sorted(self.g_inverse.get_in_neighbours(i).tolist())
            samples =  torch.cat([x[:,i:i+1] for i in parents], dim=1)
            m, s = self.gaussian_nets[i](samples)
            means[i] = m
            stds[i] = s

        return means, stds


    def loss(self, x, means, stds):
        log_probs = []

        for i in self.topological_order:
            # Calculate log-probability log(p(z|pa(z)))
            log_probs.append(torch.mean(
            self.normal_log_density(
                x[:,i:i+1], 
                means[i], 
                stds[i])
            ))

        log_likelihood = reduce((lambda x, y: x + y), log_probs)
        return -log_likelihood


    def sample(self, x):
        inverse_samples_l = [None for i in range(self.num_latent)]

        for i in self.topological_order:
            parents = sorted(self.g_inverse.get_in_neighbours(i).tolist())

            inverse_parents_sample = torch.cat([
            inverse_samples_l[i] if i < self.num_latent else 
            x[:,i:i+1]
            for i in parents], dim=1)
            means, stds = self.gaussian_nets[i](inverse_parents_sample)
            dists = dist.Normal(loc=means, scale=stds)
            inverse_samples_l[i] = dists.sample()
        
        inverse_samples_l.append(x[:,self.num_latent:])
        inverse_samples = torch.cat(inverse_samples_l, dim=1)
        return inverse_samples