import torch
import torch.nn as nn

class MaskedLayer(nn.Module):

    def __init__(self, in_size, out_size, mask):
        super(MaskedLayer, self).__init__()
        self.mask = mask
        self.layer = nn.Linear(in_size, out_size)
        self._weight = self.layer.weight
        self._bias = self.layer.bias

    
    def forward(self, x):
        w = torch.mul(self.mask, self._weight)
        x = torch.addmm(self._bias, x, w.transpose(0,1))
        return x


class MaskedMLP(nn.Module):
    def __init__(self, in_size, hidden, out_size, masks, 
                    activation_func):
        super(MaskedMLP, self).__init__()
        self.masks = masks
        in_size = in_size
        l1 = [in_size] + hidden
        l2 = hidden + [out_size]
        
        layers = []
        for m, l in enumerate(zip(l1, l2)):
            layers.append(MaskedLayer(l[0], l[1], masks[m]))
        self.layers = nn.ModuleList(layers)
        self.activation_func = activation_func


    def forward(self, x):
        for l in range(len(self.layers)-1):
            x = self.activation_func(self.layers[l](x))
        x = self.layers[-1](x)
        return x