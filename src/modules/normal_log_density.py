import torch
import torch.nn as nn

from math import pi


class NormalLogDensity(nn.Module):
    def __init__(self):
        super(NormalLogDensity, self).__init__()
        self.register_buffer("pi", torch.tensor(pi))

    
    def forward(self, x, loc, scales):
        return -0.5*(torch.log(2*self.pi) + ((x - loc)/scales)**2) - torch.log(scales)


class NormalLogDensity2(nn.Module):
    def __init__(self, loc, scales):
        super(NormalLogDensity2, self).__init__()
        self.register_buffer("pi", torch.tensor(pi))
        self.loc = loc
        self.scales = scales

    
    def forward(self, x):
        return (-0.5*(torch.log(2*self.pi) + ((x - self.loc)/self.scales)**2) - torch.log(self.scales)).sum(1) 


class StandardNormalLogDensity(nn.Module):
    def __init__(self):
        super(StandardNormalLogDensity, self).__init__()
        self.register_buffer("pi", torch.tensor(pi))

        
    def forward(self, x, sum_factors=True):
        p_x = (-0.5*(torch.log(self.pi*2) + x**2))
        if sum_factors: p_x = p_x.sum(dim=1)
        return p_x
