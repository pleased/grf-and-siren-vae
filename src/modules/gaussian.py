import torch.nn as nn

class GaussianModel(nn.Module):

    def __init__(self, in_size, hidden_dims, out_size):
        super(GaussianModel, self).__init__()
        l1 = [in_size] + hidden_dims
        l2 = hidden_dims
        layers = []
        for h1, h2 in zip(l1, l2):
            layers += [nn.Linear(h1, h2), nn.ReLU()]
        self.net = nn.Sequential(*layers)
        self.mnet = nn.Sequential(nn.Linear(h2,1), nn.Identity())
        self.snet = nn.Sequential(nn.Linear(h2,1), nn.Softplus())


    def forward(self, x):
        x = self.net(x)
        means = self.mnet(x)
        stds = self.snet(x)

        return means, stds