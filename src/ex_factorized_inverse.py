# From: Faithful Inversion of Generative Models for Effective Amortized 
# Inference

import torch
import torch.nn as nn
import torch.distributions as dist
import numpy as np
import matplotlib.pyplot as plt

from sacred import Experiment
from sacred.observers import FileStorageObserver
from functools import partial, reduce
from torch.optim.lr_scheduler import StepLR
from math import sqrt, pi

from ex_binary_tree_config import bt_ingredient, create_tree
from ex_utils import batch_iterator, reshape_batch, count_parameters, sample_batch
from modules.factory import build_factorized_inverse_binary_tree


ex = Experiment('binary_tree_factorized_inverse', ingredients=[bt_ingredient])
ex.observers.append(FileStorageObserver('./experiment_logs/binary_tree/factorized_inverse'))


@ex.config
def cfg():
    hidden_dims = [96,97]
    seed = 4


@ex.automain
def run(bt_config, hidden_dims, _run):
    """Parameterize each conditional distribution in the posterior individually. 

    Given the posterior factorizes as: 
        p(z|x) = p(z_1|pa(z_1)) x ... x p(z_n|pa(z_n))
    approximate each conditional with a normal distribution, where the location 
    and scale of each normal needs to be learned. 
    """

    # Initial setup
    device = "cpu" if not(torch.cuda.is_available()) else "cuda:0"
    batch_size = bt_config['batch_size']
    lr = bt_config['learning_rate'] 
    lr_decay = bt_config['lr_decay']
    num_train_batches = bt_config['num_train_batches']

    depth = bt_config['depth']
    bt = create_tree()
    n = bt.n
    num_latent = bt.num_latent
    # Log coefficients
    ex.info['tree_coeffs'] = bt.coeffs.tolist()

    # Draw train and test sets and return minibatch iterators
    tree_train = np.zeros((bt_config['num_train_samples'], n))
    tree_test = np.zeros((bt_config['num_test_samples'], n))
    get_data = partial(bt.sample)
    iterators = partial(batch_iterator, get_data, tree_train, tree_test, batch_size)

    # Get static batch for visualization over learning
    static_tree = bt.sample(batch_size=5)
    # Log static batches
    ex.info['static_batches'] = static_tree.tolist()
    static_samples = [np.tile(static_tree[i,:], (batch_size,1)) for i in range(5)]
    # log-likelihoods of samples from the inference network evaluated on the
    # analytical posterior, given the 5 static samples - saved as metric
    ll_p_metric_names = ["ll_p_1", "ll_p_2", "ll_p_3", "ll_p_4", "ll_p_5"]

    # Initialize model and optimizer
    model = build_factorized_inverse_binary_tree(hidden_dims=hidden_dims, bn=bt)
    model.to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_schedular = StepLR(optimizer, step_size=100, gamma=lr_decay)

    # Log model capacity
    ex.info['num model params'] = count_parameters(model)

    # Train
    for epoch in range(bt_config['num_steps']):
        train_batcher, test_batcher = iterators()
        train_loss = 0
        train_kl = 0

        for idx in range(num_train_batches):
            train_batch = torch.tensor(train_batcher().astype(np.float32)).to(device)
            means, stds = model(train_batch)
            loss = model.loss(train_batch, means, stds)

            l = loss.detach().item()
            train_loss += l 

            with torch.no_grad(): 
                z = model.sample(train_batch)
                p = bt.log_posterior(z)
                means, stds = model(z)
                q = -model.loss(z, means, stds)
                train_kl += (q - p).item()                     

            optimizer.zero_grad()
            loss.backward(retain_graph=True)
            optimizer.step()

        # log training metrics - training loss,  kl-divergence
        _run.log_scalar("training.loss", value=train_loss/num_train_batches) 
        _run.log_scalar("training.D_KL", value=train_kl/num_train_batches)

        # Log training metrics - test loss, kl-divergence
        with torch.no_grad():
            test_batch = torch.FloatTensor(test_batcher()).to(device)
            m, s = model(test_batch)
            test_loss = model.loss(test_batch, m, s).item()
            
            z = model.sample(test_batch)
            p = bt.log_posterior(z)
            m, s = model(z)
            q = -model.loss(z, m, s)
            test_kl = (q - p).item()

            _run.log_scalar("test.loss", test_loss)
            _run.log_scalar("test.D_KL", test_kl)

        # Decay learning rate every 100 epochs
        lr_schedular.step()

        # Calculate log-likelihood of samples from the inference
        # network evaluated on the analytical posterior
        if epoch%1 == 0:
            ll_p = [[],[],[],[],[]]
            for i in range(5):
                with torch.no_grad():
                    static_sample = torch.tensor(static_samples[i]).to(device)
                    inverse_sample = model.sample(static_sample)
                    ll = bt.log_posterior(inverse_sample)
                    # log training metrics - log-likelihood
                    ll_p[i] = -ll.item()/num_latent
                    _run.log_scalar(ll_p_metric_names[i], ll_p[i])

            
            print('[{}]: test {}, train {}, post [{}, {}, {}, {}, {}]'.format(
                epoch+1, 
                test_loss, 
                train_loss/num_train_batches, 
                ll_p[0], ll_p[1], ll_p[2], ll_p[3], ll_p[4]))

    # Log-likelihood of samples drawn from the true posterior
    true_ll = 0.0
    for i in range(5):
        static_sample = torch.tensor(static_samples[i]).to(device)
        x = static_sample[:,num_latent:]
        z = bt.sample_posterior(x)
        ll = bt.log_posterior(torch.cat((z,x), dim=1))
        true_ll += ll/5
    print('Neg Log-likelihood of samples drawn from the true posterior: -log(p(z|x)) = ', -(true_ll.item())/num_latent)

    # Some plots of the posterior distributions
    with torch.no_grad():
        x = torch.FloatTensor(sample_batch(bt.sample, 100000)).to(device)
        inference_network_samples = model.sample(x)
        true_posterior_samples = bt.sample_posterior(x[:,num_latent:])
        # plot zi
        for i in range(num_latent):
            ztrue = true_posterior_samples[:,i].cpu().tolist()
            zinf = inference_network_samples[:,i].cpu().tolist()
            plt.figure()
            plt.hist(x=[ztrue, zinf], bins=50, alpha=0.5, histtype='stepfilled', 
                        color=['steelblue', 'red'], edgecolor='none', density=True,
                        label=("true","inference network"))
            plt.xlabel("z_"+str(i))
            plt.ylabel("Posterior Frequency")
            plt.legend()
            plt.savefig("./experiment_logs/binary_tree/factorized_inverse/z_"+str(i))