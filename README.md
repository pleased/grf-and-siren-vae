# Graphical Residual Flow and Structured Invertible Residual Network VAE
## Incorporating Bayesian Network structure into Residual Flows and Variational Autoencoders 

This repository provides an implementation of both the Graphical Residual Flow (GRF) and the Structured Invertible Residual Network VAE (SIReN-VAE) and supports the discussions and experimental results presented in the following works: [TMLR Paper](https://openreview.net/forum?id=OsKXlWamTQ), [Thesis](https://scholar.sun.ac.za/) and ICLR Workshop Papers on [GRFs](https://openreview.net/forum?id=BNhf2nNdD-c) and [SIReN-VAEs](https://openreview.net/forum?id=HHlL2nVOwZc).
The various notebooks can be used to reproduce the figures presented in these papers.
A short video presenting the work can also be viewed [here](https://youtu.be/QCNpBmD91IE).

### Docker

Build the Docker image or pull directly from Docker Hub:

```
sudo docker build -t jmout123/grf_siren-vae .  
```
or

```
sudo docker login
sudo docker pull jmout123/grf_siren-vae:latest
```

Run Docker from the root directory and start an interactive bash session:

```
sudo docker run -p 8889:8888 -it -v "${PWD}":/grf_and_siren-vae/ jmout123/grf_siren-vae bash
```

If you want to use the GPU inside the notebook, add ```--gpus all``` after ```run``` in the above line.
To start a Jupyter notebook, run the following inside the container:

```
jupyter notebook --ip 0.0.0.0 --allow-root --no-browser
```

Then go the URL: ```localhost:8889/tree``` 
and enter either the token given in the terminal or the token given when running ```jupyter notebook list```