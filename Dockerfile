FROM continuumio/miniconda3:22.11.1

WORKDIR /grf_and_siren-vae

# Create the environment:
COPY environment.yml ./

RUN apt-get update && apt-get install -y libgtk-3-0=3.24.24-4+deb11u2 \
    && conda env create -f environment.yml 

# Make RUN commands use the new environment:
RUN echo "conda activate grf_siren-vae_env" >> ~/.bashrc